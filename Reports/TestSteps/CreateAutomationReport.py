from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.SAPStatusCodeService import SAPStatusCodeDbManagementService
from db.service.SAPStatusDataService import SAPStatusDataDbManagementService
from db.model.AutomationReportData import AutomationReportData
from db.service.AutomationReportDataDbManagementService import AutomationReportDataDbManagementService
import datetime


class CreateAutomationReport:
    logger = LogGenerator.logGen()
    parse_config_file = ParseConfigFile()
    db_file_path = parse_config_file.get_data_from_config_json("dbLocation", "db_file_path")
    pass_count = 0
    fail_count = 0
    total_test_cases = 0
    service_name = ""

    def test_create_data(self):
        automation_report_data_list = []
        automation_report_data_srv = AutomationReportDataDbManagementService()
        sap_stat_data_srv_obj = SAPStatusDataDbManagementService()
        sap_stat_code_srv_obj = SAPStatusCodeDbManagementService()
        im360_input_order_db_mgmt_srv = IM360InputOrderDbManagementService()

        try:
            self.logger.info("Getting all test case IDs from SAP status data")
            test_case_id_list = sap_stat_data_srv_obj.get_all_test_case_ids(self.db_file_path)

            for test_case_id in test_case_id_list:
                failure_reason = None
                test_case_order_data = None
                status = "Pass"
                status_level_warning = " "
                status_code = None
                status_level_fail = None
                module = " "
                self.logger.info("Getting the SAP status data by test case ID %s", test_case_id['test_case_id'])
                sap_stat_data_list = sap_stat_data_srv_obj.get_all_records_by_test_case_id(self.db_file_path,
                                                                                           test_case_id['test_case_id'])
                for sap_data in sap_stat_data_list:
                    test_case_order_data = im360_input_order_db_mgmt_srv.get_im360_input_test_case_order_detail(
                        self.db_file_path, sap_data['test_case_id'])
                    status_code = sap_data['status_code']

                    if str(sap_data['feed_validation_status']).upper() == "FAIL":
                        status_level_fail = sap_data['error_details'] + sap_data['order_id'] \
                                            + " in Subscription : " + sap_data['subscription_id']
                        status = "Fail"
                    if str(sap_data['minCommitBit_validation_status']).upper() == "FAIL":
                        status_level_fail = sap_data['error_details'] + sap_data['order_id'] \
                                            + " in Subscription : " + sap_data['subscription_id']
                        status = "Fail"
                    if str(sap_data['usage_bit_validation_status']).upper() == "FAIL":
                        status_level_fail = sap_data['error_details'] + sap_data['order_id'] \
                                            + " in Subscription : " + sap_data['subscription_id']
                        status = "Fail"
                    if str(sap_data['billing_status']).upper() == "FAIL":
                        status_level_fail = sap_data['error_details'] + sap_data['order_id'] \
                                            + " in Subscription : " + sap_data['subscription_id']
                        status = "Fail"
                    if str(sap_data['invoiced_status']).upper() == "FAIL":
                        status_level_fail = sap_data['error_details'] + sap_data['order_id'] \
                                            + " in Subscription : " + sap_data['subscription_id']
                        status = "Fail"
                    if str(sap_data['rar_status']).upper() == "FAIL":
                        status_level_fail = sap_data['error_details'] + sap_data['order_id'] \
                                            + " in Subscription : " + sap_data['subscription_id']
                        status = "Fail"

                    if str(sap_data['feed_validation_status']).upper() == "WARNING":
                        status_level_warning = status_level_warning + ", Data Mismatch during Feed validation " \
                                                                      "of Order Number/Reference : " + \
                                               sap_data['order_id'] + " in Subscription : " + \
                                               sap_data['subscription_id'] if \
                            status_level_warning.strip() != "" else "Data Mismatch during Feed validation of Order " \
                                                                    "Number/Reference : " + sap_data['order_id']
                    if str(sap_data['minCommitBit_validation_status']).upper() == "WARNING":
                        status_level_warning = status_level_warning + ", Data Mismatch during Min Commit Bit " \
                                                                      "validation of Order Number/Reference : " \
                                               + sap_data['order_id'] + " in Subscription : " + \
                                               sap_data['subscription_id'] \
                            if status_level_warning.strip() != "" else "Data Mismatch during Min Commit Bit " \
                                                                       "validation of Order Number/Reference : " + \
                                                                       sap_data['order_id'] + " in Subscription : " + \
                                                                       sap_data['subscription_id']
                    if str(sap_data['usage_bit_validation_status']).upper() == "WARNING":
                        status_level_warning = status_level_warning + ", Data Mismatch during Usage Bit validation " \
                                                                      "of Order Number/Reference : " + \
                                               sap_data['order_id'] + " in Subscription : " + \
                                               sap_data['subscription_id'] \
                            if status_level_warning.strip() != "" else "Data Mismatch during Usage Bit validation " \
                                                                       "of Order Number/Reference : " + sap_data[
                                                                           'order_id'] + " in Subscription : " + \
                                                                       sap_data['subscription_id']
                        # if status.strip() == "Fail":
                    failure_reason = sap_stat_code_srv_obj.get_reason_by_status_and_code(self.db_file_path, status,
                                                                                         status_code)

                automation_report_data_obj = AutomationReportData(sap_data['test_case_id'],
                                                                  test_case_order_data.get("test_case_name"), status,
                                                                  failure_reason, status_level_fail,
                                                                  status_level_warning, sap_data['subscription_id'],
                                                                  test_case_order_data.get("reseller_bcn"),
                                                                  test_case_order_data.get("reseller_currency"),
                                                                  test_case_order_data.get("vendor_currency"),
                                                                  test_case_order_data.get("service_name"),
                                                                  test_case_order_data.get("marketplace_name"),
                                                                  module, sap_data['image_ref']
                                                                  )
                automation_report_data_list.append(automation_report_data_obj)

            automation_report_data_srv.insert_records(self.db_file_path, automation_report_data_list)
        except Exception as e:
            self.logger.error("Exception occurred while generating the Automation report %s", e)
            raise e

    def count_status(self):
        automation_report_data_srv = AutomationReportDataDbManagementService()
        automation_report_data_list = automation_report_data_srv.get_all_record(self.db_file_path)
        self.total_test_cases = len(automation_report_data_list)
        for automation_report_data in automation_report_data_list:
            self.service_name = automation_report_data['service_name']
            if automation_report_data['status'] == "Fail":
                self.fail_count += 1
            else:
                self.pass_count += 1

        return automation_report_data_list

    def test_generate_report(self):
        try:

            automation_report_data_list = self.count_status()
            date_time = datetime.date.today()

            html_content = """<html><head><meta name="viewport" content="width=device-width, initial-scale=1">
            <font size='6' color='2432AD'><div style='width:100%; text-align:center;' 
            size='8'><u><b>FSE Automation Report</b></u></div></font><br/>
            <div style='width:30%; padding-left:12%;float:left;'><font color='101D91' size='5'>
            <b>Test Execution Summary</b></font></div>
            <div style="width:30%; float:right; text-align: right; padding-right: 12%;"><font color='101D91' size='5'>
            <b>Execution Date : </b>""" + str(date_time.strftime("%b %d %Y")) + """</font></div><br/><br/>
            <center>
            <table>
            <tr>
            <td>
            <div id="piechart" style="width: 400; "></div>
            </td>
            <td>
            <div style='padding-left: 5.5px; width: 300px; 
                padding-right: 5.5px; text-align:center; border:1.5px solid;'>
            <table cellpadding='5'>
             <tr><td><b>Service</b></td><td>:</td><td>""" \
                           + str(self.service_name) + """</td></tr>
                <tr><td><b>Total Scenarios Executed</b></td><td>:</td><td>""" \
                           + str(self.total_test_cases) + """</td></tr>
                <tr><td><b>Scenarios Passed</b></td><td>:</td><td>""" \
                           + str(self.pass_count) + """</td></tr>
                <tr><td><b>Scenarios Failed</b></td><td>:</td><td>""" \
                           + str(self.fail_count) + """</td></tr>
            </table>
            </div>
            </td>
            </tr>
            </table>
            </center>
            <style>
            tr.border-bottom td {
            border-bottom: 1.5pt solid #6C8EBF;
            
            }
.tooltip {
  position: relative;
  display: inline-block;
}

/* Tooltip text */
.tooltip .tooltiptext {
  visibility: hidden;
  width: 450px;
  background-color: black;
  color: #fff;
  text-align: left;
  padding: 5px 5px;
  border-radius: 6px;
 
  /* Position the tooltip text - see examples below! */
  position: absolute;
  z-index: 2;
}
.tooltip[data-title]:hover:after {
  content: attr(data-title);
  padding: 4px 8px;
  color: #fff;
  position: absolute;
  left: 0;  
  white-space: nowrap;  
  border-radius: 5px;  
  background:#000;
  z-index: 2;
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltip:hover .tooltiptext {
  visibility: visible;
}


.collapsible {
  color: blue;
  cursor: pointer;
  padding: 0;
  width: 50px;
  border: none;
  text-align: center;
  outline: none;
  font-size: 15px;
  background-color: A6F1A9;
  }

.active, .collapsible:hover {
  background-color: #00FFFF;
}

.content {
  padding: 0 10px;
  margin: 0 10px;
  display: none;
  overflow: hidden;
  width: 95%;
  background-color: #f1f1f1;
  clear:both;
}
            </style>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
    
            function drawChart() {
    
            var data = google.visualization.arrayToDataTable([
              ['Status',     'Count'],
              ['Pass',      """ + str(self.pass_count) + """],
              ['Fail',      """ + str(self.fail_count) + """]
            ]);
    
            var options = {
              title: 'Report Statistics',
              colors: ['green', 'red'],
              backgroundColor: 'transparent',
              width:400,
              height:300
            };
    
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    
            chart.draw(data, options);
          }
        </script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            </head><body style='background-color: #F5F5F5;'><center>"""

            for automation_report_data in automation_report_data_list:
                colour = "Green"
                test_case_name = automation_report_data['test_case_name']
                if len(automation_report_data['test_case_name'])>37:
                    test_case_name = "<div data-title='" + test_case_name + "' class='tooltip'>" + \
                                     test_case_name[0:34] + "...</div>"
                if automation_report_data['status'] == "Fail":
                    colour = "Red"
                html_content = html_content + """<div style='margin-top:5pt; width: 95%; padding: 2px 5.5px 6px 2px;
                text-align:left; border:3px solid; border-color:6C8EBF; border-radius: 25px;'>
                <div style='float:left; padding-left:10pt; width:10%;'><b>Test Case ID : </b></div>
                <div style='float:left; width:5%;'>""" + automation_report_data['test_case_id'] + """</div>"""
                html_content = html_content + """<div style='float:left; padding-left:20pt; width:12%;'><b>
                Test Case Name : </b></div><div style='float:left; width:25%;'>""" \
                               + test_case_name + """</div>"""
                html_content = html_content + """<div style='float:left; padding-left:10pt; width:10%;'><b>Subscription
                 : </b></div><div style='float:left; width:7%;'>""" + str(automation_report_data['subscription_id']) \
                               + """</div><div style='float:left; padding-left:20pt; width:5%;'><b>Status :</b>
                </div><div style='float:left; width:10%;'>
                <div style='background-color: """ + colour + """; width: 40pt; height:15pt; padding-top: 0px; 
                text-align: center; color: white;'><b>""" + automation_report_data['status'] \
                               + """</b></div></div><button class="collapsible" id='toggleBtn'>+</button>
                               <div class="content"><hr/>"""
                html_content = html_content + """<table width='100%' cellpadding='4'>
                 <tr><td width="12%"><b>Marketplace</b> <td width="8%">: 
                </td><td width="80%"> """ + automation_report_data['marketplace_name'] + """</td></tr>
                <tr><td width="12%"><b>Outcome</b></td><td width="8%">: 
                </td><td width="80%"> """ + automation_report_data['failure_reason'] + """</td></tr>"""
                html_content = html_content + """<tr><td width="12%">
                <div data-title='Shows the point at which Automation execution failed.' class='tooltip'><b>
                Failure Level</b></div></td><td width="8%"> : </td>
                <td width="80%"> """ + automation_report_data['status_level_fail'] + """</td></tr>"""
                html_content = html_content + """<tr><td width="12%">
                <div data-title='Shows the points at which data did not match across systems.' class='tooltip'>
                <b>Warning Level</b></div></td><td width="8%">:</td>
                <td width="80%">
                 """ + automation_report_data['status_level_warning'] + """</td></tr>"""
                html_content = html_content + """</table></div></div>"""

            html_content = html_content + """</center>
             <script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
	  this.textContent = '+';
    } else {
      content.style.display = "block";
	  this.textContent = '-';
    }
  });
}
</script>
            </body></html>"""

            file_path = './AutomationReports/AutomationReport_' + str(datetime.datetime.now()).replace(":",
                                                                                                       "_") + '.html'
            f = open(file_path, 'w')
            f.write(html_content)
            f.close()
        except Exception as e:
            self.logger.error("Exception occurred while generating the report %s", e)
            raise e
