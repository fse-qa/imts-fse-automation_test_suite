import json
import time

import requests
import urllib3 as urllib3

from CommonUtilities.baseSet.baseAPIUtil import BaseAPIUtility
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.miscUtility import MiscellaneousUtility
from CommonUtilities.parse_config import ParseConfigFile
from Usage.API_Models.cataloggroups_products import CataloggroupsProducts
from Usage.API_Models.usage_files_body import UsageFilesBody
from Usage.API_Models.usagefiles_period import UsagefilesPeriod
from Usage.API_Settings import apiBaseURLs
from Usage.Operation.usageDBOps import UsageDBOperation

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class APIOperation:
    logger = LogGenerator.logGen()
    parse_config_json = ParseConfigFile()
    misc_utility = MiscellaneousUtility()
    base_api_utility = BaseAPIUtility()
    connect_api_url = parse_config_json.get_data_from_config_json("default", "cbConnectApi_url", "CredConfig.json")
    connect_api_key = parse_config_json.get_data_from_config_json("default", "cbConnectApi_key", "CredConfig.json")
    usage_db_operation = UsageDBOperation()

    """
        This is to return asset details from CB Connect with respect to IM Subscription ID
            param1 = subscription_id
    """

    def get_asset_details_from_connect(self, subscription_id, test_case_id):
        asset_details = {}
        usage_items = {}
        subscription_id = str(subscription_id)
        try:
            headers = self.base_api_utility.get_header_for_connect_api()
            get_asset_api_url = self.connect_api_url + apiBaseURLs.connect_asset_request + subscription_id
            self.logger.info(
                "Going to search the asset details for Subscription Id: %s in CB Connect" % subscription_id)
            response = requests.get(get_asset_api_url, headers=headers)
            response_status_code = response.status_code
            self.logger.info("%s : is the API response status code" % response_status_code)
            response = response.json()
            for element in response:
                response_body = element
                asset_request_id = response_body["id"]
                asset_details["assetRequestId"] = asset_request_id
                product_details = element["product"]
                product_id = product_details["id"]
                product_name = product_details["name"]
                asset_details["product_id"] = product_id
                asset_details["product_name"] = product_name
                marketplace_details = element["marketplace"]
                marketplace_name = marketplace_details["name"]
                asset_details["marketplace_name"] = marketplace_name
                contract_details = element["contract"]
                contract_id = contract_details["id"]
                asset_details["contract_id"] = contract_id
                item_details = element["items"]
                for item_element in item_details:
                    items = item_element
                    usage_mpn_list = self.usage_db_operation.do_select_usage_mpns_details_as_per_testcase(test_case_id)
                    for usage_mpn_detail in usage_mpn_list:
                        if usage_mpn_detail.get("item_type") == "Usage":
                            if items["mpn"] == usage_mpn_detail.get("vendor_sku") and items[
                                "item_type"] == "PPU" and items["type"] == usage_mpn_detail.get(
                                "unit_of_measure"):
                                usage_items["usage_mpn"] = items["mpn"]
                                usage_items["mpn_name"] = items["display_name"]
                                asset_details["usage_items"] = usage_items
                        break
                self.logger.info("Getting asset details as: %s" % asset_details)
                break
        except Exception as e:
            self.logger.error(
                "Something wrong with getting asset details from CB Connect for Subscription ID: %s" % subscription_id)
            self.logger.exception(
                f'Exception occurred as : {e}')
        return asset_details

    """
        This is to create usage id in CB Connect via API for respective IM Subscription ID
            param1 = subscription_id
            param2 = usage_start_date
            param3 = usage_end_date 
    """

    def do_create_usage_in_connect(self, subscription_id, usage_start_date, usage_end_date, test_case_id):
        usage_id = None
        subscription_id = str(subscription_id)
        usage_file_name = "Usage_Upload_For_" + subscription_id
        asset_details = self.get_asset_details_from_connect(subscription_id, test_case_id)
        product_id = asset_details.get("product_id")
        contract_id = asset_details.get("contract_id")
        contract_request = CataloggroupsProducts(contract_id)
        product_request = CataloggroupsProducts(product_id)
        # usage_start_date = usage_start_date + "T01:00:00+00:00"
        # usage_end_date = usage_end_date + "T23:59:59+00:00"
        usage_period_request = UsagefilesPeriod(usage_start_date, usage_end_date)
        from_keyword = "from"
        usage_period_request = usage_period_request.to_dict()
        usage_period_request[from_keyword] = usage_period_request.pop("_from")
        create_usage_file_body = UsageFilesBody(usage_file_name,
                                                "",
                                                "",
                                                usage_period_request,
                                                "",
                                                product_request,
                                                contract_request,
                                                subscription_id)
        try:
            create_usage_file_body = create_usage_file_body.to_dict()
            # return create_usage_file_body
            usage_file_creation_api_url = self.connect_api_url + apiBaseURLs.connect_usage_file_creation
            data = json.dumps(create_usage_file_body, sort_keys=True, indent=4)
            headers = {'Content-type': 'application/json', 'Accept': 'application/json',
                       'Authorization': self.connect_api_key}
            # response = requests.post(url=usage_file_creation_api_url, headers=headers, data=data, verify=False)
            response = requests.post(url=usage_file_creation_api_url, headers=headers, data=data, verify=None)
            self.logger.info("Going to create Usage ID in CB Connect for Subscription ID: %s" % subscription_id)
            response_status_code = response.status_code
            create_usage_response_body = response.json()
            self.logger.info("%s : is the API response status code" % response_status_code)
            usage_id = create_usage_response_body["id"]
            self.logger.info("Created Usage ID in CB Connect for Subscription ID is : %s" % usage_id)
        except Exception as e:
            self.logger.error(
                "Something wrong with creating Usage ID in CB Connect for Subscription : %s" % subscription_id)
            self.logger.exception(
                f'Exception occurred as : {e}')
        return usage_id

    """
        This is to upload usage file in CB Connect via API for respective IM Subscription ID
            param1 = usage_id
            param2 = subscription_id
    """

    def do_upload_usage_file_in_connect(self, usage_id, subscription_id):
        uploaded_usage_id = None
        subscription_id = str(subscription_id)

        connect_usage_file_upload = self.base_api_utility.do_update_api_query_url(apiBaseURLs.connect_usage_file_upload,
                                                                                  usage_id)
        usage_upload_api_url = self.connect_api_url + connect_usage_file_upload
        file_path = self.misc_utility.do_search_file_name(subscription_id)
        headers = self.base_api_utility.get_header_for_connect_api()
        try:
            self.misc_utility.do_open_directory(
                self.parse_config_json.get_data_from_config_json("cbConnectUsage", "usage_output_file_location"))
            files = {'usage_file': open(file_path, 'rb')}
            response = requests.post(url=usage_upload_api_url, headers=headers, files=files, verify=False)
            self.logger.info("Going to upload Usage ID %s in CB Connect for Subscription ID" % usage_id)
            response_status_code = response.status_code
            self.logger.info("%s : is the API response status code" % response_status_code)
            upload_usage_response_body = response.json()
            uploaded_usage_id = upload_usage_response_body["id"]
        except Exception as e:
            self.logger.error(
                "Something wrong with uploading Usage in CB Connect for Subscription : %s" % subscription_id)
            self.logger.exception(
                f'Exception occurred as : {e}')

        return uploaded_usage_id

    """
        This is to submit usage report in CB Connect via API for respective IM Subscription ID
            param1 = usage_id
    """

    def do_submit_usage_report(self, usage_id):

        connect_usage_submit = self.base_api_utility.do_update_api_query_url(apiBaseURLs.connect_usage_file_submit,
                                                                             usage_id)
        usage_submit_api_url = self.connect_api_url + connect_usage_submit
        connect_usage_id_details = self.base_api_utility.do_update_api_query_url(apiBaseURLs.connect_get_usage,
                                                                                 usage_id)
        get_usage_id_details_api_url = self.connect_api_url + connect_usage_id_details
        headers = self.base_api_utility.get_header_for_connect_api()
        try:
            for retry in range(3):
                response = requests.get(url=get_usage_id_details_api_url, headers=headers)
                self.logger.info("Getting uploaded %s Usage details from CB CB Connect" % usage_id)
                get_usage_details_response_body = response.json()
                response_status_code = response.status_code
                self.logger.info("%s : is the API response status code" % response_status_code)
                if get_usage_details_response_body["status"] == "ready":
                    response = requests.post(url=usage_submit_api_url, headers=headers)
                    self.logger.info("Going to submit %s Usage ID in Connect" % usage_id)
                    response_status_code = response.status_code
                    # submit_usage_response_body = response.json()
                    self.logger.info("%s : is the API response status code" % response_status_code)
                    break
                elif get_usage_details_response_body["status"] != "ready":
                    time.sleep(10)
                else:
                    self.logger.error(
                        "Something seems wrong with the uploaded usage, please check further from CB Connect.")
        except Exception as e:
            self.logger.error("Something seems wrong with submitting the usage : %s in CB Connect" % usage_id)
            self.logger.exception(
                f'Exception occurred as : {e}')

        return get_usage_details_response_body["status"]

    def do_get_usage_status(self, usage_id):
        connect_usage_status = None
        connect_usage_id_details = self.base_api_utility.do_update_api_query_url(apiBaseURLs.connect_get_usage,
                                                                                 usage_id)
        get_usage_id_details_api_url = self.connect_api_url + connect_usage_id_details
        headers = self.base_api_utility.get_header_for_connect_api()
        try:
            for retry in range(3):
                response = requests.get(url=get_usage_id_details_api_url, headers=headers)
                self.logger.info("Getting latest status of %s Usage ID from CB Connect" % usage_id)
                get_usage_details_response_body = response.json()
                response_status_code = response.status_code
                assert (response_status_code == 200)
                if get_usage_details_response_body["id"] == usage_id and get_usage_details_response_body[
                    "status"] == "accepted":
                    connect_usage_status = get_usage_details_response_body["status"]
                    break
                elif get_usage_details_response_body["status"] != "ready":
                    time.sleep(10)
                elif get_usage_details_response_body["status"] == "invalid":
                    connect_usage_status = get_usage_details_response_body["status"]
                    break
                else:
                    self.logger.error(
                        "Something seems wrong with the uploaded usage, please check further from CB Connect.")

        except Exception as e:
            self.logger.error("Something seems wrong while getting details of usage : %s in CB Connect" % usage_id)
            self.logger.exception(
                f'Exception occurred as : {e}')

        return connect_usage_status
