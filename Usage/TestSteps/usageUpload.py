from CommonUtilities.logGeneration import LogGenerator
from Usage.Operation.usageDBOps import UsageDBOperation
from Usage.Operation.usageFile import UsageFile
from Usage.RestAPI.apiOps import APIOperation


class UploadUsageForSubscription:
    usage_data_from_db_ops = UsageDBOperation()
    api_operation = APIOperation()
    usage_file = UsageFile()
    logger = LogGenerator.logGen()

    def create_usage_file_and_upload_in_cb_connect(self, test_case_id, test_status):
        uploaded_usage_list = []

        usage_mpn_list_from_input_per_testcase = self.usage_data_from_db_ops.do_select_usage_mpns_details_as_per_testcase(
            test_case_id)
        subscription_id_per_testcases = self.usage_data_from_db_ops.do_select_im360_subs_data_as_per_testcase(
            test_case_id)
        if test_status == "Intermittent":
            usage_start_date = self.usage_data_from_db_ops.do_select_usage_dates_for_cancel().get("usage_start_date")
            usage_end_date = self.usage_data_from_db_ops.do_select_usage_dates_for_cancel().get("usage_end_date")
        else:
            usage_start_date = self.usage_data_from_db_ops.do_select_usage_dates_for_e2e().get("usage_start_date")
            usage_end_date = self.usage_data_from_db_ops.do_select_usage_dates_for_e2e().get("usage_end_date")

        usage_amount = [amount["usage_amount"] for amount in usage_mpn_list_from_input_per_testcase]
        usage_quantity = [quantity["item_quantity"] for quantity in usage_mpn_list_from_input_per_testcase]

        self.usage_file.update_usage_file_and_save(subscription_id_per_testcases, usage_start_date,
                                                   usage_end_date, usage_amount, usage_quantity, test_case_id)
        usage_id = self.api_operation.do_create_usage_in_connect(subscription_id_per_testcases, usage_start_date,
                                                                 usage_end_date, test_case_id)
        uploaded_usage_id = self.api_operation.do_upload_usage_file_in_connect(usage_id, subscription_id_per_testcases)

        self.api_operation.do_submit_usage_report(uploaded_usage_id)
        cb_connect_usage_status = self.api_operation.do_get_usage_status(uploaded_usage_id)
        if cb_connect_usage_status == "accepted":
            uploaded_usage_list.append(test_case_id)
            uploaded_usage_list.append(subscription_id_per_testcases)
            uploaded_usage_list.append(uploaded_usage_id)
            uploaded_usage_list.append(usage_start_date)
            uploaded_usage_list.append(usage_end_date)
            uploaded_usage_list.append(cb_connect_usage_status)
            self.usage_data_from_db_ops.do_insert_usage_data_as_per_testcase(uploaded_usage_list)
            self.logger.info(
                "Usage is successfully uploaded and processed for Test case : %s" % test_case_id)
        else:
            self.logger.error("Usage upload is failed for Test Case: %s" % test_case_id)
