RECORD_ID = "record_id"
MPN_NAME = "record_note"
ITEM_SEARCH_CRITERIA = "item_search_criteria"
MPN_VALUE = "item_search_value"
ASSET_SEARCH_CRITERIA = "asset_search_criteria"
ASSET_SEARCH_VALUE = "asset_search_value"
START_TIME = "start_time_utc"
END_TIME = "end_time_utc"
CATEGORY_ID = "category_id"
QUANTITY = "quantity"
AMOUNT = "amount"
TIER = "tier"


