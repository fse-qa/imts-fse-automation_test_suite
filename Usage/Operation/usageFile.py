import shutil
from typing import Any

import openpyxl as openpyxl
import pandas as pd
from CommonUtilities import RandomNumbers
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from Usage.Facade import usageExcelColumnParam, usageExcelDefaultValues
from Usage.RestAPI.apiOps import APIOperation
import os
import pathlib


class UsageFile:
    parse_config_json = ParseConfigFile()
    apiOperation = APIOperation()
    logger = LogGenerator.logGen()
    usage_start_time = "00:00:00"
    usage_end_time = "23:59:59"

    """
        This is to create usage file in XLSX for respective IM Subscription ID according to Sample Usage File
            param1 = subscription_id
            param1 = usage_start_date
            param1 = usage_end_date
            param1 = usage_amount
            param1 = usage_quantity
    """

    def do_create_usage_file(self, subscription_id, usage_start_date, usage_end_date, usage_amount, usage_quantity,
                             test_case_id):
        subscription_usage_file = None
        usage_file_details = None
        repo_path = os.path.dirname(os.path.abspath(__file__))
        root_path = pathlib.Path(repo_path)
        root_path = root_path.parent.parent
        root_path = str(root_path).replace('WindowsPath(', '').replace(')', '')
        sample_usage_file = self.parse_config_json.get_data_from_config_json("cbConnectUsage", "usage_sample_file")
        sample_usage_file = root_path + sample_usage_file
        self.logger.info("Getting sample usage file: %s as reference" % sample_usage_file)
        usage_records = pd.read_excel(sample_usage_file, sheet_name="records",
                                      index_col=0).to_string()  # Read sample file record here
        subscription_id = str(subscription_id)
        subscription_usage_file_name = "UsageFileFor_" + subscription_id + ".xlsx"
        self.logger.info("Creating usage file: %s for Subscription ID" % subscription_usage_file_name)
        subscription_usage_output_location = self.parse_config_json.get_data_from_config_json("cbConnectUsage",
                                                                                              "usage_output_file_location")
        subscription_usage_output_location = root_path + subscription_usage_output_location
        usage_start_date = usage_start_date + " " + self.usage_start_time
        usage_end_date = usage_end_date + " " + self.usage_end_time
        try:

            subscription_usage_file = subscription_usage_output_location + subscription_usage_file_name
            shutil.copyfile(sample_usage_file, subscription_usage_file)
            subs_usage_dataframe = pd.read_excel(subscription_usage_file, sheet_name="records")
            self.logger.info("Reading Usage excel file")
            customer_record_id = "CustomerRow"
            reseller_record_id = "ResellerRow"
            vendor_record_id = "VendorRow"
            customer_usage_row_value = customer_record_id + "-" + subscription_id + "-" + RandomNumbers.GenerateRandomNum()
            reseller_usage_row_value = reseller_record_id + "-" + subscription_id + "-" + RandomNumbers.GenerateRandomNum()
            vendor_usage_row_value = vendor_record_id + "-" + subscription_id + "-" + RandomNumbers.GenerateRandomNum()
            asset_details = self.apiOperation.get_asset_details_from_connect(subscription_id, test_case_id)
            usage_mpn_details = asset_details.get("usage_items")
            usage_mpn = usage_mpn_details.get("usage_mpn")
            usage_mpn_name = usage_mpn_details.get("mpn_name")
            asset_id = asset_details.get("assetRequestId")
            """
            Customer Row start
            """
            specific_customer_row: Any = subs_usage_dataframe[
                subs_usage_dataframe['record_id'] == customer_record_id].copy()
            specific_customer_row.loc[:, usageExcelColumnParam.RECORD_ID] = customer_usage_row_value
            specific_customer_row.loc[:, usageExcelColumnParam.MPN_NAME] = usage_mpn_name
            specific_customer_row.loc[:,
            usageExcelColumnParam.ITEM_SEARCH_CRITERIA] = usageExcelDefaultValues.ITEM_SEARCH_CRITERIA_VALUE
            specific_customer_row.loc[:, usageExcelColumnParam.MPN_VALUE] = usage_mpn
            specific_customer_row.loc[:,
            usageExcelColumnParam.ASSET_SEARCH_CRITERIA] = usageExcelDefaultValues.ASSET_SEARCH_CRITERIA_VALUE
            specific_customer_row.loc[:, usageExcelColumnParam.ASSET_SEARCH_VALUE] = asset_id
            specific_customer_row.loc[:, usageExcelColumnParam.START_TIME] = usage_start_date
            specific_customer_row.loc[:, usageExcelColumnParam.END_TIME] = usage_end_date
            specific_customer_row.loc[:, usageExcelColumnParam.CATEGORY_ID] = usageExcelDefaultValues.CATEGORY_ID_VALUE
            specific_customer_row.loc[:, usageExcelColumnParam.QUANTITY] = usage_quantity
            specific_customer_row.loc[:, usageExcelColumnParam.AMOUNT] = usage_amount
            specific_customer_row.loc[:, usageExcelColumnParam.TIER] = usageExcelDefaultValues.CUSTOMER_TIER_VALUE

            """
            Reseller Row start
            """
            specific_reseller_row: Any = subs_usage_dataframe[
                subs_usage_dataframe['record_id'] == reseller_record_id].copy()
            specific_reseller_row.loc[:, usageExcelColumnParam.RECORD_ID] = reseller_usage_row_value
            specific_reseller_row.loc[:, usageExcelColumnParam.MPN_NAME] = usage_mpn_name
            specific_reseller_row.loc[:,
            usageExcelColumnParam.ITEM_SEARCH_CRITERIA] = usageExcelDefaultValues.ITEM_SEARCH_CRITERIA_VALUE
            specific_reseller_row.loc[:, usageExcelColumnParam.MPN_VALUE] = usage_mpn
            specific_reseller_row.loc[:,
            usageExcelColumnParam.ASSET_SEARCH_CRITERIA] = usageExcelDefaultValues.ASSET_SEARCH_CRITERIA_VALUE
            specific_reseller_row.loc[:, usageExcelColumnParam.ASSET_SEARCH_VALUE] = asset_id
            specific_reseller_row.loc[:, usageExcelColumnParam.START_TIME] = usage_start_date
            specific_reseller_row.loc[:, usageExcelColumnParam.END_TIME] = usage_end_date
            specific_reseller_row.loc[:, usageExcelColumnParam.CATEGORY_ID] = usageExcelDefaultValues.CATEGORY_ID_VALUE
            specific_reseller_row.loc[:, usageExcelColumnParam.QUANTITY] = usage_quantity
            specific_reseller_row.loc[:, usageExcelColumnParam.AMOUNT] = usage_amount
            specific_reseller_row.loc[:, usageExcelColumnParam.TIER] = usageExcelDefaultValues.RESELLER_TIER_VALUE

            """
            Vendor Row start
            """
            specific_vendor_row: Any = subs_usage_dataframe[
                subs_usage_dataframe['record_id'] == vendor_record_id].copy()
            specific_vendor_row.loc[:, usageExcelColumnParam.RECORD_ID] = vendor_usage_row_value
            specific_vendor_row.loc[:, usageExcelColumnParam.MPN_NAME] = usage_mpn_name
            specific_vendor_row.loc[:,
            usageExcelColumnParam.ITEM_SEARCH_CRITERIA] = usageExcelDefaultValues.ITEM_SEARCH_CRITERIA_VALUE
            specific_vendor_row.loc[:, usageExcelColumnParam.MPN_VALUE] = usage_mpn
            specific_vendor_row.loc[:,
            usageExcelColumnParam.ASSET_SEARCH_CRITERIA] = usageExcelDefaultValues.ASSET_SEARCH_CRITERIA_VALUE
            specific_vendor_row.loc[:, usageExcelColumnParam.ASSET_SEARCH_VALUE] = asset_id
            specific_vendor_row.loc[:, usageExcelColumnParam.START_TIME] = usage_start_date
            specific_vendor_row.loc[:, usageExcelColumnParam.END_TIME] = usage_end_date
            specific_vendor_row.loc[:, usageExcelColumnParam.CATEGORY_ID] = usageExcelDefaultValues.CATEGORY_ID_VALUE
            specific_vendor_row.loc[:, usageExcelColumnParam.QUANTITY] = usage_quantity
            specific_vendor_row.loc[:, usageExcelColumnParam.AMOUNT] = usage_amount
            specific_vendor_row.loc[:, usageExcelColumnParam.TIER] = usageExcelDefaultValues.VENDOR_TIER_VALUE
            usage_file_details = specific_customer_row
            usage_file_details = usage_file_details.append(specific_reseller_row)
            usage_file_details = usage_file_details.append(specific_vendor_row)

            self.logger.info("Created Usage file record %s with details as below " % subscription_usage_file)
            self.logger.info(usage_file_details)
        except Exception as e:
            self.logger.error(f'Something wrong happened while creating usage file. {e}')
        return usage_file_details, subscription_usage_file

    """
        This is to update the usage file in XLSX for respective IM Subscription ID with relevant details
            param1 = subscription_id
            param1 = usage_start_date
            param1 = usage_end_date
            param1 = usage_amount
            param1 = usage_quantity
    """

    def update_usage_file_and_save(self, subscription_id, usage_start_date, usage_end_date,
                                   usage_amount,
                                   usage_quantity, test_case_id):
        subscription_id = str(subscription_id)
        try:
            get_excel_content = self.do_create_usage_file(subscription_id, usage_start_date,
                                                          usage_end_date, usage_amount,
                                                          usage_quantity, test_case_id)
            usage_file_name = get_excel_content[1]
            usage_records = get_excel_content[0]
            workbooks = openpyxl.load_workbook(usage_file_name)
            writer = pd.ExcelWriter(usage_file_name, engine='openpyxl')
            writer.book = workbooks
            writer.sheets = dict((ws.title, ws) for ws in workbooks.worksheets)
            usage_records.to_excel(writer, "records", index=False, header=False, startrow=1)
            writer.save()
            self.logger.info("Saved Usage file %s for further actions." % usage_file_name)
        except Exception as e:
            self.logger.error(f'Something wrong happened while saving usage file: {e}')

        return True
