from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from db.service.UsageUploadStatusDbManagementService import UsageDataDbManagementService
import os


class UsageDBOperation:
    parse_config_file = ParseConfigFile()
    db_file_path = parse_config_file.get_data_from_config_json("dbLocation", "db_file_path")
    usage_data_details = UsageDataDbManagementService()

    def do_select_im360_subs_data_as_per_testcase(self, test_case_id):
        subscription_id = self.usage_data_details.get_subscription_from_im360_by_testcase(self.db_file_path,
                                                                                          test_case_id)
        return subscription_id

    def do_select_usage_mpns_details_as_per_testcase(self, test_case_id):
        usage_mpn = self.usage_data_details.get_usage_mpn_from_input_by_testcase(self.db_file_path, test_case_id)
        return usage_mpn

    def do_select_usage_dates_for_e2e(self):
        usage_dates = self.usage_data_details.get_usage_dates_from_commerce_date_track_for_e2e(self.db_file_path)
        return usage_dates

    def do_select_usage_dates_for_cancel(self):
        usage_dates = self.usage_data_details.get_usage_dates_from_commerce_date_track_for_cancel(self.db_file_path)
        return usage_dates

    def do_insert_usage_data_as_per_testcase(self, usage_details_list):
        is_exist = os.path.exists(self.db_file_path)
        if not is_exist:
            self.db_file_path = os.path.dirname(os.getcwd()) + "\\" + self.db_file_path
        self.usage_data_details.insert_usage_details_by_testcase(self.db_file_path, usage_details_list)
