from selenium.webdriver.common.by import By

from CommonUtilities.baseSet.BasePage import BasePage


class CreditReview(BasePage):
    """By locators"""
    CREDIT_REVIEW_BUTTON = (By.XPATH, "//span[text()='Credit Review']")
    # SEARCHED_QUOTE_SELECT_ALL_BUTTON = (By.CSS_SELECTOR, "button[title='Select All']")
    SEARCHED_QUOTE_SELECT_ALL_BUTTON = (By.XPATH, "//*[@aria-label='Toggle selection of all rows']")
    SEARCHED_QUOTE_SELECT_CHECKBOX = (By.CSS_SELECTOR, "div[id*='id-cell-0-1']")
    EDIT_SELECTED_QUOTE_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Edit']")
    APPROVE_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Approve']")
    CREDIT_REVIEW_APPROVE_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Approve']")
    CREDIT_REVIEW_QUOTE_HEADER = (By.CSS_SELECTOR, "h1[id^='formHeaderTitle']")
    CREDIT_REVIEW_APPROVE_POPUP_CONFIRM_BUTTON = (By.XPATH, "//span[text()='Confirm']")
    # SEARCHED_RECORD_TO_REVIEW = (By.CSS_SELECTOR, "div[id*='id-cell-0-5']")
    SEARCHED_RECORD_TO_REVIEW = (By.XPATH, "//*[@col-id='im360_recordtoreviewid']//a")
    RECORD_TO_REVIEW_TEXT = (By.XPATH,
                             "//*[@data-id='im360_recordtoreviewid.fieldControl-LookupResultsDropdown_im360_recordtoreviewid_selected_tag_text']")

    """Constructor of Credit Review page"""

    def __init__(self, driver):
        super().__init__(driver)

    """This method gets into Credit hold page and searches the on hold quote. It returns the header text"""

    def search_and_get_quote_on_credit_hold(self, quote_name):
        self.do_click_by_locator(self.CREDIT_REVIEW_BUTTON)
        self.logger.info("Clicked on Credit review button on left pane")
        self.do_sleep("above_min")
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching quote on credit hold with name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.do_sleep("above_min")
        # self.do_double_click(self.SEARCHED_FIRST_QUOTE_SELECT_BUTTON)
        for retry in range(3):
            if self.do_get_attribute(self.SEARCHED_RECORD_TO_REVIEW, "aria-label") == quote_name:
                self.do_sleep("above_min")
                self.do_click_by_locator(self.SEARCHED_QUOTE_SELECT_ALL_BUTTON)
                self.logger.info("searched quote first occurrence is selected")
                try:
                    self.do_click_by_locator(self.EDIT_SELECTED_QUOTE_BUTTON)
                    self.logger.info("Clicked on the edit button")
                    break
                except:
                    self.logger.info("Edit button not found. Trying to reload..")
                    if self.do_get_attribute(self.SEARCHED_QUOTE_SELECT_CHECKBOX, "aria-selected") == "true":
                        self.do_click_by_locator(self.SEARCHED_QUOTE_SELECT_ALL_BUTTON)
                    else:
                        pass
            else:
                self.logger.info("Trying to select Quote for credit approval. Retry: %s" % str(retry))
                self.do_sleep("average")
        return self.get_element_text(self.CREDIT_REVIEW_QUOTE_HEADER)

    """This method approves the on hold quote"""

    def approve_credit_review(self, quote_name):
        self.do_sleep("max")
        if f"Review Quote: {quote_name}" in self.search_and_get_quote_on_credit_hold(quote_name):
            self.do_click_by_locator(self.APPROVE_BUTTON)
            self.logger.info("Successfully clicked the approve button to release credit hold on quote")
            self.do_sleep("above_min")
            self.do_click_by_locator(self.CREDIT_REVIEW_APPROVE_POPUP_CONFIRM_BUTTON)
            self.logger.info("Clicked on confirm button of popup")
        else:
            self.logger.error("Sorry.. Not able to approve the credit hold quote!!")
            raise Exception("Not able to approve the credit hold quote!!")

    """This method used to click on record to review text"""

    def click_on_record_to_review_text(self):
        self.do_sleep("min")
        self.do_click_by_locator(self.RECORD_TO_REVIEW_TEXT)
        self.logger.info('Clicked on Record To Review text')
