from selenium.webdriver.common.by import By

from CommonUtilities.baseSet.BasePage import BasePage


class DashboardPage(BasePage):

    """By locators"""
    USER_ICON = (By.CSS_SELECTOR, "div[id='mectrl_headerPicture']")
    SIGNOUT = (By.ID, "mectrl_body_signOut")

    """constructor of the Dashboard Page class"""

    def __init__(self, driver):
        super().__init__(driver)

    """Page actions for logout page"""

    """This is used to logout from IM360"""
    def do_logout_from_im360(self):
        self.do_click_by_locator(self.USER_ICON)
        self.do_click_by_locator(self.SIGNOUT)
        self.logger.info("Logging out...")

    # def close_browser_driver(self):
    #     self.do_sleep("above_min)
    #     self.driver.quit()
    #     self.logger.info("Closing the browser")