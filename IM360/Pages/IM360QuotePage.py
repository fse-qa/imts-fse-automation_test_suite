from selenium.webdriver.common.by import By

from CommonUtilities.baseSet.BasePage import BasePage
from CommonUtilities.readProperties import ReadConfig
from db.service.IM360ItemDbManagementService import IM360ItemDbManagementService
from db.service.IM360SubscriptionParameterDbManagementService import IM360SubscriptionParameterDbManagementService


class QuotePage(BasePage):
    """By locators"""

    """Quote dashboard section"""
    QUOTE_UNDER_SALES_BUTTON = (By.ID, "sitemap-entity-nav_quotes")
    ADD_NEW_QUOTE_BUTTON = (By.XPATH, "//button//span[contains(text(), 'New')]")
    CONVERT_QUOTE_TO_ORDER_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Create Order. Convert quote to order']")
    QUOTE_VIEW_LIST = (By.CSS_SELECTOR, "#id__1")
    # MY_QUOTE_VIEW = (By.XPATH, "//span[normalize-space()='My Quotes']")
    MY_QUOTE_VIEW = (By.XPATH, "//span[text() = 'My Quotes']")
    ALL_SUBSCRIPTION_VIEW = (By.XPATH, "//span[text() = 'All Subscriptions']")
    ALL_QUOTES_VIEW = (By.XPATH, "//span[text() = 'All Quotes']")
    INGRAM_SUBSCRIPTION_ID_FROM_QUOTE_LIST = (By.XPATH, "//div[@class='wj-row']/div[@data-id='cell-0-5']/span")
    DROP_DOWN = (By.XPATH, "//*[@data-icon-name='ChevronDown']")

    """Create draft Quote section"""
    NEW_QUOTE_HEADER = (By.XPATH, "//h1[@title='New Quote']")
    GO_BACK_BUTTON = (By.XPATH, "//button[@title='Go back']")
    SAVE_PRIMARY_QUOTE_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Save (CTRL+S)']")
    NEW_QUOTE_NAME_TEXTBOX = (By.CSS_SELECTOR, "input[data-id='name.fieldControl-text-box-text']")

    """added for assertion BCN _VALUE"""
    DROP_DOWN_VALUE = (By.XPATH, "//input[@data-id='im360_bcn.fieldControl-text-box-text']")

    """added for assertion"""
    CONTACT_COMBOBOX_POSTSELECT = (
        By.XPATH, "//*[@data-id='im360_contact.fieldControl-LookupResultsDropdown_im360_contact_selected_tag_text']")

    CONTACT_DROPDOWN_SELECT = (
        By.CSS_SELECTOR, "div[data-id='im360_contact.fieldControl-LookupResultsDropdown_im360_contact_infoContainer']")
    ACCOUNT_COMBOBOX = (By.XPATH,
                        "//input[@data-id='customerid.fieldControl-LookupResultsDropdown_customerid_textInputBox_with_filter_new']")
    ACCOUNT_DROPDOWN_SELECT = (
        By.CSS_SELECTOR, "div[data-id='customerid.fieldControl-LookupResultsDropdown_customerid_infoContainer']")
    CONTACT_LEBEL = (By.XPATH, "//span[@title='Contact']")
    CONTACT_COMBOBOX = (By.XPATH,
                        "//input[@data-id='im360_contact.fieldControl-LookupResultsDropdown_im360_contact_textInputBox_with_filter_new']")

    PAYMENT_TERM_TEXT = (By.CSS_SELECTOR,
                         "ul[data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_SelectedRecordList']")
    PAYMENT_TERM_DELETE = (By.XPATH,
                           "//button[@data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_selected_tag_delete']")
    PAYMENT_TERM_COMBOBOX = (By.XPATH,
                             "//input[@data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_textInputBox_with_filter_new']")
    PAYMENT_TERM_DROPDOWN_SELECT = (
        By.XPATH, "//div[@id='im360_paymenttermsid.fieldControl|__flyoutRootNode_SimpleLookupControlFlyout']"
                  "//div[@data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_children']"
                  "//div[@data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_tabContainer']"
                  "//ul[@data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_tab']"
                  "/li/button")
    PAYMENT_TERM_VALUE = (By.XPATH, "//input[@aria-label='Payment Terms']")
    PAYMENT_TYPE_VALUE = (By.XPATH,
                          "//div[@data-id='im360_paymenttermsid.fieldControl-LookupResultsDropdown_im360_paymenttermsid_selected_tag_text']")
    PAYMENT_TERM = (By.XPATH, "//input[@data-id='im360_terms.fieldControl-text-box-text']")
    CONTACT_VALUE = (
        By.XPATH, "//div[@data-id='im360_contact.fieldControl-LookupResultsDropdown_im360_contact_selected_tag']")
    """Addded for assertion"""
    PRIMARY_VENDOR_NBR = (
        By.XPATH,
        "//input[contains(@data-id, 'im360_primary_vendor_qv.im360_vendornumber.fieldControl-text-box-text')]")

    PRIMARY_VENDOR_DROPDOWN_SELECT = (
        By.CSS_SELECTOR, "div[data-id='im360_vendor.fieldControl-LookupResultsDropdown_im360_vendor_infoContainer']")
    CURRENCY_VALUE = (By.XPATH,
                      "//div[@data-id='transactioncurrencyid.fieldControl-LookupResultsDropdown_transactioncurrencyid_selected_tag_text']")
    NEW_QUOTE_HEADER_TAB = (By.XPATH, "//ul[@aria-label='Commands']")
    SAVE_AND_CLOSE_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Save & Close']")
    NEW_BUTTON = (By.CSS_SELECTOR, "button[aria-label='New']")
    SELECT_ADDRESS_BUTTON = (
        By.CSS_SELECTOR, "button[data-lp-id='Form:quote-quote|NoRelationship|Form|Mscrm.Form.quote.LookupAddress']")
    QUOTED_EXCHANGE_RATE_LABEL = (
        By.XPATH, "//input[@data-id='im360_exchangerate.fieldControl-decimal-number-text-input']")
    APPLIED_EXCHANGE_RATE_LABEL = (
        By.XPATH, "//input[@data-id='im360_appliedexchangerate.fieldControl-decimal-number-text-input']")
    PRICE_LIST_SPAN = (By.XPATH, "//span[@role='presentation' and @title='Price List - price_level_quotes']")
    PRICING_COLUMN_TEXTBOX = (
        By.XPATH, "//input[@data-id='Pricing_Column.im360_pricingcolumn.fieldControl-text-box-text']")
    RESELLER_PO_TEXTBOX = (By.XPATH, "//textarea[@data-id='im360_resellerpo.fieldControl-text-box-text']")
    END_USER_PO_TEXTBOX = (By.XPATH, "//textarea[@data-id='im360_enduserpo.fieldControl-text-box-text']")
    ESTIMATE_ID_TEXTBOX = (By.XPATH, "//input[@data-id='im360_estimateid.fieldControl-text-box-text']")
    CONFIGURATION_STATUS_COMBOBOX = (
        By.XPATH, "//select[@data-id='im360_configurationstatus.fieldControl-option-set-select']")
    PRIMARY_VENDOR_LABEL = (By.XPATH, "// span[ @ title = 'Primary Vendor']")
    PRIMARY_VENDOR_COMBOBOX = (By.XPATH,
                               "//input[@data-id='im360_vendor.fieldControl-LookupResultsDropdown_im360_vendor_textInputBox_with_filter_new']")
    # PRIMARY_VENDOR_DROPDOWN_SELECT = (
    #     By.CSS_SELECTOR, "div[data-id='im360_vendor.fieldControl-LookupResultsDropdown_im360_vendor_infoContainer']")
    CREATE_BID_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Create BID']")
    CREATE_COG_REQUEST_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Create COG Request']")
    CREATE_OPPORTUNITY_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Create opportunity']")
    PIVOT_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Pivot']")
    PIVOT_MAKER_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Pivot Maker']")
    FLOW_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Flows. Run Flow']")

    """Activate Quote section"""
    QUOTE_ID_TEXTBOX_AUTOGENERATE = (By.CSS_SELECTOR, "input[data-id='quotenumber.fieldControl-text-box-text']")
    ACTIVATE_QUOTE_HEADER = (By.CSS_SELECTOR, "h1[data-id='header_title']")
    ACTIVATE_QUOTE_HEADER_STATUS = (By.CSS_SELECTOR, "h1[data-id='header_title']>span")
    ADDRESS_TAB_BUTTON = (By.CSS_SELECTOR, "li[data-id='tablist-tab_Addresses']")
    BILL_TO_SEARCH_FRAME = (By.ID, "WebResource_BillToAddressActions")
    BILL_TO_SELECT_FRAME = (By.ID, "WebResource_BillToAddressSearch")
    BILL_TO_SEARCH_BUTTON = (By.ID, "id__4")
    BILL_TO_SEARCH_TEXTBOX = (By.ID, "TextField1")
    BILL_TO_SELECT_BUTTON = (By.XPATH, "//div[contains(text(), 'Select')]")
    BILL_TO_SUFFIX_VALUE = (By.XPATH, "//input[@aria-label='BillTo Suffix/ID']")
    SUMMARY_TAB_BUTTON = (By.CSS_SELECTOR, "li[data-id='tablist-tab_Summary']")

    BILL_TO_OVERRIDE_PAYMENT_TERM_POPUP_CONFIRM = (By.ID, "confirmButton")
    PRODUCT_TAB_BUTTON = (By.CSS_SELECTOR, "li[data-id='tablist-tab_Products']")
    PRODUCT_SEARCH_TEXTBOX = (By.ID, "SearchBox4")
    PRODUCT_SEARCH_BUTTON = (By.XPATH, "//div[contains(text(), 'Search')]")
    PRODUCT_SEARCH_FRAME = (By.ID, "WebResource_ProductSearchBox")
    PRODUCT_SELECT_FRAME = (By.ID, "WebResource_ProductSearch")
    CONTRACT_TERM_MORE_LINK = (By.XPATH, "//span[contains(text(), 'More...')]")
    PRODUCT_FILTER_BUTTONS_ALL = (By.CSS_SELECTOR, "button[class^='ms-Link root']")
    PRODUCT_LINE_ITEMS_ALL = (By.CLASS_NAME, "ms-List-cell")
    PRODUCT_LINE_ITEMS_RADIO_BUTTON_ALL = (By.CSS_SELECTOR, "div[class = 'ms-List-page'] i[class^='ms-Check-check']")
    PRODUCT_LINE_ITEMS_VENDOR_SKU_ALL = (By.CSS_SELECTOR,
                                         "div[data-automation-key = 'vendorSku'] span.src-components-ProductSearch-Results-SearchResults-contentWrap")
    PRODUCT_LINE_ITEMS_PLAN_ID_ALL = (By.CSS_SELECTOR,
                                      "div[data-automation-key = 'planid'] span.src-components-ProductSearch-Results-SearchResults-contentWrap")
    PRODUCT_ADD_BUTTON = (By.XPATH, "(//div[text() = 'Add'])[1]")
    PRODUCT_GRID_FRAME = (By.ID, "WebResource_ProductGrid")
    PRODUCT_GRID_CHANGE_VIEW_DROPDOWN = (By.CSS_SELECTOR,
                                         "button[class^='ms-Button ms-Button--commandBar ms-CommandBarItem-link root'] i[class^='ms-Button-menuIcon menuIcon']")
    PRODUCT_GRID_SUBSCRIPTION_VIEW_BUTTON = (By.NAME, "Subscriptions")
    PRODUCT_GRID_VENDOR_SKU = (By.CSS_SELECTOR, "div[col-id='product.vendorSku'][role='gridcell']")
    # PRODUCT_GRID_QUANTITY = (By.CSS_SELECTOR, "div[col-id='quantity'] div.ag-react-container")
    # PRODUCT_GRID_QUANTITY = (By.XPATH, "//div[@role='row' and @row-index='%s']/div[@col-id='quantity']/div[@class='ag-react-container']"%str(grid_row))
    PRODUCT_GRID_COST = (By.CSS_SELECTOR, "div[col-id='costValues'] div.ag-react-container")
    # PRODUCT_GRID_COST = (By.XPATH, "//div[@row-index='0']/div[@col-id='costValues']")  # Need to change for multiple MPN
    PRODUCT_GRID_COST_AFTER_CREDIT = (By.CSS_SELECTOR, "div[col-id='specialCost'][role='gridcell']")
    PRODUCT_GRID_PRICE = (By.CSS_SELECTOR, "div[col-id='priceDisplay'] div.ag-react-container")
    PRODUCT_GRID_PRICE_AFTER_CREDIT = (By.CSS_SELECTOR, "div[col-id='specialPrice'][role='gridcell']")
    PRODUCT_GRID_SAVE_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Save']")
    ACTIVATE_QUOTE_BUTTON = (By.CSS_SELECTOR, "button[aria-label^='Activate Quote']")
    RESELLER_BCN = (By.XPATH, "//input[@aria-label='BCN']")
    BILL_TO_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='BillTo Suffix/ID']")
    PRODUCT_GRID_SKU = (By.XPATH, "//div[@col-id='skuDisplay']/span")
    PRODUCT_GRID_SCREEN_PRICE = (By.XPATH, "//div[@col-id='screenPriceDisplay']/span")
    PRODUCT_GRID_MSRP = (By.XPATH, "//div[@col-id='product.priceRetail']/span")
    PRODUCT_GRID_ROW = (By.XPATH, "//div[@row-index]")

    """Credit review section"""
    CREDIT_REVIEW_STATUS = (By.CSS_SELECTOR, "*[aria-label='Status Reason']")
    CREDIT_REVIEW_FRAME = (By.ID, "WebResource_CreditReviewStatusIndicator")
    CREDIT_REVIEW_MESSAGE = (By.CSS_SELECTOR, "span.ms-MessageBar-innerText>span")

    """Search Quotes"""
    SEARCH_THIS_VIEW = (By.ID, "quickFind_text_1")

    """Quotes Details section"""
    SUBSCRIPTION_DETAILS_TAB = (By.CSS_SELECTOR, "li[aria-label='Subscription Details']")
    INGRAM_SUBSCRIPTION_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Ingram Subscription ID']")
    VENDOR_SUBSCRIPTION_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Vendor Subscription ID']")

    """Draft Quote Summary section"""
    SERVICE_PLAN_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Service Plan Id']")
    CONTARCT_TERM_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Contract Term']")

    """Sign In"""
    SIGN_IN_BUTTON = (By.XPATH, "//span[contains(text(), 'Sign in')]")

    db_file_path = ReadConfig.get_db_file_path()
    # im360_subs_db_mgmt_srv = IM360SubscriptionDbManagementService()
    im360_item_db_mgmt_srv = IM360ItemDbManagementService()
    im360_subs_param_db_mgmt_srv = IM360SubscriptionParameterDbManagementService()

    PRODUCT_GRID_ACTIONS = (
        By.XPATH, "//div[@col-id='actions']/div[@class='ag-react-container']/button//i[@data-icon-name='Delete']")

    CONTRACT_TERM_DROPDOWN = (By.XPATH, "//*[@id='Dropdown1-option']")
    CONTRACT_TERM_OPTIONS = (By.XPATH, "//*[@role='listbox']/button/div/span")
    APPLY_BUTTON = (By.XPATH, "//*[@class='ms-Button-textContainer textContainer-79']")
    CHANGE_CONTRACT_TERM_FRAME = (By.ID, "WebResource_ChangeContractTerm")
    MONEY_FIELD_BUTTON = (By.XPATH, "//*[text()='OK']")
    CLICK_REFRESH = (By.XPATH, "//span[text()='Refresh']")
    ERROR_POPUP = (By.XPATH, "//h1[@aria-label='Error']")
    ERROR_POPUP_DESCRIPTION = (By.XPATH, "//h2[@data-id='errorDialog_subtitle']")
    ERROR_POPUP_OK_BUTTON = (By.XPATH, "//span[text()='OK']")
    screen_shot_path = ReadConfig.getScreenshotPath()

    """Constructor of App select page"""

    def __init__(self, driver):
        super().__init__(driver)

    """Page actions for app select page"""

    """
    This is used to open the new quote form
    Author: Arpita Basu
    """

    def do_open_draft_quote_form(self):
        self.do_switch_to_parent_frame()
        self.do_sleep("above_min")
        self.do_click_by_locator(self.QUOTE_UNDER_SALES_BUTTON)
        self.logger.info("Clicked on Quote under sales")
        self.do_sleep("above_min")
        self.do_check_visibility(self.ADD_NEW_QUOTE_BUTTON)  # to wait for visibility of element
        self.do_sleep("average")
        self.do_click_by_locator(self.ADD_NEW_QUOTE_BUTTON)
        self.logger.info("Clicked on add new quote button")
        self.do_sleep("average")
        self.do_refresh_quote_page()
        self.logger.info("Checking New Quote loaded correctly or not")
        self.do_check_availability(self.NEW_QUOTE_HEADER)

    """This is used to search a quote and get into it. Returns True when successful."""

    def do_search_and_get_into_existing_quote(self, quote_name):
        self.do_sleep("above_min")
        self.do_click_by_locator(self.QUOTE_UNDER_SALES_BUTTON)
        self.logger.info("Clicked on Quote under sales")
        self.do_sleep("above_min")
        self.do_click_by_locator(self.DROP_DOWN)
        self.do_click_by_locator(self.ALL_QUOTES_VIEW)
        self.do_sleep("above_min")
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching quote with name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.do_sleep("above_min")
        self.do_click_by_locator((By.LINK_TEXT, quote_name))
        self.logger.info("Quote %s found. Getting into it!!" % quote_name)
        if (self.get_element_title(self.ACTIVATE_QUOTE_HEADER)).strip() == quote_name:
            self.logger.info("Quote searched successfully")
            return True
        else:
            self.logger.info("Some issue happened. Not able to get into the quote")
            return False

    """
    This is used to create a new draft quote
    Author: Arpita Basu  
    """

    def do_create_draft_quote(self, quote_name, account, contact, primary_vendor, im360_subs_output_data):
        # self.driver.execute_script("document.body.style.zoom='33%'")
        self.do_sleep("max")
        try:
            self.do_clear_textfield(self.NEW_QUOTE_NAME_TEXTBOX)
            self.do_send_keys(self.NEW_QUOTE_NAME_TEXTBOX, quote_name)
            self.do_get_keys(self.NEW_QUOTE_NAME_TEXTBOX)
            quote = self.do_get_keys(self.NEW_QUOTE_NAME_TEXTBOX)
            assert quote == quote_name, " quote name is not  matching"
        except Exception as e:
            raise e
        self.logger.info("Quote name %s provided" % quote_name)
        self.do_sleep("min")
        try:
            self.do_clear_textfield(self.ACCOUNT_COMBOBOX)
            self.do_send_keys(self.ACCOUNT_COMBOBOX, account)
            self.do_sleep("above_min")
            self.do_click_by_locator(self.ACCOUNT_DROPDOWN_SELECT)
            self.logger.info("Account = %s captured from UI ", account)
            self.logger.info("Account BCN=%s selected" % account)
            self.do_sleep("above_min")
            bcn_value = self.get_element_title(self.DROP_DOWN_VALUE)
            assert str(bcn_value) == str(account), 'bcn value is not matching '
        except Exception as e:
            raise e
        self.logger.info("Account BCN=%s selected" % account)
        self.do_sleep("min")
        if self.do_check_visibility(self.MONEY_FIELD_BUTTON):
            self.is_money_field_shown_click_on_button()
        try:
            self.do_clear_textfield(self.CONTACT_COMBOBOX)
            self.do_send_keys(self.CONTACT_COMBOBOX, contact)
            self.do_sleep("min")
            self.do_click_by_locator(self.CONTACT_DROPDOWN_SELECT)
            contact_name = self.get_element_title(self.CONTACT_COMBOBOX_POSTSELECT)
            assert contact_name == contact, 'contact name is not matching'
        except Exception as e:
            raise e
        self.do_sleep("above_min")
        self.logger.info("Contact %s selected" % contact)
        self.do_sleep("min")
        quoted_exchange_rate = self.get_all_elements(self.QUOTED_EXCHANGE_RATE_LABEL)
        self.do_click_by_element(quoted_exchange_rate[0])
        applied_exchange_rate = self.get_all_elements(self.APPLIED_EXCHANGE_RATE_LABEL)
        self.do_click_by_element(applied_exchange_rate[0])
        price_list = self.get_all_elements(self.PRICE_LIST_SPAN)
        self.do_click_by_element(price_list[0])
        pricing_column = self.get_all_elements(self.PRICING_COLUMN_TEXTBOX)
        self.do_click_by_element(pricing_column[0])
        reseller_po = self.get_all_elements(self.RESELLER_PO_TEXTBOX)
        self.do_click_by_element(reseller_po[0])
        end_user_po = self.get_all_elements(self.END_USER_PO_TEXTBOX)
        self.do_click_by_element(end_user_po[0])
        estimate_id = self.get_all_elements(self.ESTIMATE_ID_TEXTBOX)
        self.do_click_by_element(estimate_id[0])
        configuration_status = self.get_all_elements(self.CONFIGURATION_STATUS_COMBOBOX)
        self.do_click_by_element(configuration_status[0])
        primary_vendor_label = self.get_all_elements(self.PRIMARY_VENDOR_LABEL)
        self.do_click_by_element(primary_vendor_label[0])
        try:
            self.do_send_keys(self.PRIMARY_VENDOR_COMBOBOX, primary_vendor)
            self.do_click_by_locator(self.PRIMARY_VENDOR_DROPDOWN_SELECT)
            vendor_nbr_vsr = self.get_element_title(self.PRIMARY_VENDOR_NBR)
            assert str(vendor_nbr_vsr) == str(primary_vendor), 'primary vendor value is not matching'
        except Exception as e:
            raise e
        self.logger.info("Primary Vendor %s selected" % primary_vendor)
        self.do_sleep("above_min")
        self.do_click_by_locator(self.SAVE_PRIMARY_QUOTE_BUTTON)
        self.logger.info("Primary quote saved successfully")
        # Get output data to save in DB
        im360_subs_output_data.reseller_bcn = self.get_reseller_bcn()
        # contact_value = self.do_get_attribute(self.CONTACT_VALUE, "aria-label")
        im360_subs_output_data.currency = self.get_currency()

    """
    Validates whether a draft quote is successfully created. Searches the Quote by quote_name
    Author: Arpita Basu
    """

    def do_validate_draft_quote(self, quote_name):
        quote_id = ""
        self.do_sleep("average")
        for retry in range(3):
            quote_id = self.do_get_attribute(self.QUOTE_ID_TEXTBOX_AUTOGENERATE, "value")
            self.logger.info("Quote id: " + quote_id)
            if quote_id == "---":
                self.do_sleep("average")
            else:
                break
        if quote_name in (self.do_get_attribute(self.ACTIVATE_QUOTE_HEADER, "title")):
            self.logger.info("Header validated successfully. Good to go for activate quote. Quote id = %s" % quote_id)
            return True
        else:
            self.logger.info("Header validation failed. Quote id = %s" % quote_id)
            return False

    """This method iterates through all the filters and filter products by billing and subscription period"""

    def do_filter_by_billing_and_subscription_period(self, bill_period, subs_period):

        try:
            for element in self.get_all_elements(self.PRODUCT_FILTER_BUTTONS_ALL):
                self.logger.info(element.text.split('\n')[0])
                if bill_period == element.text.split('\n')[0] or subs_period == element.text.split('\n')[0]:
                    self.do_click_by_element(element)
                    self.logger.info("Clicked on: %s" % element.text)
        except:
            pass

    """This method selects product by plan id and sku combination"""

    def do_select_product_lines(self, sku_ids, plan_id):
        # product_line_items_radio_button = self.get_all_elements(self.PRODUCT_LINE_ITEMS_RADIO_BUTTON_ALL)
        product_line_items_radio_button = self.driver.find_elements_by_css_selector(
            "div[class = 'ms-List-page'] i[class^='ms-Check-check']")
        product_line_items_skus = self.get_all_elements(self.PRODUCT_LINE_ITEMS_VENDOR_SKU_ALL)
        product_line_items_plan_id = self.get_all_elements(self.PRODUCT_LINE_ITEMS_PLAN_ID_ALL)
        skus = sku_ids.split(",")
        self.logger.info("SKUS = %s" % skus)

        if len(product_line_items_radio_button) == len(product_line_items_skus) == len(product_line_items_plan_id):
            for sku in skus:
                for i in range(len(product_line_items_radio_button)):
                    if sku.strip() == product_line_items_skus[i].text and int(
                            product_line_items_plan_id[i].text) == plan_id:
                        self.do_click_by_element(product_line_items_radio_button[i])
                        self.logger.info("Selected one product line item: %s" % sku)
                        break
        else:
            self.logger.error("length of line items did not match!!")
            raise Exception("length of line items did not match!!")

    """This method goes to Address tab and search and select bill to suffix"""

    def do_select_bill_to(self, bill_to):
        try:
            self.do_click_by_locator(self.ADDRESS_TAB_BUTTON)
            self.logger.info("Clicked on Address tab")
            self.do_switch_to_required_frame(self.BILL_TO_SEARCH_FRAME)
            self.do_click_by_locator(self.BILL_TO_SEARCH_BUTTON)
            self.logger.info("Clicked on bill_to search button")
            self.do_sleep("above_min")
            self.do_switch_to_parent_frame()
            self.do_switch_to_required_frame(self.BILL_TO_SELECT_FRAME)
            self.do_send_keys(self.BILL_TO_SEARCH_TEXTBOX, bill_to)
            self.logger.info("Bill_to searched with value %s" % bill_to)
            self.do_sleep("above_min")
            self.do_click_by_locator(self.BILL_TO_SELECT_BUTTON)
            self.do_switch_to_parent_frame()
            # if self.is_present(self.BILL_TO_OVERRIDE_PAYMENT_TERM_POPUP_CONFIRM):
            #     self.do_click_by_locator(self.BILL_TO_OVERRIDE_PAYMENT_TERM_POPUP_CONFIRM)
            #     self.logger.info("Override payment term confirmed!!")
            self.logger.info("%s bill_to is selected" % bill_to)
        except Exception as e:
            self.logger("Exception occurred in do_select_bill_to %s", e)
            raise e

    """This method goes to Summary tab and search and select payment term"""

    def do_select_payment_term(self, payment_term):
        try:
            self.do_click_by_locator(self.SUMMARY_TAB_BUTTON)
            self.logger.info("Clicked on Summary tab")
            res_contact = self.get_all_elements(self.CONTACT_LEBEL)
            self.do_click_by_element(res_contact[0])
            quoted_exchange_rate = self.get_all_elements(self.QUOTED_EXCHANGE_RATE_LABEL)
            self.do_click_by_element(quoted_exchange_rate[0])
            payment_term_text = self.get_all_elements(self.PAYMENT_TERM_TEXT)
            self.do_mouse_hover_to_element(payment_term_text[0])
            payment_term_delete = self.get_all_elements(self.PAYMENT_TERM_DELETE)
            self.do_click_by_element(payment_term_delete[0])
            self.do_send_keys(self.PAYMENT_TERM_COMBOBOX, payment_term)
            self.do_send_keys_down_enter(self.PAYMENT_TERM_DROPDOWN_SELECT)
            self.do_sleep("min")
            # self.do_clear_textfield(self.PAYMENT_TERM)
            payment_value = self.get_element_title(self.PAYMENT_TERM)
            try:
                assert str(payment_value) == str(payment_term), 'payment term not matching'
            except Exception as e:
                raise e
            self.logger.info("payment term %s selected" % payment_term)
            self.do_sleep("min")
        except Exception as e:
            self.logger.error("Exception occurred in do_select_payment_term %s", e)
            raise e

    """This method goes to product tab and searches the product"""

    def do_search_products(self, sku_id):
        try:
            self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
            self.logger.info("Product tab is selected successfully")
            self.do_switch_to_required_frame(self.PRODUCT_SEARCH_FRAME)
            self.do_send_keys(self.PRODUCT_SEARCH_TEXTBOX, sku_id)
            self.do_click_by_locator(self.PRODUCT_SEARCH_BUTTON)
            self.logger.info("following products are searched: %s" % sku_id)
            self.do_switch_to_parent_frame()
        except Exception as e:
            self.logger.error("Exception occurred in do_search_products %s", e)
            raise e

    """This method filters searched product by bill period, subs period, plan id and add them"""

    def do_select_products(self, bill_period, subs_period, sku_id, plan_id):
        self.do_switch_to_required_frame(self.PRODUCT_SELECT_FRAME)

        try:
            self.do_click_by_locator(self.CONTRACT_TERM_MORE_LINK)
            self.logger.info("Clicked on more contract term link")
        except:
            self.logger.error("There are no more contract term")
        self.do_filter_by_billing_and_subscription_period(bill_period, subs_period)
        self.logger.info("Product filtered by billing period = %s and subs period = %s" % (bill_period, subs_period))
        self.do_sleep("above_min")
        self.do_select_product_lines(sku_id, plan_id)
        self.logger.info("Product lines with %s selected" % sku_id)
        self.do_sleep("above_min")
        self.do_click_by_locator(self.PRODUCT_ADD_BUTTON)
        self.logger.info("Selected product lines added successfully")
        self.do_switch_to_parent_frame()

    """This method changes the view to subscription view in product grid of quote"""

    def do_change_quote_view_to_subscription_view(self):
        try:
            self.do_switch_to_required_frame(self.PRODUCT_GRID_FRAME)
            self.do_click_by_locator(self.PRODUCT_GRID_CHANGE_VIEW_DROPDOWN)
            self.logger.info("Clicked on change view dropdown")
            self.do_sleep("max")
            self.do_click_by_locator(self.PRODUCT_GRID_SUBSCRIPTION_VIEW_BUTTON)
            self.logger.info("Subscription view selected")
            self.do_switch_to_parent_frame()
        except Exception as e:
            self.logger.error("Exception occurred in do_change_quote_view_to_subscription_view %e", e)
            raise (e)

    """ This method enters the quantity of added products in product grid """

    def do_provide_sku_quantity(self, sku_ids, quantities):
        self.do_switch_to_required_frame(self.PRODUCT_GRID_FRAME)
        product_grid_skus = self.get_all_elements(self.PRODUCT_GRID_VENDOR_SKU)
        # product_grid_quantity = self.get_all_elements(self.PRODUCT_GRID_QUANTITY)
        # product_grid_cost = self.get_all_elements(self.PRODUCT_GRID_COST)
        # product_grid_cost_after_credit = self.get_all_elements(self.PRODUCT_GRID_COST_AFTER_CREDIT)
        # product_grid_price = self.get_all_elements(self.PRODUCT_GRID_PRICE)
        # product_grid_price_after_credit = self.get_all_elements(self.PRODUCT_GRID_PRICE_AFTER_CREDIT)
        skus = sku_ids.split(",")
        quantity = {}
        for i in range(len(skus)):
            quantity.update({skus[i].strip(): (quantities.split(","))[i].strip()})
        self.logger.info(quantity)
        # if len(product_grid_skus) == len(product_grid_quantity):

        for sku in skus:
            for i in range(len(product_grid_skus)):
                if sku == product_grid_skus[i].text:
                    try:
                        PRODUCT_GRID_QUANTITY = (By.XPATH, "//div[@role='row' and @row-index='" + str(
                            i) + "']/div[@col-id='quantity']/div[@class='ag-react-container']")
                        product_grid_quantity = self.get_all_elements(PRODUCT_GRID_QUANTITY)

                        if i >= 2:
                            # self.do_scroll_to_element(product_grid_quantity[0])
                            self.do_click_by_element(product_grid_quantity[0])
                            product_grid_quantity = self.get_all_elements(PRODUCT_GRID_QUANTITY)

                        self.do_double_click_and_send_keys_by_element(product_grid_quantity[0], quantity.get(sku))
                        self.logger.info(f"quantity {quantity.get(sku)} set for sku {sku}")
                        break
                    except Exception as e:
                        # self.driver.close()
                        self.logger.info("Not able to add qty %s", e)
                        raise Exception("Not able to add qty %s", e)
        # else:
        # self.logger.error("Product grid lengths are not matching!!")
        # raise Exception
        self.do_switch_to_parent_frame()

    """This method fills in address and product tab and activates a quote"""

    def do_activate_quote(self, items_data, plan_id, bill_period, subs_period, bill_to, payment_term,
                          im360_subs_output_data):

        try:
            self.do_sleep("average")
            self.do_select_bill_to(bill_to)
            self.do_sleep("above_min")
            im360_subs_output_data.bill_to = self.get_bill_to()
            self.do_sleep("above_min")
            self.do_select_payment_term(payment_term)
            self.do_sleep("above_min")
            im360_subs_output_data.payment_term = self.get_payment_term()
            im360_subs_output_data.payment_type = self.get_payment_type()
            skus = ""
            quantities = ""
            for index in range(len(items_data)):
                for key in items_data[index]:
                    if key == "vendor_sku":
                        self.do_search_products(items_data[index]['vendor_sku'])
                        self.do_sleep("above_min_2")
                        self.do_select_products(bill_period, subs_period, items_data[index]['vendor_sku'], plan_id)
                        self.do_sleep("average")
                        if skus.strip() != "":
                            skus = skus + ","
                            quantities = quantities + ","
                        skus = skus + items_data[index]['vendor_sku']
                        quantities = quantities + str(items_data[index]['item_quantity'])
                # self.do_change_quote_view_to_subscription_view()
                # self.do_sleep("above_min_2)
                # self.do_provide_sku_quantity(sku_data.get("VendorSku"), sku_data.get("Quantities"))

            self.do_change_quote_view_to_subscription_view()
            self.do_sleep("above_min_2")
            self.do_provide_sku_quantity(skus, quantities)
        except Exception as e:
            self.logger.info("Exception occurred in do_activate_quote %s", e)
            raise e
        # self.do_sleep("above_min_2)
        # self.do_get_product_details_by_mpn(sku_ids)
        self.do_sleep("average")
        self.do_click_by_locator(self.PRODUCT_GRID_SAVE_BUTTON)
        self.logger.info("Product grid save button clicked successfully")
        for poll in range(3):
            if self.get_element_text(self.ACTIVATE_QUOTE_HEADER_STATUS) == "- Saved":
                # self.get_quote_name()
                # self.get_quote_id()
                # self.get_reseller_bcn()
                # self.get_contact()
                # self.get_payment_term()
                # self.get_service_plan_id()
                self.do_click_by_locator(self.ACTIVATE_QUOTE_BUTTON)
                self.logger.info("Activate Quote button clicked successfully")
                break
            elif poll == 3:
                self.logger.error("Not able to click Activate Quote, as the products are not saved successfully.")
                raise Exception("Not able to click Activate Quote, as the products are not saved successfully.")
            else:
                self.logger.info("Polling for products to save")
                self.do_sleep("average")

    """This method checks whether quote is on credit hold after activation"""

    def is_quote_on_credit_hold(self):
        # credit_hold_message = "This Quote is on Credit Review Hold."
        # self.do_switch_to_required_frame(self.CREDIT_REVIEW_FRAME)
        # if credit_hold_message in self.get_element_text(self.CREDIT_REVIEW_MESSAGE):
        credit_hold_message = "Credit Review Hold"
        if credit_hold_message in self.get_element_title(self.CREDIT_REVIEW_STATUS):
            self.do_switch_to_parent_frame()
            return True
        else:
            self.do_switch_to_parent_frame()
            return False

    """ This method clicks on convert to order button and validates the quote name """

    def convert_to_order(self, quote_name):
        self.do_sleep("min")
        self.do_click_by_locator(self.CONVERT_QUOTE_TO_ORDER_BUTTON)
        self.logger.info("Clicked to convert quote to order")
        for retry in range(3):
            if (self.do_get_attribute(self.ACTIVATE_QUOTE_HEADER, "title")) == quote_name:
                self.logger.info("Header validated successfully. Good to go for order creation!!")
                break
            else:
                if retry == 2:
                    self.logger.info("Converting quote to order has a problem!!")
                    raise Exception("Converting quote to order has a problem!!")
                self.do_sleep("average")

    """
        This method is search Quote details by Subscription
        param1 = Subscription_ID
    """

    def do_search_subscription_quote(self, subscription_id, quote_name):
        self.do_switch_to_parent_frame()
        self.do_sleep("average")
        self.do_click_by_locator(self.QUOTE_UNDER_SALES_BUTTON)
        self.logger.info("Clicked on Quotes under sales")
        self.do_click_by_locator(self.MY_QUOTE_VIEW)
        self.do_click_by_locator(self.ALL_SUBSCRIPTION_VIEW)
        self.logger.info("Changing the view from My Quote to All Subscriptions")
        self.do_send_keys(self.SEARCH_BOX, subscription_id)
        self.logger.info("Searching quote with IM Subscription Id: %s" % subscription_id)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.do_sleep("average")
        self.do_click_by_locator((By.LINK_TEXT, quote_name))
        self.logger.info("Quote %s found. Getting into it!!" % quote_name)
        if (self.get_element_title(self.ACTIVATE_QUOTE_HEADER)).strip() == quote_name:
            self.logger.info("got into the quote successfully")
            return True
        else:
            self.logger.info("Some issue happened. Not able to get into the quote")
            return False

    """
        This method is validate Ingram Subscription Id and Vendor Subscription ID details by Subscription
        param1 = Ingram Subscription Id
    """

    def do_search_subscription_all_quote(self, subscription_id, quote_name):
        self.do_switch_to_parent_frame()
        self.do_sleep("average")
        self.do_click_by_locator(self.QUOTE_UNDER_SALES_BUTTON)
        self.logger.info("Clicked on Quotes under sales")
        self.do_click_by_locator(self.MY_QUOTE_VIEW)
        self.do_click_by_locator(self.ALL_QUOTES_VIEW)
        self.logger.info("Changing the view from My Quote to All Subscriptions")
        self.do_send_keys(self.SEARCH_BOX, subscription_id)
        self.logger.info("Searching quote with IM Subscription ID : %s" % subscription_id)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.do_sleep("average")
        self.do_click_by_locator((By.LINK_TEXT, quote_name))
        self.logger.info("Quote %s found. Getting into it!!" % quote_name)
        if (self.get_element_title(self.ACTIVATE_QUOTE_HEADER)).strip() == quote_name:
            self.logger.info("got into the quote successfully")
            return True
        else:
            self.logger.info("Some issue happened. Not able to get into the quote")
            return False

    """
        This method is validate Ingram Subscription Id and Vendor Subscription ID details by Subscription
        param1 = Ingram Subscription Id
    """

    def do_validate_vendor_subscription_id_from_quote(self):
        self.do_sleep("max")
        self.do_check_availability(self.SUBSCRIPTION_DETAILS_TAB)
        self.do_sleep("min")
        self.do_click_by_locator(self.SUBSCRIPTION_DETAILS_TAB)
        self.logger.info("Clicked on Subscription Details from Quote.")
        subscription_id = self.get_element_text(self.INGRAM_SUBSCRIPTION_ID_TEXTBOX)
        print(subscription_id)

    """ This method checks for vendor subscription id and returns it """

    def get_vendor_subs_id(self):
        self.do_sleep("above_min")
        for retry in range(5):
            if self.do_check_visibility(self.SUBSCRIPTION_DETAILS_TAB):
                self.logger.info("Check the availability of Subscription Details tab")
                break
            else:
                self.do_click_by_locator(self.CLICK_REFRESH)
                self.do_sleep("above_min")

        self.do_sleep("min")
        im360_vend_subs_id = None
        self.do_click_by_locator(self.SUBSCRIPTION_DETAILS_TAB)
        self.logger.info("Clicked on Subscription Details from Quote.")
        for retry in range(3):
            if self.do_check_visibility(self.VENDOR_SUBSCRIPTION_ID_TEXTBOX):
                im360_vend_subs_id = self.do_get_attribute(self.VENDOR_SUBSCRIPTION_ID_TEXTBOX, "value")
                break

        return im360_vend_subs_id

    """ This method captures the value of reseller bcn after saving draft quote """

    def get_reseller_bcn(self):
        reseller_bcn = self.do_get_attribute(self.RESELLER_BCN, "value")
        self.logger.info("Reseller BCN Value after saving draft: %s " % reseller_bcn)
        return reseller_bcn

    """ This method captures the value of contact after saving draft quote """

    def get_contact_value(self):
        contact_value = self.do_get_attribute(self.CONTACT_VALUE, "aria-label")
        self.logger.info("Contact value after saving the draft: %s " % contact_value)
        return contact_value

    """ This method captures currency value after saving the draft quote"""

    def get_currency(self):
        currency = self.do_get_attribute(self.CURRENCY_VALUE, "title")
        self.logger.info("Currency value after saving the draft: %s " % currency)
        return currency

    # def get_quote_data(self):
    #
    #     quote_id_value = self.do_get_attribute(self.QUOTE_ID_TEXTBOX_AUTOGENERATE, "value")
    #     quote_name = self.do_get_attribute(self.NEW_QUOTE_NAME_TEXTBOX, "value")
    #     account_value = self.do_get_attribute(self.ACCOUNT_COMBOBOX, "value")
    #     contact_value = self.do_get_attribute(self.CONTACT_COMBOBOX, "value")
    #     reseller_bcn = self.do_get_attribute(self.RESELLER_BCN, "value")
    #     service_plan_id_value = self.do_get_attribute(self.SERVICE_PLAN_ID_TEXTBOX, "value")
    #     contract_term_value = self.do_get_attribute(self.CONTARCT_TERM_TEXTBOX, "value")
    #     payment_term_value = self.do_get_attribute(self.PAYMENT_TERM_COMBOBOX, "value")
    #
    #     return quote_id_value, quote_name, account_value, contact_value, reseller_bcn, service_plan_id_value, contract_term_value, payment_term_value
    #
    # def get_quote_address_tab_data(self):
    #     bill_to_value = self.do_get_attribute(self.BILL_TO_TEXTBOX,"value")
    #     return bill_to_value
    #
    # def get_subscription_tab_data(self, test_case_id):
    #     im_subscription_id = self.do_get_attribute(self.INGRAM_SUBSCRIPTION_ID_TEXTBOX, "value")
    #     vendor_subscription_id = "Test_"+im_subscription_id+"_"+test_case_id
    #
    #     return im_subscription_id, vendor_subscription_id

    """ This method clicks on all subscriptions and searches for the quote and gets its subscription id """

    def do_read_subscription_id_from_all_subscription_list_view(self, quote_name):
        ingram_subscription_id = ""
        self.do_sleep("min")
        self.do_click_by_locator(self.MY_QUOTE_VIEW)
        self.do_click_by_locator(self.ALL_SUBSCRIPTION_VIEW)
        self.logger.info("Changing the view from My Quote to All Subscriptions")
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching quote with name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        if (self.do_get_attribute(By.LINK_TEXT, quote_name)) == quote_name:
            ingram_subscription_id = self.do_get_attribute(self.INGRAM_SUBSCRIPTION_ID_FROM_QUOTE_LIST, "value")
            self.logger.info("Subscription is displayed as %s", ingram_subscription_id)
            return ingram_subscription_id
        return ingram_subscription_id

    """ This method changes view to all subscription and gets the ingram subscription id for particular quote """

    def do_get_into_subscription_from_sll_subscription_list_view(self, quote_name):
        self.do_sleep("above_min")
        self.do_click_by_locator(self.MY_QUOTE_VIEW)
        self.do_click_by_locator(self.ALL_SUBSCRIPTION_VIEW)
        self.logger.info("Changing the view from My Quote to All Subscriptions")
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching quote with name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        if (self.do_get_attribute(By.LINK_TEXT, quote_name)) == quote_name:
            ingram_subscription_id = self.do_get_attribute(self.INGRAM_SUBSCRIPTION_ID_FROM_QUOTE_LIST, "value")
            self.logger.info("Subscription is displayed as %s", ingram_subscription_id)
            return ingram_subscription_id

    """ This method gets the quote id and returns it """

    def get_quote_id(self):
        quote_id_value = self.do_get_attribute(self.QUOTE_ID_TEXTBOX_AUTOGENERATE, "value")
        return quote_id_value

    """ This method gets the quote name and returns it """

    def get_quote_name(self):
        quote_name_value = self.do_get_attribute(self.NEW_QUOTE_NAME_TEXTBOX, "value")
        return quote_name_value

    """ This method gets the reseller bcn and returns it """

    def get_reseller_bcn(self):
        reseller_bcn = self.do_get_attribute(self.RESELLER_BCN, "value")
        return reseller_bcn

    """ This method gets the contact number and returns it"""

    def get_contact(self):
        contact_value = self.do_get_attribute(self.CONTACT_COMBOBOX, "value")
        return contact_value

    """ This method is used to get the payment term """

    def get_payment_term(self):
        try:
            payment_term_value = self.do_get_attribute(self.PAYMENT_TERM_VALUE, "value")
            self.logger.info("Payment Term Value after saving: %s " % payment_term_value)
            return payment_term_value
        except Exception as e:
            self.logger.error("Exception occurred in get_payment_term %s", e)
            raise e

    """ This method is used to get the payment type """

    def get_payment_type(self):
        try:
            payment_type_value = self.do_get_attribute(self.PAYMENT_TYPE_VALUE, "title")
            self.logger.info("Payment Type Value after saving: %s " % payment_type_value)
            return payment_type_value
        except Exception as e:
            self.logger.error("Exception occurred in get_payment_type %s", e)
            raise e

    """ This method is used to get bill to suffix value """

    def get_bill_to(self):
        try:
            bill_to_suffix_value = self.do_get_attribute(self.BILL_TO_SUFFIX_VALUE, "value")
            self.logger.info("BillTo Suffix Value after saving: %s " % bill_to_suffix_value)
            return bill_to_suffix_value
        except Exception as e:
            self.logger.error("Exception occurred in get_bill_to %s", e)
            raise e

    """ This method is used to get service  plan id """

    def get_service_plan_id(self):
        service_plan_id_value = self.do_get_attribute(self.SERVICE_PLAN_ID_TEXTBOX, "value")
        return service_plan_id_value

    """ This method is used to get subscription id """

    def get_subscription_id(self):
        ingram_subscription_id = self.do_get_attribute(self.INGRAM_SUBSCRIPTION_ID_FROM_QUOTE_LIST, "value")
        return ingram_subscription_id

    """ This method is used to get product grid sku """

    def get_sku(self):
        sku_id = self.do_get_attribute(self.PRODUCT_GRID_SKU, "value")
        self.logger.info("Getting SKU")
        return sku_id

    """ This method is used to get vendor mpn """

    def get_vendor_mpn(self):
        mpn_value = self.do_get_attribute(self.PRODUCT_GRID_VENDOR_SKU, "value")
        self.logger.info("Getting MPN")
        return mpn_value

    """ This method is used to get product quantity value """

    def get_sku_quantities(self):
        sku_quantities = self.do_get_attribute(self.PRODUCT_GRID_QUANTITY, "value")
        self.logger.info("Getting value of Quantity")
        return sku_quantities

    """ This method is used to get the total cost in product grid """

    def get_cost(self):
        cost = self.do_get_attribute(self.PRODUCT_GRID_COST, "value")
        self.logger.info("Getting value of Cost as: %s" % cost)
        return cost

    """ This method is used to get cost after the credit """

    def get_cost_after_credit(self):
        cost_after_credit = self.do_get_attribute(self.PRODUCT_GRID_COST_AFTER_CREDIT, "value")
        self.logger.info("Getting value of Cost After Credit as: %s" % cost_after_credit)
        return cost_after_credit

    """ This method is used to get the product screen price """

    def get_screen_price(self):
        price = self.do_get_attribute(self.PRODUCT_GRID_SCREEN_PRICE, "value")
        self.logger.info("Getting value of Screen Price as: %s" % price)
        return price

    """ This method is used to get the quote price """

    def get_quote_price(self):
        quote_price = self.do_get_attribute(self.PRODUCT_GRID_PRICE, "value")
        self.logger.info("Getting value of Price as: %s" % quote_price)
        return quote_price

    """ This method is used to get price after the credit """

    def get_price_after_credit(self):
        price_after_credit = self.do_get_attribute(self.PRODUCT_GRID_PRICE_AFTER_CREDIT, "value")
        self.logger.info("Getting value of Price After Credit as: %s" % price_after_credit)
        return price_after_credit

    """ This method is used to get MSRP in product grid """

    def get_msrp(self):
        msrp = self.do_get_attribute(self.PRODUCT_GRID_MSRP, "value")
        self.logger.info("Getting value of MSRP as: %s" % msrp)
        return msrp

    """ This method checks the visibility of buttons at the top of quote page """

    def do_check_quote_header_availability(self):
        self.do_check_visibility(self.SAVE_PRIMARY_QUOTE_BUTTON)
        self.do_check_visibility(self.SAVE_AND_CLOSE_BUTTON)
        self.do_check_visibility(self.NEW_BUTTON)
        self.do_check_visibility(self.SELECT_ADDRESS_BUTTON)
        self.do_check_visibility(self.CREATE_BID_BUTTON)
        self.do_check_visibility(self.CREATE_COG_REQUEST_BUTTON)
        self.do_check_visibility(self.CREATE_OPPORTUNITY_BUTTON)
        self.do_check_visibility(self.PIVOT_BUTTON)
        self.do_check_visibility(self.PIVOT_MAKER_BUTTON)
        self.do_check_visibility(self.FLOW_BUTTON)

    """ This method refreshes the quote page if menu buttons are not visible """

    def do_refresh_quote_page(self):

        for retry in range(5):
            if self.do_check_visibility(self.CREATE_OPPORTUNITY_BUTTON):
                self.logger.info("Check the availability of Menu Buttons")
                break
            else:
                self.do_refresh()
                self.is_sign_in_button_shown_click_on_button()

    """ This method gets the details of products added in product grid """

    def do_get_product_details_by_mpn(self, sku_ids):
        self.do_switch_to_required_frame(self.PRODUCT_GRID_FRAME)
        product_grid_skus = self.get_all_elements(self.PRODUCT_GRID_VENDOR_SKU)
        product_grid_quantity = self.get_all_elements(self.PRODUCT_GRID_QUANTITY)
        product_grid_cost = self.get_all_elements(self.PRODUCT_GRID_COST)
        # product_grid_cost_after_credit = self.get_all_elements(self.PRODUCT_GRID_COST_AFTER_CREDIT)
        # product_grid_price = self.get_all_elements(self.PRODUCT_GRID_PRICE)
        # product_grid_price_after_credit = self.get_all_elements(self.PRODUCT_GRID_PRICE_AFTER_CREDIT)
        skus = sku_ids.split(",")
        if len(product_grid_skus) == len(product_grid_cost):
            for sku in skus:
                for i in range(len(product_grid_skus)):
                    if sku == product_grid_skus[i].text:
                        product_cost = self.get_cost()
                        product_cost_after_credit = self.get_cost_after_credit()
                        product_price = self.get_quote_price()
                        product_price_after_credit = self.get_price_after_credit()
                        product_quantity = product_grid_quantity[i].text
                        product_msrp = self.get_msrp()
                        break
        else:
            self.logger.error("Product grid lengths are not matching!!")
            raise Exception
        self.do_switch_to_parent_frame()

    """This method checks if the Sign in button is visible or not, if its visible then click on that """

    def is_sign_in_button_shown_click_on_button(self):
        self.do_sleep("above_min")
        if self.do_check_visibility(self.SIGN_IN_BUTTON):
            self.do_click_by_locator(self.SIGN_IN_BUTTON)
            self.logger.info("Successfully click onn SIGN IN button")
            return True
        else:
            return True

    def activate_quote(self, items_data, plan_id, bill_period, subs_period):
        try:
            self.do_sleep("average")
            skus = ""
            quantities = ""
            for index in range(len(items_data)):
                for key in items_data[index]:
                    if key == "vendor_sku":
                        self.do_search_products(items_data[index]['vendor_sku'])
                        self.do_sleep("above_min_2")
                        self.do_select_products(bill_period, subs_period, items_data[index]['vendor_sku'], plan_id)
                        self.do_sleep("average")
                        if skus.strip() != "":
                            skus = skus + ","
                            quantities = quantities + ","
                        skus = skus + items_data[index]['vendor_sku']
                        quantities = quantities + str(items_data[index]['item_quantity'])

            self.do_change_quote_view_to_subscription_view()
            self.do_sleep("above_min_2")
            self.do_provide_sku_quantity(skus, quantities)
        except Exception as e:
            self.logger.info("Exception occurred in do_activate_quote %s", e)
            raise e
        self.do_sleep("average")
        self.do_click_by_locator(self.PRODUCT_GRID_SAVE_BUTTON)
        self.logger.info("Product grid save button clicked successfully")
        for poll in range(3):
            if self.get_element_text(self.ACTIVATE_QUOTE_HEADER_STATUS) == "- Saved":
                self.do_click_by_locator(self.ACTIVATE_QUOTE_BUTTON)
                self.logger.info("Activate Quote button clicked successfully")
                break
            elif poll == 3:
                self.logger.error("Not able to click Activate Quote, as the products are not saved successfully.")
                raise Exception("Not able to click Activate Quote, as the products are not saved successfully.")
            else:
                self.logger.info("Polling for products to save")
                self.do_sleep("average")

    def change_quantity_and_activate_quote(self, items_data, test_case_id, change_type, quantity, marketplace_name):
        try:
            skus = ""
            quantities = ""
            for index in range(len(items_data)):
                for key in items_data[index]:
                    if key == "vendor_sku":
                        self.do_sleep("average")
                        if skus.strip() != "":
                            skus = skus + ","
                            quantities = quantities + ","
                        skus = skus + items_data[index]['vendor_sku']
                        if change_type == "increase":
                            quantities = quantities + str(int(items_data[index]['item_quantity']) + int(quantity))
                        elif change_type == "decrease":
                            quantities = quantities + str(int(items_data[index]['item_quantity']) - int(quantity))
            self.do_change_quote_view_to_subscription_view()
            self.do_sleep("above_min_2")
            self.do_provide_sku_quantity(skus, quantities)
        except Exception as e:
            self.logger.info("Exception occurred in do_activate_quote %s", e)
            raise e
        self.do_sleep("average")
        self.do_click_by_locator(self.PRODUCT_GRID_SAVE_BUTTON)
        self.logger.info("Product grid save button clicked successfully")
        for poll in range(3):
            if self.get_element_text(self.ACTIVATE_QUOTE_HEADER_STATUS) == "- Saved":
                self.do_click_by_locator(self.ACTIVATE_QUOTE_BUTTON)
                self.logger.info("Activate Quote button clicked successfully")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "Activate quote success.png")
                break
            elif poll == 3:
                self.logger.error("Not able to click Activate Quote, as the products are not saved successfully.")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                    + "_" + "Activate quote failure.png")
                raise Exception("Not able to click Activate Quote, as the products are not saved successfully.")
            else:
                self.logger.info("Polling for products to save")
                self.do_sleep("average")

    """This method delete the existing sku """

    def delete_the_existing_sku(self, screen_shot, test_case_id, marketplace_name):
        try:
            self.do_switch_to_required_frame(self.PRODUCT_GRID_FRAME)
            product_action = self.get_all_elements(self.PRODUCT_GRID_ACTIONS)
            for i in product_action:
                self.do_sleep("min")
                product_action = self.get_all_elements(self.PRODUCT_GRID_ACTIONS)
                self.do_click_by_element(product_action[0])
            self.do_switch_to_parent_frame()
            self.do_click_by_locator(self.PRODUCT_GRID_SAVE_BUTTON)
            self.logger.info("Product grid save button clicked successfully")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                + "_" + "delete_existing_sku_success.png")
        except Exception as e:
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" + "delete_existing_sku_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "delete_existing_sku_error.png"

    def change_contract_term_and_activate_quote(self, contract_term, test_case_id, marketplace_name):
        self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
        self.logger.info("Clicked on Product tab ")
        self.do_sleep("above_min")
        self.do_switch_to_required_frame(self.CHANGE_CONTRACT_TERM_FRAME)
        self.do_click_by_locator(self.CONTRACT_TERM_DROPDOWN)
        self.logger.info("Clicked on contract term dropdown")
        self.do_sleep("above_min")
        elements = self.get_all_elements(self.CONTRACT_TERM_OPTIONS)
        for e in elements:
            if str(contract_term) in e.text:
                self.logger.info("Contract Term:" + e.text)
                e.click()
                self.logger.info("clicked on contract term option 2 years")
                break
        self.do_sleep("min")
        self.do_click_by_locator(self.APPLY_BUTTON)
        self.logger.info("clicked on apply button")
        self.do_sleep("min")
        self.do_switch_to_parent_frame()
        self.do_sleep("min")
        self.do_change_quote_view_to_subscription_view()
        self.do_click_by_locator(self.PRODUCT_GRID_SAVE_BUTTON)
        self.logger.info("Product grid save button clicked successfully")
        for poll in range(3):
            if self.get_element_text(self.ACTIVATE_QUOTE_HEADER_STATUS) == "- Saved":
                self.do_click_by_locator(self.ACTIVATE_QUOTE_BUTTON)
                self.logger.info("Activate Quote button clicked successfully")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + test_case_id + "_" + marketplace_name
                    + "_" + "Activate quote success.png")
                break
            elif poll == 3:
                self.logger.error(
                    "Not able to click Activate Quote, as the products are not saved successfully.")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                    + "_" + "Activate quote failure.png")
                raise Exception("Not able to click Activate Quote, as the products are not saved successfully.")
            else:
                self.logger.info("Polling for products to save")
                self.do_sleep("average")

    def is_money_field_shown_click_on_button(self):
        self.do_sleep("above_min")
        if self.do_check_visibility(self.MONEY_FIELD_BUTTON):
            self.do_click_by_locator(self.MONEY_FIELD_BUTTON)
            self.logger.info("Successfully click on Money field button")
            return True
        else:
            return True

    def is_error_popup_shown_click_it(self):
        self.do_switch_to_parent_frame()
        if self.do_check_visibility(self.ERROR_POPUP):
            error_popup_message = self.get_element_text(self.ERROR_POPUP_DESCRIPTION)
            self.logger.info("Error popup message: " + error_popup_message)
            self.do_click_by_locator(self.ERROR_POPUP_OK_BUTTON)
            self.logger.info("Error popup OK button clicked")
            self.do_sleep("above_min")
            return True
        else:
            return True
