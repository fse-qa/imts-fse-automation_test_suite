from selenium.webdriver.common.by import By

from CommonUtilities.baseSet.BasePage import BasePage
from CommonUtilities.parse_config import ParseConfigFile


class LoginPage(BasePage):
    parse_config_json = ParseConfigFile()

    """By locators"""
    USERNAME_TEXTBOX = (By.NAME, "loginfmt")
    PASSWORD_TEXTBOX = (By.NAME, "passwd")
    SUBMIT_BUTTON = (By.ID, "idSIButton9")

    """constructor of the Login Page class"""

    def __init__(self, driver):
        super().__init__(driver)
        # self.do_sleep("average)
        url = self.parse_config_json.get_data_from_config_json("default", "im360BaseUrl", "CredConfig.json")
        self.driver.get(url)
        self.logger.info("Test URL hit: %s" % url)

    """Page actions for login page"""

    """
    This is used to login to IM360
    Author: Soumi Ghosh
    """

    def do_login_to_im360(self, username, password):
        self.do_send_keys(self.USERNAME_TEXTBOX, username)
        self.logger.info("Successfully posted the username: %s" % username)
        self.do_click_by_locator(self.SUBMIT_BUTTON)
        self.logger.info("Successfully submitted the username")
        self.do_send_keys(self.PASSWORD_TEXTBOX, password)
        self.logger.info("Successfully posted the password")
        self.do_click_by_locator(self.SUBMIT_BUTTON)
        self.logger.info("Successfully submitted the password")
        self.do_click_by_locator(self.SUBMIT_BUTTON)
        self.logger.info("Proceeding for App selection page")
