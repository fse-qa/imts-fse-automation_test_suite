from selenium.webdriver.common.by import By
from CommonUtilities.baseSet.BasePage import BasePage


class AppSelectPage(BasePage):

    """By locators"""
    IM360_SALES_APP = (By.XPATH, "//div[contains(text(), 'IM360 - Sales')]")
    APP_SELECT_FRAME = (By.ID, "AppLandingPage")

    """Constructor of App select page"""
    def __init__(self, driver):
        super().__init__(driver)

    """Page actions for app select page"""

    """
    It clicks on IM360 Sales app
    Author: Arpita Basu
    """
    def do_select_app(self):
        self.do_switch_to_required_frame(self.APP_SELECT_FRAME)
        self.do_click_by_locator(self.IM360_SALES_APP)
        self.logger.info("Successfully selected IM360-SALES app")
        self.do_switch_to_parent_frame()
