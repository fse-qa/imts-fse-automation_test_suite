import math

from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from CommonUtilities.baseSet.BasePage import BasePage
from CommonUtilities.parse_config import ParseConfigFile
from CommonUtilities.readProperties import ReadConfig
from db.model.IM360Item import IM360Item
from db.model.IM360SubscriptionParameters import IM360SubscriptionParameters


class OrderPage(BasePage):
    """By locators"""

    ORDER_BUTTON_LEFT_PANE = (By.XPATH, "//span[text()='Orders']")
    ORDER_HEADER = (By.CSS_SELECTOR, "h1[data-id='header_title']")
    MY_ORDERS_VIEW = (By.XPATH, "//span[text() = 'My Orders']")
    ALL_ORDERS_VIEW = (By.XPATH, "//span[text() = 'All Orders']")

    RESELLER_PO_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='ResellerPO']")
    VENDOR_FIELD_BUTTON = (By.CSS_SELECTOR, "li[aria-label='Vendor Fields']")
    PRODUCT_TAB_BUTTON = (By.CSS_SELECTOR, "li[data-id='tablist-tab_Products']")
    DEAL_AGREEMENT_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Deal/Agreement ID']")
    PROVISIONING_CONTACT_NAME_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Provisioning Contact Name']")
    PROVISIONING_CONTACT_EMAIL_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Provisioning Contact Email']")
    PROVISIONING_CONTACT_PHONE_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Provisioning Contact Phone']")
    VENDOR_SUBSCRIPTION_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Vendor Subscription ID']")
    RESELLER_NAME = (By.CSS_SELECTOR, "input[aria-label='Reseller Name']")
    PRORATION_MONTHS = (By.CSS_SELECTOR, "input[aria-label='Proration Months']")
    REQUESTED_SHIP_START_DATE_SELECTOR = (
        By.CSS_SELECTOR, "input[aria-label='Date of Requested Ship / Start Date']")
    REQUESTED_SHIP_START_CALENDARDATE_CURRENTDATE = (By.XPATH, "//*[contains(@class, 'ms-CalendarDay-daySelected')]")
    QUOTE_NUMBER = (By.CSS_SELECTOR, "input[aria-label='Quote Number']")
    WEB_ORDER_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Web Order ID']")
    SERIAL_NUMBER = (By.CSS_SELECTOR, "input[aria-label='Serial Number']")
    AGREEMENT_ID_VALUE = (By.XPATH, "//input[@aria-label='Deal/Agreement ID']")
    # PROVISIONING_CONTACT_NAME_VALUE = (By.XPATH,"//input[@aria-label='Provisioning Contact Name']")
    PRODUCT_GRID_FRAME = (By.ID, "WebResource_ProductGrid")
    PRODUCT_GRID_VENDOR_SKU = (By.CSS_SELECTOR, "div[col-id='product.vendorSku'][role='gridcell']")
    # PRODUCT_GRID_FRAME = (By.XPATH, "//iframe[@id='WebResource_ProductGrid']")
    PREVIEW_ORDER_BUTTON = (By.XPATH, "//div[text()='Preview Order']")
    SUBMIT_ORDER_FRAME = (By.ID, "WebResource_SubmitOrder")
    SUBMIT_ORDER_BUTTON = (By.XPATH, "//div[text()='Submit Order - No Print']")

    SUMMARY_TAB_HEADER = (By.CSS_SELECTOR, "li[aria-label='Summary']")
    ORDER_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Order ID']")
    ERP_STATUS_REASON_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='ERP Status Reason']")
    CREDIT_HOLD_LABEL = (By.XPATH, "//div[text()='Credit Hold']")
    ERP_ORDER_NUMBER_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='ERP Order No.']")
    ORDER_REVIEW_TAB_HEADER = (By.CSS_SELECTOR, "li[aria-label='Order Review']")
    ORDER_DETAILS_FRAME = (By.ID, "WebResource_OrderDetails")
    SUBSCRIPTION_ID_DISPLAY = (By.CSS_SELECTOR, "button[class^='ms-Link root']")
    ORDER_SUBMISSION_MESSAGE = (By.CSS_SELECTOR, "div[id^=MessageBar]>span>span")
    PAGE_SAVE_BUTTON = (By.XPATH, "//button[@aria-label='Save ']")
    SAVE_VENDOR_FIELD_HEADER_STATUS = (By.CSS_SELECTOR, "h1[data-id='header_title']>span")
    """End user page details"""

    END_USER_TAB = (By.CSS_SELECTOR, "li[data-id='tablist-tab_EndUser']")
    BCN_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='BCN / Account ID']")
    END_USER_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='End User Id']")
    END_USER_ACOP_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='End User ACOP Id']")
    END_USER_CONTACT_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='End User Contact ID']")
    COMPANY_NAME_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Company Name']")
    ADDRESS_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Address']")
    ADDRESS2_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Address 2']")
    ADDRESS3_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Address 3']")
    CITY_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='City']")
    STATE_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='State']")
    POSTAL_CODE_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Postal Code']")
    COUNTRY_CODE_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Country Code']")
    CONTACT_NAME_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Contact Name']")
    CONTACT_EMAIL_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Contact Email']")
    CONTACT_PHONE_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='Contact Phone']")
    END_USER_ADDRESS_SEQUENCE_NO_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='End User Address Sequence No.']")
    CB_END_USER_ID_TEXTBOX = (By.CSS_SELECTOR, "input[aria-label='CB End User ID']")
    END_USER_BUTTONS_FRAME_ID = (By.ID, "WebResource_EndUserActions")
    END_USER_SELECTION_FRAME = (By.XPATH, "//iframe[@id = 'WebResource_EndUser']")
    OPEN_SEARCH_BUTTON = (By.XPATH, "//div[text()='Open Search']")
    REMOVE_HEADER_LEVEL_ENDUDER_BUTTON = (By.XPATH, "//div[text()='Remove header level enduser']")
    END_USER_SEARCH_BUTTON = (By.XPATH, "//div[text()='Search']")
    ACOPID_LABEL = (By.XPATH, "//span[@id='header3-acopId']")
    END_USER_SELECT_BUTTON = (By.XPATH, "//div[contains(text(), 'Select')]")
    CREATE_BUTTON = (By.XPATH, "//div[text()='Create']")
    IGNORE_ADDRESS_VALIDATION_CHECKBOX = (By.XPATH, "//div//i[@data-icon-name='CheckMark']")
    SAVE_IN_ERP_BUTTON = (By.XPATH, "//div[text() = 'Save in ERP']")
    GET_CB_END_USER_ID_BUTTON = (By.XPATH, "//div[text() = 'Get CloudBlue End User ID']")
    CB_END_USER_ID = (By.XPATH, "//input[@aria-label='CB End User ID']")

    PRODUCT_GRID_COST_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='costValues']")
    PRODUCT_GRID_COST_AFTER_CREDIT_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='specialCost']")
    PRODUCT_GRID_SCREEN_PRICE_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='screenPriceDisplay']")
    PRODUCT_GRID_QUOTE_PRICE_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='priceDisplay']")
    PRODUCT_GRID_PRICE_AFTER_CREDIT_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='specialPrice']")
    PRODUCT_GRID_MSRP_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='product.priceRetail']")
    PRODUCT_GRID_TCV_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='totalContractValue']")
    PRODUCT_GRID_TCC_TEXTBOX = (By.XPATH, "//div[@row-index='0']/div[@col-id='totalContractCost']")

    """Release Subscription from  hold """

    ORDERS_BUTTON = (By.XPATH, "//span[text()='Orders']")
    SEARCHED_ORDER_SELECT_ALL_BUTTON = (By.CSS_SELECTOR, "button[title='Select All']")
    EDIT_SELECTED_ORDER_BUTTON = (By.CSS_SELECTOR, "button[aria-label='Edit']")
    RELEASE_SUBSCRIPTION_BUTTON = (By.XPATH, "//span[text()='Release Credit Hold']")
    RELEASE_HOLD_APPROVE_CONFIRMATION_POP_UP = (By.XPATH, "//span[text()='Yes - Proceed']")
    CLICK_OK_TO_RELEASE = (By.XPATH, "//span[text()='OK']")
    CHECK_STATUS_OF_SUBSCRIPTION = (By.XPATH, "//select[@aria-label='Status']")

    """Credit Card """
    SUBMIT_ORDER_TAB = (By.CSS_SELECTOR, "li[@data-id='tablist-tab_SubmitOrderConfirmation']")
    PAYMENT_METHOD_FRAME = (By.ID, "WebResource_CybersourcePaymentCapture")
    SELECT_CREDIT_CARD_LABEL = (By.XPATH, "//*[contains(text(),'Select Credit Card')]")
    PAYMENT_METHOD = (By.XPATH, "//h2[data-id='form-sectionHeader-section_PaymentMethod']")
    SELECT_CREDIT_CARD_DROPDOWN = (By.XPATH, "//span[contains(text(),'Select existing')]")
    CREDIT_CARD_LIST = (By.XPATH, "//*[@role='listbox']/button/div/span")
    ENTER_A_NEW_CREDIT_CARD = (By.XPATH, "//*[contains(text(),'Enter a New Credit Card')]")
    CONTINUE_BUTTON = (By.XPATH, "//*[text()='Continue']")
    BILLING_INFORMATION = (By.XPATH, "//*[text()='Billing Information']")
    FIRST_NAME_TEXTBOX = (By.XPATH, "bill_to_forename")
    LAST_NAME_TEXTBOX = (By.ID, "bill_to_surname")
    COMPANY_TEXTBOX = (By.ID, "bill_to_company_name")
    EMAIL_TEXTBOX = (By.ID, "bill_to_email")
    PAYMENT_DETAILS = (By.XPATH, "//h2[text()='Payment Details']")
    CARD_TYPE = (By.XPATH, "//*[contains(text(),'Card Type']")
    CARD_TYPE_LIST = (By.XPATH, "//*[@id='card_type_selection']/div/label")
    CARD_NUMBER_TEXTBOX = (By.ID, "card_number")
    CARD_EXPIRY_MONTH = (By.ID, "card_expiry_month")
    MONTH_LIST = (By.XPATH, "//*[@id='card_expiry_month']/option")
    CARD_EXPIRY_YEAR = (By.ID, "card_expiry_year")
    YEAR_LIST = (By.XPATH, "//*[@id='card_expiry_year']/option")
    CVN_NUMBER_TEXTBOX = (By.ID, "card_cvn")
    FINISH_BUTTON = (By.XPATH, "//*[@type='submit']")
    INGRAM_SUBSCRIPTION_ID_LABEL = (By.XPATH, "//label[contains(text(),'Ingram Subscription Id')]")
    CLICK_REFRESH = (By.XPATH, "//span[text()='Refresh']")

    screen_shot_path = ReadConfig.getScreenshotPath()

    """Constructor of Order page"""

    def __init__(self, driver):
        super().__init__(driver)

    """This is used to search an order and get into it. Returns True when successful."""

    def do_search_and_get_into_existing_order(self, quote_name):
        self.do_switch_to_parent_frame()
        self.do_sleep("above_min")
        self.do_click_by_locator(self.ORDER_BUTTON_LEFT_PANE)
        self.logger.info("Clicked on Order button in left pane")
        self.do_sleep("above_min")
        self.do_click_by_locator(self.MY_ORDERS_VIEW)
        self.do_click_by_locator(self.ALL_ORDERS_VIEW)
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching Order with quote name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.do_sleep("above_min")
        self.do_click_by_locator((By.LINK_TEXT, quote_name))
        self.logger.info("Order with quote name - %s found. Getting into it!!" % quote_name)
        if (self.get_element_title(self.ORDER_HEADER)).strip() == quote_name:
            self.logger.info("got into the order successfully")
            return True
        else:
            self.logger.error("Some issue happened. Not able to get into the order")
            return False

    """ This function enters the reseller po and saves it"""

    def do_input_reseller_po(self, reseller_po, im360_subs_output_data):
        try:
            self.do_sleep("min")
            self.do_clear_textfield(self.RESELLER_PO_TEXTBOX)
            self.do_send_keys(self.RESELLER_PO_TEXTBOX, reseller_po)
            reseller = self.do_get_attribute(self.RESELLER_PO_TEXTBOX, "value")
            assert reseller == reseller_po, 'Reseller PO value not matching'
            self.logger.info("Reseller PO = %s entered" % reseller_po)
            self.do_sleep("min")
            self.do_click_by_locator(self.PAGE_SAVE_BUTTON)
            self.logger.info("Clicked on Save Button in Summary tab")
            self.do_sleep("above_min")
            if self.get_element_text(self.SAVE_VENDOR_FIELD_HEADER_STATUS) == "- Saved":
                self.do_sleep("average")
                im360_subs_output_data.reseller_po = self.get_reseller_po()
        except Exception as e:
            self.logger.error("Exception occurred while inputting reseller PO %s", e)
            raise Exception("Exception occurred while inputting reseller PO %s", e)

    """ This function enters data in vendor field grid and saves it """

    def do_input_vendor_fields(self, deal_id, provisioning_contact_name, provisioning_contact_email,
                               provisioning_contact_phone, web_order_id, requested_ship_start_date,
                               test_case_id, vendor_portal_sub, im360_parameter_data_list=[]):
        try:
            self.do_click_by_locator(self.VENDOR_FIELD_BUTTON)
            self.logger.info("Successfully clicked on vendor fields tab")
            self.do_clear_textfield(self.DEAL_AGREEMENT_ID_TEXTBOX)
            self.do_send_keys(self.DEAL_AGREEMENT_ID_TEXTBOX, deal_id)
            deal = self.do_get_keys(self.DEAL_AGREEMENT_ID_TEXTBOX)
            assert deal == deal_id, 'deal id not matching'
            self.do_send_keys(self.DEAL_AGREEMENT_ID_TEXTBOX, deal_id)
            self.logger.info("deal id = %s entered" % deal_id)
            self.do_clear_textfield(self.PROVISIONING_CONTACT_NAME_TEXTBOX)
            self.do_send_keys(self.PROVISIONING_CONTACT_NAME_TEXTBOX, provisioning_contact_name)
            provision_contact_name = self.do_get_keys(self.PROVISIONING_CONTACT_NAME_TEXTBOX)
            assert provision_contact_name == provisioning_contact_name, 'provision contact name not matching'
            self.logger.info("provisioning contact name = %s entered" % provisioning_contact_name)
            self.do_clear_textfield(self.PROVISIONING_CONTACT_EMAIL_TEXTBOX)
            self.do_send_keys(self.PROVISIONING_CONTACT_EMAIL_TEXTBOX, provisioning_contact_email)
            provision_contact_mail = self.do_get_keys(self.PROVISIONING_CONTACT_EMAIL_TEXTBOX)
            assert provision_contact_mail == provisioning_contact_email, 'provision contact email not matching'
            self.logger.info("provisioning contact email = %s entered" % provisioning_contact_email)
            self.do_clear_textfield(self.PROVISIONING_CONTACT_PHONE_TEXTBOX)
            self.do_send_keys(self.PROVISIONING_CONTACT_PHONE_TEXTBOX, provisioning_contact_phone)
            provision_contact_phone = self.do_get_keys(self.PROVISIONING_CONTACT_PHONE_TEXTBOX)
            assert provision_contact_phone == provisioning_contact_phone, 'provision contact phone not matching'
            self.logger.info("provisioning contact phone = %s entered" % provisioning_contact_phone)
            reseller_name = self.get_all_elements(self.RESELLER_NAME)
            self.do_click_by_element(reseller_name[0])
            proration_months = self.get_all_elements(self.PRORATION_MONTHS)
            self.do_click_by_element(proration_months[0])
            self.do_sleep("above_min")
            self.do_click_by_locator(self.REQUESTED_SHIP_START_DATE_SELECTOR)
            self.do_sleep("min")
            self.do_click_by_locator(self.REQUESTED_SHIP_START_CALENDARDATE_CURRENTDATE)
            self.do_sleep("min")
            serial_number = self.get_all_elements(self.SERIAL_NUMBER)
            self.do_click_by_element(serial_number[0])
            self.do_sleep("min")
            # quote_number = self.get_all_elements(self.QUOTE_NUMBER)
            # self.do_click_by_element(quote_number[0])
            # self.do_sleep("min)
            element_availability = self.do_check_availability(self.WEB_ORDER_ID_TEXTBOX)
            self.logger.info(str(self.WEB_ORDER_ID_TEXTBOX) + " " + str(element_availability))
            self.do_clear_textfield(self.WEB_ORDER_ID_TEXTBOX)
            self.do_send_keys(self.WEB_ORDER_ID_TEXTBOX, web_order_id)
            order_id = self.do_get_keys(self.WEB_ORDER_ID_TEXTBOX)
            assert order_id == web_order_id, 'web order id not matching'
            self.logger.info("web_order_id = %s entered" % web_order_id)
            self.do_sleep("min")
            self.do_click_by_locator(self.PAGE_SAVE_BUTTON)
            self.logger.info("Clicked on Save Button in Vendor parameter tab")
            self.do_sleep("above_min")
            if self.get_element_text(self.SAVE_VENDOR_FIELD_HEADER_STATUS) == "- Saved":
                self.save_param_data_in_db(test_case_id, vendor_portal_sub, im360_parameter_data_list)
        except Exception as e:
            self.logger.error("Exception occurred while inputting vendor fields %s", e)
            raise Exception("Exception occurred while inputting vendor fields %s", e)

    # Get output data to save in DB
    """ This function saves agreement id,provisioning contact name,email,contact,ship start date,
         web order id, vendor portal submission in IM360 Subscription parameter table """

    def save_param_data_in_db(self, test_case_id, vendor_portal_sub, im360_parameter_data_list=[]):
        subs_param_agrm = IM360SubscriptionParameters()
        agreement_id = self.do_get_attribute(self.AGREEMENT_ID_VALUE, "value")
        self.logger.info("Deal/Agreement ID value after saving: %s " % agreement_id)
        subs_param_agrm.im360_parameter_name = 'Agreement/Deal ID'
        subs_param_agrm.parameter_value = agreement_id
        subs_param_agrm.test_case_id = test_case_id
        subs_param_agrm.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_agrm)

        subs_param_prov_contact_name = IM360SubscriptionParameters()
        prov_contact_name = self.do_get_attribute(self.PROVISIONING_CONTACT_NAME_TEXTBOX, "value")
        self.logger.info("Provisioning contact name value after saving:: %s " % prov_contact_name)
        subs_param_prov_contact_name.im360_parameter_name = 'Provisioning Contact Name'
        subs_param_prov_contact_name.parameter_value = prov_contact_name
        subs_param_prov_contact_name.test_case_id = test_case_id
        subs_param_prov_contact_name.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_prov_contact_name)

        subs_param_prov_contact_email = IM360SubscriptionParameters()
        prov_contact_email = self.do_get_attribute(self.PROVISIONING_CONTACT_EMAIL_TEXTBOX, "value")
        self.logger.info("Provisioning contact email value after saving:: %s " % prov_contact_email)
        subs_param_prov_contact_email.im360_parameter_name = 'Provisioning Contact Email'
        subs_param_prov_contact_email.parameter_value = prov_contact_email
        subs_param_prov_contact_email.test_case_id = test_case_id
        subs_param_prov_contact_email.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_prov_contact_email)

        subs_param_prov_contact_phone = IM360SubscriptionParameters()
        prov_contact_phone = self.do_get_attribute(self.PROVISIONING_CONTACT_PHONE_TEXTBOX, "value")
        self.logger.info("Provisioning contact phone value after saving:: %s " % prov_contact_phone)
        subs_param_prov_contact_phone.im360_parameter_name = 'Provisioning Contact Phone'
        subs_param_prov_contact_phone.parameter_value = prov_contact_phone
        subs_param_prov_contact_phone.test_case_id = test_case_id
        subs_param_prov_contact_phone.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_prov_contact_phone)

        subs_param_ship_start_date = IM360SubscriptionParameters()
        ship_start_date = self.do_get_attribute(self.REQUESTED_SHIP_START_DATE_SELECTOR, "value")
        self.logger.info("Requested Ship Start date value after saving:: %s " % ship_start_date)
        subs_param_ship_start_date.im360_parameter_name = 'Requested Ship / Start Date'
        subs_param_ship_start_date.parameter_value = ship_start_date
        subs_param_ship_start_date.test_case_id = test_case_id
        subs_param_ship_start_date.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_ship_start_date)

        subs_param_web_ord_id = IM360SubscriptionParameters()
        web_order_id = self.do_get_attribute(self.WEB_ORDER_ID_TEXTBOX, "value")
        self.logger.info("Web OrderID value after saving:: %s " % web_order_id)
        subs_param_web_ord_id.im360_parameter_name = 'Web Order ID'
        # Work Around: As of now removing the data of web_order_id from DB
        # Need to have a correct implementation latter
        subs_param_web_ord_id.parameter_value = None
        subs_param_web_ord_id.test_case_id = test_case_id
        subs_param_web_ord_id.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_web_ord_id)

        subs_param_vend_portal_sub = IM360SubscriptionParameters()
        subs_param_vend_portal_sub.im360_parameter_name = 'Vendor Portal Submission'
        subs_param_vend_portal_sub.parameter_value = vendor_portal_sub
        subs_param_vend_portal_sub.test_case_id = test_case_id
        subs_param_vend_portal_sub.im360_subscription_id = None
        im360_parameter_data_list.append(subs_param_vend_portal_sub)

        return im360_parameter_data_list

    """ This function adds vendor subscription id to IM360_Subscription_Parameters table"""

    def add_vendor_subs_id_in_param(self, test_case_id, subs_id, im360_subs_tbl_id, im360_parameter_data_list=[]):
        subs_param = IM360SubscriptionParameters()
        subs_param.im360_parameter_name = 'Vendor Subscription ID'
        subs_param.parameter_value = "test_" + str(subs_id) + "_" + str(test_case_id)
        subs_param.test_case_id = test_case_id
        subs_param.im360_subscription_tbl_id = im360_subs_tbl_id
        im360_parameter_data_list.append(subs_param)

    """ This function checks the contact phone value if it is nan then fills necessary fields then searches and uses 
    existing end user or fills fields in End User tab and creates a new end user """

    def do_add_end_user(self, company_name, contact_phone, contact_name, contact_email, address, city, postal_code,
                        state):
        self.do_click_by_locator(self.END_USER_TAB)
        self.logger.info("Clicked on EndUser tab")
        try:
            if str(contact_phone) == "None":
                self.logger.info("Start searching the end user")
                address_text = self.get_all_elements(self.ADDRESS_TEXTBOX)
                self.do_click_by_element(address_text[0])
                state_text = self.get_all_elements(self.STATE_TEXTBOX)
                self.do_click_by_element(state_text[0])
                contact_email_text = self.get_all_elements(self.CONTACT_EMAIL_TEXTBOX)
                self.do_click_by_element(contact_email_text[0])
                cb_end_user = self.get_all_elements(self.CB_END_USER_ID_TEXTBOX)
                self.do_click_by_element(cb_end_user[0])
                self.do_sleep("min")
                self.do_switch_to_required_frame(self.END_USER_BUTTONS_FRAME_ID)
                self.do_click_by_locator(self.OPEN_SEARCH_BUTTON)
                self.logger.info("Clicked on Open search Button")
                self.do_sleep("min")
                self.do_switch_to_parent_frame()
                self.do_send_keys(self.COMPANY_NAME_TEXTBOX, company_name)
                self.logger.info("Company Name = %s entered" % company_name)
                self.do_sleep("min")
                self.do_switch_to_required_frame(self.END_USER_BUTTONS_FRAME_ID)
                self.do_click_by_locator(self.END_USER_SEARCH_BUTTON)
                self.logger.info("search end user button is clicked")
                self.do_sleep("average")
                self.do_switch_to_parent_frame()
                address_text = self.get_all_elements(self.ADDRESS_TEXTBOX)
                self.do_click_by_element(address_text[0])
                state_text = self.get_all_elements(self.STATE_TEXTBOX)
                self.do_click_by_element(state_text[0])
                contact_email_text = self.get_all_elements(self.CONTACT_EMAIL_TEXTBOX)
                self.do_click_by_element(contact_email_text[0])
                cb_end_user = self.get_all_elements(self.CB_END_USER_ID_TEXTBOX)
                self.do_click_by_element(cb_end_user[0])
                self.do_sleep("min")
                self.do_switch_to_required_frame(self.END_USER_SELECTION_FRAME)
                acopid_label = self.get_all_elements(self.ACOPID_LABEL)
                self.do_click_by_element(acopid_label[0])
                self.do_click_by_locator(self.END_USER_SELECT_BUTTON)
                self.logger.info("End User select button is clicked")
                self.do_sleep("above_min")
                self.do_switch_to_parent_frame()
                self.do_switch_to_required_frame(self.END_USER_BUTTONS_FRAME_ID)
                self.do_click_by_locator(self.GET_CB_END_USER_ID_BUTTON)
                self.logger.info("Get CB End User ID button is clicked after searching the User")
                self.do_sleep("average")
                self.do_switch_to_parent_frame()
                cb_end_user_id = self.do_get_attribute(self.CB_END_USER_ID, "value")
                assert cb_end_user_id != '---'
                self.logger.info("CB End User ID is %s ", cb_end_user_id)
            else:
                self.logger.info("Start creating the end user")
                address_text = self.get_all_elements(self.ADDRESS_TEXTBOX)
                self.do_click_by_element(address_text[0])
                state_text = self.get_all_elements(self.STATE_TEXTBOX)
                self.do_click_by_element(state_text[0])
                contact_email_text = self.get_all_elements(self.CONTACT_EMAIL_TEXTBOX)
                self.do_click_by_element(contact_email_text[0])
                cb_end_user = self.get_all_elements(self.CB_END_USER_ID_TEXTBOX)
                self.do_click_by_element(cb_end_user[0])
                self.do_sleep("min")
                self.do_switch_to_required_frame(self.END_USER_BUTTONS_FRAME_ID)
                self.do_click_by_locator(self.OPEN_SEARCH_BUTTON)
                self.do_sleep("min")
                self.do_click_by_locator(self.CREATE_BUTTON)
                self.do_sleep("min")
                self.do_switch_to_parent_frame()
                try:
                    self.do_clear_textfield(self.COMPANY_NAME_TEXTBOX)
                    self.do_send_keys(self.COMPANY_NAME_TEXTBOX, company_name)
                    company_value = self.do_get_keys(self.COMPANY_NAME_TEXTBOX)
                    assert company_value == company_name, 'company name value not matching'
                except Exception as e:
                    raise e
                self.logger.info("company name = %s entered" % company_name)
                self.do_sleep("min")
                self.do_send_keys(self.CONTACT_PHONE_TEXTBOX, math.trunc(contact_phone) if str(contact_phone) != "nan"

                else contact_phone)
                self.logger.info(
                    "contact phone = %s entered" % math.trunc(contact_phone) if str(contact_phone) != "None"
                    else contact_phone)

                self.do_sleep("min")
                try:
                    self.do_clear_textfield(self.CONTACT_NAME_TEXTBOX)
                    self.do_send_keys(self.CONTACT_NAME_TEXTBOX, contact_name)
                    contact = self.do_get_keys(self.CONTACT_NAME_TEXTBOX)
                    assert contact == contact_name, 'contact name not matching'
                except Exception as e:
                    raise e
                self.logger.info("contact name = %s entered" % contact_name)
                self.do_sleep("min")
                try:
                    self.do_clear_textfield(self.CONTACT_EMAIL_TEXTBOX)
                    self.do_send_keys(self.CONTACT_EMAIL_TEXTBOX, contact_email)
                    contact_mail = self.do_get_keys(self.CONTACT_EMAIL_TEXTBOX)
                    assert contact_mail == contact_email, 'contact mail not matching'
                except Exception as e:
                    raise e
                self.logger.info("contact email = %s entered" % contact_email)
                self.do_sleep("min")
                try:
                    self.do_clear_textfield(self.ADDRESS_TEXTBOX)
                    self.do_send_keys(self.ADDRESS_TEXTBOX, address)
                    address_text = self.do_get_keys(self.ADDRESS_TEXTBOX)
                    assert address_text == address, 'address is not matching'
                except Exception as e:
                    raise e
                self.logger.info("address = %s entered" % address)
                self.do_sleep("min")
                try:
                    self.do_clear_textfield(self.CITY_TEXTBOX)
                    self.do_send_keys(self.CITY_TEXTBOX, city)
                    city_text = self.do_get_keys(self.CITY_TEXTBOX)
                    assert city_text == city, 'city is not matching'
                except Exception as e:
                    raise e
                self.logger.info("city = %s entered" % city)
                # self.do_send_keys(self.POSTAL_CODE_TEXTBOX, math.trunc(postal_code) if postal_code != "nan" else
                #                   postal_code)
                # self.do_send_keys(self.POSTAL_CODE_TEXTBOX, postal_code)
                # self.do_send_keys(self.POSTAL_CODE_TEXTBOX, math.trunc(postal_code)
                #                   if postal_code.replace('.', '', 1).isdigit() else postal_code)
                self.do_send_keys(self.POSTAL_CODE_TEXTBOX, math.trunc(postal_code)
                if isinstance(postal_code, float) else postal_code)
                self.logger.info("postal code = %s entered" % math.trunc(postal_code)
                                 if isinstance(postal_code, float) else postal_code)
                self.do_sleep("min")
                try:
                    self.do_clear_textfield(self.STATE_TEXTBOX)
                    self.do_send_keys(self.STATE_TEXTBOX, state)
                    state_text = self.do_get_keys(self.STATE_TEXTBOX)
                    assert state_text == state, 'state is not matching'
                except Exception as e:
                    raise e
                self.logger.info("state = %s entered" % state)
                self.do_sleep("min")
                # self.do_click_by_locator(self.IGNORE_ADDRESS_VALIDATION_CHECKBOX)
                # self.logger.info("ignore address validation check box is checked")
                self.do_sleep("min")
                self.do_switch_to_required_frame(self.END_USER_BUTTONS_FRAME_ID)
                self.do_click_by_locator(self.SAVE_IN_ERP_BUTTON)
                self.logger.info("save in ERP is clicked")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.GET_CB_END_USER_ID_BUTTON)
                self.logger.info("Get CB EndUserID is clicked after creating the User")
                self.do_sleep("above_average")
                self.do_switch_to_parent_frame()
                self.do_sleep("min")
                cb_end_user_id = self.do_get_attribute(self.CB_END_USER_ID, "value")
                assert cb_end_user_id != '---'
                self.logger.info("CB End User ID is %s ", cb_end_user_id)
        except Exception as e:
            self.do_switch_to_parent_frame()
            self.logger.error("Not able to add/search end user %s", e)
            raise e

    """This method switches to product grid checks for payment type and submits the order accordingly """

    def do_submit_order(self, im360_subs_output_data, im360_item_data_list=[], test_data=None):
        contract_term = None
        billing_frequency = None
        self.do_sleep("average")
        try:
            self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
            self.logger.info("Clicked on products tab")
            self.do_sleep("average")
            self.do_switch_to_required_frame(self.PRODUCT_GRID_FRAME)
            product_grid_skus = self.get_all_elements(self.PRODUCT_GRID_VENDOR_SKU)
            for row_index in range(len(product_grid_skus)):
                im360_item = IM360Item()
                im360_item.cost = self.get_product_cost(row_index)
                im360_item.cost_after_credit = self.get_product_cost_after_credit(row_index)
                im360_item.price = self.get_product_screen_price(row_index)
                im360_item.price_after_credit = self.get_product_price_after_credit(row_index)
                im360_item.vendor_total_contract_value = self.get_product_tcc(row_index)
                im360_item.reseller_total_contract_value = self.get_product_tcv(row_index)
                im360_item.sku = self.get_sku(row_index)
                im360_item.item_quantity = self.get_qty(row_index)
                im360_item.vendor_sku = self.get_vendor_sku(row_index)
                im360_item.applicable_at_renewal = self.get_applicable_at_renewal_flag(row_index)
                im360_item.test_case_id = im360_subs_output_data.test_case_id
                im360_item_data_list.append(im360_item)
                contract_term = self.get_contract_term(row_index)
                billing_frequency = self.get_billing_frequency(row_index)

            contract_term_split = str(contract_term).split(" ")
            im360_subs_output_data.subscription_period = contract_term_split[0]
            im360_subs_output_data.subscription_period_unit = contract_term_split[1]
            billing_frequency_split = str(billing_frequency).split(" ")
            im360_subs_output_data.billing_period = billing_frequency_split[0]
            im360_subs_output_data.billing_period_unit = billing_frequency_split[1]

            self.do_sleep("average")
            self.do_click_by_locator(self.PREVIEW_ORDER_BUTTON)
            self.logger.info("Preview order button clicked")
            self.logger.info("Get the payment type from config file")
            payment_term = int(ParseConfigFile().get_data_from_config_json("paymentTerm", "term_id_cc"))
            if test_data.get('payment_term') == payment_term:
                self.driver.switch_to.default_content()
                self.do_switch_to_required_frame(self.PAYMENT_METHOD_FRAME)
                self.do_check_visibility(self.SELECT_CREDIT_CARD_LABEL)
                self.do_add_credit_card_details(test_data.get("saved_credit_card_name"),
                                                test_data.get("cc_first_name"),
                                                test_data.get("cc_last_name"),
                                                test_data.get("cc_email"),
                                                test_data.get("cc_card_type"),
                                                test_data.get("cc_card_number"),
                                                test_data.get("cc_expiration_month"),
                                                test_data.get("cc_expitation_year"),
                                                test_data.get("cc_cvn"))

                self.do_sleep("min")
                self.do_switch_to_parent_frame()
                self.do_sleep("min")
                self.do_switch_to_required_frame(self.SUBMIT_ORDER_FRAME)
                self.do_click_by_locator(self.SUBMIT_ORDER_BUTTON)
                self.logger.info("Submit order button clicked")
                self.do_switch_to_parent_frame()
                self.do_switch_to_required_frame(self.ORDER_DETAILS_FRAME)
                # self.do_check_availability(self.ORDER_SUBMISSION_MESSAGE)
                submit_order_message = "The Order was Submitted, but the ERP System did not provide enough " \
                                       "information to display Order Details."
                self.do_sleep("min")
                if self.do_check_visibility(self.ORDER_SUBMISSION_MESSAGE):
                    submit_order_message_after_submit = self.get_element_text(self.ORDER_SUBMISSION_MESSAGE)
                    if submit_order_message == submit_order_message_after_submit:
                        self.do_switch_to_parent_frame()
                        return True
                else:
                    self.do_sleep("min")
                    if self.do_check_visibility(self.INGRAM_SUBSCRIPTION_ID_LABEL):
                        self.do_switch_to_parent_frame()
                        return True
                    else:
                        return False
            else:
                self.logger.info("Checking the Submit order button visibility")
                self.do_switch_to_parent_frame()
                self.do_sleep("min")

                self.do_switch_to_required_frame(self.SUBMIT_ORDER_FRAME)
                self.do_click_by_locator(self.SUBMIT_ORDER_BUTTON)
                self.logger.info("Submit order button clicked")
                self.do_switch_to_parent_frame()
                self.do_switch_to_required_frame(self.ORDER_DETAILS_FRAME)

                self.do_sleep("above_average")
                submit_order_message = "The Order was Submitted, but the ERP System did not provide enough " \
                                       "information to display Order Details."
                self.do_sleep("min")
                if self.do_check_visibility(self.ORDER_SUBMISSION_MESSAGE):
                    submit_order_message_after_submit = self.get_element_text(self.ORDER_SUBMISSION_MESSAGE)
                    if submit_order_message == submit_order_message_after_submit:
                        self.do_switch_to_parent_frame()
                        return True
                else:
                    self.do_sleep("min")
                    if self.do_check_visibility(self.INGRAM_SUBSCRIPTION_ID_LABEL):
                        self.do_switch_to_parent_frame()
                        return True
                    else:
                        return False
        except Exception as e:
            raise Exception("Exception occurred while submitting the orders %s", e)

    """ This function is used to check if the order submitted successfully """

    def is_order_submitted_successfully(self):
        # self.do_sleep(180)
        # self.driver.refresh()
        self.do_sleep("average")
        if "Credit Hold" in self.get_element_text(self.CREDIT_HOLD_LABEL):
            # im360_subs_output_data.order_number = self.get_erp_order_number()
            return True
        else:
            return False

    """ This function is used to release subscription from hold """

    def release_subscription_from_hold(self, quote_name, screen_shot, test_case_id, marketplace_name):
        # search order from order tab to release hold, search box and button from base page
        self.do_click_by_locator(self.ORDERS_BUTTON)
        self.logger.info("Clicked on Orders button on left pane")
        self.do_sleep("above_min")
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching order with name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.select_order(quote_name, screen_shot, marketplace_name, test_case_id)
        self.do_sleep("above_min")
        self.do_click_by_locator(self.RELEASE_SUBSCRIPTION_BUTTON)
        self.logger.info("Clicked on Release credit Hold Button to release subscription")
        self.do_sleep("above_min")
        self.do_click_by_locator(self.RELEASE_HOLD_APPROVE_CONFIRMATION_POP_UP)
        self.logger.info("Clicked on confirm button of popup")
        self.do_sleep("above_average")
        self.do_click_by_locator(self.CLICK_OK_TO_RELEASE)
        self.logger.info("Clicked on OK button of popup")
        self.do_sleep("above_min")
        self.driver.save_screenshot(
            self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name + "_" +
            "Release subscription from hold success.png")

    """ This function is used to check if subscription is created by checking erp order number is generated or not """

    def is_subscription_created(self):
        self.do_click_by_locator(self.SUMMARY_TAB_HEADER)
        self.logger.info("Clicked on summary tab")
        status = ""
        for poll in range(3):
            if "---" in self.do_get_attribute(self.ERP_ORDER_NUMBER_TEXTBOX, "value"):
                self.do_sleep("average")
                self.logger.info("Checking for ERP order number...")
                self.do_click_by_locator(self.CLICK_REFRESH)
                self.do_sleep("above_min")
                status = False
            else:
                self.do_get_attribute(self.ERP_ORDER_NUMBER_TEXTBOX, "value")
                self.logger.info(
                    "ERP order number received. No - %s" % self.get_element_title(self.ERP_ORDER_NUMBER_TEXTBOX))
                status = True
                break
        return status

    """ This method is used to checks and displays the reason for subscription error """

    def get_error_on_subscription(self):
        error_msg = self.get_element_title(self.ERP_STATUS_REASON_TEXTBOX)
        self.logger.info("ERROR received on subscription creation")
        if error_msg == "SUCCESS":
            self.logger.error("Didn't received response from Tibco in Async call")
        else:
            self.logger.error(f"ERP Status reason: {error_msg}")
        raise Exception(error_msg)

    """ This method gets the subscription id from order review grid """

    def get_subscription_id(self):
        self.do_click_by_locator(self.ORDER_REVIEW_TAB_HEADER)
        self.logger.info("clicked on order review tab")
        self.do_switch_to_required_frame(self.ORDER_DETAILS_FRAME)
        subscription_id = ""
        for retry in range(3):
            try:
                subscription_id = self.get_element_text(self.SUBSCRIPTION_ID_DISPLAY)
                self.logger.info("SUBSCRIPTION ID: %s" % subscription_id)
                break
            except Exception as e:
                if retry == 2:
                    self.logger.error("Error in order review page")
                    self.logger.error(e)
                    raise e
                else:
                    self.do_sleep("average")
        self.do_switch_to_parent_frame()
        return subscription_id

    """ This method is used to get the vendor subscription id """

    def get_vendor_subs_id(self):
        self.do_click_by_locator(self.VENDOR_FIELD_BUTTON)
        for retry in range(3):
            if self.do_get_attribute(self.VENDOR_SUBSCRIPTION_ID_TEXTBOX, "value") == "---":
                self.do_sleep("above_min")
            else:
                break
        return self.do_get_attribute(self.VENDOR_SUBSCRIPTION_ID_TEXTBOX, "value")

    # def get_order_details_value(self):
    #     erp_order_no = self.do_get_attribute(self.ERP_ORDER_NUMBER_TEXTBOX, "value")
    #     reseller_po = self.do_get_attribute(self.RESELLER_PO_TEXTBOX, "value")
    #     self.do_click_by_locator(self.VENDOR_FIELD_BUTTON)
    #     deal_id = self.do_get_attribute(self.DEAL_AGREEMENT_ID_TEXTBOX, "value")
    #     provisioning_contact_name = self.do_get_attribute(self.PROVISIONING_CONTACT_NAME_TEXTBOX, "value")
    #     provisioning_contact_email = self.do_get_attribute(self.PROVISIONING_CONTACT_EMAIL_TEXTBOX, "value")
    #     provisioning_contact_phone = self.do_get_attribute(self.PROVISIONING_CONTACT_PHONE_TEXTBOX, "value")

    """ This method is used to get the deal id"""

    def get_deal_id(self):
        deal_id = self.get_element_text(self.DEAL_AGREEMENT_ID_TEXTBOX)
        self.logger.info("Getting value of Deal_id as: %s" % deal_id)
        return deal_id

    """ This method is used to get the erp order number """

    def get_erp_order_number(self, screen_shot, test_case_id, marketplace_name):
        try:
            erp_order_no = self.do_get_attribute(self.ERP_ORDER_NUMBER_TEXTBOX, "value")
            self.logger.info("Getting value of ERP_Order_Number as: %s" % erp_order_no)
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" +
                                        marketplace_name + "_" + "get_erp_order_successful.png")
            return erp_order_no
        except Exception as e:
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" +
                                        marketplace_name + "_" + "get_erp_order_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "get_erp_order_error.png"
            raise e

    """ This method is used to get the reseller po """

    def get_reseller_po(self):
        reseller_po = self.do_get_attribute(self.RESELLER_PO_TEXTBOX, "value")
        self.logger.info("Getting value of Reseller_PO as: %s" % reseller_po)
        return reseller_po

    """ This method is used to get provisioning contact name """

    def get_provisioning_contact_name(self):
        provisioning_contact_name = self.get_element_text(self.PROVISIONING_CONTACT_NAME_TEXTBOX)
        self.logger.info("Getting value of Provisioning_Contact_Name as: %s" % provisioning_contact_name)
        return provisioning_contact_name

    """" This method is used to get provisioning contact email """

    def get_provisioning_contact_email(self):
        provisioning_contact_email = self.get_element_text(self.PROVISIONING_CONTACT_EMAIL_TEXTBOX)
        self.logger.info("Getting value of Provisioning_Contact_Email as: %s" % provisioning_contact_email)
        return provisioning_contact_email

    """ This method is used to get provisioning contact phone"""

    def get_provisioning_contact_phone(self):
        provisioning_contact_phone = self.get_element_text(self.PROVISIONING_CONTACT_PHONE_TEXTBOX)
        self.logger.info("Getting value of Provisioning_Contact_Phone as: %s" % provisioning_contact_phone)
        return provisioning_contact_phone

    """ This function is used to get the total cost of products added """

    def get_product_cost(self, row_index):
        cost = None
        try:
            PRODUCT_GRID_COST = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='costValues']")
            cost = self.get_element_text(PRODUCT_GRID_COST)
            self.logger.info("Getting value of Cost as: %s" % cost)
        except Exception as e:
            self.logger.error("Not able to get cost " + e)
            raise Exception

        return cost

    """ This function is used to get the cost after the credit """

    def get_product_cost_after_credit(self, row_index):
        cost_after_credit = None
        try:
            PRODUCT_GRID_COST_AFTER_CREDIT = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='specialCost']")
            cost_after_credit = self.get_element_text(PRODUCT_GRID_COST_AFTER_CREDIT)
            self.logger.info("Getting value of Cost after credit as: %s" % cost_after_credit)
        except Exception as e:
            self.logger.error("Not able to get cost after credit " + e)
            raise Exception
        return cost_after_credit

    """ This method is used to get the product screen price """

    def get_product_screen_price(self, row_index):
        price = None
        try:
            PRODUCT_GRID_PRICE = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='priceDisplay']")
            price = self.get_element_text(PRODUCT_GRID_PRICE)
            self.logger.info("Getting value of Price as: %s" % price)
        except Exception as e:
            self.logger.error("Not able to get price " + e)
            raise Exception
        return price

    """ This method is used to get product quote price """

    def get_product_quote_price(self):
        quote_price = self.get_element_text(self.PRODUCT_GRID_QUOTE_PRICE_TEXTBOX)
        self.logger.info("Getting value of Price as: %s" % quote_price)
        return quote_price

    """ This method gives the product price after credit """

    def get_product_price_after_credit(self, row_index):
        price_after_credit = None
        try:
            PRODUCT_GRID_PRICE_AFTER_CREDIT = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='specialPrice']")
            price_after_credit = self.get_element_text(PRODUCT_GRID_PRICE_AFTER_CREDIT)
            self.logger.info("Getting value of Price after credit as: %s" % price_after_credit)
        except Exception as e:
            self.logger.error("Not able to get price after credit " + e)
            raise Exception
        return price_after_credit

    """ This method returns value of product MSRP """

    def get_product_msrp(self):
        msrp = self.get_element_text(self.PRODUCT_GRID_MSRP_TEXTBOX)
        self.logger.info("Getting value of MSRP as: %s" % msrp)
        return msrp

    """ This method used to get total contract value """

    def get_product_tcv(self, row_index):
        tcv = None
        try:
            PRODUCT_GRID_TCV = (
                By.XPATH,
                "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='totalContractValue']")
            tcv = self.get_element_text(PRODUCT_GRID_TCV)
            self.logger.info("Getting value of TCV as: %s" % tcv)
        except Exception as e:
            self.logger.error("Not able to get TCV " + e)
            raise Exception
        return tcv

    """ This method is used to get total contract cost """

    def get_product_tcc(self, row_index):
        tcc = None
        try:
            PRODUCT_GRID_TCC = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='totalContractCost']")
            tcc = self.get_element_text(PRODUCT_GRID_TCC)
            self.logger.info("Getting value of TCC as: %s" % tcc)
        except Exception as e:
            self.logger.error("Not able to get TCC " + e)
            raise Exception
        return tcc

    """ This method is used to get billing frequency selected for the order """

    def get_billing_frequency(self, row_index):
        bill_frequency = None
        try:
            PRODUCT_GRID_BILLING_FREQUENCY = (
                By.XPATH,
                "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='billingFrequencyDisplay']")
            bill_frequency = self.get_element_text(PRODUCT_GRID_BILLING_FREQUENCY)
            self.logger.info("Getting value of Billing Frequency as: %s" % bill_frequency)
        except Exception as e:
            self.logger.error("Not able to get Billing Frequency " + e)
            raise Exception
        return bill_frequency

    """ This function is used to get contract term selected for the order """

    def get_contract_term(self, row_index):
        contract_term = None
        try:
            PRODUCT_GRID_CONTRACT_TERM = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='contractTerm']")
            contract_term = self.get_element_text(PRODUCT_GRID_CONTRACT_TERM)
            self.logger.info("Getting value of Contract Term as: %s" % contract_term)
        except Exception as e:
            self.logger.error("Not able to get Contract Term " + e)
            raise Exception
        return contract_term

    """ This function is used to get vendor sku for the order """

    def get_vendor_sku(self, row_index):
        vendor_sku = None
        try:
            PRODUCT_GRID_VENDOR_SKU = (
                By.XPATH, "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='product.vendorSku']")
            vendor_sku = self.get_element_text(PRODUCT_GRID_VENDOR_SKU)
            self.logger.info("Getting value of Vendor SKU as: %s" % vendor_sku)
        except Exception as e:
            self.logger.error("Not able to get Vendor SKU " + e)
            raise Exception
        return vendor_sku

    """ This method is used to get the value of if applicable at renewal """

    def get_applicable_at_renewal_flag(self, row_index):
        applicable_at_renewal_flag = None
        try:
            PRODUCT_GRID_APPLICABLE_AT_RENEWAL = (
                By.XPATH,
                "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='applicableAtRenewalDisplay']")
            applicable_at_renewal_flag = self.get_element_text(PRODUCT_GRID_APPLICABLE_AT_RENEWAL)
            self.logger.info("Getting value of Applicable at Renewal as: %s" % applicable_at_renewal_flag)
        except Exception as e:
            self.logger.error("Not able to get Applicable at Renewal " + e)
            raise Exception
        return applicable_at_renewal_flag

    """ This method is used to get the added product name by row index"""

    def get_sku(self, row_index):
        sku = None
        try:
            PRODUCT_GRID_SKU = (
                By.XPATH,
                "//div[@role='row' and @row-index='" + str(
                    row_index) + "']/div[@col-id='skuDisplay']/div[@class='ag-react-container']")
            sku = self.get_element_text(PRODUCT_GRID_SKU)
            self.logger.info("Getting value of SKU as: %s" % sku)
        except Exception as e:
            self.logger.error("Not able to get SKU " + e)
            raise Exception
        return sku

    """ This function is used to get the quantity of the product added """

    def get_qty(self, row_index):
        qty = None
        try:
            PRODUCT_GRID_QTY = (
                By.XPATH,
                "//div[@role='row' and @row-index='" + str(
                    row_index) + "']/div[@col-id='quantity']/div[@class='ag-react-container']")
            qty = self.get_element_text(PRODUCT_GRID_QTY)
            self.logger.info("Getting value of Quantity as: %s" % qty)
        except Exception as e:
            self.logger.error("Not able to get Quantity " + e)
            raise Exception
        return qty

    """ This method searchs the order and click on edit """

    def select_order(self, quote_name, screen_shot, test_case_id, marketplace_name):
        self.do_sleep("above_min")
        try:
            self.do_click_by_locator((By.LINK_TEXT, quote_name))
            self.logger.info("searched order is selected")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name + "_"
                + "select_order_successful.png")
        except Exception as e:
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name + "_" +
                "select_order_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "select_order_error.png"
            self.logger.info("Search order not found." + e)

    """ This method is used to get subscription status of the order """

    def do_get_subscription_status(self, test_case_id, marketplace_name):
        try:
            status = self.do_get_attribute(self.CHECK_STATUS_OF_SUBSCRIPTION, "title")
            return status
        except Exception as e:
            self.logger.info("Subscription status not found." + e)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name + "_"
                + "subscription status failure.png")

    """ This method checks if credit card is present in payment dropdown, if present then selects it else it adds a new 
    credit card details for payment """

    def do_add_credit_card_details(self, saved_credit_card_name, cc_first_name, cc_last_name, cc_email, cc_card_type,
                                   cc_card_number, cc_expiration_month, cc_expitation_year, cc_cvn):

        self.do_sleep("above_min")
        self.do_click_by_locator(self.SELECT_CREDIT_CARD_DROPDOWN)
        self.logger.info("Clicked on Select Credit Card dropdown")
        try:
            if not cc_first_name:
                if self.credit_card_visible(saved_credit_card_name):
                    self.do_click_by_locator(self.CONTINUE_BUTTON)
            else:
                self.logger.info("Start entering new credit card details")
                self.do_click_by_locator(self.ENTER_A_NEW_CREDIT_CARD)
                self.do_click_by_locator(self.CONTINUE_BUTTON)
                self.do_check_availability(self.BILLING_INFORMATION)
                self.do_send_keys(self.FIRST_NAME_TEXTBOX, cc_first_name)
                self.do_send_keys(self.LAST_NAME_TEXTBOX, cc_last_name)
                self.do_mouse_hover_to_element(self.EMAIL_TEXTBOX)
                self.do_send_keys(self.EMAIL_TEXTBOX, cc_email)
                self.do_sleep("above_min")
                self.do_mouse_hover_to_element(self.PAYMENT_DETAILS)
                self.do_check_availability(self.CARD_TYPE)
                credit_type_list = self.get_all_elements(self.CARD_TYPE_LIST)
                for card_type in credit_type_list:
                    if cc_card_type in card_type.text:
                        cc_card_type.click()
                        self.logger.info("Selected the card type:" + card_type.text)
                self.do_send_keys(self.CARD_NUMBER_TEXTBOX, cc_card_number)
                self.do_sleep("min")
                select = Select(self.CARD_EXPIRY_MONTH)
                select.select_by_visible_text(cc_expiration_month)
                select = Select(self.CARD_EXPIRY_YEAR)
                self.do_sleep("min")
                select.select_by_visible_text(cc_expitation_year)
                self.do_mouse_hover_to_element(self.CVN_NUMBER_TEXTBOX)
                self.do_sleep("min")
                self.do_send_keys(self.CVN_NUMBER_TEXTBOX, cc_cvn)
                self.do_sleep("min")
                self.do_click_by_locator(self.FINISH_BUTTON)
        except Exception as e:
            self.logger.error("Not able to add/search credit card %s", e)
            raise e

    """ This method is used to check whether the specified credit card name is present in dropdown """

    def credit_card_visible(self, saved_credit_card_name):
        try:
            elements = self.get_all_elements(self.CREDIT_CARD_LIST)
            for e in elements:
                if saved_credit_card_name in e.text:
                    self.logger.info("Card Name:" + e.text)
                    e.click()
                    return True
        except Exception as e:
            self.logger.error("Not able to get Save credit card name " + e)
            raise Exception

    def release_modified_subscription_from_hold(self):
        self.do_click_by_locator(self.RELEASE_SUBSCRIPTION_BUTTON)
        self.logger.info("Clicked on Release credit Hold Button to release subscription")
        self.do_sleep("above_min")
        self.do_click_by_locator(self.RELEASE_HOLD_APPROVE_CONFIRMATION_POP_UP)
        self.logger.info("Clicked on confirm button of popup")
        self.do_sleep("above_average")
        self.do_click_by_locator(self.CLICK_OK_TO_RELEASE)
        self.logger.info("Clicked on OK button of popup")
        self.do_sleep("above_min")

    """This is used to search an order and get into it. Returns True when successful."""

    def do_search_and_get_credit_hold_order(self, quote_name):
        self.do_switch_to_parent_frame()
        self.do_sleep("above_min")
        self.do_click_by_locator(self.ORDER_BUTTON_LEFT_PANE)
        self.logger.info("Clicked on Order button in left pane")
        self.do_sleep("above_min")
        self.do_click_by_locator(self.MY_ORDERS_VIEW)
        self.do_click_by_locator(self.ALL_ORDERS_VIEW)
        self.do_send_keys(self.SEARCH_BOX, quote_name)
        self.logger.info("Searching Order with quote name: %s" % quote_name)
        self.do_sleep("min")
        self.do_click_by_locator(self.SEARCH_BUTTON)
        self.do_sleep("above_min")
        self.do_click_by_locator(self.CREDIT_HOLD_TITLE)
        self.logger.info("Order with quote name - %s found. Getting into it!!" % quote_name)
        self.do_click_by_locator(self.EDIT_SELECTED_ORDER_BUTTON)
        if (self.get_element_title(self.ORDER_HEADER)).strip() == quote_name:
            self.logger.info("got into the order successfully")
            return True
        else:
            self.logger.error("Some issue happened. Not able to get into the order")
            return False

    def do_input_vendor_change_deal_id_fields(self, change_deal_id):
        try:
            self.do_sleep("above_min")
            self.do_click_by_locator(self.VENDOR_FIELD_BUTTON)
            self.logger.info("Successfully clicked on vendor fields tab")
            self.do_clear_textfield(self.DEAL_AGREEMENT_ID_TEXTBOX)
            self.do_send_keys(self.DEAL_AGREEMENT_ID_TEXTBOX, change_deal_id)
            self.logger.info("deal id = %s entered" % change_deal_id)
            self.do_sleep("min")
            self.do_click_by_locator(self.PAGE_SAVE_BUTTON)
            self.logger.info("Clicked on Save Button in Vendor parameter tab")
            self.do_sleep("above_min")
        except Exception as e:
            self.logger.error("Exception occurred while inputting vendor change deal id fields %s", e)
            raise Exception("Exception occurred while inputting vendor change deal id fields %s", e)

    """ This method is used to get billing frequency selected for the order """

    def get_updated_billing_frequency(self, row_index):
        bill_frequency = None
        billing_period_unit = None
        try:
            self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
            self.logger.info("Clicked on products tab")
            self.do_sleep("average")
            self.do_switch_to_required_frame(self.PRODUCT_GRID_FRAME)
            product_grid_skus = self.get_all_elements(self.PRODUCT_GRID_VENDOR_SKU)
            PRODUCT_GRID_BILLING_FREQUENCY = (
                By.XPATH,
                "//div[@role='row' and @row-index='" + str(row_index) + "']/div[@col-id='billingFrequencyDisplay']")
            bill_frequency = self.get_element_text(PRODUCT_GRID_BILLING_FREQUENCY)
            self.logger.info("Getting value of Billing Frequency as: %s" % bill_frequency)
            billing_frequency_split = str(bill_frequency).split(" ")
            billing_period_unit = billing_frequency_split[1]
            self.logger.info(billing_period_unit)
            self.do_switch_to_parent_frame()
        except Exception as e:
            self.logger.error("Not able to get Billing Frequency " + e)
            raise Exception
        return billing_period_unit