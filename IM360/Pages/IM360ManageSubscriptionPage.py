from selenium.webdriver.common.by import By

from CommonUtilities import readWriteTestData
from CommonUtilities.baseSet.BasePage import BasePage
from CommonUtilities.parse_config import ParseConfigFile
from CommonUtilities.readProperties import ReadConfig
from IM360.Facade.PrepareObject import PrepareObject
from IM360.Pages.IM360CreditReviewPage import CreditReview
from IM360.Pages.IM360OrderPage import OrderPage
from IM360.Pages.IM360QuotePage import QuotePage
from IM360.TestSteps.createOrder import CreateOrder
from db.model.IM360Status import IM360Status
from db.model.IM360Subscription import IM360Subscription
from db.service.IM360InputItemDbManagementService import IM360InputItemDbManagementService
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360ItemDbManagementService import IM360ItemDbManagementService
from db.service.IM360StatusDbManagementService import IM360StatusDbManagementService
from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService
from db.service.IM360SubscriptionParameterDbManagementService import IM360SubscriptionParameterDbManagementService


class IM360ManageSubscriptionPage(BasePage):
    db_file_path = ReadConfig.get_db_file_path()
    im360_subscription_db_management_service_obj = IM360SubscriptionDbManagementService()
    test_data_order = readWriteTestData.load_excel_to_dictionary(ReadConfig.get_test_data_file(), "OrderData")

    """By locators"""
    QUOTE_UNDER_SALES_BUTTON = (By.ID, "sitemap-entity-nav_quotes")

    MANAGE_SUBSCRIPTION_FRAME = (By.ID, "WebResource_ChangePaymentTerms")
    MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON = (By.XPATH, "//span[text()='Manage Subscription']")
    SELECT_CHANGE_TERM = (By.XPATH, "//SPAN[text()='Change Payment Terms']")
    PAYMENT_TERM_NOT_FLOORING_OPTION = (By.XPATH, "//span[text()='No']")
    NEXT_BUTTON = (By.XPATH, "//div[text()= 'Next']")
    SELECT_BILL_TO_SUFFIX_000 = (By.XPATH, "//div[@data-selection-index='0']")
    # CHANGE_PAYMENT_TERM_COMBOBOX = (By.XPATH, "//div[@id='ComboBox29wrapper']")
    CHANGE_PAYMENT_TERM_COMBOBOX = (
        By.XPATH, "//*[@class='ms-Button ms-Button--icon ms-ComboBox-CaretDown-button root-182']")
    SELECT_PAYMENT_TERM_600 = (By.XPATH, "//span[contains(text(), '600')]")
    FINISH_BUTTON = (By.XPATH, "//div[text()= 'Finish']")
    PAYMENT_TERM_SELECTED_TEXT = (By.XPATH, "//div[contains(text(), '600 - NET 60 DAYS')]")
    # OK_BUTTON_FRAME = (By.ID, "ClientApiFrame_id-1377")
    MODAL_DIV = (By.XPATH, "//div[@id='modalDialogView_199']")
    BUTTON_VIEW_DIV = (By.XPATH, "//div[@id='buttonView']")
    OK_BUTTON = (By.XPATH, "//span[text()='OK']")

    SELECT_CANCEL_SUBSCRIPTION = (By.XPATH, "//SPAN[text()='Cancel Subscription']")
    CANCEL_CONFIRMATION_POP_UP = (By.XPATH, "//span[text()='Yes, I really want to Cancel']")
    STATUS_REASON_VALUE = (By.XPATH, "//div[@aria-label='Status Reason']")

    CHECK_STATUS_OF_SUBSCRIPTION = (By.XPATH, "//select[@aria-label='Status']")
    CHECK_STATUS_REASON_OF_SUBSCRIPTION = (By.XPATH, "//div[@aria-label='Status Reason']")

    CONFIRM_MODIFY_SUBSCRIPTION = (By.XPATH, "//span[text()='Yes, I want to Modify the Subscription']")
    SELECT_PRODUCTS = (By.XPATH, "//div[text()='Products']")

    SAVE_CHANGED_QTY = (By.XPATH, "//span[text()='Save']")
    SUMMARY = (By.XPATH, "//li[@id='tab0_49']")
    SELECT_KEEP_CURRENT_DATE = (By.XPATH, "//select[@title = 'Keep my Current End Date']")
    SELECT_SUBSCRIPTION_ENDDATE = (By.XPATH, "//div[@data-id='im360_subscriptionenddateoption.fieldControl-option-set"
                                             "-select-container']")

    SUBSCRIPTION_END_DATE_COMBOBOX = (By.XPATH, "//*[@aria-label='Subscription End Date Option']")
    # SUBSCRIPTION_END_DATE_OPTION = (By.XPATH, "//*[@aria-label='Subscription End Date Option']/option[2]")
    PRODUCT_TAB_BUTTON = (By.CSS_SELECTOR, "li[data-id='tablist-tab_Products']")

    SELECT_MODIFY_SUBSCRIPTION = (By.XPATH, "//SPAN[text()='Modify Subscription']")
    CONFIRM_MODIFYING_THE_SUBSCRIPTION_POP_UP = (By.XPATH, "//*[@aria-label='Confirm Modifying the Subscription']")
    SUBSCRIPTION_END_DATE_OPTION = (By.XPATH, "//*[@aria-label='Subscription End Date Option']")
    KEEP_MY_CURRENT_END_DATE = (By.XPATH, "//*[text()='Keep my Current End Date']")
    SUMMARY_TAB = (By.CSS_SELECTOR, "li[data-id='tablist-summary_tab']")
    KEEP_MY_CURRENT_END_DATE_OPTION = (By.XPATH, "//*[@aria-label='Subscription End Date Option']/option[2]")
    SELECT_SWITCH_PLAN = (By.XPATH, "//SPAN[text()='Switch Plan']")
    CONFIRM_SWITCH_PLAN = (By.XPATH, "//*[text()='Yes, I want to Switch Plan']")
    CLICK_REFRESH = (By.XPATH, "//span[text()='Refresh']")
    # CLICK_REFRESH = (By.ID, "quote|NoRelationship|Form|Mscrm.Modern.refreshCommand18")
    EFFECTIVE_NEW_TERM_OPTION = (By.XPATH, "//*[text()='Effective New Term']")
    PAYMENT_TERM_OPTIONS = (By.XPATH, "//*[@class='ms-ComboBox-optionText css-187']")
    PAYMENT_TYPE = (By.XPATH, "//*[@id='root']")

    im360_item_db_mgmt_srv = IM360ItemDbManagementService()
    im360_subs_param_db_mgmt_srv = IM360SubscriptionParameterDbManagementService()
    im360_subs_db_mgmt_srv = IM360SubscriptionDbManagementService()
    item_management_srv_obj = IM360InputItemDbManagementService()

    im360_status_list = []
    im360_item_data_list = []
    im360_parameter_data_list = []
    im360_subscription_list = []
    screen_shot = {"path": " "}
    screen_shot_path = ReadConfig.getScreenshotPath()

    """constructor of the Manage Subscription Page class"""

    def __init__(self, driver):
        super().__init__(driver)

    """Page actions for manage subscription page page"""

    """ This function cancels the subscription using testcase id and inserts data in db for cancellation """

    def do_cancel_subscription(self, test_case_id):
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        order_management_srv_obj = IM360InputOrderDbManagementService()
        quote_page = QuotePage(self.driver)
        create_page = CreateOrder(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)

            # if quote_page.do_search_and_get_into_existing_quote(quote_name):
            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_average")
                self.logger.info("about to click on manage subscription frame")
                self.do_sleep("min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_sleep("min")
                self.do_click_by_locator(self.SELECT_CANCEL_SUBSCRIPTION)
                self.logger.info("Cancel subscription option is selected from dropdown")
                self.do_click_by_locator(self.CANCEL_CONFIRMATION_POP_UP)
                self.logger.info("Cancelled subscription successfully ")
                self.do_sleep("average")
                self.do_refresh()
                create_page.is_sign_in_button_shown()
                self.do_sleep("average")
                self.do_click_by_locator(self.CLICK_REFRESH)
                self.do_sleep("max")
                cancellation_status = self.do_get_attribute(self.STATUS_REASON_VALUE, "title")
                self.logger.info("Cancellation status fetched from IM360 UI is : %s", cancellation_status)
                self.do_sleep("min")
                assert cancellation_status == "Cancellation Requested"
                self.logger.info("Cancellation requested successfully for the quote: %s", quote_name)
                self.logger.info("Inserting duplicate rows into database")
                im360_subs_tbl_id = self.im360_subs_db_mgmt_srv.insert_duplicate_row(self.db_file_path, test_case_id)
                im360_subs_secnd_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                    self.db_file_path, test_case_id)
                self.im360_item_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_secnd_max_id,
                                                                 im360_subs_tbl_id)
                self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_secnd_max_id,
                                                                       im360_subs_tbl_id)
                self.logger.info("Duplicate rows inserted successfully into database")

                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name +
                    "_" + "cancellation requested successful.png")
            else:
                raise Exception("Exception occurred during searching the quote %s", quote_name)
        except Exception as e:
            self.logger.error("Exception occurred during cancelling the subscription: %s", quote_name)
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name +
                "_" + "cancellation error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\Im360\\Error\\" + "cancellation error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_page.is_error_popup_shown()
            create_page.is_sign_in_button_shown()
            raise e

    """ This function validates the cancellation of subscription"""

    def do_validate_cancellation(self, test_case_id):
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        order_management_srv_obj = IM360InputOrderDbManagementService()
        create_page = CreateOrder(self.driver)
        quote_page = QuotePage(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj. \
                get_quote_name_by_test_case_id(self.db_file_path, test_case_id)
            self.logger.info("Searching the subscription ID : %s", subs_id)
            if quote_page.do_search_subscription_all_quote(subs_id, quote_name):
                # if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_average")
                self.logger.info("Getting the subscription status")
                status = self.do_get_attribute(self.CHECK_STATUS_OF_SUBSCRIPTION, "title")
                self.logger.info("Subscription status %s fetched", status)
                self.logger.info("Getting the subscription status reason")
                status_reason = self.do_get_attribute(self.CHECK_STATUS_REASON_OF_SUBSCRIPTION, "title")
                self.logger.info("Subscription status reason %s fetched", status_reason)
                assert status == 'Closed'
                self.im360_subs_db_mgmt_srv.update_status_by_test_case_id_and_tbl_id(
                    self.db_file_path, test_case_id, status)
                self.logger.info("Subscription cancellation status updated successfully")
                """adding data to im360status table"""
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path, test_case_id)
                test_case_id = test_case_order_data.get("test_case_id")
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "subscription cancellation successful.png")
            else:
                raise Exception
        except Exception as e:
            self.logger.error("Exception occurred while validating the subscription cancellation: %s",
                              quote_name)
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" + " subscription cancellation error.png")
            self.screen_shot[
                "path"] = self.screen_shot_path + "\\IM360\\Error\\" + "subscription cancellation error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_page.is_error_popup_shown()
            create_page.is_sign_in_button_shown()
            raise e

    """ This function is used to change Payment term to NET 600"""

    def do_change_payment_term(self, test_case_id):

        im360_status_management_srv_obj = IM360StatusDbManagementService()
        order_management_srv_obj = IM360InputOrderDbManagementService()
        create_page = CreateOrder(self.driver)
        quote_page = QuotePage(self.driver)
        credit_review = CreditReview(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)
            test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                self.db_file_path,
                test_case_id)
            payment_term = str(test_case_order_data.get("change_payment_term"))

            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_click_by_locator(self.SELECT_CHANGE_TERM)
                self.logger.info("Change payment term option is selected from dropdown")
                self.do_sleep("above_min")
                self.do_switch_to_required_frame(self.MANAGE_SUBSCRIPTION_FRAME)
                self.do_click_by_locator(self.PAYMENT_TERM_NOT_FLOORING_OPTION)
                self.logger.info("selected No for flooring option")
                self.do_click_by_locator(self.NEXT_BUTTON)
                self.logger.info("Clicked on Next button")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.SELECT_BILL_TO_SUFFIX_000)
                self.logger.info("Selected Bill To suffix as 000")
                self.do_click_by_locator(self.NEXT_BUTTON)
                self.logger.info("Clicked on Next button")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.CHANGE_PAYMENT_TERM_COMBOBOX)
                self.logger.info("Change payment term combobox clicked")
                self.do_sleep("above_min")
                elements = self.get_all_elements(self.PAYMENT_TERM_OPTIONS)
                for e in elements:
                    if payment_term in e.text:
                        self.logger.info("Payment Term:" + e.text)
                        e.click()
                        self.logger.info("clicked on payment term ")
                        break
                self.logger.info("Payment term Net 60 days selected")
                self.do_click_by_locator(self.NEXT_BUTTON)
                self.logger.info("Clicked on Next button")
                self.do_sleep("above_min")

                payment_term_selected_text = self.get_element_text(self.PAYMENT_TYPE)
                if payment_term in payment_term_selected_text:
                    payment_type = payment_term_selected_text.split("-")[1].strip()
                    self.do_click_by_locator(self.FINISH_BUTTON)
                    self.logger.info("Clicked on finish button")
                    self.do_sleep("max")
                    self.do_switch_to_parent_frame()

                    if self.do_check_visibility(self.OK_BUTTON):
                        self.do_click_by_locator(self.OK_BUTTON)
                    else:
                        self.logger.error("Ok button is not found in the confirmation popup")
                        raise Exception("Ok button is not visible")

                    self.logger.info("Payment term is changed, trying to approve credit hold")
                    credit_review.approve_credit_review(quote_name)
                    self.do_sleep("average")
                    self.logger.info("Credit review performed successfully, trying to update table with latest term")
                    im360_subs_tbl_id = self.im360_subs_db_mgmt_srv.insert_records_with_updated_term(
                        self.db_file_path,
                        test_case_id,
                        payment_term,
                        payment_type)
                    im360_subs_secnd_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                        self.db_file_path, test_case_id)
                    self.im360_item_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_secnd_max_id,
                                                                     im360_subs_tbl_id)
                    self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_secnd_max_id,
                                                                           im360_subs_tbl_id)
                    self.logger.info("Payment term updated successfully in DB")
                    """adding data to im360status table"""
                    testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                                test_case_id)
                    service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
                    reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(
                        self.db_file_path, test_case_id)
                    vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                    marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                     test_case_id)
                    im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                                   reseller_currency, vendor_currency, marketplace_name)
                    self.im360_status_list.append(im360_status_obj)
                    im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" +
                                                marketplace_name + "_" + "payment term updated successful.png")
                else:
                    self.logger.error("Payment term can not be selected")
                    raise Exception
            else:
                raise Exception

        except Exception as e:
            self.logger.info("Exception occurred while trying to change payment term %s", e)
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(
                self.db_file_path, test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                + "_" + "payment term update error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Success\\" + "payment term update error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_page.is_error_popup_shown()
            create_page.is_sign_in_button_shown()
            raise

    """ This function is used to add new SKU"""

    def do_add_new_sku(self, test_case_id):
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        order_management_srv_obj = IM360InputOrderDbManagementService()
        item_management_srv_obj = IM360InputItemDbManagementService()
        im360_subs_output_data = IM360Subscription()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        prepare_obj = PrepareObject(self.driver)
        quote_page = QuotePage(self.driver)
        credit_review = CreditReview(self.driver)
        create_order_steps = CreateOrder(self.driver)
        order = OrderPage(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)
            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_click_by_locator(self.SELECT_MODIFY_SUBSCRIPTION)
                self.logger.info("Modify Subscription option is selected from dropdown")
                self.do_sleep("above_min")
                self.do_check_visibility(self.CONFIRM_MODIFYING_THE_SUBSCRIPTION_POP_UP)
                self.logger.info("Confirm Modifying the Subscription popup visible")
                self.do_sleep("above_average")
                self.do_click_by_locator(self.CONFIRM_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Yes, I want to Modify the Subscription button of popup")
                self.do_sleep("above_max")
                self.do_click_by_locator(self.SUBSCRIPTION_END_DATE_OPTION)
                self.do_click_by_locator(self.KEEP_MY_CURRENT_END_DATE)
                test_case_item_data = item_management_srv_obj.get_item_test_case_details(self.db_file_path,
                                                                                         test_case_id)
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path, test_case_id)
                is_in_credit_hold = create_order_steps.add_new_sku_and_activate_quote(self.db_file_path,
                                                                                      test_case_order_data,
                                                                                      test_case_item_data,
                                                                                      im360_subs_output_data,
                                                                                      existing=False)
                self.logger.info("Credit hold status %s", str(is_in_credit_hold))
                if is_in_credit_hold:
                    create_order_steps.approve_from_credit_hold(test_case_order_data, self.screen_shot)
                create_order_steps.convert_quote_to_order(test_case_order_data, self.screen_shot)
                self.im360_item_data_list.clear()
                im360_subs_output_data.test_case_id = test_case_order_data.get("test_case_id")
                order.do_submit_order(im360_subs_output_data, self.im360_item_data_list, test_case_order_data)

                im360_sub_row_id = self.im360_subs_db_mgmt_srv.insert_duplicate_row(self.db_file_path,
                                                                                    test_case_id)
                im360_subs_second_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                    self.db_file_path, test_case_id)

                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_item_data_list)
                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_parameter_data_list)
                im360_item_db_mgmt_srv.save_im360_item(self.db_file_path, self.im360_item_data_list)
                self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_second_max_id,
                                                                       im360_sub_row_id)
                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "add new sku successful.png")
        except Exception as e:
            self.logger.info("Exception occurred while trying to add new sku %s", e)
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" + "add new sku error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "add new sku error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_order_steps.is_error_popup_shown()
            create_order_steps.is_sign_in_button_shown()
            raise e

    """This function used to increase the quantity of products"""""

    def do_increase_qty_of_products(self, test_case_id):
        parse_config_file = ParseConfigFile()
        quantity = parse_config_file.get_data_from_config_json("ChangeQuantity", "change_qty")
        order_management_srv_obj = IM360InputOrderDbManagementService()
        item_management_srv_obj = IM360InputItemDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        im360_subs_output_data = IM360Subscription()
        prepare_obj = PrepareObject(self.driver)
        order_page = OrderPage(self.driver)
        create_order = CreateOrder(self.driver)
        quote_page = QuotePage(self.driver)
        credit_review = CreditReview(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)
            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_sleep("min")
                self.do_click_by_locator(self.SELECT_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Modify subscription link")
                self.do_sleep("min")
                self.do_click_by_locator(self.CONFIRM_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Confirm modify subscription button")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.SUBSCRIPTION_END_DATE_COMBOBOX)
                self.do_click_by_locator(self.KEEP_MY_CURRENT_END_DATE_OPTION)
                self.logger.info("Selected subscription end date option")
                self.do_sleep("min")
                self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
                self.logger.info("Clicked on Product tab ")
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path, test_case_id)
                test_case_item_data = item_management_srv_obj.get_item_test_case_details(self.db_file_path,
                                                                                         test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                filtered_data = create_order.filtered_items_by_testcase_id_and_item_type(self.db_file_path,
                                                                                         test_case_order_data,
                                                                                         test_case_item_data)
                quote_page = QuotePage(self.driver)
                quote_page.change_quantity_and_activate_quote(filtered_data, test_case_id, "increase", quantity,
                                                              marketplace_name)
                self.do_sleep("average")
                quote_page.is_quote_on_credit_hold()
                self.do_sleep("above_min")
                credit_review.search_and_get_quote_on_credit_hold(quote_name)
                credit_review.approve_credit_review(quote_name)
                self.do_sleep("average")
                quote_page.do_search_subscription_all_quote(subs_id, quote_name)
                self.do_sleep("above_min")
                quote_page.convert_to_order(quote_name)
                self.do_sleep("average")
                self.im360_item_data_list.clear()
                im360_subs_output_data.test_case_id = test_case_order_data.get("test_case_id")
                order_page.do_submit_order(im360_subs_output_data, self.im360_item_data_list, test_case_order_data)

                im360_sub_row_id = self.im360_subs_db_mgmt_srv.insert_duplicate_row(self.db_file_path,
                                                                                    test_case_id)
                im360_subs_second_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                    self.db_file_path, test_case_id)

                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_item_data_list)
                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_parameter_data_list)
                im360_item_db_mgmt_srv.save_im360_item(self.db_file_path, self.im360_item_data_list)
                self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_second_max_id,
                                                                       im360_sub_row_id)
                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "adding new quantity successful.png")

            else:
                raise Exception
        except Exception as e:
            self.logger.error("Exception occurred increasing the quantity")
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" "Increase quantity error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "increase quantity error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_order.is_error_popup_shown()
            create_order.is_sign_in_button_shown()
            raise e

    """This function used to decreases the quantity of products"""""

    def do_decrease_qty_of_products(self, test_case_id):
        parse_config_file = ParseConfigFile()
        quantity = parse_config_file.get_data_from_config_json("ChangeQuantity", "change_qty")
        order_management_srv_obj = IM360InputOrderDbManagementService()
        item_management_srv_obj = IM360InputItemDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        im360_subs_output_data = IM360Subscription()
        prepare_obj = PrepareObject(self.driver)
        order_page = OrderPage(self.driver)
        create_order = CreateOrder(self.driver)
        quote_page = QuotePage(self.driver)
        credit_review = CreditReview(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)
            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_sleep("min")
                self.do_click_by_locator(self.SELECT_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Modify subscription link")
                self.do_sleep("min")
                self.do_click_by_locator(self.CONFIRM_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Confirm modify subscription button")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.SUBSCRIPTION_END_DATE_COMBOBOX)
                self.do_click_by_locator(self.KEEP_MY_CURRENT_END_DATE_OPTION)
                self.logger.info("Selected subscription end date option")
                self.do_sleep("min")
                self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
                self.logger.info("Clicked on Product tab ")
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path, test_case_id)
                test_case_item_data = item_management_srv_obj.get_item_test_case_details(self.db_file_path,
                                                                                         test_case_id)
                filtered_data = create_order.filtered_items_by_testcase_id_and_item_type(self.db_file_path,
                                                                                         test_case_order_data,
                                                                                         test_case_item_data)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                quote_page.change_quantity_and_activate_quote(filtered_data, test_case_id, "decrease", quantity,
                                                              marketplace_name)
                self.do_sleep("average")
                quote_page.is_quote_on_credit_hold()
                self.do_sleep("above_min")
                credit_review.search_and_get_quote_on_credit_hold(quote_name)
                credit_review.approve_credit_review(quote_name)
                self.do_sleep("average")
                quote_page.do_search_subscription_all_quote(subs_id, quote_name)
                self.do_sleep("above_min")
                quote_page.convert_to_order(quote_name)
                self.do_sleep("average")
                self.im360_item_data_list.clear()
                im360_subs_output_data.test_case_id = test_case_order_data.get("test_case_id")
                order_page.do_submit_order(im360_subs_output_data, self.im360_item_data_list, test_case_order_data)

                im360_sub_row_id = self.im360_subs_db_mgmt_srv.insert_duplicate_row(self.db_file_path,
                                                                                    test_case_id)
                im360_subs_second_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                    self.db_file_path, test_case_id)

                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_item_data_list)
                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_parameter_data_list)
                im360_item_db_mgmt_srv.save_im360_item(self.db_file_path, self.im360_item_data_list)
                self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_second_max_id,
                                                                       im360_sub_row_id)
                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "Decrease quantity successful.png")
            else:
                message = "Exception occured during searching quote by subscription id"
                raise Exception(message)
        except Exception as e:
            self.logger.error("Exception occurred decreasing the quantity" + str(e))
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + "Decrease quantity error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "decrease quantity error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_order.is_error_popup_shown()
            create_order.is_sign_in_button_shown()
            raise e

    """This function used to switch plan-change billing frequency monthly - yearly"""""

    def switch_plan(self, test_case_id):
        order_management_srv_obj = IM360InputOrderDbManagementService()
        item_management_srv_obj = IM360InputItemDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        im360_subs_output_data = IM360Subscription()
        create_order_steps = CreateOrder(self.driver)
        order = OrderPage(self.driver)
        prepare_obj = PrepareObject(self.driver)
        quote_page = QuotePage(self.driver)
        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)
            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_sleep("min")
                self.do_click_by_locator(self.SELECT_SWITCH_PLAN)
                self.logger.info("Clicked on Switch Plan link")
                self.do_sleep("min")
                self.do_click_by_locator(self.CONFIRM_SWITCH_PLAN)
                self.logger.info("Clicked on Confirm modify subscription button")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.SUBSCRIPTION_END_DATE_COMBOBOX)
                self.do_click_by_locator(self.KEEP_MY_CURRENT_END_DATE_OPTION)
                self.logger.info("Selected subscription end date option")
                self.do_sleep("min")
                self.do_click_by_locator(self.PRODUCT_TAB_BUTTON)
                self.logger.info("Clicked on Product tab ")
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path, test_case_id)
                self.logger.info(test_case_order_data)
                test_case_item_data = item_management_srv_obj.get_item_test_case_details(self.db_file_path,
                                                                                         test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                quote_page.delete_the_existing_sku(self.screen_shot, test_case_id, marketplace_name)
                is_in_credit_hold = create_order_steps.add_sku_with_change_plan_id(self.db_file_path,
                                                                                   test_case_order_data,
                                                                                   test_case_item_data)

                self.logger.info("Credit hold status %s", str(is_in_credit_hold))
                if is_in_credit_hold:
                    create_order_steps.approve_from_credit_hold(test_case_order_data, self.screen_shot)
                create_order_steps.convert_quote_to_order(test_case_order_data, self.screen_shot)
                change_deal_id = test_case_order_data.get("change_deal_id")
                order.do_input_vendor_change_deal_id_fields(change_deal_id)
                self.im360_item_data_list.clear()
                im360_subs_output_data.test_case_id = test_case_order_data.get("test_case_id")
                billing_frequency = order.get_updated_billing_frequency(1)
                order.do_submit_order(im360_subs_output_data, self.im360_item_data_list, test_case_order_data)

                im360_sub_row_id = self.im360_subs_db_mgmt_srv.insert_duplicate_row(self.db_file_path,
                                                                                    test_case_id)
                self.im360_subs_db_mgmt_srv.update_the_billing_frequency(self.db_file_path,
                                                                                test_case_id, billing_frequency)
                im360_subs_second_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                    self.db_file_path, test_case_id)

                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_item_data_list)
                prepare_obj.set_key_for_im360_output_data(im360_sub_row_id, self.im360_parameter_data_list)
                im360_item_db_mgmt_srv.save_im360_item(self.db_file_path, self.im360_item_data_list)
                self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_second_max_id,
                                                                       im360_sub_row_id)
                self.im360_subs_param_db_mgmt_srv.update_records_with_updated_deal_id(self.db_file_path,
                                                                                      test_case_id, change_deal_id)
                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "switch plan successful.png")
        except Exception as e:
            self.logger.error("Exception occurred switching plan")
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" + "switch plan error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "switch plan error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_order_steps.is_error_popup_shown()
            create_order_steps.is_sign_in_button_shown()
            raise e

    """ Function to change contract term """

    def do_change_contract_term(self, test_case_id):
        order_management_srv_obj = IM360InputOrderDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        im360_subs_output_data = IM360Subscription()
        quote_page = QuotePage(self.driver)
        credit_review = CreditReview(self.driver)
        create_order_steps = CreateOrder(self.driver)
        order = OrderPage(self.driver)

        try:
            subs_id = self.im360_subscription_db_management_service_obj.get_sub_id_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
            quote_name = self.im360_subscription_db_management_service_obj.get_quote_name_by_test_case_id(
                self.db_file_path, test_case_id)
            if quote_page.do_search_subscription_quote(subs_id, quote_name):
                self.do_sleep("above_min")
                self.do_click_by_locator(self.MANAGE_SUBSCRIPTION_DROPDOWN_BUTTON)
                self.logger.info("Clicked on Manage subscription dropdown")
                self.do_sleep("min")
                self.do_click_by_locator(self.SELECT_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Modify subscription link")
                self.do_sleep("min")
                self.do_click_by_locator(self.CONFIRM_MODIFY_SUBSCRIPTION)
                self.logger.info("Clicked on Confirm modify subscription button")
                self.do_sleep("above_min")
                self.do_click_by_locator(self.SUBSCRIPTION_END_DATE_COMBOBOX)
                self.do_click_by_locator(self.EFFECTIVE_NEW_TERM_OPTION)
                self.logger.info("Selected subscription end date option")
                self.do_sleep("average")
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path,
                    test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                contract_term = test_case_order_data.get("change_subscription_period")
                quote_page.change_contract_term_and_activate_quote(contract_term, test_case_id, marketplace_name)
                self.do_sleep("average")
                quote_page.is_quote_on_credit_hold()
                self.do_sleep("above_min")
                credit_review.search_and_get_quote_on_credit_hold(quote_name)
                credit_review.approve_credit_review(quote_name)
                self.do_sleep("average")
                quote_page.do_search_subscription_all_quote(subs_id, quote_name)
                self.do_sleep("above_min")
                quote_page.convert_to_order(quote_name)
                self.do_sleep("average")
                change_deal_id = test_case_order_data.get("change_deal_id")
                order.do_input_vendor_change_deal_id_fields(change_deal_id)
                im360_item_data_list = []
                order.do_submit_order(im360_subs_output_data, im360_item_data_list, test_case_order_data)
                im360_sub_row_id = self.im360_subs_db_mgmt_srv.insert_duplicate_row(self.db_file_path, test_case_id)
                self.im360_subs_db_mgmt_srv.update_records_with_updated_subscription_period(self.db_file_path,
                                                                                            test_case_id, contract_term)
                im360_subs_second_max_id = self.im360_subs_db_mgmt_srv.get_second_max_id_by_test_case_id(
                    self.db_file_path,
                    test_case_id)
                self.im360_item_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_second_max_id,
                                                                 im360_sub_row_id)
                self.im360_subs_param_db_mgmt_srv.insert_duplicate_row(self.db_file_path, im360_subs_second_max_id,
                                                                       im360_sub_row_id)
                self.im360_subs_param_db_mgmt_srv.update_records_with_updated_deal_id(self.db_file_path,
                                                                                      test_case_id, change_deal_id)

                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "Contract term change successful.png")
            else:
                message = "Exception occured during searching quote by subscription id"
                raise Exception(message)
        except Exception as e:
            self.logger.error("Exception occurred during changing contract term" + str(e))
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" + "contract term change error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "decrease quantity error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_order_steps.is_error_popup_shown()
            create_order_steps.is_sign_in_button_shown()
            raise e
