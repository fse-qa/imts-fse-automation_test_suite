from CommonUtilities import readWriteTestData
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.readProperties import ReadConfig
from IM360.Facade.PrepareObject import PrepareObject
from IM360.TestSteps.createOrder import CreateOrder
from db.model.IM360Status import IM360Status
from db.model.IM360Subscription import IM360Subscription
from db.service.IM360InputItemDbManagementService import IM360InputItemDbManagementService
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360ItemDbManagementService import IM360ItemDbManagementService
from db.service.IM360StatusDbManagementService import IM360StatusDbManagementService
from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService
from db.service.IM360SubscriptionParameterDbManagementService import IM360SubscriptionParameterDbManagementService


class IM360createOrders:
    logger = LogGenerator.logGen()
    db_file_path = ReadConfig.get_db_file_path()
    im360_input_order_list = []
    im360_input_item_data_list = []

    im360_subscription_list = []
    im360_item_data_list = []
    im360_parameter_data_list = []

    im360_status_list = []
    screen_shot = {"path": " "}

    """
    This method will create a new order of Cisco following the steps:
    Create draft quote, Provide Bill_to address, payment term, Add products with their quantity,
    Activate quote, release the same from credit hold, convert the quote to order, add reseller PO,
    add vendor specific fields, add end user and then submit the order.
    Once subscription is submitted, release that from Hold and finally get the ERP order number.
    This method will save the order data in the DB along with the ERP order number.
    Author : Sourav Mukherjee
    """

    def create_new_order(self, driver, test_case_id):
        readWriteTestData.update_test_data_sheet(ReadConfig.get_test_data_file(), "OrderData", test_case_id)
        test_data_order = readWriteTestData.load_excel_to_dictionary(ReadConfig.get_test_data_file(), "OrderData")
        test_data_items = readWriteTestData.load_excel_to_dictionary(ReadConfig.get_test_data_file(), "Items")
        self.logger.info(test_data_order)
        create_order_steps = CreateOrder(driver)
        order_management_srv_obj = IM360InputOrderDbManagementService()
        item_management_srv_obj = IM360InputItemDbManagementService()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        im360_subs_param_db_mgmt_srv = IM360SubscriptionParameterDbManagementService()
        im360_subs_db_mgmt_srv = IM360SubscriptionDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()

        im360_subs_output_data = IM360Subscription()
        prepare_obj = PrepareObject(driver)
        self.logger.info("Filtering the orders by test case ID %s", test_case_id)
        filtered_order_data = create_order_steps.filtered_orders_by_testcase_id(test_data_order, test_case_id)
        for order_index, test_data_order in filtered_order_data.iterrows():
            self.im360_input_order_list.clear()
            self.im360_input_item_data_list.clear()
            self.im360_subscription_list.clear()
            self.im360_item_data_list.clear()
            self.im360_parameter_data_list.clear()
            self.im360_status_list.clear()
            try:
                self.logger.info(test_data_order)
                im360_subs_output_data.test_case_id = test_data_order.get("TestCaseId")

                im360_input_order_data = prepare_obj.prepare_im360_inp_ord_data_obj(test_data_order)
                self.im360_input_order_list.append(im360_input_order_data)

                im360_input_order_row_id = order_management_srv_obj.save_im360_input_order(self.db_file_path,
                                                                                           self.im360_input_order_list)
                im360_input_order_id = order_management_srv_obj.get_id_by_row_id(self.db_file_path,
                                                                                 im360_input_order_row_id)
                filtered_item_data = create_order_steps.filtered_items_by_testcase_id(test_data_order,
                                                                                      test_data_items)

                for item_index, item_data in filtered_item_data.iterrows():
                    im360_input_item_data_obj = prepare_obj.prepare_im360_inp_item_data_obj(item_data,
                                                                                            im360_input_order_id)
                    self.im360_input_item_data_list.append(im360_input_item_data_obj)

                item_management_srv_obj.save_im360_input_item(self.db_file_path, self.im360_input_item_data_list)

                # Fetch the data from database
                test_case_item_data = item_management_srv_obj.get_item_test_case_details(self.db_file_path,
                                                                                         test_case_id)
                test_case_order_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                    self.db_file_path, test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)

                create_order_steps.open_draft_quote_form(self.screen_shot, test_case_id, marketplace_name)
                create_order_steps.create_draft_quote(test_case_order_data, im360_subs_output_data,
                                                      self.screen_shot)
                is_in_credit_hold = create_order_steps.activate_quote(self.db_file_path, test_case_order_data,
                                                                      test_case_item_data, im360_subs_output_data,
                                                                      existing=False)
                self.logger.info("Credit hold status %s", str(is_in_credit_hold))
                if is_in_credit_hold:
                    create_order_steps.approve_credit_hold_on_Quote(test_case_order_data, self.screen_shot)

                create_order_steps.convert_quote_to_order(test_case_order_data, self.screen_shot)
                create_order_steps.submit_order(im360_subs_output_data, self.screen_shot, self.im360_item_data_list,
                                                self.im360_parameter_data_list, test_case_order_data)

                quote_name = test_case_order_data.get("quote_name")
                im360_subs_output_data.quote_name = quote_name
                self.im360_subscription_list.append(im360_subs_output_data)
                im360_sub_row_id = im360_subs_db_mgmt_srv.save_im360_subscription(self.db_file_path,
                                                                                  self.im360_subscription_list)
                im360_sub_id = im360_subs_db_mgmt_srv.get_id_by_row_id(self.db_file_path, im360_sub_row_id)

                prepare_obj.set_key_for_im360_output_data(im360_sub_id, self.im360_item_data_list)
                prepare_obj.set_key_for_im360_output_data(im360_sub_id, self.im360_parameter_data_list)
                im360_item_db_mgmt_srv.save_im360_item(self.db_file_path, self.im360_item_data_list)
                im360_subs_param_db_mgmt_srv.save_im360_subscription_parameter(self.db_file_path,
                                                                               self.im360_parameter_data_list)
                test_case_id = test_data_order.get("TestCaseId")

                """adding data to im360status table"""

                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(
                    self.db_file_path,
                    test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)

            except Exception as e:
                self.logger.error(str(test_data_order.get("TestCaseId") + " -- This Test case got failed"))
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(
                    self.db_file_path,
                    test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                               str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                               marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                create_order_steps.is_error_popup_shown()
                create_order_steps.is_sign_in_button_shown()
                self.logger.exception(e)
                raise e
