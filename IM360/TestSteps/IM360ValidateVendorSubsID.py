from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.readProperties import ReadConfig
from IM360.TestSteps.createOrder import CreateOrder
from db.model.IM360Status import IM360Status
from db.service.CBCStatusDbManagementService import CBCStatusDbManagementService
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360StatusDbManagementService import IM360StatusDbManagementService
from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService
from db.service.IM360SubscriptionParameterDbManagementService import IM360SubscriptionParameterDbManagementService


class ValidateVendorSubscriptionID:
    logger = LogGenerator.logGen()
    db_file_path = ReadConfig.get_db_file_path()
    im360_status_list = []
    screen_shot = {"path": " "}
    screen_shot_path = ReadConfig.getScreenshotPath()

    """ 
    This method firstly check the subscription status in cbc_status table and if it is CP then logs into im360 UI
    and then fetches the vendor subscription id from DB and compares it with data in IM360 UI.
    """

    def validate_vendor_subscription_id_from_quote(self, driver, test_case_id):
        im360_sub_db_mgmt_srv_obj = IM360SubscriptionDbManagementService()
        cbc_sts_db_mgmt_srv_obj = CBCStatusDbManagementService()
        im360_subs_param_db_mgmt_srv_obj = IM360SubscriptionParameterDbManagementService()
        im360_input_ord_db_mgmt_srv_obj = IM360InputOrderDbManagementService()
        order_management_srv_obj = IM360InputOrderDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        validate_order_steps = CreateOrder(driver)
        try:
            self.logger.info("Getting subscription by test case ID from database to validate vendor subscription ID")
            im360_subs_ids = im360_sub_db_mgmt_srv_obj.get_all_sub_by_test_case_id(self.db_file_path, test_case_id)
            if len(im360_subs_ids) == 0:
                raise Exception("No records found in the database with test case ID %s", test_case_id)

            for im360_sub_id in im360_subs_ids:
                self.logger.info("Checking if the subscription is in CP status in CBC")
                if str(cbc_sts_db_mgmt_srv_obj.get_status_by_sub_details(self.db_file_path,
                                                                         im360_sub_id['subscription_id'],
                                                                         im360_sub_id['order_number'])).strip() == 'CP':
                    vendor_subscription_id = \
                        im360_subs_param_db_mgmt_srv_obj.get_param_val_by_id_and_param_name(self.db_file_path,
                                                                                            im360_sub_id['id'],
                                                                                            'Vendor Subscription ID')
                    self.logger.info("Subscription is in CP status, logging into the IM360 portal")
                    self.logger.info("Start searching the subscription %s " % im360_sub_id['subscription_id'])
                    marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                     test_case_id)
                    validate_order_steps.search_subscription(im360_sub_id['subscription_id'],
                                                             im360_sub_id['quote_name'], test_case_id, marketplace_name)
                    self.logger.info("Getting Subscriptions status from IM360 UI")
                    subscription_status = validate_order_steps.get_subscription_status()
                    self.logger.info("Successfully fetched the subscription status %s ", subscription_status)
                    im360_sub_db_mgmt_srv_obj.update_status_by_test_case_id(self.db_file_path, test_case_id,
                                                                            subscription_status)
                    self.logger.info("Validating Vendor Subscriptions ID : %s" % vendor_subscription_id)
                    validate_order_steps.validate_vendor_subs_id_from_quote(vendor_subscription_id, test_case_id,
                                                                            marketplace_name)
                else:
                    self.logger.info("Either status of the subscription is blank or status is something other than CP")
                    raise Exception

                """adding data to im360status table"""
                testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                            test_case_id)
                service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                         test_case_id)
                reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                                   test_case_id)
                vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
                marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                                 test_case_id)
                im360_status_obj = IM360Status(test_case_id, testcase_name, "success", " ", service_name, " ",
                                               reseller_currency, vendor_currency, marketplace_name)
                self.im360_status_list.append(im360_status_obj)
                im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
                driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" + marketplace_name
                    + "_" + "validate vendor id successful.png")


        except Exception as e:
            self.logger.error(
                "Exception occurred while validating vendor subscription ID. retry- %s" % str(e))
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             test_case_id)
            driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                                   + "_" + "validate vendor id error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\Im360\\Error\\" + "validate vendor id error.png"
            im360_status_obj = IM360Status(test_case_id, testcase_name, "failed", repr(e), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            validate_order_steps.is_error_popup_shown()
            validate_order_steps.is_sign_in_button_shown()
            raise e
