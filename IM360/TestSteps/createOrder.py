from CommonUtilities import readWriteTestData
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from CommonUtilities.readProperties import ReadConfig
from IM360.Facade.BrowserSet import BrowserSettings
from IM360.Pages.IM360AppSelectPage import AppSelectPage
from IM360.Pages.IM360CreditReviewPage import CreditReview
from IM360.Pages.IM360DashboardPage import DashboardPage
from IM360.Pages.IM360LoginPage import LoginPage
from IM360.Pages.IM360OrderPage import OrderPage
from IM360.Pages.IM360QuotePage import QuotePage
from db.service.IM360InputItemDbManagementService import IM360InputItemDbManagementService


class CreateOrder:
    logger = LogGenerator.logGen()
    parse_config_json = ParseConfigFile()

    screen_shot_path = ReadConfig.getScreenshotPath()

    """constructor of the createOrder Page class"""

    def __init__(self, driver):
        self.driver = driver

    """
    This Method will first clear the browser history and cache and then will log in to the portal reading the
    URL, Login ID and Password from the config file. 
    Author : Soumi Ghosh
    """

    def login(self):
        clear_browser_and_cache = BrowserSettings(self.driver)
        clear_browser_and_cache.do_clear_browser_history_and_cache()
        login = LoginPage(self.driver)
        marketplace = readWriteTestData.get_marketplace(ReadConfig.get_test_data_file(), "OrderData")
        self.logger.info(marketplace)
        username = self.parse_config_json.get_data_from_config_json(marketplace[0], "im360UserName", "CredConfig.json")
        password = self.parse_config_json.get_data_from_config_json(marketplace[0], "enc_im360Password",
                                                                    "CredConfig.json")
        try:
            login.do_login_to_im360(username, password)
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + "Login_successful.png")
            self.logger.info("Successfully logged in to IM360")
        except Exception as e:
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + "login_error.png")
            self.logger.error("Login unsuccessful!!")
            self.logger.exception(e)
            raise e

    """
    This Method will select the app in IM360 to proceed further with the order creation steps after
    logging in to the portal.
    Author: Arpita Basu
    """

    def select_app(self):
        select_app_page = AppSelectPage(self.driver)
        for retry in range(3):
            try:
                select_app_page.do_select_app()
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + "sales_app_select_successful.png")
                self.logger.info("Successfully selected the Sales app")
                break
            except Exception as e:
                self.logger.error("Trying to select Sales app. Attempt: " + str(retry + 1))
                if retry == 2:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + "select_app_error.png")
                    self.logger.error("Failed to select the Sales app")
                    self.logger.exception(e)
                    raise e
                self.driver.refresh()

    """
    This method will open new draft Quote
    Author: Arpita Basu
    """

    def open_draft_quote_form(self, screen_shot, test_case_id, marketplace_name):
        create_draft_quote = QuotePage(self.driver)

        for retry in range(3):
            try:
                create_draft_quote.do_open_draft_quote_form()
                self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_"
                                            + marketplace_name + "_" + "open_draft_quote_form_successful.png")
                break
            except Exception as e:
                self.logger.error("Not able to open draft quote form. Retry-%s" % str(retry + 1))
                self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_"
                                            + marketplace_name + "_" + "open_draft_quote_form_error.png")
                screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "open_draft_quote_form_error.png"
                self.logger.error("Failed to open draft quote form")
                self.logger.exception(e)
                raise e
                # if retry == 2:
                #     self.driver.save_screenshot(".\\ScreenShots\\" + "open_draft_quote_form_error.png")
                #     screen_shot["path"] = ".\\ScreenShots\\" + "open_draft_quote_form_error.png"
                #     self.logger.error("Failed to open draft quote form")
                #     self.logger.exception(e)
                #     raise e
                # else:
                #     # self.driver.refresh()

    """
    This is used to create draft quote using the data in input file
    Author: Arpita Basu
    """

    def create_draft_quote(self, test_data, im360_subs_output_data, screen_shot):
        create_draft_quote = QuotePage(self.driver)
        try:
            create_draft_quote.do_create_draft_quote(test_data.get("quote_name"),
                                                     test_data.get("reseller_bcn"),
                                                     test_data.get("contact"),
                                                     test_data.get("primary_vendor"), im360_subs_output_data)
            if create_draft_quote.do_validate_draft_quote(test_data.get("quote_name")):
                self.logger.info("Successfully created draft quote with quote name: " + test_data.get("quote_name"))
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                    test_data.get("marketplace_name") + "_" +
                    "draft_quote_created_success.png")
            else:
                raise Exception("Could not able to create draft quote with quote name: %s", test_data.get("quote_name"))
        except Exception as e:
            self.logger.error("Not able to create draft quote")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "open_draft_quote_form_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "create_draft_quote_form_error.png"
            self.logger.exception(e)
            raise e

    """
    This is used to search quote by the quote_name provided in test data
    """

    def search_quote(self, test_data):
        activate_quote = QuotePage(self.driver)
        for retry in range(3):
            try:
                if activate_quote.do_search_and_get_into_existing_quote(test_data.get("quote_name")) is True:
                    self.logger.info("Going to activate quote!!")
                    self.driver.save_screenshot(
                        self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_"
                        + test_data.get("marketplace_name") + "_" +
                        "search_quote_successful.png")
                    break
                else:
                    raise Exception("Could not able to search quote with name %s", test_data.get("quote_name"))
            except Exception as ex:
                self.logger.error("Not able to search Quote and get into it. retry- %s" % str(retry))
                if retry == 2:
                    self.driver.save_screenshot(
                        self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_"
                        + test_data.get("marketplace_name") + "_" + "search_quote_error.png")
                    self.logger.exception(ex)
                    raise ex
                self.driver.refresh()
                self.is_sign_in_button_shown()

    """ This method filters the IM360 input item table by testcase id and item type and returns the details """

    def filtered_items_by_testcase_id_and_item_type(self, db_file_path, test_data, items_data):
        item_management_srv_obj = IM360InputItemDbManagementService()
        item_test_case_id = items_data[0]['test_case_id']
        test_item_type = items_data[0]['item_type']
        test_order_action = items_data[0]['order_action']
        try:
            if (item_test_case_id == test_data.get('test_case_id')) & (
                    test_item_type == "Fixed") & (test_order_action == "CreateOrder"):
                filtered_item_data = item_management_srv_obj.get_filtered_items_test_case_details(db_file_path,
                                                                                                  item_test_case_id,
                                                                                                  test_item_type,
                                                                                                  test_order_action)
                self.logger.info(filtered_item_data)
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                    test_data.get(
                        "marketplace_name") + "_" + "filtered_items_by_testcase_id_success.png")
                return filtered_item_data
            else:
                raise Exception("Item test case id and order data test case is not same and item type is not same -%s ",
                                items_data[0]['item_type'])
        except Exception as e:
            self.logger.error("Not able to filter items by testcase id")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "filtered_items_by_testcase_id_error.png")

            self.logger.exception(e)
            raise e

    def filtered_items_by_testcase_id_and_item_type_and_order_action(self, db_file_path, test_data, items_data):
        item_management_srv_obj = IM360InputItemDbManagementService()
        item_test_case_id = items_data[0]['test_case_id']
        test_item_type = items_data[0]['item_type']
        test_order_action = items_data[2]['order_action']
        self.logger.info(item_test_case_id)
        self.logger.info(test_item_type)
        self.logger.info(test_order_action)
        try:
            if (item_test_case_id == test_data.get('test_case_id')) & (
                    test_item_type == "Fixed") & (test_order_action == "AddSKU"):
                filtered_item_data = item_management_srv_obj.get_filtered_items_test_case_details(db_file_path,
                                                                                                  item_test_case_id,
                                                                                                  test_item_type,
                                                                                                  test_order_action)
                self.logger.info(filtered_item_data)
                return filtered_item_data
            else:
                raise Exception(
                    "Item test case id and order data test case is not same and item type and order action are not same -%s ",
                    items_data[0]['item_type'])
        except Exception as e:
            self.logger.error("Not able to filter items by testcase id")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "filtered_items_by_testcase_id_error.png")
            self.logger.exception(e)
            raise e

    """ This method filters items by testcase id and returns item data """

    def filtered_items_by_testcase_id(self, test_data, items_data):
        filtered_item_data = items_data.loc[((items_data.TestCaseId == test_data.get('TestCaseId')))]
        return filtered_item_data

    """ This method filters order by testcase id and returns order data """

    def filtered_orders_by_testcase_id(self, test_data_order, test_case_id):
        filtered_order_data = test_data_order.loc[(test_data_order.TestCaseId == test_case_id)]
        return filtered_order_data

    """ This method activates the quote """

    def activate_quote(self, db_file_path, test_data, items_data, im360_subs_output_data, existing=True):
        activate_quote = QuotePage(self.driver)
        try:
            if existing is True:
                self.search_quote(test_data)
            self.logger.info("Filtering the items")
            filtered_item_data = self.filtered_items_by_testcase_id_and_item_type(db_file_path, test_data, items_data)
            self.logger.info("Getting %s items after filtering", str(len(filtered_item_data)))
            activate_quote.do_activate_quote(filtered_item_data,
                                             test_data.get("service_plan_id"),
                                             test_data.get("billing_period"),
                                             (str(test_data.get("subscription_period")).strip() +
                                              " " +
                                              (test_data.get("subscription_period_unit")).strip()),
                                             test_data.get("bill_to"),
                                             test_data.get("payment_term"), im360_subs_output_data
                                             )
            self.logger.info("Quote Activated Successfully!!")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "activate_quote_successful.png")
            return activate_quote.is_quote_on_credit_hold()
        except Exception as e:
            # self.driver.close()
            self.logger.error("Exception occurred in activate_quote %s", e)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "activate_quote_error.png")
            self.logger.error("Failed to activate quote with quote name: " + str(test_data.get("quote_name")))
            raise e

    """ This method approves the credit review for the quote """

    def approve_credit_hold_on_Quote(self, test_data, screen_shot):
        credit_review = CreditReview(self.driver)
        quote = QuotePage(self.driver)
        try:
            credit_review.approve_credit_review(test_data.get("quote_name"))
            self.logger.info("Validating credit review approval")
            self.search_quote(test_data)
            if not quote.is_quote_on_credit_hold():
                self.logger.info("Credit Hold is successfully released")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                    test_data.get("marketplace_name") + "_" +
                    "approve_credit_hold_on_quote_successful.png")
            else:
                raise Exception("Some error happened. Credit hold is not released")
        except Exception as e:
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "approve_credit_hold_on_quote_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "approve_credit_hold_on_quote_error.png"
            self.logger.error("Failed to approve quote with credit hold!! name: " + str(test_data.get("quote_name")))
            self.logger.exception(e)
            raise e

    """ This method converts quote to order """

    def convert_quote_to_order(self, test_data, screen_shot):
        quote = QuotePage(self.driver)
        try:
            quote.convert_to_order(test_data.get("quote_name"))
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "convert_quote_to_order_successful.png")
        except Exception as e:
            self.logger.info("Not able to convert quote to order %s", e)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "convert_quote_to_order_error.png")
            self.logger.error("Failed to convert quote to order.. name: " + str(test_data.get("quote_name")))
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "convert_quote_to_order_error.png"
            self.logger.exception(e)
            raise e

    def approve_from_credit_hold(self, test_data, screen_shot):
        credit_review = CreditReview(self.driver)
        quote = QuotePage(self.driver)
        try:
            credit_review.approve_credit_review(test_data.get("quote_name"))
            self.logger.info("Validating credit review approval")
            self.logger.info("10")
            credit_review.click_on_record_to_review_text()
        except Exception as e:
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "approve_credit_hold_on_quote_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "approve_credit_hold_on_quote_error.png"
            self.logger.error("Failed to approve quote with credit hold!! name: " + str(test_data.get("quote_name")))
            self.logger.exception(e)
            raise e

    """ This method fills reseller po, vendor fields and end user fields and submits the order """

    def submit_order(self, im360_subs_output_data, screen_shot, im360_item_data_list=[], im360_parameter_data_list=[],
                     test_data=None):
        order = OrderPage(self.driver)
        try:
            order.do_input_reseller_po(str(test_data.get("reseller_po")), im360_subs_output_data)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "submit_input_reseller_po_successful.png")
            order.do_input_vendor_fields(str(test_data.get("deal_id")),
                                         test_data.get("provisioning_contact_name"),
                                         test_data.get("provisioning_contact_email"),
                                         str(test_data.get("provisioning_contact_phone")),
                                         str(test_data.get("web_order_id")),
                                         test_data.get("reqested_ship_start_date"),
                                         test_data.get("test_case_id"),
                                         test_data.get("vendor_portal_submission"),
                                         im360_parameter_data_list)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "input_vendor_fields_successful.png")
            order.do_add_end_user(test_data.get("company_name"),
                                  test_data.get("contact_phone"),
                                  test_data.get("contact_name"),
                                  test_data.get("contact_email"),
                                  test_data.get("address"),
                                  test_data.get("city"),
                                  test_data.get("postal_code"),
                                  test_data.get("state"))
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "add_end_user_successful.png")
            if order.do_submit_order(im360_subs_output_data, im360_item_data_list, test_data):
                self.logger.info("Successfully submitted the order!!")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                    test_data.get("marketplace_name") + "_" +
                    "submit_order_order_successful.png")
                # submit_stat = False
                # for retry_status in range(2):
                #     submit_stat = order.is_order_submitted_successfully(im360_subs_output_data)
                #     if submit_stat:
                #         break
                #     if retry_status < 2:
                #         self.driver.refresh()

                # if submit_stat:
                #     self.logger.info("Successfully submitted the order!!")
                # else:
                #     order.FZ_error_on_subscription()
                # order.release_subscription_from_hold(test_data.get("QuoteName"))
            else:
                self.logger.info("Something went wrong in order submission..")
                raise Exception("Something went wrong in order submission..")
        except Exception as e:
            self.logger.error("Exception occurred in submit_order %s", e)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "submit_order_error.png")
            screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "submit_order_error.png"
            raise e

    """ This method adds the vendor subscription id in the IM360_subscription_pararmeter table """

    def add_vendor_subs_id_in_param(self, test_case_id, subs_id, im360_subs_tbl_id, im360_parameter_data_list=[]):
        order = OrderPage(self.driver)
        order.add_vendor_subs_id_in_param(test_case_id, subs_id, im360_subs_tbl_id, im360_parameter_data_list)

    """ This method is used to check if subscription id generated or not """

    def get_subscription_id_or_error(self, test_data):
        order = OrderPage(self.driver)
        sub_id = None
        try:
            if order.is_subscription_created():
                self.logger.info("Subscription created successfully, trying to get subscription ID.")
                self.driver.save_screenshot(
                    self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                    test_data.get("marketplace_name") + "_" + "subscription_id_successful.png")
                sub_id = order.get_subscription_id()
            else:
                order.get_error_on_subscription()
        except Exception as e:
            self.logger.info("Not able to get subscription id or error message")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "error_or_subscription_error.png")
            self.logger.error("Failed to get subscription id or error message")
            self.logger.exception(e)
            raise e

        return sub_id

    """ This method is used to get into the oder using quote name """

    def search_order(self, quote_name, test_case_id, marketplace_name, screen_shot):
        order = OrderPage(self.driver)
        for retry in range(3):
            try:
                if order.do_search_and_get_into_existing_order(quote_name) is True:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" +
                                                marketplace_name + "_" + "search_order_successful.png")
                    break
                else:
                    raise Exception("Some issue happened. Not able to get into the order")
            except Exception as ex:
                screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "search_order_error.png"
                self.logger.error("Not able to search order and get into it. retry- %s" % str(retry))
                if retry == 2:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" +
                                                marketplace_name + "_" +
                                                "search_order_error.png")
                    raise ex
                self.driver.refresh()
                self.is_sign_in_button_shown()

    """ This method search the order in credit hold """

    def search_and_select_the_credit_hold_order(self, quote_name, test_case_id, marketplace_name, screen_shot):
        order = OrderPage(self.driver)
        for retry in range(3):
            try:
                if order.do_search_and_get_credit_hold_order(quote_name) is True:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" +
                                                marketplace_name + "_"
                                                + "search_order_successful.png")
                    break
                else:
                    raise Exception("Some issue happened. Not able to get into the order")
            except Exception as ex:
                screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "search_order_error.png"
                self.logger.error("Not able to search order and get into it. retry- %s" % str(retry))
                if retry == 2:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" +
                                                marketplace_name + "_"
                                                + "search_order_error.png")
                    self.logger.exception(ex)
                    raise ex
                self.driver.refresh()
                self.is_sign_in_button_shown()

    """ This method is used to check the vendor subscription id """

    def validate_vendor_subs_id(self, test_case_id, marketplace_name, ven_subs_id):
        order = OrderPage(self.driver)
        try:
            if ven_subs_id == order.get_vendor_subs_id():
                self.logger.info("Vendor subscription id received in IM360")
                self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" +
                                            marketplace_name + "_" +
                                            "getVendorSubsID_successful.png")
                return True
            else:
                return False
        except Exception as ex:
            self.logger.error("Something went wrong wile getting Vendor Subscription ID")
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" +
                                        marketplace_name + "_" +
                                        "getVendorSubsID_error.png")
            self.logger.exception(ex)
            raise ex

    """ This method searches quote details from subscription id from all subscription view """

    def search_subscription(self, subscription_id, quote_name, test_case_id, marketplace_name):
        quote = QuotePage(self.driver)
        for retry in range(3):
            try:
                if quote.do_search_subscription_quote(subscription_id, quote_name) is True:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_" +
                                                marketplace_name + "_" +
                                                "search_subscription_successful.png")
                    break
                else:
                    raise Exception
            except Exception as ex:
                self.logger.info("Not able to search order and get into it. retry- %s" % str(retry))
                if retry == 2:
                    self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" +
                                                marketplace_name + "_" +
                                                "search_subscription_error.png")
                    self.logger.exception(ex)
                    raise ex
                self.driver.refresh()
                self.is_sign_in_button_shown()

    # def validate_vendor_subs_id_from_quote(self, ven_subs_id):
    #     quote = QuotePage(self.driver)
    #     try:
    #         if ven_subs_id == quote.get_vendor_subs_id(ven_subs_id):
    #             self.logger.info("Vendor subscription id received in IM360")
    #             return True
    #         else:
    #             return False
    #     except Exception as ex:
    #         self.logger.error("Something went wrong wile getting Vendor Subscription ID")
    #         self.driver.save_screenshot(".\\ScreenShots\\" + "getVendorSubsIDFromQuote_error.png")
    #         self.logger.exception(ex)
    #         raise ex

    """ This method validates vendor subscription id in IM360 and DB """

    def validate_vendor_subs_id_from_quote(self, ven_subs_id, test_case_id, marketplace_name):
        quote = QuotePage(self.driver)
        try:
            im360_ven_subs_id = quote.get_vendor_subs_id()
            self.logger.info("Vendor subscription id in database %s", ven_subs_id)
            self.logger.info("Vendor subscription id received from IM360 %s", im360_ven_subs_id)
            assert im360_ven_subs_id == ven_subs_id
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id +
                                        "_" + marketplace_name + "_" +
                                        "getVendorSubsIDFromQuote_successful.png")
        except Exception as ex:
            self.logger.error("Something went wrong wile getting Vendor Subscription ID")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_case_id + "_" + marketplace_name
                + "_" +
                "getVendorSubsIDFromQuote_error.png")
            self.logger.exception(ex)
            raise ex

    """ This method searches subscription if found saves in db or else throws error """

    def get_subscription_id_or_error_from_quote_list(self, quote_name, test_case_id, marketplace_name,
                                                     im360_subs_output_data):
        order = OrderPage(self.driver)
        quote = QuotePage(self.driver)
        sub_id = None
        try:
            if order.is_subscription_created():
                sub_id = quote.do_read_subscription_id_from_all_subscription_list_view(quote_name)
                im360_subs_output_data.subscription_id = sub_id
                self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + test_case_id + "_"
                                            + marketplace_name + "_" +
                                            "get subscription_id_successful.png")
            else:
                order.get_error_on_subscription()
        except Exception as e:
            self.logger.info("Not able to get subscription id or error message")
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + test_case_id +
                                        "_" + marketplace_name + "_" +
                                        "error_on_subscription_error.png")
            self.logger.error("Failed to get subscription id or error message")
            self.logger.exception(e)
            raise e

    """ This method logs out from IM360 """

    def logout(self):
        logout = DashboardPage(self.driver)
        try:
            logout.do_logout_from_im360()
            self.logger.info("Successfully logged out from IM360")
            self.logger.info("Closing the browser")
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Success\\" + "logout_successful.png")
            # logout.close_browser_driver()
            # self.logger.info("Closed the browser instance")
        except Exception as e:
            self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + "logout_error.png")
            self.logger.error("Logout unsuccessful!!")
            self.logger.exception(e)
            raise e

    """ This method is used to search and selects the order """

    def select_order(self):
        order = OrderPage(self.driver)
        order.select_order(quote_name=None, screen_shot=None, test_case_id=None, marketplace_name=None)

    """ This method checks for subscription status """

    def get_subscription_status(self):
        order = OrderPage(self.driver)
        return order.do_get_subscription_status(test_case_id=None, marketplace_name=None)

    """ This method is used to handle sign-in popups during automation if apprears clicks on sign in button """

    def is_sign_in_button_shown(self):
        create_draft_quote = QuotePage(self.driver)
        for retry in range(4):
            try:
                create_draft_quote.is_sign_in_button_shown_click_on_button()
                self.logger.info("Successfully click on Sign In button")
                break
            except Exception as e:
                self.logger.error("Trying to click on Sign in button. Attempt: " + str(retry + 1))
                self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + "sign_in_error.png")
                self.logger.error("Failed to click on Sign in button")
                self.logger.exception(e)
                raise e
        return True

    """ This method is used to add the new sku and activate the quote for change order """

    def add_new_sku_and_activate_quote(self, db_file_path, test_data, items_data, im360_subs_output_data,
                                       existing=True):
        activate_quote = QuotePage(self.driver)
        try:
            if existing is True:
                self.search_quote(test_data)
            self.logger.info("Filtering the items")
            filtered_item_data = self.filtered_items_by_testcase_id_and_item_type_and_order_action(db_file_path,
                                                                                                   test_data,
                                                                                                   items_data)
            self.logger.info("Getting %s items after filtering", str(len(filtered_item_data)))
            activate_quote.activate_quote(filtered_item_data,
                                          test_data.get("service_plan_id"),
                                          test_data.get("billing_period"),
                                          (str(test_data.get("subscription_period")).strip() +
                                           " " +
                                           (test_data.get("subscription_period_unit")).strip()),
                                          )
            self.logger.info("Quote Activated Successfully!!")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" +
                "activate_quote_successful.png")
            return activate_quote.is_quote_on_credit_hold()
        except Exception as e:
            self.logger.error("Exception occurred in activate_quote %s", e)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "activate_quote_error.png")
            self.logger.error("Failed to activate quote with quote name: " + str(test_data.get("quote_name")))
            raise e

    """ This method is used to add the new sku and activate the quote for change order """

    def add_sku_with_change_plan_id(self, db_file_path, test_data, items_data):
        activate_quote = QuotePage(self.driver)
        try:
            self.logger.info("Filtering the items")
            filtered_item_data = self.filtered_items_by_testcase_id_and_item_type(db_file_path, test_data, items_data)
            self.logger.info("Getting %s items after filtering", str(len(filtered_item_data)))
            activate_quote.activate_quote(filtered_item_data,
                                          test_data.get("change_service_plan_id"),
                                          test_data.get("change_billing_period"),
                                          (str(test_data.get("subscription_period")).strip() +
                                           " " +
                                           (test_data.get("subscription_period_unit")).strip()),
                                          )
            self.logger.info("Quote Activated Successfully!!")
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "activate_quote_successful.png")
            return activate_quote.is_quote_on_credit_hold()
        except Exception as e:
            self.logger.error("Exception occurred in activate_quote %s", e)
            self.driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + test_data.get("test_case_id") + "_" +
                test_data.get("marketplace_name") + "_" + "activate_quote_error.png")
            self.logger.error("Failed to activate quote with quote name: " + str(test_data.get("quote_name")))
            raise e

    def is_error_popup_shown(self):
        create_draft_quote = QuotePage(self.driver)
        for retry in range(4):
            try:
                create_draft_quote.is_error_popup_shown_click_it()
                break
            except Exception as e:
                self.logger.error("Trying to click on OK button. Attempt: " + str(retry + 1))
                self.driver.save_screenshot(self.screen_shot_path + "\\IM360\\Error\\" + "error_popup_click_error.png")
                self.logger.error("Failed to click on OK button")
                self.logger.exception(e)
                raise e
        return True
