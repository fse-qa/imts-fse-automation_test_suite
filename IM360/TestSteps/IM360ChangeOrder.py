from CommonUtilities.logGeneration import LogGenerator
from IM360.Pages.IM360ManageSubscriptionPage import IM360ManageSubscriptionPage


class IM360ChangeOrder:
    logger = LogGenerator.logGen()

    """ This method cancels the subscription """

    def cancel_subscription(self, driver, test_case_id):
        cancel_subs = IM360ManageSubscriptionPage(driver)
        cancel_subs.do_cancel_subscription(test_case_id)

    """ This method validates the cancelled subscription """

    def validate_cancellation(self, driver, test_case_id):
        cancel_subs = IM360ManageSubscriptionPage(driver)
        cancel_subs.do_validate_cancellation(test_case_id)

    """ This method changes the term of subscription """

    def change_term_of_subscription(self, driver, test_case_id):
        change_term = IM360ManageSubscriptionPage(driver)
        change_term.do_change_payment_term(test_case_id)

    """ This method add new sku of subscription """

    def add_new_sku_of_subscription(self, driver, test_case_id):
        change_term = IM360ManageSubscriptionPage(driver)
        change_term.do_add_new_sku(test_case_id)

    """This method increase the quantity of products"""

    def increase_qty_of_products(self, driver, test_case_id):
        change_qty = IM360ManageSubscriptionPage(driver)
        change_qty.do_increase_qty_of_products(test_case_id)

    """This method decrease the quantity of products"""

    def decrease_qty_of_products(self, driver, test_case_id):
        change_qty = IM360ManageSubscriptionPage(driver)
        change_qty.do_decrease_qty_of_products(test_case_id)

    """ This method Switch plan to change billing frequency monthly - yearly"""

    def switch_plan(self, driver, test_case_id):
        change_term = IM360ManageSubscriptionPage(driver)
        change_term.switch_plan(test_case_id)

    """ This method Changes Contract Term"""

    def change_contract_term(self, driver, test_case_id):
        change_term = IM360ManageSubscriptionPage(driver)
        change_term.do_change_contract_term(test_case_id)
