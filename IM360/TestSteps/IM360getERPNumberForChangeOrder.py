from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.readProperties import ReadConfig
from IM360.Pages.IM360OrderPage import OrderPage
from IM360.TestSteps.createOrder import CreateOrder
from db.model.IM360Status import IM360Status
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360ItemDbManagementService import IM360ItemDbManagementService
from db.service.IM360StatusDbManagementService import IM360StatusDbManagementService
from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService


class GetERPNumberForChangeOrder:
    logger = LogGenerator.logGen()
    db_file_path = ReadConfig.get_db_file_path()
    im360_parameter_data_list = []
    im360_status_list = []
    screen_shot = {"path": " "}
    screen_shot_path = ReadConfig.getScreenshotPath()
    """
    This method will search for the order using the quote name and then release subscription on hold
    and gets the erp order number from the summary tab and updates the same in IM360_subscription table.
    """

    def get_erp_number_for_change_order_by_test_case_id(self, driver, input_test_case_id):
        create_order_steps = CreateOrder(driver)
        im360_input_order_db_mgmt_srv = IM360InputOrderDbManagementService()
        im360_subs_db_mgmt_srv = IM360SubscriptionDbManagementService()
        im360_item_db_mgmt_srv = IM360ItemDbManagementService()
        order_management_srv_obj = IM360InputOrderDbManagementService()
        im360_status_management_srv_obj = IM360StatusDbManagementService()
        order = OrderPage(driver)
        try:
            self.logger.info("Getting quote name from im360_input_order table")
            input_quote_name = im360_input_order_db_mgmt_srv.get_quote_name_by_test_case_id(self.db_file_path,
                                                                                            input_test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             input_test_case_id)
            test_data = order_management_srv_obj.get_im360_input_test_case_order_detail(
                self.db_file_path,
                input_test_case_id)
            self.logger.info("Quote name successfully fetched from im360_input_order table")
            self.logger.info("Searching order to get ERP order no")
            create_order_steps.search_and_select_the_credit_hold_order(input_quote_name, input_test_case_id,
                                                                       marketplace_name, self.screen_shot)
            erp_order = order.get_erp_order_number(self.screen_shot, input_test_case_id, marketplace_name)
            order_stat = False
            for retry_status in range(2):
                order_stat = order.is_order_submitted_successfully()

                if order_stat:
                    break
                if retry_status < 2:
                    self.driver.refresh()
                    create_order_steps.is_sign_in_button_shown()
            if order_stat:
                order.release_modified_subscription_from_hold()

            self.logger.info("Fetching the Subscription ID")
            subscription_id = create_order_steps.get_subscription_id_or_error(test_data)
            self.logger.info("Successfully fetched the Subscription ID %s. Updating the "
                             "table im360_subscription", subscription_id)

            im360_subs_max_id = im360_subs_db_mgmt_srv.get_max_id_by_test_case_id(
                self.db_file_path, input_test_case_id)
            self.logger.info(im360_subs_max_id)

            im360_subs_db_mgmt_srv.update_order_id(self.db_file_path, input_test_case_id, erp_order,
                                                   im360_subs_max_id)
            self.logger.info("Successfully update the im360_subscription table. Updating the im360_item table")

            im360_item_db_mgmt_srv.update_row(self.db_file_path, input_test_case_id, subscription_id)
            self.logger.info("Successfully update the im360_item table.")

            """adding data to im360status table"""
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        input_test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     input_test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               input_test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           input_test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             input_test_case_id)
            im360_status_obj = IM360Status(input_test_case_id, testcase_name, "success", " ", service_name, " ",
                                           reseller_currency, vendor_currency, marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Success\\" + input_test_case_id + "_" + marketplace_name
                + "_" + "get erp order Id.png")

        except Exception as ex:
            self.logger.info("Exception occurred while searching order for getting ERP order Id and modifying the "
                             "database. retry- %s" % str(ex))
            testcase_name = order_management_srv_obj.get_test_case_name_by_test_case_id(self.db_file_path,
                                                                                        input_test_case_id)
            service_name = order_management_srv_obj.get_service_name_by_test_case_id(self.db_file_path,
                                                                                     input_test_case_id)
            reseller_currency = order_management_srv_obj.get_reseller_currency_by_test_case_id(self.db_file_path,
                                                                                               input_test_case_id)
            vendor_currency = order_management_srv_obj.get_vendor_currency_by_test_case_id(self.db_file_path,
                                                                                           input_test_case_id)
            marketplace_name = order_management_srv_obj.get_marketplace_name_by_test_case_id(self.db_file_path,
                                                                                             input_test_case_id)
            driver.save_screenshot(
                self.screen_shot_path + "\\IM360\\Error\\" + input_test_case_id + "_" + marketplace_name
                + "_" + "get erp order number error.png")
            self.screen_shot["path"] = self.screen_shot_path + "\\IM360\\Error\\" + "get erp order number error.png"
            im360_status_obj = IM360Status(input_test_case_id, testcase_name, "failed", repr(ex), service_name,
                                           str(self.screen_shot["path"]), reseller_currency, vendor_currency,
                                           marketplace_name)
            self.im360_status_list.append(im360_status_obj)
            im360_status_management_srv_obj.save_im360_status(self.db_file_path, self.im360_status_list)
            create_order_steps.is_error_popup_shown()
            create_order_steps.is_sign_in_button_shown()
            raise ex
