from IM360.RestAPI.baseAPI import BaseAPI
from CommonUtilities.readProperties import ReadConfig
import apiBaseURLs


class CloudblueSimpleAPI(BaseAPI):

    get_subscription_details_by_id_base_url = "https://api.rbtest.int.zone/api/v1/subscriptions/"
    get_cb_orders_by_subs_id_base_url = "https://api.rbtest.int.zone/api/v1/orders?subscriptionId="

    def get_subscription_details(self, sub_id):
        token = self.get_token(ReadConfig.getCBAuthTokenUrl(),
                               ReadConfig.getCBAuthTokenUsername(),
                               ReadConfig.getCBAuthTokenPassword())
        url = self.get_subscription_details_by_id_base_url + str(sub_id)
        response = self.send_request(token["token"], url)
        return response

    def get_cb_orders_by_subscription_id(self, sub_id):
        token = self.get_token(ReadConfig.getCBAuthTokenUrl(),
                               ReadConfig.getCBAuthTokenUsername(),
                               ReadConfig.getCBAuthTokenPassword())
        url = self.get_cb_orders_by_subs_id_base_url + str(sub_id)
        response = self.send_request(token["token"], url)
        return response

    def get_im_subscription_status_by_cb_subscription_id(self,subscription_id):
        token = self.get_cb_token(ReadConfig.getCBAPIUrl(),
                                  ReadConfig.getCBAPIUsername(),
                                  ReadConfig.getCBAPIPassword())
        subscription_status_url = ReadConfig.getCBAPIUrl() + apiBaseURLs.CB_COMMERCE_SUBSCRIPTION_ID
        response = self.send_api_request()
        # return response









