from CommonUtilities.baseSet.BaseFacade import BaseFacade
from IM360.RestAPI.cloudblueSimpleAPI import CloudblueSimpleAPI


class GetOrderStatus(BaseFacade):

    def get_order_status(self, subs_id, order_id):
        response = CloudblueSimpleAPI().get_cb_orders_by_subscription_id(subs_id)
        order_list = response.get("data")
        for order in order_list:
            if order.get("id") == order_id:
                status = order.get("status")
                self.logger.info(f"order status of subs id = {subs_id} is {status}")
                return status

