from CommonUtilities.logGeneration import LogGenerator
from db.model.IM360InputItem import IM360InputItem
from db.model.IM360InputOrder import IM360InputOrder


class PrepareObject:
    logger = LogGenerator.logGen()

    def __init__(self, driver):
        self.driver = driver

    def prepare_im360_inp_ord_data_obj(self, test_data):
        im360_input_order_obj = IM360InputOrder(test_data.get("TestCaseId"), test_data.get("TestCaseName"),
                                                test_data.get("QuoteName"),
                                                test_data.get("ResellerBCN"), test_data.get("Contact"),
                                                test_data.get("PaymentTerm"), test_data.get("BillTo"),
                                                test_data.get("DealID"), test_data.get("BillingPeriod"),
                                                test_data.get("SubscriptionPeriodunit"),
                                                test_data.get("SubscriptionPeriodvalue"),
                                                test_data.get("ServicePlanID"), test_data.get("ResellerPO"),
                                                test_data.get("PrimaryVendor"), test_data.get("CompanyID"),
                                                test_data.get("CompanyName"), test_data.get("ContactPhone"),
                                                test_data.get("ContactName"), test_data.get("ContactEmail"),
                                                test_data.get("Address"), test_data.get("City"),
                                                test_data.get("PostalCode"), test_data.get("State"),
                                                test_data.get("WebOrderID"),
                                                test_data.get("RequestedShipStartDate"),
                                                test_data.get("AgreementID"),
                                                test_data.get("ProvisioningContactName"),
                                                test_data.get("ProvisioningContactEmail"),
                                                test_data.get("ProvisioningContactPhone"),
                                                test_data.get("VendorPortalSubmission"),
                                                test_data.get("SavedCreditCardName"),
                                                test_data.get("CC_FirstName"), test_data.get("CC_LastName"),
                                                test_data.get("CC_Email"), test_data.get("CC_CardType"),
                                                test_data.get("CC_CardNumber"), test_data.get("CC_ExpirationMonth"),
                                                test_data.get("CC_ExpirationYear"), test_data.get("CC_CVN"),
                                                test_data.get("ResellerCurrency"), test_data.get("VendorCurrency"),
                                                test_data.get("ServiceName"), test_data.get("MarketplaceName"),
                                                test_data.get("ChangeServicePlanID"),
                                                test_data.get("ChangeBillingPeriod"),
                                                test_data.get("ChangeSubscriptionPeriod"),
                                                test_data.get("ChangePaymentTerm"), test_data.get("ChangeDealID"),
                                                test_data.get("TestType"))
        return im360_input_order_obj

    def prepare_im360_inp_item_data_obj(self, item_data, im360_input_order_data_tbl_id):
        im360_input_item_obj = IM360InputItem(item_data.get("TestCaseId"), item_data.get("ItemMpn"),
                                              item_data.get("Quantities"), item_data.get("VendorSku"),
                                              item_data.get("ItemType"), item_data.get("UnitOfMeasure"),
                                              item_data.get("UsageAmount"), item_data.get("OrderAction"),
                                              im360_input_order_data_tbl_id)
        return im360_input_item_obj

    def set_key_for_im360_output_data(self, value, list=[]):
        for lst in list:
            lst.im360_subscription_tbl_id = value
