from db.database_operations import DataOperations
from db.util.artifact_operations import ArtifactOperations


class TestArchiveArtifacts:
    data_operations = DataOperations()
    artifact_operations = ArtifactOperations()

    def test_rebase_database(self):
        self.data_operations.create_database_backup()
        self.data_operations.truncate_database_data()

    def test_archive_logs(self):
        self.artifact_operations.archive_logs()

    def test_archive_reports(self):
        self.artifact_operations.archive_reports()

    def test_archive_screenshots(self):
        self.artifact_operations.archive_screenshots()
