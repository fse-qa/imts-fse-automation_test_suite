import pytest
from parameterized import parameterized

from CommonUtilities.parse_config import ParseConfigFile
from Tests.cbConnect.test_case_base_connect import TestCaseBaseConnect
from Tests.test_script_constant import TestCaseID
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService


class TestConnectCancelOrderApproval:
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path", "config.json")
    test_type = ParseConfigFile().get_data_from_config_json("testType", "test_type", "config.json")
    im360_inp_ord_db_mgmt_srv = IM360InputOrderDbManagementService()
    marketplaces = im360_inp_ord_db_mgmt_srv.get_distinct_marketplaces(db_file_path)

    def approve_order(self, name, marketplace):
        if not self.im360_inp_ord_db_mgmt_srv.count_marketplaces_testcase(self.db_file_path, marketplace, name):
            pytest.skip("There is no record for test case ID " + name + " for marketplace " + marketplace)

        if self.im360_inp_ord_db_mgmt_srv.get_test_type_by_test_case_id(self.db_file_path, name) != self.test_type:
            pytest.skip("Skipping test case ID " + name + " as it is not belongs to test_type " + self.test_type
                        + "  or no data present in the database")

        test_case_base = TestCaseBaseConnect(name)
        test_case_base.approve_cancel_order(marketplace)

    @parameterized.expand(marketplaces)
    def test_cancel_order_approval_tc093(self, marketplace):
        self.approve_order(TestCaseID.TS093, marketplace)

    @parameterized.expand(marketplaces)
    def test_cancel_order_approval_tc022(self, marketplace):
        self.approve_order(TestCaseID.TS022, marketplace)
