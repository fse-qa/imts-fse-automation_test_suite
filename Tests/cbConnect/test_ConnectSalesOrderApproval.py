import pytest
from parameterized import parameterized

from CommonUtilities.parse_config import ParseConfigFile
from Tests.cbConnect.test_case_base_connect import TestCaseBaseConnect
from Tests.test_script_constant import TestCaseID
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService


class TestConnectSalesOrderApproval:
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path", "config.json")
    test_type = ParseConfigFile().get_data_from_config_json("testType", "test_type", "config.json")
    im360_inp_ord_db_mgmt_srv = IM360InputOrderDbManagementService()
    marketplaces = im360_inp_ord_db_mgmt_srv.get_distinct_marketplaces(db_file_path)

    def approve_order(self, name, marketplace):
        if not self.im360_inp_ord_db_mgmt_srv.count_marketplaces_testcase(self.db_file_path, marketplace, name):
            pytest.skip("There is no record for test case ID " + name + " for marketplace " + marketplace)

        if self.im360_inp_ord_db_mgmt_srv.get_test_type_by_test_case_id(self.db_file_path, name) != self.test_type:
            pytest.skip("Skipping test case ID " + name + " as it is not belongs to test_type " + self.test_type
                        + "  or no data present in the database")

        test_case_base = TestCaseBaseConnect(name)
        test_case_base.approve_sales_order(marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc001(self, marketplace):
        self.approve_order(TestCaseID.TS001, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc026(self, marketplace):
        self.approve_order(TestCaseID.TS026, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc093(self, marketplace):
        self.approve_order(TestCaseID.TS093, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc031(self, marketplace):
        self.approve_order(TestCaseID.TS031, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc017(self, marketplace):
        self.approve_order(TestCaseID.TS017, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc041(self, marketplace):
        self.approve_order(TestCaseID.TS041, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc005(self, marketplace):
        self.approve_order(TestCaseID.TS005, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc022(self, marketplace):
        self.approve_order(TestCaseID.TS022, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc101(self, marketplace):
        self.approve_order(TestCaseID.TS101, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc116(self, marketplace):
        self.approve_order(TestCaseID.TS116, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc134(self, marketplace):
        self.approve_order(TestCaseID.TS134, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc139(self, marketplace):
        self.approve_order(TestCaseID.TS139, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc002(self, marketplace):
        self.approve_order(TestCaseID.TS002, marketplace)
