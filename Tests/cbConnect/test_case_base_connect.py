from CBConnect.TestSteps.ApproveCancelOrder import ApproveCancelOrder
from CBConnect.TestSteps.ApproveChangeOrder import ApproveChangeOrder
from CBConnect.TestSteps.ApproveSalesOrder import ApproveSalesOrder


class TestCaseBaseConnect:
    def __init__(self, name) -> None:
        self.name = name

    def approve_sales_order(self, marketplace):
        approve_sales_order = ApproveSalesOrder()
        approve_sales_order.execute(self.name, marketplace)

    def approve_cancel_order(self, marketplace):
        approve_cancel_order = ApproveCancelOrder()
        approve_cancel_order.execute(self.name, marketplace)

    def approve_change_order(self, marketplace):
        approve_cancel_order = ApproveChangeOrder()
        approve_cancel_order.execute(self.name, marketplace)
