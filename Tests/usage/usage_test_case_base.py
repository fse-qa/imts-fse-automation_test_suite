from Usage.TestSteps.usageUpload import UploadUsageForSubscription


class UsageTestCaseBase:

    def __init__(self, name) -> None:
        self.name = name

    """
    This method upload the usage for subscription depending CB date movement
    Author: Soumi Ghosh
    Param: N/A 
    """

    def upload_usage_for_subscription(self):
        usage_upload = UploadUsageForSubscription()
        usage_upload.create_usage_file_and_upload_in_cb_connect(self.name,'')

    """
    This method upload the usage for cancelled subscription for cancellation period depending CB date movement
    Author: Soumi Ghosh
    Param: N/A 
    """

    def upload_usage_for_cancelled_subscription(self):
        usage_upload = UploadUsageForSubscription()
        usage_upload.create_usage_file_and_upload_in_cb_connect(self.name, 'Intermittent')
