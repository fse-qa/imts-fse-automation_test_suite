from Tests.test_script_constant import TestCaseID
from Tests.usage.usage_test_case_base import UsageTestCaseBase

"""
Pytest Command : pytest -rA -v Tests/usage/test_uploadUsageForChangeMonth.py
"""


def do_upload_usage_for_subscription(name):
    usage_upload = UsageTestCaseBase(name)
    usage_upload.upload_usage_for_subscription()


def do_upload_usage_for_cancelled_subscription(name):
    usage_upload = UsageTestCaseBase(name)
    usage_upload.upload_usage_for_cancelled_subscription()


class TestUploadUsageForChangeMonth:

    """
    Batch 1 Test Script List
    """

    def test_upload_usage_for_ts026(self):
        do_upload_usage_for_subscription(TestCaseID.TS026)

    """
    Batch 2 Sprint 1 Test Script List
    """

    def test_upload_usage_for_ts022(self):
        do_upload_usage_for_cancelled_subscription(TestCaseID.TS022)

    """
    Batch 3 Sprint 1 Test Script List
    """

    def test_upload_usage_for_ts017(self):
        do_upload_usage_for_subscription(TestCaseID.TS017)

    def test_upload_usage_for_ts031(self):
        do_upload_usage_for_subscription(TestCaseID.TS031)

    def test_upload_usage_for_ts041(self):
        do_upload_usage_for_subscription(TestCaseID.TS041)

    """
    Batch 3 Sprint 2 Test Script List
    """

    def test_upload_usage_for_ts139(self):
        do_upload_usage_for_subscription(TestCaseID.TS139)

    def test_upload_usage_for_ts134(self):
        do_upload_usage_for_subscription(TestCaseID.TS134)