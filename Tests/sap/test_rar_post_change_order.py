from SAP.Facade.FilterOperations import Filter
from SAP.Pages.RR_SAP_Execution import SapGui
from Tests.sap.test_base_sap import TestSAPBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestRarPostChangeOrder(BaseTest, TestSAPBase):
    fetch_tc_status = True
    current_pytest = 'test_rar_post_change_order'
    last_pytest = 'test_change_order_operations'

    def trigger_operations(self, tc_id):
        image_path = '../../Output'
        sub_id = None
        self.rar_details = {}
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)
            sub_id = Filter().fetch_subscription_id(tc_id)
            self.logger.info(f'Subscription id for {tc_id} is {sub_id}.')

            self.rar_details = SapGui().sapLoginfor_rar(image_path, tc_id, sub_id)

            if self.rar_details["pob_price"] == self.rar_details["pc_tcv"] and self.rar_details["pob_cost"] == \
                    self.rar_details["pc_commit_cost"]:
                self.logger.info("Values are same on both columns.RAR process is completed successfully" + "PCTCV: " +
                                 self.rar_details["pc_tcv"] + "PCCOMMITCOST:" + self.rar_details[
                                     "pc_commit_cost"] + "POBPRICE :"
                                 + self.rar_details["pob_price"] + "POBCOST:" + self.rar_details["pob_cost"])
                self.update_rar(tc_id, sub_id, self.rar_details, status='Pass', error_reason='')
            else:
                self.logger.info(
                    "Values are not same on both columns.RAR process is not completed successfully" + "PCTCV: " +
                    self.rar_details["pc_tcv"] + "PCCOMMITCOST:" + self.rar_details["pc_commit_cost"] + "POBPRICE :" +
                    self.rar_details[
                        "pob_price"] + "POBCOST:" + self.rar_details["pob_cost"])
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to perform SAP Operations! " + str(e))
            self.update_rar(tc_id, sub_id, self.rar_details, status='Fail', error_reason=str(e))
            raise e

    def test_rar_tc093(self):
        self.trigger_operations(TestCaseID.TS093)

    def test_rar_tc022(self):
        self.trigger_operations(TestCaseID.TS022)

    def test_rar_tc031(self):
        self.trigger_operations(TestCaseID.TS031)

    def test_rar_tc041(self):
        self.trigger_operations(TestCaseID.TS041)

    def test_rar_tc017(self):
        self.trigger_operations(TestCaseID.TS017)

    def test_rar_tc134(self):
        self.trigger_operations(TestCaseID.TS134)

    def test_rar_tc139(self):
        self.trigger_operations(TestCaseID.TS139)
