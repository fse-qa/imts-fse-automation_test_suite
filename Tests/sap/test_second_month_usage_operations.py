import pytest

from SAP.TestSteps.BaseTestMethods import BaseTestMethods
from Tests.sap.test_base_sap import TestSAPBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestSecondMonthUsageOperations(BaseTest, TestSAPBase):
    fetch_tc_status = True
    current_pytest = 'test_second_month_usage_operations'
    last_pytest = 'test_second_minimum_commit_operations'

    def trigger_operations(self, tc_id) -> None:
        """ This method performs the second usage bits validation. """
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)

            sub_id = self.get_subscription_id(tc_id)

            base_test_methods_obj = BaseTestMethods(tc_id, sub_id)
            if not base_test_methods_obj.perform_operations_on_bits(bit_type='usage'):
                raise Exception('Usage Bits Validation Failed!!')

        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to perform SAP second month Usage Operations! " + str(e))
            raise e

    @pytest.mark.test
    def test_second_month_usage_operation_tc022(self):
        self.trigger_operations(TestCaseID.TS022)

    @pytest.mark.test
    def test_second_month_usage_operation_tc017(self):
        self.trigger_operations(TestCaseID.TS017)

    @pytest.mark.test
    def test_second_month_usage_operation_tc026(self):
        self.trigger_operations(TestCaseID.TS026)

    @pytest.mark.test
    def test_second_month_usage_operation_tc031(self):
        self.trigger_operations(TestCaseID.TS031)

    @pytest.mark.test
    def test_second_month_usage_operation_tc041(self):
        self.trigger_operations(TestCaseID.TS041)

    @pytest.mark.test
    def test_second_month_usage_operation_tc134(self):
        self.trigger_operations(TestCaseID.TS134)

    @pytest.mark.TS139
    def test_second_month_usage_operation_tc0139(self):
        self.trigger_operations(TestCaseID.TS139)
