from SAP.TestSteps.BaseTestMethods import BaseTestMethods
from Tests.sap.test_base_sap import TestSAPBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestFirstMonthUsageOperations(BaseTest, TestSAPBase):
    fetch_tc_status = True
    current_pytest = 'test_first_month_usage_operations'
    last_pytest = 'test_first_minimum_commit_operations'
    sub_id = None

    def trigger_operations(self, tc_id) -> None:
        """ This method performs the first usage bits validation. """
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)

            self.sub_id = self.get_subscription_id(tc_id)

            base_test_methods_obj = BaseTestMethods(tc_id, self.sub_id)
            if not base_test_methods_obj.perform_operations_on_bits(bit_type='usage'):
                raise Exception('Usage Bits Validation Failed!!')

        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to perform SAP first month Usage Operations! " + str(e))
            raise e

    def test_first_month_usage_operation_tc001(self):
        self.trigger_operations(TestCaseID.TS001)

    def test_first_month_usage_operation_tc026(self):
        self.trigger_operations(TestCaseID.TS026)

    def test_first_month_usage_operation_tc093(self):
        self.trigger_operations(TestCaseID.TS093)

    def test_first_month_usage_operation_tc005(self):
        self.trigger_operations(TestCaseID.TS005)

    def test_first_month_usage_operation_tc022(self):
        self.trigger_operations(TestCaseID.TS022)

    def test_first_month_usage_operation_tc116(self):
        self.trigger_operations(TestCaseID.TS116)

    #     assert self.get_usage_amount(self.sub_id, 'TSR2') == 0.0
    #     assert self.get_usage_amount(self.sub_id, 'TSV2') == 0.0

    def test_first_month_usage_operation_tc101(self):
        self.trigger_operations(TestCaseID.TS101)

    def test_first_month_usage_operation_tc031(self):
        self.trigger_operations(TestCaseID.TS031)

    def test_first_month_usage_operation_tc041(self):
        self.trigger_operations(TestCaseID.TS041)

    def test_first_month_usage_operation_tc017(self):
        self.trigger_operations(TestCaseID.TS017)

    def test_first_month_usage_operation_tc139(self):
        self.trigger_operations(TestCaseID.TS139)

    def test_first_month_usage_operation_tc134(self):
        self.trigger_operations(TestCaseID.TS134)

    def test_first_month_usage_operation_tc002(self):
        self.trigger_operations(TestCaseID.TS002)
