from SAP.Facade.SAPOperations import SAPOperations
from SAP.Pages.RR_SAP_Execution import SapGui
from SAP.Pages.billing import Billing
from SAP.Pages.invoicing import Invoicing


class SAPTestBase:

    def __init__(self, name=None) -> None:
        self.name = name
        self.sap_gui = SapGui()

    def login(self):
        self.sap_gui.login()

    def logout(self):
        self.sap_gui.log_off_sap()

    def bill_provider_contract(self, provider_contract, billable_bit_class, bill_from=None):
        """Bills document for the given provider contract

        Args:
            provider_contract (str): Provider contract number, which is same as IM Subscription ID
            billable_bit_class (str): Bit class based on whether it is reseller or vendor
            bill_from (str): Date on which you want the document to be billed

        Returns:
            str: Billing document generated for the given provider contract
        """
        try:
            billing_document_number = None
            billing = Billing(session=self.sap_gui.session,
                              provider_contract=provider_contract,
                              billable_bit_class=billable_bit_class,
                              bill_from=bill_from)
            if billing.fill_display_of_billable_items():
                billing_document_number = billing.bill_provider_contract()
            billing.exit_display_of_billable_items()
            return billing_document_number
        except Exception as ex:
            SAPOperations().take_screenshot(f'billing_failed_{provider_contract}', status='error')
            raise ex

    def invoice_provider_contract(self, provider_contract, billable_bit_class, bill_from=None):
        """Invoices document for the given provider contract

        Args:
            provider_contract (str): Provider contract number, which is same as IM Subscription ID
            billable_bit_class (str): Bit class based on whether it is reseller or vendor
            bill_from (str): Date on which you want the document to be invoiced

        Returns:
            (str, str): Reconciliation Key and Invoice Document Number
        """
        try:
            invoice_document_number, recon_key = None, None
            invoicing = Invoicing(session=self.sap_gui.session,
                                  provider_contract=provider_contract,
                                  billable_bit_class=billable_bit_class,
                                  bill_from=bill_from)
            if invoicing.fill_display_of_billable_items():
                invoice_document_number, recon_key = invoicing.invoice_provider_contract()
            invoicing.exit_display_of_billable_items()
            return invoice_document_number, recon_key
        except Exception as ex:
            SAPOperations().take_screenshot(f'invoicing_failed_{provider_contract}', status='error')
            raise ex
