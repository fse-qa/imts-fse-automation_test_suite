from SAP.TestSteps.BaseTestMethods import BaseTestMethods
from Tests.sap.test_base_sap import TestSAPBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestFirstMinimumCommitOperations(BaseTest, TestSAPBase):
    fetch_tc_status = True
    current_pytest = 'test_first_minimum_commit_operations'
    last_pytest = 'test_initial_order_billing_invoicing'

    def trigger_operations(self, tc_id, check_bits_empty=False) -> None:
        """ This method performs the first min commit bits validation. """
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)

            sub_id = self.get_subscription_id(tc_id)

            base_test_methods_obj = BaseTestMethods(tc_id, sub_id)
            bits_availability_status = base_test_methods_obj.perform_operations_on_bits(bit_type='min commit')
            if check_bits_empty:
                assert not bits_availability_status
                return
            if not bits_availability_status:
                raise Exception('Min Commit Bits Validation Failed!!')

        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to perform SAP first month Min Commit Operations! " + str(e))
            raise e

    def test_first_month_min_commit_operation_tc001(self):
        self.trigger_operations(TestCaseID.TS001)

    def test_first_month_min_commit_operation_tc026(self):
        self.trigger_operations(TestCaseID.TS026)

    def test_first_month_min_commit_operation_tc093(self):
        self.trigger_operations(TestCaseID.TS093)

    def test_first_month_min_commit_operation_tc005(self):
        self.trigger_operations(TestCaseID.TS005)

    def test_first_month_min_commit_operation_tc022(self):
        self.trigger_operations(TestCaseID.TS022)

    def test_first_month_min_commit_operation_tc116(self):
        self.trigger_operations(TestCaseID.TS116)

    def test_first_month_min_commit_operation_tc101(self):
        self.trigger_operations(TestCaseID.TS101, check_bits_empty=True)

    def test_first_month_min_commit_operation_tc031(self):
        self.trigger_operations(TestCaseID.TS031)

    def test_first_month_min_commit_operation_tc041(self):
        self.trigger_operations(TestCaseID.TS041)

    def test_first_month_min_commit_operation_tc017(self):
        self.trigger_operations(TestCaseID.TS017)

    def test_first_month_min_commit_operation_tc139(self):
        self.trigger_operations(TestCaseID.TS139)

    def test_first_month_min_commit_operation_tc134(self):
        self.trigger_operations(TestCaseID.TS134)

    def test_first_month_min_commit_operation_tc002(self):
        self.trigger_operations(TestCaseID.TS002)
