import pytest

from SAP.Facade.CBCOperations import CBCOperations
from Tests.sap.test_base_sap import TestSAPBase
from Tests.sap.test_case_base import SAPTestBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestBillingAndInvoicingPostSecondUsage(BaseTest, TestSAPBase):
    tc_object = None
    fetch_tc_status = True
    current_pytest = 'test_billing_invoicing_post_second_usage'
    last_pytest = 'test_second_month_usage_operations'

    def bill_document(self, provider_contract, billable_bit_class, bill_from, tc_id):
        try:
            self.logger.info("Billing bits for [%s] with bit class [%s]", provider_contract, billable_bit_class)
            billing_document_number = self.tc_object.bill_provider_contract(provider_contract=provider_contract,
                                                                            billable_bit_class=billable_bit_class,
                                                                            bill_from=bill_from
                                                                            )
            if billing_document_number:
                self.logger.info(
                    "Billing Document ID generated for provider contract [%s]: [%s]", provider_contract,
                    billing_document_number)

                self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                                  bill_doc_num=billing_document_number, status='Pass')
        except Exception as ex:
            self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                              error_reason=str(ex))
            raise ex

    def invoice_document(self, provider_contract, billable_bit_class, bill_from, tc_id):
        try:
            self.logger.info("Invoicing billing document for provider_contract [%s] with bit class [%s]",
                             provider_contract, billable_bit_class)
            invoice_document_number, recon_key = self.tc_object.invoice_provider_contract(
                provider_contract=provider_contract,
                billable_bit_class=billable_bit_class,
                bill_from=bill_from
            )
            if invoice_document_number and recon_key:
                self.logger.info(
                    "Invoiced Document ID generated for provider contract [%s]: [%s] with recon_key [%s]",
                    provider_contract,
                    invoice_document_number, recon_key)
                self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                                  invoice_doc_num=invoice_document_number, recon_key=recon_key,
                                                  operation='Invoicing', status='Pass')
        except Exception as ex:
            self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                              operation='Invoicing', error_reason=str(ex))
            raise ex

    def billing_and_invoicing(self, tc_id, billable_bit_class):
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)
            provider_contract = self.get_subscription_id(tc_id)
            bill_from = CBCOperations().get_latest_date()
            self.tc_object = SAPTestBase()
            self.tc_object.login()
            self.bill_document(provider_contract, billable_bit_class, bill_from, tc_id)
            self.invoice_document(provider_contract, billable_bit_class, bill_from, tc_id)
        except Exception as ex:
            raise ex
        finally:
            if self.tc_object:
                self.tc_object.logout()

    def billing_and_invoicing_post_second_usage(self, tc_id):
        self.billing_and_invoicing(tc_id, 'TIN1')
        self.billing_and_invoicing(tc_id, 'TCO1')

    @pytest.mark.test
    def test_billing_invoicing_post_second_usage_tc022(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS022)

    @pytest.mark.test
    def test_billing_invoicing_post_second_usage_tc017(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS017)

    @pytest.mark.test
    def test_billing_invoicing_post_second_usage_tc026(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS026)

    @pytest.mark.test
    def test_billing_invoicing_post_second_usage_tc031(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS031)

    @pytest.mark.test
    def test_billing_invoicing_post_second_usage_tc041(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS041)

    @pytest.mark.test
    def test_billing_invoicing_post_second_usage_tc134(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS134)

    @pytest.mark.TS139
    def test_billing_invoicing_post_second_usage_tc139(self):
        self.billing_and_invoicing_post_second_usage(TestCaseID.TS139)
