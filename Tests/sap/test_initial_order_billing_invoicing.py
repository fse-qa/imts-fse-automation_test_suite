from SAP.Facade.CBCOperations import CBCOperations
from Tests.sap.test_base_sap import TestSAPBase
from Tests.sap.test_case_base import SAPTestBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestInitialOrderBillingAndInvoicing(BaseTest, TestSAPBase):
    tc_object = None
    fetch_tc_status = True
    current_pytest = 'test_initial_order_billing_invoicing'
    last_pytest = 'test_initial_order_operations'

    def bill_document(self, provider_contract, billable_bit_class, bill_from, tc_id):
        try:
            self.logger.info("Billing bits for [%s] with bit class [%s]", provider_contract, billable_bit_class)
            billing_document_number = self.tc_object.bill_provider_contract(provider_contract=provider_contract,
                                                                            billable_bit_class=billable_bit_class,
                                                                            bill_from=bill_from
                                                                            )
            if billing_document_number:
                self.logger.info(
                    "Billing Document ID generated for provider contract [%s]: [%s]", provider_contract,
                    billing_document_number)

                self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                                  bill_doc_num=billing_document_number, status='Pass')
        except Exception as ex:
            self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                              error_reason=str(ex))
            raise ex

    def invoice_document(self, provider_contract, billable_bit_class, bill_from, tc_id):
        try:
            self.logger.info("Invoicing billing document for provider_contract [%s] with bit class [%s]",
                             provider_contract, billable_bit_class)
            invoice_document_number, recon_key = self.tc_object.invoice_provider_contract(
                provider_contract=provider_contract,
                billable_bit_class=billable_bit_class,
                bill_from=bill_from
            )
            if invoice_document_number and recon_key:
                self.logger.info(
                    "Invoiced Document ID generated for provider contract [%s]: [%s] with recon_key [%s]",
                    provider_contract,
                    invoice_document_number, recon_key)
                self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                                  invoice_doc_num=invoice_document_number, recon_key=recon_key,
                                                  operation='Invoicing', status='Pass')
        except Exception as ex:
            self.update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                              operation='Invoicing', error_reason=str(ex))
            raise ex

    def billing_and_invoicing(self, tc_id, billable_bit_class):
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)
            provider_contract = self.get_subscription_id(tc_id)
            bill_from = CBCOperations().get_latest_date()
            self.tc_object = SAPTestBase()
            self.tc_object.login()
            self.bill_document(provider_contract, billable_bit_class, bill_from, tc_id)
            self.invoice_document(provider_contract, billable_bit_class, bill_from, tc_id)
        except Exception as ex:
            raise ex
        finally:
            if self.tc_object:
                self.tc_object.logout()

    def initial_order_billing_and_invoicing(self, tc_id):
        self.billing_and_invoicing(tc_id, 'TIN1')
        self.billing_and_invoicing(tc_id, 'TCO1')

    def test_initial_order_billing_invoicing_tc001(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS001)

    def test_initial_order_billing_invoicing_tc026(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS026)

    def test_initial_order_billing_invoicing_tc093(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS093)

    def test_initial_order_billing_invoicing_tc005(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS005)

    def test_initial_order_billing_invoicing_tc101(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS101)

    def test_initial_order_billing_invoicing_tc022(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS022)

    def test_initial_order_billing_invoicing_tc116(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS116)

    def test_initial_order_billing_invoicing_tc031(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS031)

    def test_initial_order_billing_invoicing_tc041(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS041)

    def test_initial_order_billing_invoicing_tc017(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS017)

    def test_initial_order_billing_invoicing_tc139(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS139)

    def test_initial_order_billing_invoicing_tc134(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS134)

    def test_initial_order_billing_invoicing_tc002(self):
        self.initial_order_billing_and_invoicing(TestCaseID.TS002)
