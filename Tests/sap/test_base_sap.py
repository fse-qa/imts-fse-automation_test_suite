import sys

import pytest

from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.FilterOperations import Filter
from SAP.Facade.IM360Operations import IM360Operations
from SAP.Facade.SAPOperations import SAPOperations
from SAP.Facade.WebAppOperations import WebAppOperations
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService


class TestSAPBase:
    fetch_tc_status = True
    tc_id = None
    current_pytest = 'test_initial_order_operations'
    last_pytest = 'test_initial_order_operations'
    tc_skipped = None

    @classmethod
    def setup_class(cls):
        webapp_obj = WebAppOperations()
        if not webapp_obj.call_health_check_api():
            raise Exception(f'WebApp Health Check failed! Please check if it is Running... ')

    @classmethod
    def check_test_case_status(cls, tc_id, prev_pytest):
        cls.tc_skipped = False
        if cls.fetch_tc_status:
            filter_obj = Filter()
            test_case_status = filter_obj.get_test_case_latest_status(tc_id, prev_pytest)
            if not test_case_status or test_case_status != 'PASSED':
                cls.tc_skipped = True
                pytest.skip(f'The last status for {tc_id} was {test_case_status or "MISSING"}! Skipping the test')

    @staticmethod
    def set_tc_id(value):
        TestSAPBase.tc_id = value

    @classmethod
    def teardown(cls):
        filter_obj = Filter()
        if cls.tc_skipped:
            return
        if not hasattr(sys, "last_type"):
            filter_obj.update_test_case_latest_status(cls.tc_id, cls.current_pytest, 'PASSED')
        else:
            filter_obj.update_test_case_latest_status(cls.tc_id, cls.current_pytest, 'FAILED')

    @staticmethod
    def get_subscription_id(tc_id):
        sub_id = Filter().fetch_subscription_id(tc_id)
        if not sub_id:
            raise Exception(f"Subscription id not found for {tc_id}")
        return sub_id

    @staticmethod
    def get_usage_amount(sub_id, bill_item_type):
        return SAPOperations().fetch_usage_amount(sub_id, bill_item_type)

    @staticmethod
    def get_quantity_for_given_sku(sub_id, sku_part, bill_item_type, number_of_records=1):
        return SAPOperations().get_quantity_for_given_sku(sub_id, sku_part, bill_item_type,
                                                          number_of_records)

    @staticmethod
    def get_amount_and_cost_for_given_sku(sub_id, sku_part, quantity):
        return SAPOperations().get_amount_and_cost_for_given_sku(sub_id, sku_part, quantity)

    @staticmethod
    def get_im360_item_data_for_added_sku(sub_id):
        return IM360Operations().get_im360_item_data_for_added_sku(sub_id)

    @staticmethod
    def get_contract_information(sub_id):
        return SAPOperations().get_contract_information(sub_id)

    @staticmethod
    def update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id, bill_doc_num='NA',
                                     invoice_doc_num='NA', recon_key='NA', operation='Billing', status='Fail',
                                     error_reason=''):
        return Filter().update_billing_and_invoicing(provider_contract, billable_bit_class, bill_from, tc_id,
                                                     bill_doc_num, invoice_doc_num, recon_key, operation, status,
                                                     error_reason)

    @staticmethod
    def update_rar(tc_id, provider_contract, rar_details, status, error_reason):
        return Filter().update_rar(tc_id=tc_id,
                                   provider_contract=provider_contract,
                                   pob_id=rar_details.get("pob_id", 'NA'),
                                   g2n_rev=rar_details.get("g2n_rev", 'NA'),
                                   g2n_cost=rar_details.get("g2n_cost", 'NA'),
                                   pob_price=rar_details.get("pob_price", 'NA'),
                                   pob_cost=rar_details.get("pob_cost", 'NA'),
                                   pc_tcv=rar_details.get("pc_tcv", 'NA'),
                                   pc_commit_cost=rar_details.get("pc_commit_cost", 'NA'),
                                   operation='RAR',
                                   status=status,
                                   error_reason=error_reason)

    @classmethod
    def check_test_type(cls, tc_id):
        cls.tc_skipped = False
        db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path", "config.json")
        test_type = ParseConfigFile().get_data_from_config_json("testType", "test_type", "config.json")
        im360_inp_ord_db_mgmt_srv = IM360InputOrderDbManagementService()
        if im360_inp_ord_db_mgmt_srv.get_test_type_by_test_case_id(db_file_path, tc_id) != test_type:
            cls.tc_skipped = True
            pytest.skip("Test case " + tc_id + " does not belong to " + test_type + " category")
