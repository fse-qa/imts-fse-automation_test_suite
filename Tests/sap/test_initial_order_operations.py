from SAP.TestSteps.BaseTestMethods import BaseTestMethods
from Tests.sap.test_base_sap import TestSAPBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


class TestInitialOrderOperations(BaseTest, TestSAPBase):
    fetch_tc_status = False
    sub_id = None

    def trigger_operations(self, tc_id) -> None:
        """ This method does the subscription validation and if it is fine, it performs the bits validation. """
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)

            self.sub_id = self.get_subscription_id(tc_id)
            self.logger.info(f'Subscription id for {tc_id} is {self.sub_id}.')

            base_test_methods_obj = BaseTestMethods(tc_id, self.sub_id)
            if not base_test_methods_obj.perform_operations_on_subscription():
                raise Exception('Subscription Validation Failed!!')
            else:
                if not base_test_methods_obj.perform_operations_on_bits(bit_type='min commit'):
                    raise Exception('Bits Validation Failed!!')

        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to perform SAP Initial Order Operations! " + str(e))
            raise e

    def test_initial_order_operation_tc001(self):
        self.trigger_operations(TestCaseID.TS001)

    def test_initial_order_operation_tc026(self):
        self.trigger_operations(TestCaseID.TS026)

    def test_initial_order_operation_tc022(self):
        self.trigger_operations(TestCaseID.TS022)

    def test_initial_order_operation_tc093(self):
        self.trigger_operations(TestCaseID.TS093)
        BaseTestMethods(TestCaseID.TS093, self.sub_id).validate_cc_number()

    def test_initial_order_operation_tc005(self):
        self.trigger_operations(TestCaseID.TS005)
        BaseTestMethods(TestCaseID.TS005, self.sub_id).validate_cc_number()

    def test_initial_order_operation_tc101(self):
        self.trigger_operations(TestCaseID.TS101)
        # BaseTestMethods(TestCaseID.TS101, self.sub_id).validate_prepaid_sub() # TODO

    def test_initial_order_operation_tc116(self):
        self.trigger_operations(TestCaseID.TS116)
        BaseTestMethods(TestCaseID.TS116, self.sub_id).validate_cc_number()

    def test_initial_order_operation_tc031(self):
        self.trigger_operations(TestCaseID.TS031)

    def test_initial_order_operation_tc041(self):
        self.trigger_operations(TestCaseID.TS041)

    def test_initial_order_operation_tc017(self):
        self.trigger_operations(TestCaseID.TS017)

    def test_initial_order_operation_tc139(self):
        self.trigger_operations(TestCaseID.TS139)

    def test_initial_order_operation_tc134(self):
        self.trigger_operations(TestCaseID.TS134)

    def test_initial_order_operation_tc002(self):
        self.trigger_operations(TestCaseID.TS002)
        BaseTestMethods(TestCaseID.TS002, self.sub_id).validate_cc_number()
