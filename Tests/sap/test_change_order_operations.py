import datetime

import pytest

from CommonUtilities.readProperties import ReadConfig
from CommonUtilities.readWriteTestData import load_excel_to_dictionary
from SAP.Facade.SAPOperations import SAPOperations
from SAP.TestSteps.BaseTestMethods import BaseTestMethods
from Tests.sap.test_base_sap import TestSAPBase
from Tests.test_base import BaseTest
from Tests.test_script_constant import TestCaseID


@pytest.mark.change_order_operation
class TestChangeOrderOperations(BaseTest, TestSAPBase):
    fetch_tc_status = True
    current_pytest = 'test_change_order_operations'
    last_pytest = 'test_billing_invoicing_post_first_usage'
    sub_id = None
    test_data_items = load_excel_to_dictionary(ReadConfig.get_sap_input_test_data_file(), "Items")

    def trigger_operations(self, tc_id, added_sku=False) -> None:
        """ This method does the subscription validation for change order
        and if it is fine, it performs the bits validation. """
        self.check_test_type(tc_id)
        try:
            self.set_tc_id(tc_id)
            self.check_test_case_status(tc_id, self.last_pytest)

            self.sub_id = self.get_subscription_id(tc_id)
            self.filtered_item_data = self.test_data_items.loc[self.test_data_items.TestCaseId == tc_id]

            base_test_methods_obj = BaseTestMethods(tc_id, self.sub_id)
            if not base_test_methods_obj.perform_operations_on_subscription():
                raise Exception('Subscription Validation Failed after change Order!!')
            else:
                if not base_test_methods_obj.perform_operations_on_bits(bit_type='min commit change',
                                                                        added_sku=added_sku):
                    raise Exception('Min Commit Bits Validation Failed!!')

        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to perform SAP Change Order Operations! " + str(e))
            raise e

    def validate_quantity_and_amount_change(self):
        row_id = next(iter(self.filtered_item_data.get('ItemMpn').to_dict()))
        item_mpn = self.filtered_item_data.get('ItemMpn')[row_id]
        quantity_change = self.filtered_item_data.get('QuantityChange')[row_id]
        original_reseller_billing_quantities = self.get_quantity_for_given_sku(self.sub_id, item_mpn, 'TSR1')
        updated_reseller_billing_quantities = self.get_quantity_for_given_sku(self.sub_id, item_mpn, 'TSR5', 2)
        original_vendor_billing_quantities = self.get_quantity_for_given_sku(self.sub_id, item_mpn, 'TSV1')
        updated_vendor_billing_quantities = self.get_quantity_for_given_sku(self.sub_id, item_mpn, 'TSV5', 2)
        assert abs(updated_reseller_billing_quantities[1][0]) == abs(
            original_reseller_billing_quantities[0][0]) + quantity_change
        assert abs(updated_vendor_billing_quantities[1][0]) == abs(
            original_vendor_billing_quantities[0][0]) + quantity_change
        updated_total_amount, updated_cost = self.get_amount_and_cost_for_given_sku(
            self.sub_id, item_mpn, updated_reseller_billing_quantities[1][0])
        assert f"{float(updated_total_amount.replace(',', '')):.2f}" == \
               f"{self.filtered_item_data.get('TotalAmount')[row_id]:.2f}"
        assert f"{float(updated_cost.replace(',', '')):.2f}" == f"{self.filtered_item_data.get('Cost')[row_id]:.2f}"

    def validate_new_sku_added(self):
        row_id = next(iter(self.filtered_item_data.get('ItemMpn').to_dict()))
        item_mpn = self.filtered_item_data.get('ItemMpn')[row_id]
        item_data = self.get_im360_item_data_for_added_sku(self.sub_id)
        added_sku = False
        for data in item_data:
            if data[6] == item_mpn:
                assert f'${float(data[4].replace("$", "")):.2f}' == \
                       f"${self.filtered_item_data.get('TotalAmount')[row_id]:.2f}"
                assert f'${float(data[5].replace("$", "")):.2f}' == \
                       f"${self.filtered_item_data.get('Cost')[row_id]:.2f}"
                added_sku = True
        assert added_sku

    def validate_contract_term_updated(self):
        contract_term_data = self.get_contract_information(self.sub_id)
        assert len(contract_term_data) == 2
        ctr_start_date = datetime.datetime.strptime(contract_term_data[0][0], "%m/%d/%YT%H:%M:%SZ")
        ctr_end_date = datetime.datetime.strptime(contract_term_data[0][1], "%m/%d/%YT%H:%M:%SZ")
        assert int((ctr_end_date - ctr_start_date).days / 365) == contract_term_data[0][2]

    def validate_cost_and_amount_for_billing_period(self):
        row_id = next(iter(self.filtered_item_data.get('ItemMpn').to_dict()))
        item_mpn = self.filtered_item_data.get('ItemMpn')[row_id]
        sap_obj = SAPOperations()
        sub_data = sap_obj.get_end_date_and_billing_period_from_subscription_table(self.sub_id)
        bits_cost = sap_obj.get_cost_from_bits_table(item_mpn, self.sub_id)
        bits_amount = sap_obj.get_amount_from_bits_table(item_mpn, self.sub_id)
        end_date = sub_data[0][5]
        end_date = end_date[:10]
        assert end_date == self.filtered_item_data.get('billingenddate')[row_id].strftime("%m/%d/%Y") and sub_data[0][
            12] == str(self.filtered_item_data.get('billingperiod')[row_id])
        assert bits_amount[1][7].replace(',', '') == str(self.filtered_item_data.get('TotalAmount')[row_id])
        assert bits_cost[1][7].replace(',', '') == str(self.filtered_item_data.get('Cost')[row_id])

    def test_change_order_operation_tc093(self):
        # cancellation with credit card without usage
        self.trigger_operations(TestCaseID.TS093)
        BaseTestMethods(TestCaseID.TS093, self.sub_id).validate_cancellation()

    def test_change_order_operation_tc022(self):
        # Test case deals with Cancellation.
        self.trigger_operations(TestCaseID.TS022)
        BaseTestMethods(TestCaseID.TS022, self.sub_id).validate_cancellation()

    def test_change_order_operation_tc017(self):
        # Test case deals with increase in order quantity.
        self.trigger_operations(TestCaseID.TS017)
        self.validate_quantity_and_amount_change()

    def test_change_order_operation_tc041(self):
        # Test case deals with decrease in order quantity.
        self.trigger_operations(TestCaseID.TS041)
        self.validate_quantity_and_amount_change()

    def test_change_order_operation_tc031(self):
        # Test case deals with increase in sku quantity.
        self.trigger_operations(TestCaseID.TS031, added_sku=True)
        self.validate_new_sku_added()

    def test_change_order_operation_tc139(self):
        # Test case deals with increase in contract term.
        self.trigger_operations(TestCaseID.TS139)
        self.validate_contract_term_updated()

    def test_change_order_operation_tc134(self):
        # Test case deals with billing period.
        self.trigger_operations(TestCaseID.TS134)
        self.validate_cost_and_amount_for_billing_period()
