# This test file is responsible to create the orders for all the test cases,
# for which new order is required
import pytest

from CommonUtilities import readWriteTestData
from CommonUtilities.readProperties import ReadConfig
from Tests.im360.test_base_im360 import BaseTest
from Tests.im360.test_case_base import TestCaseBase
from Tests.test_script_constant import TestCaseID


class TestIM360CreateOrder(BaseTest):

    def check_test_type(self, name):
        input_sheet_test_type = readWriteTestData.get_data_field_for_tc(ReadConfig.get_test_data_file(), "OrderData",
                                                                        49, name)
        config_file_test_type = ReadConfig.getTestType()
        if input_sheet_test_type != config_file_test_type:
            pytest.skip(name + " doesnt fall in " + config_file_test_type + " category")

    def create_order(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.create_order()

    def test_create_order_tc001(self):
        self.create_order(TestCaseID.TS001)

    def test_create_order_tc026(self):
        self.create_order(TestCaseID.TS026)

    def test_create_order_tc093(self):
        self.create_order(TestCaseID.TS093)

    def test_create_order_tc005(self):
        self.create_order(TestCaseID.TS005)

    def test_create_order_tc022(self):
        self.create_order(TestCaseID.TS022)

    def test_create_order_tc101(self):
        self.create_order(TestCaseID.TS101)

    def test_create_order_tc116(self):
        self.create_order(TestCaseID.TS116)

    def test_create_order_tc031(self):
        self.create_order(TestCaseID.TS031)

    def test_create_order_tc041(self):
        self.create_order(TestCaseID.TS041)

    def test_create_order_tc017(self):
        self.create_order(TestCaseID.TS017)

    def test_create_order_tc134(self):
        self.create_order(TestCaseID.TS134)

    def test_create_order_tc002(self):
        self.create_order(TestCaseID.TS002)

    def test_create_order_tc139(self):
        self.create_order(TestCaseID.TS139)
