# This test file is responsible to Validate the Vendor Subscription ID of the
# newly placed orders, once that is approved in connect

import pytest

from CommonUtilities import readWriteTestData
from CommonUtilities.readProperties import ReadConfig
from Tests.im360.test_base_im360 import BaseTest
from Tests.im360.test_case_base import TestCaseBase
from Tests.test_script_constant import TestCaseID


class TestIM360ValidateVendorSubscriptionId(BaseTest):

    def check_test_type(self, name):
        input_sheet_test_type = readWriteTestData.get_data_field_for_tc(ReadConfig.get_test_data_file(), "OrderData",
                                                                        49, name)
        config_file_test_type = ReadConfig.getTestType()
        if input_sheet_test_type != config_file_test_type:
            pytest.skip(name + " doesnt fall in " + config_file_test_type + " category")

    def validate_vendor_subs_id(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.validate_vendor_subs_id()

    def test_validate_vendor_subscription_id_tc001(self):
        self.validate_vendor_subs_id(TestCaseID.TS001)

    def test_validate_vendor_subscription_id_tc026(self):
        self.validate_vendor_subs_id(TestCaseID.TS026)

    def test_validate_vendor_subscription_id_tc093(self):
        self.validate_vendor_subs_id(TestCaseID.TS093)

    def test_validate_vendor_subscription_id_tc22(self):
        self.validate_vendor_subs_id(TestCaseID.TS022)

    def test_validate_vendor_subscription_id_tc101(self):
        self.validate_vendor_subs_id(TestCaseID.TS101)

    def test_validate_vendor_subscription_id_tc116(self):
        self.validate_vendor_subs_id(TestCaseID.TS116)

    def test_validate_vendor_subscription_id_tc005(self):
        self.validate_vendor_subs_id(TestCaseID.TS005)

    def test_validate_vendor_subscription_id_tc031(self):
        self.validate_vendor_subs_id(TestCaseID.TS031)

    def test_validate_vendor_subscription_id_tc041(self):
        self.validate_vendor_subs_id(TestCaseID.TS041)

    def test_validate_vendor_subscription_id_tc017(self):
        self.validate_vendor_subs_id(TestCaseID.TS017)

    def test_validate_vendor_subscription_id_tc134(self):
        self.validate_vendor_subs_id(TestCaseID.TS134)

    def test_validate_vendor_subscription_id_tc002(self):
        self.validate_vendor_subs_id(TestCaseID.TS002)

    def test_validate_vendor_subscription_id_tc139(self):
        self.validate_vendor_subs_id(TestCaseID.TS139)
