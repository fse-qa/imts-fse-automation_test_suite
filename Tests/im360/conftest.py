import time

import pytest
from selenium import webdriver

from CommonUtilities.readProperties import ReadConfig
from IM360.TestSteps.createOrder import CreateOrder


@pytest.fixture(params=["chrome"], autouse=True, scope="class")
def init_driver(request):
    if request.param == "chrome":
        options = webdriver.ChromeOptions()
        options.add_argument("--incognito")
        web_driver = webdriver.Chrome(executable_path=ReadConfig.get_chrome_executable_path(), options=options)
    elif request.param == "firefox":
        web_driver = webdriver.Firefox(executable_path=ReadConfig.get_firefox_executable_path())
    else:
        web_driver = webdriver.Ie(executable_path=ReadConfig.get_ie_executable_path())
    request.cls.driver = web_driver
    web_driver.maximize_window()
    create_order_steps = CreateOrder(web_driver)
    create_order_steps.login()
    create_order_steps.select_app()
    create_order_steps.is_sign_in_button_shown()
    yield
    time.sleep(5)
    create_order_steps.logout()
    web_driver.quit()


############## Modify Report ############################


def pytest_configure(config):
    config._metadata["Project Name"] = "Automatic Data Creator (ACDC)"
    config._metadata["Designed By"] = "RR QA Team"
    config._metadata["Tester"] = "RR QA Team"


def pytest_metadata(metadata):
    metadata.pop("JAVA_HOME", None)
    metadata.pop("Plugins", None)
