# This test file is responsible for any kind of change in the order,for example: Term change
# cancellation, Upsize, Downsize etc.
import pytest

from CommonUtilities import readWriteTestData
from CommonUtilities.readProperties import ReadConfig
from Tests.im360.test_base_im360 import BaseTest
from Tests.im360.test_case_base import TestCaseBase
from Tests.test_script_constant import TestCaseID


class TestIM360ChangeOrder(BaseTest):

    def check_test_type(self, name):
        input_sheet_test_type = readWriteTestData.get_data_field_for_tc(ReadConfig.get_test_data_file(), "OrderData",
                                                                        49, name)
        config_file_test_type = ReadConfig.getTestType()
        if input_sheet_test_type != config_file_test_type:
            pytest.skip(name + " doesnt fall in " + config_file_test_type + " category")

    def change_payment_term(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.change_payment_term()

    def cancel_subscription(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.cancel_subscription(name)

    def add_new_sku(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.add_new_sku(name)

    def increase_qty_of_products(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.increase_qty_of_products(name)

    def decrease_qty_of_products(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.decrease_qty_of_products(name)

    def switch_plan(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.switch_plan(name)

    def change_contract_term(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.change_contract_term(name)

    def test_change_order_tc026(self):
        self.change_payment_term(TestCaseID.TS026)

    def test_change_order_tc093(self):
        self.cancel_subscription(TestCaseID.TS093)

    def test_change_order_tc022(self):
        self.cancel_subscription(TestCaseID.TS022)

    def test_add_new_sku_tc031(self):
        self.add_new_sku(TestCaseID.TS031)

    def test_change_order_tc017(self):
        self.increase_qty_of_products(TestCaseID.TS017)

    def test_decrease_sku_quantity_tc041(self):
        self.decrease_qty_of_products(TestCaseID.TS041)

    def test_switch_plan_tc134(self):
        self.switch_plan(TestCaseID.TS134)

    def test_change_contract_term_tc139(self):
        self.change_contract_term(TestCaseID.TS139)
