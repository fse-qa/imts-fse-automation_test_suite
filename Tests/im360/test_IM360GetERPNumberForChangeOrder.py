# This test file is responsible to get the ERP Number of the change orders
import pytest

from CommonUtilities import readWriteTestData
from CommonUtilities.readProperties import ReadConfig
from Tests.im360.test_base_im360 import BaseTest
from Tests.im360.test_case_base import TestCaseBase
from Tests.test_script_constant import TestCaseID


class TestIM360GetSubscriptionId(BaseTest):

    def check_test_type(self, name):
        input_sheet_test_type = readWriteTestData.get_data_field_for_tc(ReadConfig.get_test_data_file(), "OrderData",
                                                                        49, name)
        config_file_test_type = ReadConfig.getTestType()
        if input_sheet_test_type != config_file_test_type:
            pytest.skip(name + " doesnt fall in " + config_file_test_type + " category")

    def get_erp_number_for_change_order(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.get_erp_number_for_change_order()

    def test_get_erp_number_for_change_order_tc031(self):
        self.get_erp_number_for_change_order(TestCaseID.TS031)

    def test_get_erp_number_for_change_order_tc017(self):
        self.get_erp_number_for_change_order(TestCaseID.TS017)

    def test_get_erp_number_for_change_order_tc041(self):
        self.get_erp_number_for_change_order(TestCaseID.TS041)

    def test_get_erp_number_for_change_order_tc134(self):
        self.get_erp_number_for_change_order(TestCaseID.TS134)

    def test_get_erp_number_for_change_order_tc139(self):
        self.get_erp_number_for_change_order(TestCaseID.TS139)
