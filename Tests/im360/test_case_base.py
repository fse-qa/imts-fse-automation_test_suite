from IM360.TestSteps.IM360ChangeOrder import IM360ChangeOrder
from IM360.TestSteps.IM360ValidateVendorSubsID import ValidateVendorSubscriptionID
from IM360.TestSteps.IM360createOrders import IM360createOrders
from IM360.TestSteps.IM360getERPNumberForChangeOrder import GetERPNumberForChangeOrder
from IM360.TestSteps.IM360getSubscriptionID import GetSubscriptionID


class TestCaseBase:

    def __init__(self, driver, name) -> None:
        self.driver = driver
        self.name = name

    def create_order(self):
        order = IM360createOrders()
        order.create_new_order(self.driver, self.name)

    # Fetch the IMSubscription ID of the newly placed order
    def get_subscription_id(self):
        subscription_id = GetSubscriptionID()
        subscription_id.get_subscription_id_by_test_case_id(self.driver, self.name)

    # Once the subscription is approved in Connect Validate the VendorSubscription ID of the Subscription
    def validate_vendor_subs_id(self):
        validate_vendor_sub_id = ValidateVendorSubscriptionID()
        validate_vendor_sub_id.validate_vendor_subscription_id_from_quote(self.driver, self.name)

    # Change the Payment Term of the subscription to Net60- 600
    def change_payment_term(self):
        change_term = IM360ChangeOrder()
        change_term.change_term_of_subscription(self.driver, self.name)

    # Validate if the subscription is cancelled by checking the status. Status should be Closed
    def validate_cancellation(self):
        cancel_susbs = IM360ChangeOrder()
        cancel_susbs.validate_cancellation(self.driver, self.name)

    # Cancel the subscription
    def cancel_subscription(self, driver):
        cancel_susbs = IM360ChangeOrder()
        cancel_susbs.cancel_subscription(self.driver, self.name)

    # add new sku for subscription
    def add_new_sku(self, driver):
        new_sku = IM360ChangeOrder()
        new_sku.add_new_sku_of_subscription(self.driver, self.name)

    # Fetch the erp number of the change order
    def get_erp_number_for_change_order(self):
        subscription_id = GetERPNumberForChangeOrder()
        subscription_id.get_erp_number_for_change_order_by_test_case_id(self.driver, self.name)

        # Increase the quantity of products

    def increase_qty_of_products(self, driver):
        change_qty = IM360ChangeOrder()
        change_qty.increase_qty_of_products(self.driver, self.name)

    # Decrease the quantity of products
    def decrease_qty_of_products(self, driver):
        change_qty = IM360ChangeOrder()
        change_qty.decrease_qty_of_products(self.driver, self.name)

    # Switch the plan to change billing frequency monthly - yearly
    def switch_plan(self, driver):
        change_qty = IM360ChangeOrder()
        change_qty.switch_plan(self.driver, self.name)

    # Change Contract Term
    def change_contract_term(self, driver):
        change_qty = IM360ChangeOrder()
        change_qty.change_contract_term(self.driver, self.name)
