# This test file is responsible to validate the cancellation of the subscription

import pytest

from CommonUtilities import readWriteTestData
from CommonUtilities.readProperties import ReadConfig
from Tests.im360.test_base_im360 import BaseTest
from Tests.im360.test_case_base import TestCaseBase
from Tests.test_script_constant import TestCaseID


class TestValidateCancellation(BaseTest):

    def check_test_type(self, name):
        input_sheet_test_type = readWriteTestData.get_data_field_for_tc(ReadConfig.get_test_data_file(), "OrderData",
                                                                        49, name)
        config_file_test_type = ReadConfig.getTestType()
        if input_sheet_test_type != config_file_test_type:
            pytest.skip(name + " doesnt fall in " + config_file_test_type + " category")

    def validate_cancellation(self, name):
        self.check_test_type(name)
        test_case_base = TestCaseBase(self.driver, name)
        test_case_base.validate_cancellation()

    def test_validate_cancel_tc093(self):
        self.validate_cancellation(TestCaseID.TS093)

    def test_validate_cancel_tc022(self):
        self.validate_cancellation(TestCaseID.TS022)
