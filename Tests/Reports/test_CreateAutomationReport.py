from Reports.TestSteps.CreateAutomationReport import CreateAutomationReport


class TestCreateAutomationReport:
    def test_create_data(self):
        create_automation_report_obj = CreateAutomationReport()
        create_automation_report_obj.test_create_data()

    def test_generate_report(self):
        create_automation_report_obj = CreateAutomationReport()
        create_automation_report_obj.test_generate_report()
