import pytest

from CBCommerce.TestSteps.checkStatus import Status
from CBConnect.Operations.GetSubscriptionList import GetSubscriptionList
from CommonUtilities.parse_config import ParseConfigFile
from db.service.CBCStatusDbManagementService import CBCStatusDbManagementService
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService


class TestCaseBaseCommerce:
    def __init__(self, name, target_status_code) -> None:
        self.name = name
        self.target_status_code = target_status_code

    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path", "config.json")
    test_type = ParseConfigFile().get_data_from_config_json("testType", "test_type", "config.json")

    def update_latest_order_status(self, marketplace=None):
        im360_subscription_db_management_service = IM360SubscriptionDbManagementService()
        im360_inp_ord_db_mgmt_srv = IM360InputOrderDbManagementService()

        if not im360_inp_ord_db_mgmt_srv.count_marketplaces_testcase(self.db_file_path, marketplace,
                                                                     self.name):
            pytest.skip("There is no record for test case ID " + self.name + " for marketplace " + marketplace)

        if im360_inp_ord_db_mgmt_srv.get_test_type_by_test_case_id(self.db_file_path, self.name) != self.test_type:
            pytest.skip("Skipping test case ID " + self.name + " as it is not belongs to test_type " + self.test_type +
                        " or no data present in the database")

        if self.target_status_code == "LO":
            im360_subscription_list = GetSubscriptionList().list(self.name)
            if len(im360_subscription_list) == 0:
                pytest.skip("number of subscription is zero for test case %s CBCommerce LO status check",
                                self.name)

        order_list = im360_subscription_db_management_service.get_sub_details_for_status_check(self.db_file_path,
                                                                                               self.name)
        if order_list:
            latest_order_tuple = order_list[-1]
            test_case_id, sub_id, order_id = latest_order_tuple
            status_db_management_service = CBCStatusDbManagementService()

            check_lo_status = status_db_management_service.get_status_by_sub_details(self.db_file_path,
                                                                                     sub_id, order_id)
            # If there is row present in CBC_status table then skipping the test
            if self.target_status_code == "LO" and check_lo_status is not None:
                pytest.skip()

            if self.target_status_code == "CP":
                if check_lo_status != "LO":
                    raise Exception("The subscription %s with status code LO not found into cbc_status table",
                                    sub_id)
            status = Status()
            cbc_status_data_obj = status.get_status(latest_order_tuple, self.target_status_code, marketplace)
            if cbc_status_data_obj.status == self.target_status_code:
                if self.target_status_code == "LO":
                    status_db_management_service.insert_cbc_status_data(self.db_file_path, cbc_status_data_obj)
                else:
                    status_db_management_service.update_status(self.db_file_path, cbc_status_data_obj)
            else:
                raise Exception("The subscription %s with status code %s not found", sub_id,
                                self.target_status_code)
        else:
            raise Exception("No subscription found for checking the status for test case ID ", self.name)
