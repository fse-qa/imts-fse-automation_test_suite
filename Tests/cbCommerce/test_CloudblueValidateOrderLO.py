import pytest
from parameterized import parameterized

from CommonUtilities.parse_config import ParseConfigFile
from Tests.cbCommerce.test_case_base_commerce import TestCaseBaseCommerce
from Tests.test_script_constant import TestCaseID
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService


class TestCloudblueValidateOrderLO:
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path", "config.json")
    im360_inp_ord_db_mgmt_srv = IM360InputOrderDbManagementService()
    marketplaces = im360_inp_ord_db_mgmt_srv.get_distinct_marketplaces(db_file_path)

    def update_latest_order_status(self, name, marketplace):
        test_case_base = TestCaseBaseCommerce(name, "LO")
        test_case_base.update_latest_order_status(marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc001(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS001, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc026(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS026, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc093(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS093, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc031(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS031, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc017(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS017, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc041(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS041, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc005(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS005, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc022(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS022, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc101(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS101, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc116(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS116, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc134(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS134, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc139(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS139, marketplace)

    @parameterized.expand(marketplaces)
    def test_approve_order_tc002(self, marketplace):
        self.update_latest_order_status(TestCaseID.TS002, marketplace)
