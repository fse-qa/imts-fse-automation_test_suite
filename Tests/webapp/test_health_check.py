from SAP.Facade.WebAppOperations import WebAppOperations
from Tests.test_base import BaseTest


class TestHealthCheck(BaseTest):

    def test_webapp_health_check(self):
        if WebAppOperations().call_health_check_api():
            self.logger.info("Web App is up and running...")
        else:
            self.logger.error("Something is wrong with Web App status, please check...")


