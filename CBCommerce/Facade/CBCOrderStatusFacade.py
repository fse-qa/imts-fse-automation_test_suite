import time

from CBCommerce.Facade.BaseFacade import BaseFacade
from CBCommerce.RestAPI.cloudblueSimpleAPI import CloudblueSimpleAPI
from db.model.CBCStatus import CBCStatus


class GetOrderStatus(BaseFacade):
    # cbc_status_dataframe = []
    actual_status = None

    def get_latest_order_status(self, test_case_id, subs_id, order_id, max_wait_time, retry_time_period,
                                target_status_code, marketplace=None):
        # cbc_status_data = {'Test_Case_Id': '', 'Subscription_ID': None, 'Order_ID': '', 'Status': ''}

        time_elapsed = 0
        # cbc_status_data['Test_Case_Id'] = test_case_id
        # cbc_status_data['Subscription_ID'] = subs_id
        # cbc_status_data['Subscription_ID'] = order_id

        while time_elapsed <= max_wait_time:
            status_code = self.fetch_status_code(subs_id, order_id, marketplace)
            if status_code == target_status_code:
                self.actual_status = status_code
                self.logger.info(f"Provisioning complete for subscription {subs_id}, order id {order_id}"
                                 f" against Test Case {test_case_id}.")
                break
            elif status_code == "PF":
                self.actual_status = status_code
                self.logger.info(f"Provisioning failed for subscription {subs_id}, order id {order_id}"
                                 f" against Test Case {test_case_id}.")
                break
            else:
                self.actual_status = status_code
                self.logger.info(f"order {order_id} has not reached {target_status_code} status yet."
                                 f"Retrying in {retry_time_period} seconds.")
                time.sleep(retry_time_period)
                time_elapsed += retry_time_period
                if time_elapsed > max_wait_time:
                    self.logger.info(
                        f"The order {order_id} with subscription {subs_id} was not successfully completed in "
                        f"cloudblue Commerce against Test Case {test_case_id}. Last status is {status_code}")

        cbc_status_data_obj = CBCStatus(test_case_id, subs_id, order_id, self.actual_status)
        # self.cbc_status_dataframe.append(cbc_status_data_obj)
        return cbc_status_data_obj

    def fetch_status_code(self, subs_id, order_id, marketplace=None):
        response = CloudblueSimpleAPI().get_cb_orders_by_subscription_id(subs_id, marketplace)
        order_list = response.get("data")
        for order in order_list:
            if order.get("id") == str(order_id):
                status = order.get("status")
                status_code = order.get("statusCode")
                self.logger.info(f"order status of subs id: {subs_id} is {status} and the status code is {status_code}")
                return status_code
