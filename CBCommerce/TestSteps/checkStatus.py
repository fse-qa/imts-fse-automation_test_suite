from CBCommerce.Facade.CBCOrderStatusFacade import GetOrderStatus
from CommonUtilities.parse_config import ParseConfigFile


class Status:
    max_wait_time_per_subscription = ParseConfigFile().get_data_from_config_json("delayDetails", "max_delay")
    retry_interval = ParseConfigFile().get_data_from_config_json("delayDetails", "retry_interval")

    def get_status(self, latest_order, target_status_code, marketplace = None):
        # order_status_data = []
        test_case_id, sub_id, order_id = latest_order
        get_ord_status_obj = GetOrderStatus()
        cbc_status_data_obj = get_ord_status_obj.get_latest_order_status(test_case_id, sub_id, order_id,
                                                                         int(self.max_wait_time_per_subscription),
                                                                         int(self.retry_interval), target_status_code,
                                                                         marketplace)

        # order_status_data.append(get_ord_stat_result[0])

        return cbc_status_data_obj

# if __name__ == '__main__':
#     # Pyset1().pyset1()
#     Status().get_status(('TS-001', '1045986', '1048294'))
