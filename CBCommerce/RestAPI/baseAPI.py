import requests
from requests.auth import HTTPBasicAuth
from CommonUtilities.logGeneration import LogGenerator


class BaseAPI:
    logger = LogGenerator.logGen()

    def get_token(self, api_token_url, username, password):
        response = requests.post(api_token_url, auth=HTTPBasicAuth(username, password))
        response_status = response.status_code
        self.logger.info("Response Status of getToken call: %s" % str(response_status))
        self.logger.info(response.json())
        return response.json()

    def send_request(self, token, api_url):
        # response_body = self.get_token(api_token_url, username, password)  # TODO fetch data from config file
        # auth_token = response_body['token']
        token_as_header = {'Authorization': 'Bearer ' + token}
        self.logger.info("URL: %s" % api_url)
        response = requests.get(api_url, headers=token_as_header)
        self.logger.info(response.json())
        return response.json()


