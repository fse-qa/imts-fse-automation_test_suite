from CBCommerce.RestAPI.baseAPI import BaseAPI
from CommonUtilities.parse_config import ParseConfigFile


class CloudblueSimpleAPI(BaseAPI):
    get_subscription_details_by_id_base_url = "https://api.rb-test-auto.int.zone/api/v1/subscriptions/"
    get_cb_orders_by_subs_id_base_url = "https://api.rb-test-auto.int.zone/api/v1/orders?subscriptionId="

    def get_subscription_details(self, sub_id):
        # token = self.get_token(ReadConfig.getCBAuthTokenUrl(),
        #                        ReadConfig.getCBAuthTokenUsername(),
        #                        ReadConfig.getCBAuthTokenPassword())
        token = self.get_token(
            ParseConfigFile.get_data_from_config_json("cloudblueSimpleApiData", "cbBasicAuthTokenUrl"),
            ParseConfigFile.get_data_from_config_json("cloudblueSimpleApiData", "cbBasicAuthUsername"),
            ParseConfigFile.get_data_from_config_json("cloudblueSimpleApiData", "enc_cbBasicAuthPassword"))
        url = self.get_subscription_details_by_id_base_url + str(sub_id)
        response = self.send_request(token["token"], url)
        return response

    def get_cb_orders_by_subscription_id(self, sub_id, marketplace=None):
        # token = self.get_token(ReadConfig.getCBAuthTokenUrl(),
        #                        ReadConfig.getCBAuthTokenUsername(),
        #                        ReadConfig.getCBAuthTokenPassword())

        token = self.get_token(ParseConfigFile().get_data_from_config_json(marketplace,
                                                                           "cbCommerceBasicAuthTokenUrl",
                                                                           "CredConfig.json"),
                               ParseConfigFile().get_data_from_config_json(marketplace,
                                                                           "cbCommerceBasicAuthUsername",
                                                                           "CredConfig.json"),
                               ParseConfigFile().get_data_from_config_json(marketplace,
                                                                           "enc_cbCommerceBasicAuthPassword",
                                                                           "CredConfig.json"))
        url = self.get_cb_orders_by_subs_id_base_url + str(sub_id)
        response = self.send_request(token["token"], url)

        return response
