import time

from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.CBCOperations import CBCOperations
from SAP.Facade.ConnectOperations import ConnectOperations
from SAP.Facade.FilterOperations import Filter
from SAP.Facade.SAPOperations import SAPOperations
from SAP.Facade.WebAppOperations import WebAppOperations
from SAP.Pages.RR_SAP_Execution import SapGui
from SAP.TestSteps.ValidateBitsData import BitsValidation
from SAP.TestSteps.ValidateSubscriptionData import SubscriptionValidation
from Tests.test_base import BaseTest
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360ItemDbManagementService import IM360ItemDbManagementService
from db.service.IM360StatusDbManagementService import IM360StatusDbManagementService
from db.service.SAPMPNDataService import SAPMPNDbManagementService


class BaseTestMethods(BaseTest):

    def __init__(self, tc_id, sub_id):
        self.tc_id = tc_id
        self.sub_id = sub_id
        self.latest_order_id = None
        self.latest_order_reference = None
        self.latest_system_date = None
        self.usage_date = None
        self.usage_order_reference = None
        self.sub_in_web_app = None
        self.cbc_status = None
        self.im360_status = None
        self.con_status = None
        self.im360_failure_image = None
        self.im360_error = None
        self.sap_failure_image = None
        self.usage_error = None

        self.filter_obj = Filter()
        self.cbc_object = CBCOperations()
        self.webapp_obj = WebAppOperations()
        self.connect_obj = ConnectOperations()
        self.sap_obj = SAPOperations()
        self.sapgui_obj = None
        self.sub_val_obj = SubscriptionValidation()
        self.bit_val_obj = BitsValidation()
        self.im360_status_obj = IM360StatusDbManagementService()

    config_obj = ParseConfigFile()

    db_file_path = config_obj.get_data_from_config_json("dbLocation", "db_file_path")
    max_wait_period = int(config_obj.get_data_from_config_json("retryParams", "max_wait_period"))
    wait_frequency = int(config_obj.get_data_from_config_json("retryParams", "wait_frequency"))

    def perform_operations_on_subscription(self) -> bool:

        """
            This function checks if the Provider Contract has reached SAP successfully,
            thereby calls for the validation of it's subscription feed and
            returns a True/False based on validation status.
        """

        try:
            # fetching latest system date
            self.latest_system_date = self.cbc_object.get_latest_date()
            self.logger.info(f'Current System Date is {self.latest_system_date}')

            # fetching latest order id
            self.latest_order_id = self.filter_obj.fetch_latest_order_id(self.sub_id)
            self.logger.info(f'Latest Order ID is : {self.latest_order_id}')

            # fetching latest order reference
            self.latest_order_reference = self.webapp_obj.fetch_order_number \
                (self.sub_id, self.filter_obj.format_date(self.latest_system_date, 'WebApp'))
            self.logger.info(f'Latest Order reference is : {self.latest_order_reference}')

            self.logger.info(f"Performing Operations for Test Case : {self.tc_id} with Subscription id: {self.sub_id}")

            # Checking for im360 status
            self.im360_status, self.im360_error, self.im360_failure_image = self.im360_status_obj. \
                get_status_and_image_by_sub_id(self.db_file_path, self.tc_id)
            if self.im360_status != 'success':
                self.logger.info(f"Subscription failed in IM360. "
                                 f"Hence, Operations for Test Case ID {self.tc_id} Aborted!")
                self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id, self.latest_order_id,
                                                          'FAIL', None, None, None, None, None, 'NP-S-1',
                                                          self.im360_failure_image, self.im360_error,
                                                          self.latest_system_date)

                return False

            else:
                # fetch the subscription status from CB Commerce.
                self.cbc_status = self.filter_obj.fetch_subscription_status(self.sub_id, self.latest_order_id)
                self.logger.info(f'Status of Subscription {self.sub_id} in CB is {self.cbc_status}')

                if self.cbc_status != 'CP':
                    self.logger.error(f"Operations for Test Case ID {self.tc_id} Aborted.")
                    self.con_status, error_msg = self.connect_obj.fetch_connect_status_data(self.tc_id)
                    self.logger.info("Subscription Status in Connect is " + str(self.con_status))

                    if self.con_status != 'APPROVED':
                        status_code = 'IN-S-1'
                        self.logger.error(f"Connect Error Message: {self.con_status['error_message']}")
                        self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id, self.latest_order_id,
                                                                  'FAIL', None, None, None, None, None, status_code,
                                                                  None, error_msg, self.latest_system_date)
                    else:
                        status_code = 'IN-S-2'
                        self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id, self.latest_order_id,
                                                                  'FAIL', None, None, None, None, None, status_code,
                                                                  None, None, self.latest_system_date)

                    return False

                else:
                    if not self.subscription_availability_in_web_app(self.sub_id):
                        # wait mechanism is already implemented in the above function
                        self.logger.error(f"Subscription {self.sub_id} could not be fetched. \n"
                                          f"Operations for Test Case ID {self.tc_id} Aborted.")
                        self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id, self.latest_order_id,
                                                                  'FAIL', None, None, None, None, None, 'NA-S-1',
                                                                  None, None, self.latest_system_date)
                        return False

                    else:
                        self.logger.info(f"Subscription {self.sub_id} found in Web App. Checking for SAP response.")
                        # check for response of subscription feed in Web App.
                        sap_response, response_message = WebAppOperations().fetch_sap_response(self.sub_id,
                                                                                               self.latest_order_id)
                        if sap_response != 'SUCCESS':
                            self.logger.error(f"SAP Response Failure: {response_message}")
                            self.logger.info(f"Subscription {self.sub_id} with Order Number {self.latest_order_id}"
                                             f" did not reach SAP. \n"
                                             f"Operations for Test Case ID {self.tc_id} Aborted.")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_id,
                                                                      'FAIL', None, None, None, None, None, 'NA-S-2',
                                                                      self.sap_failure_image, None,
                                                                      self.latest_system_date)
                            return False

                        else:
                            self.logger.info(f"Fetching SAP GUI data for Subscription {self.sub_id}.")
                            if not self.collect_subscription_data_from_SAP_GUI():
                                self.logger.error(f"SAP Data not fetched successfully! "
                                                  f"Operations for Test Case ID {self.tc_id} Aborted.")
                                return False
                            else:
                                self.logger.info(f"SAP data fetched successfully for Test Case {self.tc_id}. ")

                        self.logger.info(f'\n\t *** Validating Subscription {self.sub_id} *** ')
                        if not self.sub_val_obj.validate_subscription(self.sub_id, self.latest_order_id,
                                                                      self.latest_order_reference):
                            self.logger.info(f"Proceeding with the next steps with Warning for Sub {self.sub_id}!!")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_id,
                                                                      'WARNING', None, None, None, None, None, 'BP-S-1',
                                                                      None, self.sub_val_obj.error_message,
                                                                      self.latest_system_date)
                            return True
                        else:
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_id,
                                                                      'PASS', None, None, None, None, None, 'SUC',
                                                                      None, None, self.latest_system_date)
                            return True

        except Exception as e:
            self.logger.error(f"Exception occurred while trying to perform operations on Test Case {self.tc_id} with "
                              f"subscription {self.sub_id} : " + str(e))
            return False

    def perform_operations_on_bits(self, bit_type, added_sku=False) -> bool:

        """
            This function checks if Bits (based on the bit_type) has reached SAP successfully,
            thereby calls for the validation of the bits and
            returns a True/False based on validation status.
        """

        bits_availability_status = None

        try:
            self.latest_system_date = self.cbc_object.get_latest_date()
            self.logger.info(f'Current System Date is {self.latest_system_date}')
            if bit_type == 'min commit' or bit_type == 'cancel' or bit_type == 'min commit change':
                self.latest_order_reference = self.webapp_obj.fetch_order_number \
                    (self.sub_id, self.filter_obj.format_date(self.latest_system_date, 'WebApp'))

                bits_availability_status = self.bits_availability_in_web_app(self.sub_id, self.latest_order_reference,
                                                                             bit_type, added_sku=added_sku)
            elif bit_type == 'usage':
                self.usage_date = self.connect_obj.fetch_usage_date(self.tc_id)
                self.usage_order_reference = self.webapp_obj.fetch_usage_order_number \
                    (self.sub_id, self.filter_obj.format_date(self.usage_date, 'WebApp'), self.latest_order_reference)
                bits_availability_status = self.bits_availability_in_web_app(self.sub_id, self.usage_order_reference,
                                                                             bit_type)
                self.latest_order_reference = self.usage_order_reference

            if bits_availability_status is None:
                self.logger.error(f"No Bit found for Subscription {self.sub_id} "
                                  f"with Order Number {self.latest_order_reference}. \n"
                                  f"Operations for Test Case ID {self.tc_id} Aborted.")
                if bit_type != 'usage':
                    self.logger.info(f"No {bit_type} bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None,
                                                              'FAIL', None, None, None, None, 'NA-B-1', None, None,
                                                              self.latest_system_date)
                else:
                    self.logger.info(f"No {bit_type} bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None, None,
                                                              'FAIL', None, None, None, 'NA-U-1', None,
                                                              self.usage_error,
                                                              self.latest_system_date)
                return False

            elif bits_availability_status == 'Res Missing':
                self.logger.error(f"All Reseller Bits are missing for Subscription {self.sub_id} "
                                  f"with Order Number {self.latest_order_reference}. \n"
                                  f"Operations for Test Case ID {self.tc_id} Aborted.")
                if bit_type != 'usage':
                    self.logger.info(f"No {bit_type} Reseller bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None,
                                                              'FAIL', None, None, None, None, 'MI-B-1', None, None,
                                                              self.latest_system_date)
                else:
                    self.logger.info(f"No {bit_type} Reseller bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None, None,
                                                              'FAIL', None, None, None, 'MI-U-1', None,
                                                              self.usage_error,
                                                              self.latest_system_date)
                return False

            elif bits_availability_status == 'Ven Missing':
                self.logger.info(f"All Vendor Bits are missing for Subscription {self.sub_id} "
                                 f"with Order Number {self.latest_order_reference}. \n"
                                 f"Operations for Test Case ID {self.tc_id} Aborted.")
                if bit_type != 'usage':
                    self.logger.info(f"No {bit_type} Vendor bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None,
                                                              'FAIL', None, None, None, None, 'MI-B-2', None, None,
                                                              self.latest_system_date)
                else:
                    self.logger.info(f"No {bit_type} Vendor bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None, None,
                                                              'FAIL', None, None, None, 'MI-U-2', None,
                                                              self.usage_error,
                                                              self.latest_system_date)
                return False

            elif bits_availability_status == 'Res Missing Some':
                self.logger.info(f"Some reseller Bits are missing for Subscription {self.sub_id} "
                                 f"with Order Number {self.latest_order_reference}. \n"
                                 f"Operations for Test Case ID {self.tc_id} Aborted.")
                if bit_type != 'usage':
                    self.logger.info(
                        f"Some {bit_type} Reseller bits are available in Web App for sub {self.sub_id} with "
                        f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None,
                                                              'FAIL', None, None, None, None, 'MI-B-8', None, None,
                                                              self.latest_system_date)
                return False

            elif bits_availability_status == 'Ven Missing Some':
                self.logger.info(f"Some Vendor Bits are missing for Subscription {self.sub_id} "
                                 f"with Order Number {self.latest_order_reference}. \n"
                                 f"Operations for Test Case ID {self.tc_id} Aborted.")
                if bit_type != 'usage':
                    self.logger.info(f"Some {bit_type} Vendor bits are available in Web App for sub {self.sub_id} with "
                                     f"Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None,
                                                              'FAIL', None, None, None, None, 'MI-B-9', None, None,
                                                              self.latest_system_date)
                return False

            elif bits_availability_status == 'Ven and Res Missing Some':
                self.logger.info(f"Both Vendor and Reseller Bits are missing for Subscription {self.sub_id} "
                                 f"with Order Number {self.latest_order_reference}. \n"
                                 f"Operations for Test Case ID {self.tc_id} Aborted.")
                if bit_type != 'usage':
                    self.logger.info(f"Some {bit_type} Vendor and Reseller bits are available in Web App for sub "
                                     f"{self.sub_id} with Order Number {self.latest_order_reference}")
                    self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                              self.latest_order_reference, None,
                                                              'FAIL', None, None, None, None, 'MI-B-10', None, None,
                                                              self.latest_system_date)
                return False

            else:
                self.logger.info(
                    f"All Bits for Subscription {self.sub_id} with Order Number {self.latest_order_reference} "
                    f"is available in Web App. Collecting bits data from SAP GUI.")
                self.logger.info("All bits available in Webapp")
                is_bits_collection_successful = self.collect_bits_data_from_SAP_GUI(bit_type)
                if is_bits_collection_successful is False:
                    self.logger.info(
                        f"Bits for {self.sub_id} with Order Number {self.latest_order_reference}"
                        f"was not collected successfully from SAP GUI.")
                    return False
                else:
                    self.logger.info(f'Bits successfully fetched for subscription {self.sub_id} with Order Number '
                                     f'{self.latest_order_reference}.')
                    bits_availability_status_in_sap = self.bits_availability_in_sap(self.sub_id,
                                                                                    self.latest_order_reference,
                                                                                    bit_type, added_sku=added_sku)
                    if bits_availability_status_in_sap is None:
                        # Print the log message in the function itself.
                        self.logger.info(f"Bits could not be fetched for Subscription {self.sub_id} "
                                         f"with Order Number {self.latest_order_id} from SAP. \n"
                                         f"Operations for Test Case ID {self.tc_id} Aborted.")
                        if bit_type != 'usage':
                            self.logger.info(f"No {bit_type} bits are available in SAP for sub {self.sub_id} with "
                                             f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None,
                                                                      'FAIL', None, None, None, None, 'NA-B-2',
                                                                      self.sap_failure_image, None,
                                                                      self.latest_system_date)
                        else:
                            self.logger.info(f"No {bit_type} bits are available in SAP for sub {self.sub_id} with "
                                             f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'FAIL', None, None, None, 'NA-U-2',
                                                                      self.sap_failure_image, self.usage_error,
                                                                      self.latest_system_date)
                        return False

                    elif bits_availability_status_in_sap == 'Res Missing':
                        if bit_type != 'usage':
                            self.logger.info(
                                f"No {bit_type} Reseller bits are available in SAP for sub {self.sub_id} with "
                                f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None,
                                                                      'FAIL', None, None, None, None, 'MI-B-3',
                                                                      self.sap_failure_image, None,
                                                                      self.latest_system_date)
                        else:
                            self.logger.info(
                                f"No {bit_type} Reseller bits are available in SAP for sub {self.sub_id} with "
                                f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'FAIL', None, None, None, 'MI-U-3',
                                                                      self.sap_failure_image,
                                                                      self.usage_error, self.latest_system_date)
                        return False

                    elif bits_availability_status_in_sap == 'Ven Missing':
                        if bit_type != 'usage':
                            self.logger.info(
                                f"No {bit_type} Vendor bits are available in SAP for sub {self.sub_id} with "
                                f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None,
                                                                      'FAIL', None, None, None, None, 'MI-B-4',
                                                                      self.sap_failure_image,
                                                                      None, self.latest_system_date)
                        else:
                            self.logger.info(
                                f"No {bit_type} Vendor bits are available in SAP for sub {self.sub_id} with "
                                f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'FAIL', None, None, None, 'MI-U-4',
                                                                      self.sap_failure_image,
                                                                      self.usage_error, self.latest_system_date)
                        return False

                    elif bits_availability_status_in_sap == 'Res Missing Some':
                        if bit_type != 'usage':
                            self.logger.info(
                                f"Some {bit_type} Reseller bits are not available in SAP for sub {self.sub_id} with "
                                f"Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None,
                                                                      'FAIL', None, None, None, None, 'MI-B-5',
                                                                      self.sap_failure_image,
                                                                      None, self.latest_system_date)
                        return False

                    elif bits_availability_status_in_sap == 'Ven Missing Some':
                        if bit_type != 'usage':
                            self.logger.info(
                                f"Some {bit_type} Vendor bits are not available in SAP for sub {self.sub_id} "
                                f"with Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'FAIL', None, None, None, 'MI-B-6',
                                                                      self.sap_failure_image,
                                                                      None, self.latest_system_date)
                        return False

                    elif bits_availability_status_in_sap == 'Ven and Res Missing Some':
                        if bit_type != 'usage':
                            self.logger.info(f"Some {bit_type} Vendor and Reseller bits are not available in SAP for "
                                             f"sub {self.sub_id} with Order Number {self.latest_order_reference}")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'FAIL', None, None, None, 'MI-B-7',
                                                                      self.sap_failure_image,
                                                                      None, self.latest_system_date)
                        return False

                    if not self.bit_val_obj.validate_bits(self.sub_id, self.latest_order_reference, bit_type):
                        if bit_type != 'usage':
                            self.logger.info(
                                f"All fields of {bit_type} bits did not match for subscription {self.sub_id} "
                                f"with Order Number {self.latest_order_reference}.")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None,
                                                                      'WARNING', None, None, None, None, 'BP-B-1',
                                                                      self.sap_failure_image,
                                                                      self.bit_val_obj.error_message,
                                                                      self.latest_system_date)
                        else:
                            self.logger.info(
                                f"All fields of {bit_type} bits did not match for subscription {self.sub_id} "
                                f"with Order Number {self.latest_order_reference}.")
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'WARNING', None, None, None, 'BP-B-2',
                                                                      self.sap_failure_image,
                                                                      self.usage_error, self.latest_system_date)
                        return True

                    else:
                        self.logger.info(f"All fields of {bit_type} bits match for subscription {self.sub_id} "
                                         f"with Order Number {self.latest_order_reference}.")
                        if bit_type != 'usage':
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None,
                                                                      'PASS', None, None, None, None, 'SUC',
                                                                      None, None, self.latest_system_date)
                        else:
                            self.sap_obj.update_sap_status_data_table(self.tc_id, self.sub_id,
                                                                      self.latest_order_reference, None, None,
                                                                      'PASS', None, None, None, 'SUC',
                                                                      None, None, self.latest_system_date)
                        return True

        except Exception as e:
            self.logger.error(f"Exception occurred while trying to perform bit operations on "
                              f"Subscription {self.sub_id} with Order Number {self.latest_order_reference} : " + str(e))
            return False

    def subscription_availability_in_web_app(self, sub_id) -> bool:
        """ This method checks if the subscription feed is available in WebApp. """

        for count in range(int(self.max_wait_period / self.wait_frequency)):
            self.sub_in_web_app = WebAppOperations().fetch_web_app_subs_data(sub_id, self.latest_order_id,
                                                                             self.latest_order_reference)
            if self.sub_in_web_app is None:
                time.sleep(self.wait_frequency)
            else:
                break
        if self.sub_in_web_app is None:
            return False
        else:
            return True

    def bits_availability_in_web_app(self, sub_id, order_id, bit_type, added_sku=False) -> bool:
        """ This method checks if all expected bits are available in WebApp. """

        bits_list = []
        status = None

        for count in range(int(self.max_wait_period / self.wait_frequency)):
            bits_list = self.webapp_obj.fetch_web_app_bits_data(sub_id, order_id)
            self.logger.info('Web App Bits fetched successfully for validation.')
            if bits_list is None:
                time.sleep(self.wait_frequency)
            else:
                break

        if bit_type == 'min commit' or bit_type == 'cancel' or bit_type == 'min commit change':
            status = self.filter_obj.is_all_bits_fetched_from_webapp(
                bits_list, IM360ItemDbManagementService().get_all_sku_by_sub_details(self.db_file_path, self.sub_id),
                added_sku=added_sku)
        elif bit_type == 'usage':
            status = self.filter_obj.is_all_bits_fetched_from_webapp(bits_list, IM360ItemDbManagementService().
                                                                     get_usage_sku_by_sub_details(self.db_file_path,
                                                                                                  self.tc_id))
        return status

    def bits_availability_in_sap(self, sub_id, order_id, bit_type, added_sku=False):
        """ This method checks if all expected bits are available in SAP. """

        status = None
        sap_bits_list = self.sap_obj.fetch_sap_bits_data(sub_id, order_id)

        if bit_type == 'min commit' or bit_type == 'cancel':
            status = self.filter_obj.all_bits_received_in_sap \
                (sap_bits_list, IM360ItemDbManagementService().get_all_sku_by_sub_details(self.db_file_path,
                                                                                          self.sub_id))
        elif bit_type == 'min commit change':
            status = self.filter_obj.all_bits_received_in_sap \
                (sap_bits_list,
                 IM360ItemDbManagementService().get_all_sku_by_sub_details(self.db_file_path, self.sub_id),
                 change_order_bits=True, added_sku=added_sku)
        elif bit_type == 'usage':
            status = self.filter_obj.all_bits_received_in_sap \
                (sap_bits_list, IM360ItemDbManagementService().get_usage_sku_by_sub_details(self.db_file_path,
                                                                                            self.tc_id))

        return status

    def collect_subscription_data_from_SAP_GUI(self):
        """ This method fetches Subscription data from SAP GUI. """

        order_type = 'Feed'
        image_path = '../../Output'
        date_of_origin = None
        order_number = None

        self.sapgui_obj = SapGui()
        return self.sapgui_obj.sapLogin(image_path, self.tc_id, self.sub_id, order_number, order_type, date_of_origin)

    def collect_bits_data_from_SAP_GUI(self, bit_type):
        """ This method fetches Bits data from SAP GUI. """

        self.sapgui_obj = SapGui()
        order_type = 'Bits'
        image_path = '../../Output'

        if bit_type == 'min commit' or bit_type == 'cancel':
            bit_from_date = Filter().format_date(self.latest_system_date, 'SAP')
            return self.sapgui_obj.sapLogin(image_path, self.tc_id, self.sub_id, self.latest_order_reference,
                                            order_type, bit_from_date)

        elif bit_type == 'min commit change':
            bit_from_date = Filter().format_date(self.latest_system_date, 'SAP')
            return self.sapgui_obj.sapLogin(image_path, self.tc_id, self.sub_id, self.latest_order_reference,
                                            order_type, bit_from_date, skip_date_check=True)

        elif bit_type == 'usage':
            usage_from_date = Filter().format_date(self.usage_date, 'SAP')
            return self.sapgui_obj.sapLogin(image_path, self.tc_id, self.sub_id, self.latest_order_reference,
                                            order_type, usage_from_date)

    def validate_payment_term_change(self) -> None:
        """ This method validates if the payment term change happened successfully. """
        self.sap_obj.compare_payment_terms(self.sub_id)

    def validate_cancellation(self) -> None:
        """ This method validates if the cancellation happened successfully. """
        self.latest_system_date = self.cbc_object.get_latest_date()
        self.sap_obj.validate_cancel_operation(self.latest_system_date, self.sub_id, self.tc_id)

    def validate_cc_number(self) -> None:
        """ This method validated if the credit card number matches across all systems. """
        im360_cc_num = IM360InputOrderDbManagementService().get_cc_number(self.db_file_path, self.tc_id)
        sap_cc_num = SAPMPNDbManagementService().get_cc_number(self.db_file_path, self.tc_id)

        if im360_cc_num != sap_cc_num:
            raise Exception(f'CC Number did not match! \n IM360: {im360_cc_num} \tSAP: {sap_cc_num}')

    def validate_prepaid_sub(self) -> None:
        status = None
        self.latest_system_date = self.cbc_object.get_latest_date()
        """
            This method validates data between two table sap_mpn_data and sap_bits_data.
        """
        sap_obj = SAPOperations()
        # fetch data from sap_mpn_data
        sap_mpn_data = sap_obj.fetch_sap_mpn_data(self.sub_id)
        self.latest_order_reference = self.webapp_obj.fetch_order_number(self.sub_id, self.filter_obj.format_date(
            self.latest_system_date, 'WebApp'))
        # fetch data from sap_bits_data
        sap_bits = sap_obj.fetch_sap_bits_data(self.sub_id, self.latest_order_reference)
        status = self.sap_obj.validate_prepaid_term(self.sub_id, sap_bits, sap_mpn_data)

        if status:
            self.logger.info("Test is executed successfully")
        else:
            self.logger.error(f"Operations for Test Case ID {self.tc_id} Aborted.")
            raise Exception(f"Operations for Test Case ID {self.tc_id} Aborted.")
