"""
fetch IM360 and Tibco Data here
flat comparison between Tibco and SAP bits data
Update status in status table
"""
import re

from SAP.Facade.SAPOperations import SAPOperations
from SAP.Facade.WebAppOperations import WebAppOperations
from Tests.test_base import BaseTest


class BitsValidation(BaseTest):

    def __init__(self):
        self.error_message = 'Did not match across all Systems: \n'

    def validate_bits(self, sub_id, order_id, bit_type) -> bool:
        """ This method validates the content of the bits across Web App and SAP. """
        expected_bit_types = None

        if bit_type == 'min commit':
            expected_bit_types = ['TSR1', 'TSV1']
        elif bit_type == 'min commit change':
            expected_bit_types = ['TSR5', 'TSV5']
        elif bit_type == 'usage':
            expected_bit_types = ['TSR2', 'TSV2']
        elif bit_type == 'cancel':
            expected_bit_types = ['TSR5', 'TSV5', 'TSR1', 'TSV1']

        self.logger.info(f'Proceeding with {bit_type} bit Validation. ')
        web_app_bits = WebAppOperations().fetch_web_app_bits_data(sub_id, order_id)
        self.logger.info(f'Bits Data collected successfully from Web App for validation. {web_app_bits}')
        sap_bits = SAPOperations().fetch_sap_bits_data(sub_id, order_id)
        self.logger.info(f'Bits Data collected successfully from SAP for validation. {sap_bits}')

        return self.fulfill_validation_criteria_and_compare(web_app_bits, sap_bits, expected_bit_types)

    def compare(self, web_app_bit, sap_bit):
        """ This method does the actual comparison of bits data across WebApp and SAP. """

        flag = True
        self.logger.info(f"Matching data for {sap_bit['bill_item_type']} bit for SKU "
                         f"{sap_bit['sku_part']}")

        if sap_bit['im_sub_id'] != web_app_bit['im_subscription_id']:
            flag = False
            self.logger.warning(f"Subscription ID did not match. \n SAP: {sap_bit['im_sub_id']} "
                                f"\t and \t Web App: {web_app_bit['im_subscription_id']}")
            self.error_message += 'im_sub_id, '

        if sap_bit['order_number'] != web_app_bit['order_number']:
            flag = False
            self.logger.warning(f"Order Number did not match. \n SAP: {sap_bit['order_number']} "
                                f"\t and \t Web App: {web_app_bit['order_number']}")
            self.error_message += 'order_number, '

        if sap_bit['bcn_number'] != web_app_bit['bcn']:
            flag = False
            self.logger.error(f"BCN did not match. SAP: {sap_bit['bcn_number']} "
                              f"\t and \t Web App: {web_app_bit['bcn']}")
            self.error_message += 'bcn_number, '

        if self.convert_sap_date_format(sap_bit['from_date']) != \
                web_app_bit['bit_date_from']:
            flag = False
            self.logger.warning(f"The Bit From Date did not match. \n SAP: {sap_bit['from_date']}"
                                f"\t and \t Web App: {web_app_bit['bit_date_from']}")
            self.error_message += 'from_date, '

        if self.convert_sap_date_format(sap_bit['to_date']) != \
                web_app_bit['bit_date_to']:
            flag = False
            self.logger.warning(f"The Bit To Date did not match. \n SAP: {sap_bit['to_date']}"
                                f"\t and \t Web App: {web_app_bit['bit_date_to']}")
            self.error_message += 'to_date, '

        if sap_bit['bill_item_type'] != web_app_bit['rb_bit_type']:
            flag = False
            self.logger.warning(
                f"The Billable Item Type did not match. \n SAP: {sap_bit['bill_item_type']} "
                f"\t and \t Web App: {web_app_bit['rb_bit_type']}")
            self.error_message += 'bill_item_type, '

        if web_app_bit['bit_amount'] is None:
            web_app_bit['bit_amount'] = '0.00'
        if type(sap_bit['amount']) == str:
            sap_bit['amount'] = re.sub('[,]', '', sap_bit['amount'])
        if round(float(sap_bit['amount']), 2) \
                != float(web_app_bit['bit_amount']):
            flag = False
            self.logger.warning(f"The Bit Amount did not match. \n SAP: {sap_bit['amount']} "
                                f"\t and \t Web App: {web_app_bit['bit_amount']}")
            self.error_message += 'amount, '

        if float(abs(sap_bit['billing_quantity'])) != float(
                web_app_bit['bit_quantity']):
            flag = False
            self.logger.warning(
                f"The Billing Quantity did not match. \n SAP: {float(sap_bit['billing_quantity'])}"
                f" \t and \t Web App: {float(web_app_bit['bit_quantity'])}")
            self.error_message += 'billing_quantity, '

        if float(sap_bit['duration']) != float(web_app_bit['duration']):
            flag = False
            self.logger.warning(f"The Duration did not match. \n SAP: {float(sap_bit['duration'])}"
                                f" \t and \t Web App: {float(web_app_bit['duration'])}")
            self.error_message += 'duration, '

        if web_app_bit['unit_price'] is None:
            web_app_bit['unit_price'] = '0.00'
        if round(float(self.negative_conversion(sap_bit['unit_prc_of_prod'])), 2) != \
                float(web_app_bit['unit_price']):
            flag = False
            self.logger.warning(
                f"The Unit Price did not match. \n SAP: {sap_bit['unit_prc_of_prod']}"
                f" \t and \t Web App: {web_app_bit['unit_price']}")
            self.error_message += 'unit_price, '

        if web_app_bit['unit_cost'] is None:
            web_app_bit['unit_cost'] = '0.00'
        if round(float(self.negative_conversion(sap_bit['unit_cost_of_prod'])), 2) != \
                float(web_app_bit['unit_cost']):
            flag = False
            self.logger.warning(
                f"The Unit Cost did not match did not match. \n SAP: {sap_bit['unit_cost_of_prod']}"
                f" \t and \t Web App: {web_app_bit['unit_cost']}")
            self.error_message += 'unit_cost, '

        if sap_bit['sku_part'] != web_app_bit['sku']:
            flag = False
            self.logger.warning(f"The SKU did not match. \n SAP: {sap_bit['sku_part']}"
                                f" \t and \t Web App: {web_app_bit['sku']}")
            self.error_message += 'sku_part, '

        if sap_bit['ven_sub_id'] != web_app_bit['vendor_subscription_id']:
            flag = False
            self.logger.warning(
                f"The Vendor Subscription ID did not match. \n SAP: {sap_bit['ven_sub_id']} "
                f"\t and \t Web App: {web_app_bit['vendor_subscription_id']}")
            self.error_message += 'ven_sub_id, '

        self.error_message = self.format_string(self.error_message)

        if flag is True:
            self.logger.info('Data for all bits have matched successfully! ')

        return flag

    def fulfill_validation_criteria_and_compare(self, web_app_bits, sap_bits, expected_bit_types) -> bool:
        """ This method checks and compares if all bits validation criteria are met. """

        flag = True
        self.logger.info('Checking if all validation criteria is fulfilled.')
        extra_bits = []
        for sap_bit in sap_bits:
            if sap_bit['bill_item_type'] not in expected_bit_types:
                extra_bits.append(extra_bits)
        if len(extra_bits) > 0:
            self.logger.info(f"Unexpected Bit Types found for the  following SKUs : {extra_bits}")
        else:
            for webapp_bit in web_app_bits:
                for sap_bit in sap_bits:
                    if (sap_bit['bill_item_type'] == webapp_bit['rb_bit_type']) \
                            and (sap_bit['sku_part'] == webapp_bit['sku']):
                        flag = self.compare(webapp_bit, sap_bit)

        return flag

    @staticmethod
    def convert_sap_date_format(date):
        """ This method formats the date based on SAP requirement. """

        date_parts = date.split('/')
        return date_parts[2] + date_parts[0] + date_parts[1]

    @staticmethod
    def negative_conversion(value):
        """ This method formats negative values in SAP
        in the form '10.00-' """

        if value.find('-') != -1:
            value = value.replace('-', '')
            value = '-' + value

        return value

    @staticmethod
    def format_string(msg):
        msg_lst = list(msg)
        msg_lst.pop()
        msg_lst[-1] = '!'
        return ''.join(msg_lst)
