import re

from SAP.Facade.IM360Operations import IM360Operations
from SAP.Facade.SAPOperations import SAPOperations
from SAP.Facade.WebAppOperations import WebAppOperations
from Tests.test_base import BaseTest


class SubscriptionValidation(BaseTest):

    def __init__(self):
        self.error_message = 'Did not match across all Systems: '
        self.sub_error_message = '\n Subscription: '
        self.mpn_error_message = '\n MPN: '

    def validate_subscription(self, sub_id, order_id, order_ref) -> bool:
        """ This method validates the content of a subscription (General Data) across all IM360, WebApp and SAP. """
        flag = True

        # Fetching subscription data from IM360 table
        im360_obj = IM360Operations()
        im360_sub_data = im360_obj.fetch_im360_subs_data(sub_id, order_id)
        self.logger.info(f"IM360 Sub Data Fetched: {im360_sub_data}")

        # Fetching subscription data from WebApp table
        web_app_obj = WebAppOperations()
        web_app_sub_data = web_app_obj.fetch_web_app_subs_data(sub_id, order_id, order_ref)
        self.logger.info(f"Web App Sub Data fetched. {web_app_sub_data}")

        # Fetching subscription data from SAP table
        sap_obj = SAPOperations()
        sap_subscription_data = sap_obj.fetch_sap_subs_data(sub_id)
        self.logger.info(f"SAP Sub Data fetched. {sap_subscription_data}")

        self.logger.info('\nAttempting to validate Subscription data across the 3 systems... ')
        flag = self.compare_subscription_data(im360_sub_data, web_app_sub_data, sap_subscription_data)
        self.logger.info('\nSubscription Data validated successfully.')

        self.logger.info('\nAttempting to validate MPN data across the 3 systems... ')
        flag = self.validate_mpn(sub_id, order_id)
        self.logger.info('\nMPN Data validated successfully.')

        self.error_message += self.sub_error_message + self.mpn_error_message

        return flag

    def validate_mpn(self, sub_id, order_id) -> bool:
        """ This method validates the MPN related data of a subscription across all IM360, WebApp and SAP. """

        # Fetching MPN data from IM360 table
        im360_obj = IM360Operations()
        im360_mpn_data = im360_obj.fetch_im360_mpn_data(sub_id, order_id)
        self.logger.info(f'IM360 Data fetched for MPN Validation. \n IM360 MPN Data : {im360_mpn_data}')

        # Fetching MPN data from Web App table
        web_app_obj = WebAppOperations()
        web_app_mpn_data = web_app_obj.fetch_web_app_mpn_data(sub_id, order_id)
        self.logger.info(f'WebApp Data fetched for MPN Validation. \n Web App MPN Data : {web_app_mpn_data}')

        # Fetching MPN data from SAP Table
        sap_obj = SAPOperations()
        sap_mpn_data = sap_obj.fetch_sap_mpn_data(sub_id)
        self.logger.info(f'SAP data fetched for MPN Validation. \n SAP MPN Data : {sap_mpn_data}')

        return self.validate_mpn_criteria_and_compare(im360_mpn_data, web_app_mpn_data, sap_mpn_data)

    def compare_subscription_data(self, im360_sub, web_app_sub, sap_sub) -> bool:
        """ This method does the actual comparison of subscription data across IM360, WebApp and SAP. """

        flag = True
        self.logger.info(f"\nComparing data for subscription {sap_sub['provider_contract']} for all 3 Systems... ")

        if (str(sap_sub['provider_contract']) != str(im360_sub['subscription_id'])) or \
                (str(sap_sub['provider_contract']) != str(web_app_sub['subscription_id'])):
            flag = False
            self.logger.info(
                f"Provider Contract / Subscription ID did not match. \n SAP: {sap_sub['provider_contract']} "
                f"\t\t Web App: {web_app_sub['subscription_id']} "
                f"\t\t IM360: {im360_sub['subscription_id']}")
            self.sub_error_message += 'provider_contract, '

        if sap_sub['name'] != web_app_sub['subscription_name']:
            flag = False
            self.logger.error(f"Subscription Name did not match. \n SAP: {sap_sub['name']} "
                              f"\t\t Web App: {web_app_sub['subscription_name']}")
            self.sub_error_message += 'name, '

        # sap_sub['contract_start'], web_app_sub['subscription_start_date'] = \
        #     self.manipulate_date(sap_sub['contract_start'], web_app_sub['subscription_start_date'])
        # if sap_sub['contract_start'] != web_app_sub['subscription_start_date']:
        web_date = web_app_sub['subscription_start_date']
        web_date = web_date[8:10] + "." + web_date[5:7] + "." + web_date[0:4]
        sap_date = sap_sub['contract_start']
        sap_date = sap_date[0:10]
        sap_date = sap_date[3:5] + "." + sap_date[0:2] + "." + sap_date[6:10]

        if sap_date != web_date:
            flag = False
            self.logger.error(f"Contract Start date did not match. \n SAP: {sap_sub['contract_start']} "
                              f"\t\t Web App: {web_app_sub['subscription_start_date']}")
            self.sub_error_message += 'contract_start, '

        # sap_sub['contract_end'], web_app_sub['subscription_end_date'] = \
        #     self.manipulate_date(sap_sub['contract_end'], web_app_sub['subscription_end_date'])
        # if sap_sub['contract_end'] != web_app_sub['subscription_end_date']:
        web_end_date = web_app_sub['subscription_end_date']
        web_end_date = web_end_date[8:10] + "." + web_end_date[5:7] + "." + web_end_date[0:4]
        sap_end_date = sap_sub['contract_end']
        sap_end_date = sap_end_date[0:10]
        sap_end_date = sap_end_date[3:5] + "." + sap_end_date[0:2] + "." + sap_end_date[6:10]
        if sap_end_date != web_end_date:
            flag = False
            self.logger.error(f"Contract End date did not match. \n SAP: {sap_sub['contract_end']} "
                              f"\t\t Web App: {web_app_sub['subscription_end_date']}")
            self.sub_error_message += 'contract_end, '

        if (sap_sub['key_in_external_system'] != 'MD' + str(im360_sub['reseller_bcn'])) or \
                (sap_sub['key_in_external_system'] != 'MD' + str(web_app_sub['reseller_bcn'])):
            flag = False
            self.logger.error(f"Reseller BCN did not match. \n SAP: {sap_sub['key_in_external_system']} "
                              f"\t\t Web App: {str(web_app_sub['reseller_bcn'])} "
                              f"\t\t IM360: {'MD' + str(im360_sub['reseller_bcn'])}")
            self.sub_error_message += 'key_in_external_system, '

        if (sap_sub['im_vendor_sub_id'] != im360_sub['vendor_sub_id']) or \
                (sap_sub['im_vendor_sub_id'] != web_app_sub['vendor_subscription_id']):
            flag = False
            self.logger.error(f"Vendor Subscription ID did not match. \n SAP: {sap_sub['im_vendor_sub_id']} "
                              f"\t\t Web App: {im360_sub['vendor_sub_id']} \t\t IM360: "
                              f"{web_app_sub['vendor_subscription_id']}")
            self.sub_error_message += 'im_vendor_sub_id, '

        sap_payment_terms = int(sap_sub['im_payment_terms'])
        im360_payment_term = im360_sub['payment_term']
        # if (sap_payment_terms != im360_payment_term) or \
        #         (sap_payment_terms != web_app_sub['payment_method']):
        if sap_payment_terms != im360_payment_term:
            flag = False
            self.logger.error(f"Payment Term ID did not match. \n SAP: {sap_sub['im_payment_terms']} "
                              f"\t\t Web App: {web_app_sub['payment_method']} \t\t IM360: {im360_sub['payment_term']}")
            self.sub_error_message += 'im_payment_terms, '

        if (float(sap_sub['subscription_period']) != float(im360_sub['subscription_period'])) or \
                (float(sap_sub['subscription_period']) != float(web_app_sub['subscription_period'])):
            flag = False
            self.logger.error(f"Subscription Period did not match. \n SAP: {sap_sub['subscription_period']} "
                              f"\t\t Web App: {web_app_sub['subscription_period']} "
                              f"\t\t IM360: {im360_sub['subscription_period']}")
            self.sub_error_message += 'subscription_period, '

        if (sap_sub['subscrip_period_unit'] != im360_sub['subscription_period_unit']) or \
                (sap_sub['subscrip_period_unit'] != web_app_sub['subscription_period_unit']):
            flag = False
            self.logger.error(f"Subscription Period Unit did not match. \n SAP: {sap_sub['subscrip_period_unit']} "
                              f"\t\t Web App: {web_app_sub['subscription_period_unit']} "
                              f"\t\t IM360: {im360_sub['subscription_period_unit']}")
            self.sub_error_message += 'subscrip_period_unit, '

        web_billing_period = int(web_app_sub['billing_period'])
        if (sap_sub['billing_period'] != im360_sub['billing_period']) or \
                (sap_sub['billing_period'] != web_billing_period):
            flag = False

        if (float(sap_sub['billing_period']) != float(im360_sub['billing_period'])) or \
                (float(sap_sub['billing_period']) != float(web_app_sub['billing_period'])):
            flag = False
            self.logger.error(f"Billing Period did not match. \n SAP: {sap_sub['billing_period']} "
                              f"\t\t Web App: {web_app_sub['billing_period']} "
                              f"\t\t IM360: {im360_sub['billing_period']}")
            self.sub_error_message += 'billing_period, '

        if (sap_sub['billing_period_unit'] != im360_sub['billing_period_unit']) or \
                (sap_sub['billing_period_unit'] != web_app_sub['billing_period_unit']):
            flag = False
            self.logger.error(f"Billing Period Unit did not match. \n SAP: {sap_sub['billing_period_unit']} "
                              f"\t\t Web App: {web_app_sub['billing_period_unit']} "
                              f"\t\t IM360: {im360_sub['billing_period_unit']}")
            self.sub_error_message += 'billing_period_unit, '

        if (sap_sub['status'].upper() != im360_sub['status'].upper()) or \
                (sap_sub['status'].upper() != web_app_sub['status']):
            flag = False
            self.logger.error(
                f"Status did not match. \n SAP: {sap_sub['status']} \t\t Web App: {web_app_sub['status']} "
                f"\t\t IM360: {im360_sub['status']}")
            self.sub_error_message += 'status, '

        if sap_sub['plant'] != web_app_sub['marketplace_id']:
            flag = False
            self.logger.error(
                f"Market Place did not match. \n SAP: {sap_sub['plant']} \t\t Web App: {web_app_sub['marketplace_id']}")
            self.sub_error_message += 'plant, '

        self.sub_error_message = self.format_string(self.sub_error_message)

        if flag is True:
            self.logger.info("Subscription data matched in all 3 systems... ")

        return flag

    def compare_mpn_data(self, im360_mpn, web_app_mpn, sap_mpn):
        """ This method does the actual comparison of mpn data across IM360, WebApp and SAP. """

        flag = True

        self.logger.info(f"SAP MPN: {sap_mpn}")
        self.logger.info(f"\n Matching MPN data for SKU '{sap_mpn['product_id']}'")
        self.logger.info(f"\n Matching MPN data for SKU {sap_mpn['product_id']}")

        sap_mpn['subscription_id'] = str(sap_mpn['subscription_id'])
        im360_mpn['subscription_id'] = str(im360_mpn['subscription_id'])
        web_app_mpn['subscription_id'] = str(web_app_mpn['subscription_id'])
        if (sap_mpn['subscription_id'] != im360_mpn['subscription_id']) or (sap_mpn['subscription_id']
                                                                            != web_app_mpn['subscription_id']):
            flag = False
            self.logger.warning(f"Subscription ID did not match. \n SAP: {sap_mpn['subscription_id']} "
                                f"\t\t Web App: {web_app_mpn['subscription_id']} "
                                f"\t\t IM360: {im360_mpn['subscription_id']}")
            self.mpn_error_message += 'subscription_id, '

        self.logger.info("Validating TCV value")
        try:
            sap_mpn_total_amount = int(float(sap_mpn['total_amount'].replace(',', '.')))
        except Exception:
            sap_mpn_total_amount = int(float(sap_mpn['total_amount'].replace(',', '')))
        im360_mpn_reseller_total_contract_value = int(float(self.remove_currency(
            im360_mpn['reseller_total_contract_value']).replace(',', '')))
        web_app_mpn_reseller_total_contract_value = int(float(web_app_mpn['reseller_total_contract_value']))
        if sap_mpn_total_amount != im360_mpn_reseller_total_contract_value or \
                sap_mpn_total_amount != web_app_mpn_reseller_total_contract_value:
            flag = False
            self.logger.warning(f"TCV did not match. \n SAP: {sap_mpn_total_amount}"
                                f" \t\t Web App: {web_app_mpn_reseller_total_contract_value} "
                                f"\t\t IM360: {im360_mpn_reseller_total_contract_value}")
            self.mpn_error_message += 'total_amount, '

        self.logger.info("Validating TCC value")
        try:
            sap_mpn_cost = int(float(sap_mpn['cost'].replace(',', '.')))
        except Exception:
            sap_mpn_cost = int(float(sap_mpn['cost'].replace(',', '')))
        im360_mpn_vendor_total_contract_value = int(float(self.remove_currency(
            im360_mpn['vendor_total_contract_value']).replace(',', '')))
        web_app_mpn_vendor_total_contract_value = int(float(web_app_mpn['vendor_total_contract_value']))
        if (sap_mpn_cost != im360_mpn_vendor_total_contract_value) or (
                sap_mpn_cost != web_app_mpn_vendor_total_contract_value):
            flag = False
            self.logger.warning(f"TCC did not match. \n SAP: {sap_mpn_cost} "
                                f"\t\t Web App: {web_app_mpn_vendor_total_contract_value}"
                                f" \t\t IM360: {im360_mpn_vendor_total_contract_value}")
            self.mpn_error_message += 'cost, '

        self.logger.info("Validating quantity")
        if (int(float(sap_mpn['quantity'].replace(',', '.'))) != int(float(im360_mpn['item_quantity']))) or \
                (int(float(sap_mpn['quantity'].replace(',', '.'))) != int(float(web_app_mpn['item_quantity']))):
            flag = False
            self.logger.warning(f"Item Quantity did not match. \n SAP: {sap_mpn['quantity']}"
                                f" \t\t Web App: {web_app_mpn['item_quantity']}"
                                f" \t\t IM360: {im360_mpn['item_quantity']}")
            self.mpn_error_message += 'quantity, '

        self.logger.info("Validating whether cancellable or not")
        if (sap_mpn['cb_cancellable_flag'] == 'N' and web_app_mpn['cancelable'] != '0') or \
                (sap_mpn['cb_cancellable_flag'] == 'Y' and web_app_mpn['cancelable'] != '1'):
            flag = False
            self.logger.warning(f"Cancellable Type did not match.\n SAP: {sap_mpn['cb_cancellable_flag']}"
                                f" \t\t Web App: {web_app_mpn['cancelable']}")
            self.mpn_error_message += 'cb_cancellable_flag, '

        self.logger.info("Validating item type")
        if (sap_mpn['item_type'] != im360_mpn['item_type'].upper()) or \
                (sap_mpn['item_type'] != web_app_mpn['billing_type']):
            flag = False
            self.logger.warning(f"Item Type did not match. \n SAP: {sap_mpn['item_type']}"
                                f" \t\t Web App: {web_app_mpn['billing_type']}"
                                f" \t\t IM360: {im360_mpn['item_type']}")
            self.mpn_error_message += 'item_type, '

        self.logger.info("Validating product id")
        if (sap_mpn['product_id'] != im360_mpn['sku']) or \
                (sap_mpn['product_id'] != web_app_mpn['item_code']):
            flag = False
            self.logger.warning(f"SKU did not match. \n SAP: {sap_mpn['product_id']}"
                                f" \t\t Web App: {web_app_mpn['item_code']}"
                                f" \t\t IM360: {im360_mpn['sku']}")
            self.mpn_error_message += 'product_id, '

        self.logger.info("Validating currency")
        if (sap_mpn['currency'] != im360_mpn['currency']) or \
                (sap_mpn['currency'] != web_app_mpn['currency']):
            flag = False
            self.logger.warning(f"Currency did not match. \n SAP: {sap_mpn['currency']}"
                                f" \t\t Web App: {web_app_mpn['currency']}"
                                f" \t\t IM360: {im360_mpn['currency']}")
            self.mpn_error_message += 'currency, '

        self.mpn_error_message = self.format_string(self.mpn_error_message)

        if flag is True:
            self.logger.info("MPN data matched for all the 3 systems.")

        return flag

    def validate_mpn_criteria_and_compare(self, im360_mpn, web_app_mpn, sap_mpn):
        flag = True
        # if (len(sap_mpn) != len(im360_mpn)) or (len(sap_mpn) != len(web_app_mpn)):
        #     flag = False
        # else:
        for im360_item in im360_mpn:
            for web_app_item in web_app_mpn:
                for sap_item in sap_mpn:
                    if (sap_item['product_id'] == web_app_item['item_code']) and (sap_item['product_id'] ==
                                                                                  im360_item['sku']):
                        flag = self.compare_mpn_data(im360_item, web_app_item, sap_item)
                        break
        return flag

    @staticmethod
    def remove_currency(price):
        trim = re.compile(r'[^\d.,]+')
        return trim.sub('', price)

    @staticmethod
    def manipulate_date(sap_date, web_app_date):
        sap_date = sap_date.split('T')
        sap_date_parts = sap_date[0].split('/')
        sap_date = sap_date_parts[2] + '-' + sap_date_parts[0] + '-' + sap_date_parts[1]
        web_app_date = web_app_date.split('T')

        return sap_date, web_app_date[0]

    @staticmethod
    def format_string(msg):
        msg_lst = list(msg)
        msg_lst.pop()
        msg_lst[-1] = '!'
        return ''.join(msg_lst)
