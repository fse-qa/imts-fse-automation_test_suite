import sys
import traceback

# from pywinauto import Application, Desktop
from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.SAPOperations import SAPOperations
from Tests.test_base import BaseTest
from db.model.SAPBitsData import SAPBitsData
from db.service.SAPBitsDataService import SAPBitsDataDbManagementService


class SapGui_Bits(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def __init__(self, test_id, subs_id, order_number, from_date, export_file_path=None, session=None):
        self.subs_id = subs_id
        self.test_id = test_id
        # self.image_location = image_location
        self.order_number = order_number
        self.export_file_path = export_file_path
        self.session = session
        self.from_date = from_date

    def subscription_bits_details(self, skip_date_check=False):
        try:
            # MAXIMIZE
            self.session.findById("wnd[0]").maximize()

            # Type 'Displey Provider Contract' value in textbox i.e. 'fp_vt3'
            self.session.findById("wnd[0]/tbar[0]/okcd").text = "fkkbixbit_mon"
            # Press Enter to continue
            self.session.findById("wnd[0]").sendVKey(0)

            # Screen: Provider Contract Display: Initial screen
            # Provide Subscription ID within 'Provider Contract'
            # self.session.findById("wnd[0]/usr/ctxtVTREF-LOW").text = self.subs_id

            # Click on Other Selections
            self.session.findById("wnd[0]/tbar[1]/btn[25]").press()
            # Select 'IM Subscription ID' and add within panel
            self.session.findById("wnd[1]/shellcont/shell").selectNode("        136")
            self.session.findById("wnd[1]/shellcont/shell").topNode = "        135"
            self.session.findById("wnd[1]/shellcont/shell").doubleClickNode("        136")
            # Provide Subscription ID
            self.session.findById(
                "wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/txt%%DYN004-LOW").text = self.subs_id

            if not skip_date_check:
                # Select 'From Date' and add within panel
                self.session.findById("wnd[1]/shellcont/shell").selectNode("         22")
                self.session.findById("wnd[1]/shellcont/shell").topNode = "         11"
                self.session.findById("wnd[1]/shellcont/shell").doubleClickNode("         22")
                # Provide From Date
                self.session.findById("wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/ctxt%%DYN002-LOW").text = \
                    self.from_date

            # Save this changes
            self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
            # self.session.findById("wnd[0]/usr/ctxtBITDATE-LOW").text = self.from_date
            # self.session.findById("wnd[0]/usr/ctxtBITDATE-HIGH").text = self.from_date
            # Execute this selection to get vendor and reseller bits
            self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

            # Provider Contract i.e Subscription ID in CB exists
            if self.session.findById("wnd[0]/sbar").text != 'No data found for display':
                # Taking screenshots of that window. Screen: Display Of Billable Items
                SAPOperations().take_screenshot(f'display_of_billable_items_{self.subs_id}')
                # Select 0 and 1 row
                # self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").selectedRows = "0, 1"

                # *****************************************

                """
                Changing the Layout. This might need to be removed!
                """
                self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").pressToolbarContextButton \
                    ("&MB_VARIANT")
                self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").selectContextMenuItem("&LOAD")
                self.session.findById(
                    "wnd[1]/usr/subSUB_CONFIGURATION:SAPLSALV_CUL_LAYOUT_CHOOSE:0500/cntlD500_CONTAINER/shellcont/"
                    "shell").currentCellRow = 11
                self.session.findById(
                    "wnd[1]/usr/subSUB_CONFIGURATION:SAPLSALV_CUL_LAYOUT_CHOOSE:0500/cntlD500_CONTAINER/shellcont/"
                    "shell").firstVisibleRow = 0
                self.session.findById(
                    "wnd[1]/usr/subSUB_CONFIGURATION:SAPLSALV_CUL_LAYOUT_CHOOSE:0500/cntlD500_CONTAINER/shellcont/"
                    "shell").selectedRows = "11"
                self.session.findById(
                    "wnd[1]/usr/subSUB_CONFIGURATION:SAPLSALV_CUL_LAYOUT_CHOOSE:0500/cntlD500_CONTAINER/shellcont/"
                    "shell").clickCurrentCell()
                # *******************************************

                bits_lst = []

                bits_row_count = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").RowCount

                for row_num in range(bits_row_count):
                    sap_bits_data_object = None
                    if row_num % 10 == 0:
                        scroll_count = row_num

                    self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").selectedRows = row_num

                    grid_xpath = "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell"

                    # bits_dict_per_row.update({'test_id': self.test_id})
                    # bits_dict_per_row.update({'imSubscriptionId': self.session.findById(
                    #     grid_xpath).GetCellValue(row_num, "ZZ_IM_SUB_ID")})

                    self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").firstVisibleRow = scroll_count
                    order_num = self.session.findById(grid_xpath).GetCellValue(row_num, "ZZ_ORDER_NUM")

                    if order_num == self.order_number:

                        im_sub_id = self.session.findById(
                            grid_xpath).GetCellValue(row_num, "ZZ_IM_SUB_ID")

                        # Opening the Details Window
                        self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").pressToolbarButton(
                            "&DETAIL")
                        # Apply loop to find out which is the sku part
                        detail_row_count = self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").RowCount
                        sku = ''
                        for row in range(detail_row_count):
                            self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").selectedRows = row
                            if self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").GetCellValue(row,
                                                                                                         "COLUMNTEXT") \
                                    == 'SKU PART':
                                self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").selectedRows = row
                                sku = self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").GetCellValue(row,
                                                                                                                "VALUE")

                            else:
                                pass
                        self.session.findById("wnd[1]").close()
                        from_date = (self.session.findById(
                            grid_xpath).GetCellValue(row_num, "BITDATE_FROM")).replace(".", "")
                        to_date = (self.session.findById(
                            grid_xpath).GetCellValue(row_num, "BITDATE_TO")).replace(".", "")

                        if str(self.session.findById(grid_xpath).GetCellValue(row_num, "BIT_QUANTITY")).__contains__(
                                "-"):
                            bit_quantity = "-" + (self.session.findById(
                                grid_xpath).GetCellValue(row_num, "BIT_QUANTITY")).replace("-", "")
                        else:
                            bit_quantity = self.session.findById(
                                grid_xpath).GetCellValue(row_num, "BIT_QUANTITY")
                        if str(self.session.findById(grid_xpath).GetCellValue(row_num, "BIT_AMOUNT")).__contains__("-"):
                            bit_amount = "-" + (self.session.findById(
                                grid_xpath).GetCellValue(row_num, "BIT_AMOUNT")).replace("-", "")
                        else:
                            bit_amount = self.session.findById(
                                grid_xpath).GetCellValue(row_num, "BIT_AMOUNT")
                        ven_sub_id = self.session.findById(
                            grid_xpath).GetCellValue(row_num, "ZZ_VEN_SUB_ID")

                        bit_class = self.session.findById(grid_xpath).GetCellValue(row_num, "BITCAT")

                        if str(self.session.findById(grid_xpath).GetCellValue(row_num, "ZZ_DURATION")).__contains__(
                                "-"):
                            duration = (self.session.findById(
                                grid_xpath).GetCellValue(row_num, "ZZ_DURATION")).replace("-", "")
                        else:
                            duration = self.session.findById(
                                grid_xpath).GetCellValue(row_num, "ZZ_DURATION")
                        bcn_number = (self.session.findById(
                            grid_xpath).GetCellValue(row_num, "ZZ_BCN")).replace("MD", "")
                        unit_price = self.session.findById(
                            grid_xpath).GetCellValue(row_num, "ZZ_UNIT_PRICE")
                        unit_cost = self.session.findById(
                            grid_xpath).GetCellValue(row_num, "ZZ_UNIT_COST")
                        bill_item_type = self.session.findById(grid_xpath).GetCellValue(row_num, "BITTYPE")

                        sap_bits_data_object = SAPBitsData(im_sub_id, order_num, bcn_number, from_date, to_date,
                                                           bill_item_type,
                                                           bit_amount, bit_quantity, duration, unit_price, unit_cost,
                                                           sku, ven_sub_id, bit_class)

                    if sap_bits_data_object is not None:
                        bits_lst.append(sap_bits_data_object)

                # write bits data to DB
                bits_lst = list(set(bits_lst))
                self.write_bits_details_to_db(self.db_file_path, bits_lst)

                # # Export to Excel
                # self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
                # self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").selectContextMenuItem("&XXL")
                # self.session.findById("wnd[1]/usr/radRB_OTHERS").select()
                # self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
                # self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.export_file_path
                # self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = self.subs_id + "_Display_Of_Billable_Items.xlsx"
                # self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
                self.session.findById("wnd[0]").close()

                # # Excel window close
                # time.sleep(10)
                # windows = Desktop(backend="uia").windows()
                # print([w.window_text() for w in windows])
                # text = str(self.subs_id)+"_Display_Of_Billable_Items.xlsx - Excel"
                # app = Application(backend="uia").connect(title=text)
                # print(app.windows())
                # app.window(text).close()

            else:
                # Provider Contract i.e Subscription ID in CB does not exists. Will wait max 15 mnt for this
                self.logger.info('Waiting for Re execute ')

        except Exception as e:
            SAPOperations().take_screenshot(f'display_of_billable_items_{self.subs_id}', status='error')
            exception_type, exception_object, exception_traceback = sys.exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            self.logger.error("Exception Object: ", exception_object)
            self.logger.error("Exception Traceback: ", traceback.print_exc())
            self.logger.error("Exception type: ", exception_type)
            self.logger.error("File name: ", filename)
            self.logger.error("Line number: ", line_number)
            self.logger.error("[Method: subscription_details] \n[Internal Message: " + str(sys.exc_info()) + " ]")
            return False

        return True

    def write_bits_details_to_db(self, db_path, bits_list):
        SAPBitsDataDbManagementService().insert_sap_bits_data(db_path, bits_list)
