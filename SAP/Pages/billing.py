import datetime
import logging

from SAP.Facade.SAPOperations import SAPOperations

logger = logging.getLogger(__name__)


class Billing:
    def __init__(self, session, provider_contract, billable_bit_class=None, bill_from=None, to_date=None) -> None:
        self.session = session
        self.provider_contract = provider_contract
        self.bill_from = bill_from
        self.to_date = to_date
        self.billable_bit_class = billable_bit_class

    def filter_by_provider_contract(self):
        # Provide Subscription ID within 'Provider Contract'
        if not self.provider_contract:
            return

        # Select 'IM Subscription ID' and add within panel
        self.session.findById("wnd[1]/shellcont/shell").selectNode("        136")
        self.session.findById("wnd[1]/shellcont/shell").topNode = "        135"
        self.session.findById("wnd[1]/shellcont/shell").doubleClickNode("        136")

        # Provide Subscription ID
        self.session.findById(
            "wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/txt%%DYN004-LOW").text = self.provider_contract

    def filter_by_bill_from_date(self):
        # Select 'From Date' and add within panel
        if not self.bill_from:
            return

        self.session.findById("wnd[1]/shellcont/shell").selectNode("         22")
        self.session.findById("wnd[1]/shellcont/shell").topNode = "         11"
        self.session.findById("wnd[1]/shellcont/shell").doubleClickNode("         22")
        # Provide From Date
        self.session.findById("wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/ctxt%%DYN002-LOW").text = self.bill_from

    def fill_display_of_billable_items(self):
        # MAXIMIZE
        self.session.findById("wnd[0]").maximize()

        # Type 'Display Provider Contract' value in textbox i.e. 'fp_vt3'
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "fkkbixbit_mon"
        # Press Enter to continue
        self.session.findById("wnd[0]").sendVKey(0)

        # Select Billable Items for selection variant
        self.session.findById("wnd[0]/usr/cmbSELM").key = "02"

        # Click on Other Selections
        self.session.findById("wnd[0]/tbar[1]/btn[25]").press()

        self.filter_by_provider_contract()
        # self.filter_by_bill_from_date()

        # Save this changes
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()

        # Execute the selection
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Check the Bill Icon is visible
        try:
            assert self.session.findById("wnd[0]/tbar[1]/btn[31]").text == 'Bill'
            return True
        except Exception as ex:
            logger.warning("No data found for the given search. Data could already be billed. %s", str(ex))

    def bill_provider_contract(self):
        SAPOperations().take_screenshot(f'billable_records_{self.provider_contract}_{self.billable_bit_class}')
        # Get total number of records
        number_of_records = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").RowCount
        billable_records = []

        # Select all rows which are to be billed
        for row in range(number_of_records):
            bill_document_number = self.session.findById(
                "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(row, "BILLDOCNO")
            bit_class = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(row, 'BITCAT')
            if not bill_document_number and bit_class == self.billable_bit_class:
                billable_records.append(str(row))
            elif bit_class == self.billable_bit_class:
                logger.info("Billing Document [%s] already generated for the given bit", bill_document_number)

        if billable_records:
            self.session.findById(
                "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").selectedRows = ",".join(billable_records)

            # Click on Bill Button
            self.session.findById("wnd[0]/tbar[1]/btn[31]").press()

            # Select Billing Process
            self.session.findById("wnd[1]").sendVKey(4)
            self.session.findById("wnd[2]/usr/lbl[1,3]").caretPosition = 2
            self.session.findById("wnd[2]").sendVKey(2)

            # Select Billing date
            if not self.bill_from:
                self.bill_from = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(
                    int(billable_records[0]), 'BILL_FIRST')
            self.bill_from = datetime.datetime.strptime(self.bill_from, '%Y-%m-%d').strftime('%Y/%m/%d')
            date_today = datetime.date.today().strftime('%Y/%m/%d')
            if date_today > self.bill_from:
                self.bill_from = date_today
            self.session.findById("wnd[1]/usr/ctxtFKKBIX_PARAMS-BILL_DATE").text = datetime.datetime.strptime(
                self.bill_from, '%Y/%m/%d').strftime('%m/%d/%Y')

            # Check Simulate run checkbox for debugging. Use only in debug mode
            # self.session.findById("wnd[1]/usr/chkFKKBIX_PARAMS-SIMURUN").setFocus
            # self.session.findById("wnd[1]/usr/chkFKKBIX_PARAMS-SIMURUN").selected = True

            # Execute Billing
            self.session.findById("wnd[1]/tbar[0]/btn[5]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

            # Select All together
            self.session.findById("wnd[1]/tbar[0]/btn[19]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[0]").press()

            SAPOperations().take_screenshot(f'billed_records__{self.provider_contract}_{self.billable_bit_class}')
            return self.session.findById(
                "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(int(billable_records[0]), "BILLDOCNO")

    def exit_display_of_billable_items(self):
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
