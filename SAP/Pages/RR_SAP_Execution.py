import subprocess
import sys
import time

import win32com.client
import win32com.client

from CommonUtilities.parse_config import ParseConfigFile
from SAP.Pages.RR_SAP_Execution_Bits_Details import SapGui_Bits
from SAP.Pages.RR_SAP_Execution_Subs_Details import SapGui_Subs
from SAP.Pages.RR_SAP_Execution_rar_details import SapGui_RAR
from Tests.test_base import BaseTest


# from pywinauto import Application


class SapGui(BaseTest):
    session = None

    def __init__(self):
        try:
            # Close already open SAP window
            self.sap_window_close()

            # Open SAP window
            self.path = ParseConfigFile().get_data_from_config_json("sapCredentials", "file_location")
            subprocess.Popen(self.path)
            time.sleep(5)

            self.SapGuiAuto = win32com.client.GetObject("SAPGUI")
            if not type(self.SapGuiAuto) == win32com.client.CDispatch:
                return

            application = self.SapGuiAuto.GetScriptingEngine
            self.connection = application.OpenConnection("IM Services - Test", True)
            time.sleep(3)

            self.session = self.connection.Children(0)
        except:
            # print(sys.exc_info()[0])
            self.logger.error(
                'Not able to Connect TS1. Check internet / VPN connection.. [Method: __init__] \n[Internal Message: ' + str(
                    sys.exc_info()[0]) + " ]")

    def login(self):
        try:
            # Screen: SAP
            # THE CLIENT
            self.session.findById("wnd[0]/usr/txtRSYST-MANDT").text = "100"
            # USERNAME
            # fetch from json
            self.session.findById(
                "wnd[0]/usr/txtRSYST-BNAME").text = ParseConfigFile().get_data_from_config_json("sapCredentials",
                                                                                                "username")
            # PASSWORD
            # fetch from json
            self.session.findById(
                "wnd[0]/usr/pwdRSYST-BCODE").text = ParseConfigFile().get_data_from_config_json("sapCredentials",
                                                                                                "enc_password")
            # LANGUAGE
            self.session.findById("wnd[0]/usr/txtRSYST-LANGU").text = "EN"
            # ENTER
            self.session.findById("wnd[0]").sendVKey(0)

        except:
            # print(sys.exc_info()[0])
            self.logger.error(
                "Not able to login in TS1 [Method: sapLogin] \n[Internal Message: " + str(sys.exc_info()[1]) + " ]")
        time.sleep(2)

    # def sapLogin(self, image_path, export_file_path, test_id, subs_id, order_id):
    def sapLogin(self, image_path, test_id, provider_contract, order_num, ord_type, date_of_origin,
                 skip_date_check=False):
        self.login()

        # Subscription Details
        if ord_type == 'Feed':
            subs_object = SapGui_Subs(test_id, provider_contract, image_path, session=self.session)
            status = subs_object.subscription_details()

            # Log Off SAP
            self.log_off_sap()

            # SAP window close
            self.sap_window_close()

            return status

        else:
            # Bits details
            bits_object = SapGui_Bits(test_id, provider_contract, order_num, date_of_origin, image_path, self.session)
            status = bits_object.subscription_bits_details(skip_date_check=skip_date_check)
            # local_object.__init__(self, subs_id, image_path)

            # Bits Details

            # Log Off SAP
            self.log_off_sap()

            # SAP window close
            self.sap_window_close()

            return status

    def log_off_sap(self):
        # Window close
        self.session.findById("wnd[0]").close()
        # Popup: Log Off
        # Clicking confirmation of Log Off
        self.session.findById("wnd[1]/usr/btnSPOP-OPTION1").press()

    def sap_window_close(self):
        try:
            print("To be reverted...")
            # app = Application(backend="uia").connect(title="SAP Logon 760", timeout=5)
            # app.SapLogon760.close()

        except:
            self.logger.error("No existing window open")

    # def collect_subscription_data(self, tc_id, provider_contract, ord_type, image_path, date_of_origin):
    #     ord_num = None
    #     return self.sapLogin(image_path, tc_id, provider_contract, ord_num, ord_type, date_of_origin)

    # def collect_mpn_data(self, contract):
    #     print("Code to collect mpn data")
    #     return 'Status of Function that gets called...'

    # def collect_bits_data(self, tc_id, provider_contract, ord_num, ord_type, image_path, date_of_origin):
    #     return self.sapLogin(image_path, tc_id, provider_contract, ord_num, ord_type, date_of_origin)

    def sapLoginfor_rar(self, image_path, test_id, provider_contract):
        try:
            # Screen: SAP
            # THE CLIENT
            self.session.findById("wnd[0]/usr/txtRSYST-MANDT").text = "100"
            # USERNAME
            # fetch from json
            self.session.findById("wnd[0]/usr/txtRSYST-BNAME").text = ParseConfigFile().get_data_from_config_json \
                ("sapCredentials", "username")
            # PASSWORD
            # fetch from json
            self.session.findById("wnd[0]/usr/pwdRSYST-BCODE").text = ParseConfigFile().get_data_from_config_json \
                ("sapCredentials", "enc_password")
            # LANGUAGE
            self.session.findById("wnd[0]/usr/txtRSYST-LANGU").text = "EN"
            # ENTER
            self.session.findById("wnd[0]").sendVKey(0)

        except:
            # print(sys.exc_info()[0])
            self.logger.error("Not able to login in TS1 [Method: sapLogin] \n[Internal Message: "
                              + str(sys.exc_info()[1]) + " ]")
        time.sleep(2)

        try:
            subs_object = SapGui_RAR(test_id, provider_contract, image_path, session=self.session)
            return subs_object.rar_details()

        except Exception as ex:
            self.logger.error("Failed to capture rar details. " + str(ex))
            raise ex

        finally:
            # Log Off SAP
            self.log_off_sap()

            # SAP window close
            self.sap_window_close()

# if __name__ == '__main__':
#     # # Call SAP by Tinker
#     # window = Tk()
#     # window.geometry('200x60')
#     # btn = Button(window, text="Login SAP", command=lambda: SapGui().sapLogin("1074007", "../image/"))
#     # btn.pack()
#     # mainloop()
#
#     # Call SAP directly

# SapGui().sapLogin("../../Output/", "../../Output")
# SapGui().collect_subscription_data('TC001', '1046031', 'Feed', '../../Output', '12/04/2022')
# SapGui().collect_bits_data('TS-001', '1046039', 'R4017', 'Bits', '../../Output', '10/16/2023')

# 1045927, 1041871, 1045971, 1045986
