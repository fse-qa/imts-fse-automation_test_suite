import datetime
import logging

from SAP.Facade.SAPOperations import SAPOperations

logger = logging.getLogger(__name__)


class Invoicing:
    def __init__(self, session, provider_contract, billable_bit_class=None, bill_from=None, to_date=None) -> None:
        self.session = session
        self.provider_contract = provider_contract
        self.bill_from = bill_from
        self.to_date = to_date
        self.billable_bit_class = billable_bit_class

    def filter_by_provider_contract(self):
        # Provide Subscription ID within 'Provider Contract'
        if not self.provider_contract:
            return

        # Select 'IM Subscription ID' and add within panel
        self.session.findById("wnd[1]/shellcont/shell").selectNode("        136")
        self.session.findById("wnd[1]/shellcont/shell").topNode = "        135"
        self.session.findById("wnd[1]/shellcont/shell").doubleClickNode("        136")

        # Provide Subscription ID
        self.session.findById(
            "wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/txt%%DYN004-LOW").text = self.provider_contract

    def filter_by_bill_from_date(self):
        # Select 'From Date' and add within panel
        if not self.bill_from:
            return

        self.session.findById("wnd[1]/shellcont/shell").selectNode("         22")
        self.session.findById("wnd[1]/shellcont/shell").topNode = "         11"
        self.session.findById("wnd[1]/shellcont/shell").doubleClickNode("         22")
        # Provide From Date
        self.session.findById("wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/ctxt%%DYN002-LOW").text = self.bill_from

    def fill_display_of_billable_items(self):
        # MAXIMIZE
        self.session.findById("wnd[0]").maximize()

        # Type 'Display Provider Contract' value in textbox i.e. 'fp_vt3'
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "fkkbixbit_mon"
        # Press Enter to continue
        self.session.findById("wnd[0]").sendVKey(0)

        # Select Billed but not Invoiced for selection variant
        self.session.findById("wnd[0]/usr/cmbSELM").key = "04"

        # Click on Other Selections
        self.session.findById("wnd[0]/tbar[1]/btn[25]").press()

        self.filter_by_provider_contract()
        # self.filter_by_bill_from_date()

        # Save this changes
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()

        # Execute the selection
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Check the Invoice Icon is visible
        try:
            assert self.session.findById("wnd[0]/tbar[1]/btn[33]").text == 'Invoice'
            return True
        except Exception as ex:
            logger.warning("No data found for the given search. Data could already be invoiced. %s", str(ex))

    def invoice_provider_contract(self):
        SAPOperations().take_screenshot(f'invoice_records__{self.provider_contract}_{self.billable_bit_class}')
        # Get total number of records
        number_of_records = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").RowCount
        invoice_records = []
        # Select all rows which are to be invoiced
        for row in range(number_of_records):
            bill_document_number = self.session.findById(
                "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(row, "BILLDOCNO")
            bit_class = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(row, 'BITCAT')
            if not bill_document_number and bit_class == self.billable_bit_class:
                pass
            elif bit_class == self.billable_bit_class:
                logger.info("Billing Document [%s] already generated for the given bit", bill_document_number)
                invoice_records.append(str(row))

        if invoice_records:
            self.session.findById(
                "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").selectedRows = ",".join(invoice_records)

            # Click on Invoice Button
            self.session.findById("wnd[0]/tbar[1]/btn[33]").press()
            # Select Invoice Process
            if self.billable_bit_class == 'TIN1':  # TI
                self.session.findById("wnd[1]").sendVKey(4)
                self.session.findById("wnd[2]/usr/lbl[1,9]").setFocus()
                self.session.findById("wnd[2]/usr/lbl[1,9]").caretPosition = 0
                self.session.findById("wnd[2]").sendVKey(2)
            else:  # TC
                self.session.findById("wnd[1]").sendVKey(4)
                self.session.findById("wnd[2]/usr/lbl[1,7]").setFocus()
                self.session.findById("wnd[2]/usr/lbl[1,7]").caretPosition = 1
                self.session.findById("wnd[2]").sendVKey(2)

            # Select Invoice date
            if not self.bill_from:
                self.bill_from = self.session.findById("wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(
                    int(invoice_records[0]), 'BILL_FIRST')

            self.bill_from = datetime.datetime.strptime(self.bill_from, '%Y-%m-%d').strftime('%Y/%m/%d')
            date_today = datetime.date.today().strftime('%Y/%m/%d')
            if date_today > self.bill_from:
                self.bill_from = date_today

            self.bill_from = datetime.datetime.strptime(self.bill_from, '%Y/%m/%d').strftime('%m/%d/%Y')

            self.session.findById("wnd[1]/usr/ctxtG_INV_PARAMS-BLDAT").text = self.bill_from
            self.session.findById("wnd[1]/usr/ctxtG_INV_PARAMS-BUDAT").text = self.bill_from

            # Check Simulate run checkbox for debugging. Use only in debug mode
            # self.session.findById("wnd[1]/usr/chkG_INV_PARAMS-SIMURUN").setFocus()
            # self.session.findById("wnd[1]/usr/chkG_INV_PARAMS-SIMURUN").selected = True

            recon_key = self.session.findById("wnd[1]/usr/ctxtG_INV_PARAMS-FIKEY").text

            # Execute Invoicing
            self.session.findById("wnd[1]/tbar[0]/btn[5]").press()
            try:
                self.session.findById("wnd[1]/usr/btnBUTTON_1").press()
                self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
            except Exception as ex:
                self.session.findById("wnd[1]/tbar[0]/btn[19]").press()
                self.session.findById("wnd[1]/tbar[0]/btn[8]").press()
                self.session.findById("wnd[1]/usr/btnBUTTON_1").press()
                self.session.findById("wnd[1]/tbar[0]/btn[0]").press()

            invoice_document_number = self.session.findById(
                "wnd[0]/usr/cntlMON_CONTROL/shellcont/shell").GetCellValue(int(invoice_records[0]), "INVDOCNO")

            SAPOperations().take_screenshot(f'invoiced_records__{self.provider_contract}_{self.billable_bit_class}')

            return invoice_document_number, recon_key
        return None, None

    def exit_display_of_billable_items(self):
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
