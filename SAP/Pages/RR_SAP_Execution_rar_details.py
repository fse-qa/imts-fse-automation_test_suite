import logging
import sys
import traceback
from datetime import date

import CommonUtilities.RandomNumbers
from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.SAPOperations import SAPOperations

logger = logging.getLogger(__name__)


class SapGui_RAR():
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")
    status = {}

    today = date.today()
    SAPDateFormat = ParseConfigFile().get_data_from_config_json("dateFormat", "SAPDateFormat")
    fdate = today.strftime(SAPDateFormat)
    year = today.year

    def __init__(self, test_id, subs_id, image_location, export_file_path=None, session=None):
        self.subs_id = subs_id
        self.test_id = test_id
        self.image_location = image_location
        self.export_file_path = export_file_path
        self.session = session

    def fp_rai_transf_page(self):
        logger.info("Test execution is started for fp_rai_transf_page")

        # MAXIMIZE
        self.session.findById("wnd[0]").maximize()

        # Enter FP_RAI_TRANSF  tcode
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "FP_RAI_TRANSF"

        # Press Enter key
        self.session.findById("wnd[0]").sendVKey(0)

        # Enter Current date in Date Id field
        self.session.findById("wnd[0]/usr/ctxtFKKAKTIV2_DYNP_1000-LAUFD").text = self.fdate

        # Enter random Number in identification field
        self.session.findById(
            "wnd[0]/usr/ctxtFKKAKTIV2_DYNP_1000-LAUFI").text = CommonUtilities.RandomNumbers.GenerateRandomNum()

        # Press Enter key -->Contract field is displayed in editable format
        self.session.findById("wnd[0]").sendVKey(0)

        # generate new revenue accounting items
        self.session.findById(
            "wnd[0]/usr/subTABSTRIP:SAPLATAB:0100/tabsTABSTRIP100/tabpTAB01/ssubSUBSC:SAPLATAB:0200/subAREA1:SAPLFKK_RA_RAI_TRANSF:2000/chkPA_STEP2").selected = True

        # Enter Provider contract id in contract field
        self.session.findById(
            "wnd[0]/usr/subTABSTRIP:SAPLATAB:0100/tabsTABSTRIP100/tabpTAB01/ssubSUBSC:SAPLATAB:0200/"
            "subAREA3:SAPLFKK_RA_RAI_TRANSF:2002/ctxtSO_VTREF-LOW").text = self.subs_id

        # Enter current date in Selection to field
        self.session.findById(
            "wnd[0]/usr/subTABSTRIP:SAPLATAB:0100/tabsTABSTRIP100/tabpTAB01/ssubSUBSC:SAPLATAB:0200/"
            "subAREA3:SAPLFKK_RA_RAI_TRANSF:2002/ctxtPA_DATE").text = self.fdate
        self.session.findById(
            "wnd[0]/usr/subTABSTRIP:SAPLATAB:0100/tabsTABSTRIP100/tabpTAB01/ssubSUBSC:SAPLATAB:0200/"
            "subAREA3:SAPLFKK_RA_RAI_TRANSF:2002/ctxtPA_DATE").setFocus()
        self.session.findById(
            "wnd[0]/usr/subTABSTRIP:SAPLATAB:0100/tabsTABSTRIP100/tabpTAB01/ssubSUBSC:SAPLATAB:0200/"
            "subAREA3:SAPLFKK_RA_RAI_TRANSF:2002/ctxtPA_DATE").caretPosition = 5

        # Click on Save button on schedule job window
        self.session.findById("wnd[0]/tbar[0]/btn[11]").press()

        # Click on Schedule program run option
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Click on ok button on Schedule job window
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()

        # Click on Application log button
        # self.session.findById(
        #     "wnd[0]/usr/subTABSTRIP:SAPLATAB:0100/tabsTABSTRIP100/tabpTAB06/ssubSUBSC:SAPLATAB:0200/"
        #     "subAREA2:SAPLFKKAKTIV2:1320/btnAPEM").press()

        # Click on navigation back button for getting main page
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()

    def farr_rai_mon_page(self):
        logger.info("Test execution is started for farr_rai_mon_page page")

        # Enter FARR_RAI_MON  tcode
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "FARR_RAI_MON"

        # Click on Keyboard Enter button
        self.session.findById("wnd[0]").sendVKey(0)

        # Enter Provider contract id in Header Id field
        self.session.findById("wnd[0]/usr/txtS_HEADID-LOW").text = self.subs_id
        self.session.findById("wnd[0]/usr/txtS_HEADID-LOW").setFocus()
        self.session.findById("wnd[0]/usr/txtS_HEADID-LOW").caretPosition = 7
        # Click on execute button f8
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # select first row from table
        self.session.findById(
            "wnd[0]/usr/tabsTABSTRIP/tabpFC1/ssubSUB:SAPLFARR_RAI_MON:0301/cntlCONTROL_TAB_MI/shellcont/"
            "shell").currentCellRow = 0
        self.session.findById(
            "wnd[0]/usr/tabsTABSTRIP/tabpFC1/ssubSUB:SAPLFARR_RAI_MON:0301/cntlCONTROL_TAB_MI/shellcont/"
            "shell").selectedRows = "0"

        # Click on process button
        self.session.findById("wnd[0]/tbar[1]/btn[29]").press()

        # Click on ok button on processed itema window
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()

        # Navigate back to the main page
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()

    def zrar_rac_page(self):
        logger.info("Test execution is started for zrar_rac_page ")

        # Enter zrar_rac tcode
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "zrar_rac"

        # Click on Enter keyboard button
        self.session.findById("wnd[0]/tbar[0]/btn[0]").press()

        # Enter company code
        self.session.findById("wnd[0]/usr/ctxtS_BUKRS-LOW").text = "8190"

        # Enter Provider contract id
        self.session.findById("wnd[0]/usr/txtS_VTREF-LOW").text = self.subs_id

        self.session.findById("wnd[0]/usr/txtS_VTREF-LOW").setFocus()
        self.session.findById("wnd[0]/usr/txtS_VTREF-LOW").caretPosition = 4

        # click on execute button
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Validation on POBPRICE and PCTCV column values,both values should be same
        self.validation_rar_process()

        # Select first row from the table
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").modifyCheckbox(0, "CHECK", True)

        # Click on Detailed revenue option
        self.session.findById("wnd[0]/tbar[1]/btn[7]").press()

        # Get substring value for posting period
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "PERIOD"
        posting_period = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "PERIOD")
        posting_period = posting_period[4:]
        self.status["posting_period"] = posting_period
        # Navigate back to the main page
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()

    def farr_rev_transfer_page(self):
        logger.info("Test execution is started for farr_rev_transfer_page")

        # Enter Farr_rev_transfer tcode
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "Farr_rev_transfer"

        # Click on Keyboard enter key
        self.session.findById("wnd[0]/tbar[0]/btn[0]").press()

        # Enter Company code
        self.session.findById("wnd[0]/usr/ctxtSO_BUKRS-LOW").text = "8190"

        # Enter Accounting principle value
        self.session.findById("wnd[0]/usr/ctxtP_ACCPR").text = "GAAP"

        # Enter fiscal year
        self.session.findById("wnd[0]/usr/txtP_YEAR").text = self.year

        # Enter posting period
        self.session.findById("wnd[0]/usr/txtP_PERIOD").text = self.status["posting_period"]
        # Enter rar contract id0
        self.session.findById("wnd[0]/usr/txtSO_CNTRS-LOW").text = self.status["contract_id"]

        # Click on Dialog mode checkbox
        self.session.findById("wnd[0]/usr/chkP_XDIA").setFocus()
        self.session.findById("wnd[0]/usr/chkP_XDIA").selected = True

        # Click on execute button
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Get revenue_started_msg on Display log window
        rowcount = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").RowCount
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").currentCellColumn = "T_MSG"
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").selectedRows = "1"
        revenue_started_msg_farr_rev_transfer_page = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").GetCellValue(1, "T_MSG")

        # Get revenue_completed_msg on Display log window
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").currentCellColumn = "T_MSG"
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").selectedRows = "18"
        revenue_completed_msg_farr_rev_transfer_page = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").GetCellValue(rowcount - 1, "T_MSG")

        # Navigate back to main page
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()

        self.status["revenue_started_msg_farr_liability_calc"] = revenue_started_msg_farr_rev_transfer_page
        self.status["revenue_completed_msg_farr_liability_calc"] = revenue_completed_msg_farr_rev_transfer_page
        logger.info(revenue_completed_msg_farr_rev_transfer_page)

    def farr_liability_calc_page(self):
        logger.info("Test execution is started for farr_liability_calc_page ")

        # Enter farr_liability_calc tcode
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "farr_liability_calc"

        # Press Enter key from keybaord
        self.session.findById("wnd[0]/tbar[0]/btn[0]").press()

        # Enter Company code
        self.session.findById("wnd[0]/usr/ctxtSO_BUKRS-LOW").text = "8190"

        # Enter Accounting principle value
        self.session.findById("wnd[0]/usr/ctxtP_ACCPR").text = "GAAP"

        # Enter Fiscal year value
        self.session.findById("wnd[0]/usr/txtP_YEAR").text = self.year

        # Enter Posting period
        self.session.findById("wnd[0]/usr/txtP_PERIOD").text = self.status["posting_period"]

        self.session.findById("wnd[0]/usr/ctxtSO_CNTRS-LOW").text = "41"
        self.session.findById("wnd[0]/usr/ctxtSO_CNTRS-LOW").setFocus()
        self.session.findById("wnd[0]/usr/ctxtSO_CNTRS-LOW").caretPosition = 2
        self.session.findById("wnd[0]").sendVKey(0)

        # Enter rar contract id in contract field
        self.session.findById("wnd[0]/usr/ctxtSO_CNTRS-LOW").text = self.status["contract_id"]

        # Click on Dialog mode checkbox
        self.session.findById("wnd[0]/usr/chkP_XDIA").setFocus()
        self.session.findById("wnd[0]/usr/chkP_XDIA").selected = True

        # Click on execute button
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Get revenue_started_msg for farr_liability_calc
        rowcount_farr_liability_calc = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").RowCount
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").currentCellColumn = "T_MSG"
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").selectedRows = "1"
        revenue_started_msg_farr_liability_calc = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").GetCellValue(1, "T_MSG")

        # Get revenue_completed_msg  for farr_liability_calc
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").currentCellColumn = "T_MSG"
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").selectedRows = "18"
        revenue_completed_msg_farr_liability_calc = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").GetCellValue(rowcount_farr_liability_calc - 1, "T_MSG")

        # Navigate back to main page
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        logger.info(revenue_completed_msg_farr_liability_calc)
        self.status["revenue_started_msg_farr_liability_calc"] = revenue_started_msg_farr_liability_calc
        self.status["revenue_completed_msg_farr_liability_calc"] = revenue_completed_msg_farr_liability_calc

    def farr_rev_post_page(self):
        logger.info("Test execution is started for farr_rev_post_page ")

        # Enter Farr_rev_post tcode
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "Farr_rev_post"

        # Clidk on Enter key from keybaord
        self.session.findById("wnd[0]/tbar[0]/btn[0]").press()

        # Enter Company code
        self.session.findById("wnd[0]/usr/ctxtSO_BUKRS-LOW").text = "8190"

        # Enter Accounting principle value
        self.session.findById("wnd[0]/usr/ctxtP_ACCPR").text = "GAAP"

        # Enter Fiscal year
        self.session.findById("wnd[0]/usr/txtP_YEAR").text = self.year

        # Enter Posting period
        self.session.findById("wnd[0]/usr/txtP_PERIOD").text = self.status["posting_period"]

        # Enter rar contrac id in contract field
        self.session.findById("wnd[0]/usr/txtSO_CNTRS-LOW").text = self.status["contract_id"]
        self.session.findById("wnd[0]/usr/txtSO_CNTRS-LOW").setFocus()
        self.session.findById("wnd[0]/usr/txtSO_CNTRS-LOW").caretPosition = 4

        # Click on execute button
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        # Get revenue_started_msg  on Farr_rev_post page
        rowcount_farr_rev_post = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").RowCount
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").currentCellColumn = "T_MSG"
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").selectedRows = "1"
        revenue_started_msg_farr_rev_post = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").GetCellValue(1, "T_MSG")

        # Get revenue_completed_msg on Farr_rev_post page
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").currentCellColumn = "T_MSG"
        self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").selectedRows = "1"
        revenue_completed_msg_farr_rev_post = self.session.findById(
            "wnd[0]/usr/subSUBSCREEN:SAPLSBAL_DISPLAY:0101/cntlSAPLSBAL_DISPLAY_CONTAINER/shellcont/shell/shellcont[1]/"
            "shell").GetCellValue(rowcount_farr_rev_post - 1, "T_MSG")

        # Navigate back to main page
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
        logger.info(revenue_completed_msg_farr_rev_post)
        self.status["revenue_started_msg_farr_rev_post"] = revenue_started_msg_farr_rev_post
        self.status["revenue_completed_msg_farr_rev_post"] = revenue_completed_msg_farr_rev_post

    def nzg2npost_page(self):
        logger.info("Test execution is started for nzg2npost_page ")
        # Type '/nzg2npost' value in textbox
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "/nzg2npost"
        # Press Enter to continue
        self.session.findById("wnd[0]/tbar[0]/btn[0]").press()

        # Enter company id
        self.session.findById("wnd[0]/usr/ctxtP_BUKRS").text = "8190"
        # Enter Account principle
        self.session.findById("wnd[0]/usr/txtP_ACC_PR").text = "GAAP"

        self.session.findById("wnd[0]/usr/txtP_POPER").text = "202"

        self.session.findById("wnd[0]").sendVKey(2)
        # Enter posting period
        self.session.findById("wnd[0]/usr/txtP_POPER").text = self.status["posting_period"]

        # Enter Year
        self.session.findById("wnd[0]/usr/txtP_GJAHR").text = self.year
        # Enter pob id
        self.session.findById("wnd[0]/usr/txtS_POB_ID-LOW").text = self.status["pob_id"]
        # click on execute f8 button
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        # if posting is already done then pop message is displayed
        try:
            if self.session.findById("wnd[1]/tbar[0]/btn[0]").text:
                stu = self.session.findById("wnd[1]/tbar[0]/btn[0]").text
                logger.info("Posting is already done for nzg2npost_page")
        except Exception as ex:
            logger.info("Posting completed for nzg2npost_page")

        try:
            self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
            logger.info("Posting completed for nzg2npost_page")
        except Exception:
            pass

    def rar_details(self):
        try:
            self.fp_rai_transf_page()
            self.farr_rai_mon_page()
            self.zrar_rac_page()
            self.farr_rev_transfer_page()
            self.farr_liability_calc_page()
            self.farr_rev_post_page()
            self.nzg2npost_page()
        except Exception as ex:
            SAPOperations().take_screenshot(f'rar_failed_{self.subs_id}', status='error')
            exception_type, exception_object, exception_traceback = sys.exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            logger.info("Exception Object: ", exception_object)
            logger.info("Exception Traceback: ", traceback.print_exc())
            logger.info("Exception type: ", exception_type)
            logger.info("File name: ", filename)
            logger.info("Line number: ", line_number)

            print("[Method: subscription_details] \n[Internal Message: " + str(sys.exc_info()) + " ]")
        return self.status

    # ******************************************************** Validation ********************************************
    def validation_rar_process(self):

        # Get G2NREV value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "G2NREV"
        g2n_rev = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "G2NREV")

        # Get pob_id value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "POBID"
        pob_id = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "POBID")

        # Get contract_id value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "CONTRACTID"
        contract_id = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "CONTRACTID")

        # Get G2NCOST value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "G2NCOST"
        g2n_cost = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "G2NCOST")

        # Get POBPRICE value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "TRXPRICE"
        pob_price = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "TRXPRICE")

        # Get POBCOST value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "COST"
        pob_cost = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "COST")

        # Get PCTCV value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "PCTCV"
        pc_tcv = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "PCTCV")

        # Get PCCOMMITCOST value
        self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").currentCellColumn = "PCCOMMITCOST"
        pc_commit_cost = self.session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell").GetCellValue(0, "PCCOMMITCOST")

        self.status["g2n_rev"] = g2n_rev
        self.status["pob_id"] = pob_id
        self.status["contract_id"] = contract_id
        self.status["g2n_cost"] = g2n_cost
        self.status["pob_price"] = pob_price
        self.status["pob_cost"] = pob_cost
        self.status["pc_tcv"] = pc_tcv
        self.status["pc_commit_cost"] = pc_commit_cost
