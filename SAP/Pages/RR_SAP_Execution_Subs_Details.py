import sys
import time
# from comtypes.client import CreateObject
# from pywinauto import Application
import traceback

from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.SAPOperations import SAPOperations
from Tests.test_base import BaseTest
from db.model.SAPMPNData import SAPMPNData
from db.model.SAPSubscriptionData import SAPSubscriptionData
from db.service.SAPMPNDataService import SAPMPNDbManagementService
from db.service.SAPSubscriptionDataService import SAPSubscriptionDataDbManagementService


class SapGui_Subs(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")
    cc_number = None

    def __init__(self, test_id, subs_id, image_location, export_file_path=None, session=None):
        self.subs_id = subs_id
        self.test_id = test_id
        self.image_location = image_location
        self.export_file_path = export_file_path
        self.session = session

    def subscription_details(self):
        try:
            # MAXIMIZE
            self.session.findById("wnd[0]").maximize()

            # Type 'Displey Provider Contract' value in textbox i.e. 'fp_vt3'
            self.session.findById("wnd[0]/tbar[0]/okcd").text = "fp_vt3"
            # Press Enter to continue
            self.session.findById("wnd[0]").sendVKey(0)

            # Screen: Provider Contract Display: Initial screen
            # Provide Subscription ID within 'Provider Contract'
            self.session.findById("wnd[0]/usr/subA01P01:SAPLFKK_VT_BDT:0100/ctxtDFKK_VT_H-VTKEY").text = self.subs_id
            self.session.findById("wnd[0]/tbar[0]/btn[0]").press()

            # Provider Contract i.e Subscription ID in CB exists
            if self.session.findById("wnd[0]/sbar").text == 'The validity time was set automatically':
                # Screen: Get provider contract display: general data
                if not self.get_provider_contract_display_general_data(self.subs_id, self.image_location):
                    return False

                # Screen: Get provider contract display: Items: Revenue Accounting
                if not self.get_provider_contract_display_items(self.subs_id, self.image_location):
                    return False
                # self.get_provider_contract_mpn_details(self.subs_id, self.image_location)

                # Back two times to get 'SAP Easy Access' window
                self.session.findById("wnd[0]/tbar[0]/btn[3]").press()
                self.session.findById("wnd[0]/tbar[0]/btn[3]").press()

            else:
                # Provider Contract i.e Subscription ID in CB does not exists. Will wait max 15 mnt for this
                self.logger.info('Waiting for Re execute ')
        except Exception as e:
            exception_type, exception_object, exception_traceback = sys.exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            self.logger.error("Exception Object: ", exception_object)
            self.logger.error("Exception Traceback: ", traceback.print_exc())
            self.logger.error("Exception type: ", exception_type)
            self.logger.error("File name: ", filename)
            self.logger.error("Line number: ", line_number)
            self.logger.error("[Method: subscription_details] \n[Internal Message: " + str(sys.exc_info()) + " ]")
            return False

        return True

    def get_provider_contract_display_general_data(self, subs_id, image_location):
        try:
            subs_lst = []
            subs_dict = {}

            # MAXIMIZE
            self.session.findById("wnd[0]").maximize()
            # Select 'Get provider contract display: general data' tab
            self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01").select()
            subscriptionid = self.session.findById(
                "wnd[0]/usr/subGENSUB_HD:SAPLBUSS:1001/subA01P01:SAPLFKK_VT_BDT:0150/ctxtDFKK_VT_H-VTKEY").text
            # Contract Management Data
            subscriptionname = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/txtDFKK_VT_H-VTBEZ").text
            subscriptionstartdate = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/ctxtFKK_VT_DYNP-VTBEG_DATE").text
            subscriptionstarttime = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/ctxtFKK_VT_DYNP-VTBEG_TIME").text
            subscriptionenddate = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/ctxtFKK_VT_DYNP-VTEND_DATE").text
            subscriptionendtime = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/ctxtFKK_VT_DYNP-VTEND_TIME").text
            customeracopeuid = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/txtDFKK_VT_H-VTALT").text
            status = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200"
                "/txtFKK_VT_DYNP-STATUTXT").text

            # IM General Section Fields
            vendorsubscriptionid = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_VEN_SUB_ID").text
            bill_to = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_BILLTO").text
            payment_method = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_PAYMETHOD").text
            subscriptionperiod = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_SUB_PERIOD").text
            subscriptionperiodunit = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_SUB_PERIOD_UNIT").text
            billingperiod = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_BILLING_PERIOD").text
            billingperiodunit = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA03P01:SAPLZVKK_BDT_H_PRC:1000"
                "/txtDFKK_VT_H-ZZ_BILLING_PERIOD_UNIT").text

            # Select 'Get provider contract display: Items: Account Assignment' tab
            self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR03").select()
            # select plant
            plant = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR03/ssubGENSUB:SAPLBUSS:7029/subA04P01:SAPLZFICA_PC_SCREEN600:1000"
                "/ctxtDFKK_VT_I-ZZ_PLANT").text

            contract_start = subscriptionstartdate + "T" + subscriptionstarttime + "Z"
            contract_end = subscriptionenddate + "T" + subscriptionendtime + "Z"
            key_in_external_system = customeracopeuid
            company_code = ''
            flooring_bp = ''

            sap_feed_obj = SAPSubscriptionData(self.test_id, subscriptionid, subscriptionname, contract_start,
                                               contract_end, key_in_external_system, company_code, vendorsubscriptionid,
                                               bill_to, payment_method, subscriptionperiod, subscriptionperiodunit,
                                               billingperiod, billingperiodunit, status, flooring_bp, plant)

            subs_lst.append(sap_feed_obj)
            # Taking screenshots of that window
            # Connect SAP Window
            # app = Application(backend="uia").connect(title="SAP Logon 760", timeout=10)
            """
            # To be reverted
            app = Application(backend="uia").connect(title="SAP Logon 760", timeout=100)
            app.ProviderContractDisplayGeneralData.set_focus()
            debug_image = app.ProviderContractDisplayGeneralData.capture_as_image()
            debug_image.save(image_location + subs_id + "_Provider_Contract_Disply_General.png")
            
            """

            # # Taking screenshots of full page
            # with mss.mss() as sct:
            #     monitor = {"top": 160, "left": 160, "width": 160, "height": 135}
            #     img_array = np.array(sct.grab(monitor))
            #     filename = sct.shot(output="output.png")

            # inserting the subscription data into the database
            self.write_sub_details_to_db(self.db_file_path, subs_lst)

        except:
            exception_type, exception_object, exception_traceback = sys.exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            self.logger.error("Exception Object: ", exception_object)
            self.logger.error("Exception Traceback: ", traceback.print_exc())
            self.logger.error("Exception type: ", exception_type)
            self.logger.error("File name: ", filename)
            self.logger.error("Line number: ", line_number)
            self.logger.error("[Method: get_provider_contract_display_general_data] \n[Internal Message: " + str(
                sys.exc_info()[1]) + " ]")
            return False

        return True

    def get_provider_contract_display_items(self, subs_id, image_location):
        try:
            self.session.findById("wnd[0]").maximize()
            # Select 'Get provider contract display: Items: General Data' tab
            SAPOperations().take_screenshot(f'display_of_items_general_tab_{self.subs_id}')
            self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR02").select()
            # Select End of Duration
            eod = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR02/ssubGENSUB:SAPLBUSS:7052/subA03P09:SAPLFKK_VT_BDT:0317"
                "/ctxtFKK_VT_DYNP-VALID_TO_CTRTERM_DATE").text

            # Select 'Get provider contract display: Items: Convergent Invoicing' tab
            self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR04").select()
            SAPOperations().take_screenshot(f'display_of_items_convergent_invoicing_tab_{self.subs_id}')
            # Select Payment card ID
            payment_card_id = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR04/ssubGENSUB:SAPLBUSS:7040/subA04P01:SAPLZVKK_BDT_PRC_I:1000"
                "/txtDFKK_VT_I-CB_CCARD_ID").text
            self.logger.info(f"Payment Card ID: {payment_card_id}")

            if len(payment_card_id) != 0:
                self.cc_number = self.fetch_cc_number(payment_card_id)

            # Select 'Get provider contract display: Items: Revenue Accounting' tab
            self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05").select()
            SAPOperations().take_screenshot(f'display_of_items_revenue_accounting_tab_{self.subs_id}')
            # Select Exchange Rate
            exchange_rate = self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073"
                                                  "/subA05P01:SAPLZVKK_BDT_PRC:1000/txtDFKK_VT_I-ZZ_FXRATE").text

            # Taking screenshots of that window
            """
            # To be reverted
            app = Application(backend="uia").connect(title="SAP Logon 760", timeout=100)
            app.ProviderContractDisplayGeneralData.set_focus()
            debug_image = app.ProviderContractDisplayItemsRevenueAccounting.capture_as_image()
            debug_image.save(image_location + subs_id + "_Provider_Contract_Disply_Items_Revenue_Accounting.png")
            """

            mpn_lst = []

            # Select Active Item
            self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                "/radFKK_VT_DYNP-XGRID").select()
            row_count = self.session.findById(
                "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                "/cntlCONT_VT_POS/shellcont/shell").RowCount
            # Table id
            grid_val = "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300" \
                       "/cntlCONT_VT_POS/shellcont/shell"

            sap_mpn_obj = None

            for row_number in range(row_count):
                scroll_point = 0
                mpn_dict = {}
                if row_number % 6 == 0:
                    scroll_point = row_number
                self.session.findById(
                    "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                    "/cntlCONT_VT_POS/shellcont/shell").currentCellRow = row_number
                self.session.findById(
                    "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                    "/cntlCONT_VT_POS/shellcont/shell").selectedRows = row_number
                self.session.findById(
                    "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                    "/cntlCONT_VT_POS/shellcont/shell").pressButtonCurrentCell()
                time.sleep(2)

                self.session.findById(
                    "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                    "/cntlCONT_VT_POS/shellcont/shell").firstVisibleRow = scroll_point

                # Finds out the item_type
                # self.session.findById(
                #     "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300"
                #     "/cntlCONT_VT_POS/shellcont/shell").pressToolbarButton("&DETAIL")
                # self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").setCurrentCell(39, "VALUE")
                # self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").firstVisibleRow = 23
                # self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").selectedRows = "39"
                # item_type = self.session.findById("wnd[1]/usr/cntlGRID/shellcont/shell").GetCellValue(39, "VALUE")
                # self.session.findById("wnd[1]").close()

                self.session.findById(
                    "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300/cntlCONT_VT_POS/shellcont/shell").firstVisibleRow = row_number
                self.session.findById(
                    "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA02P01:SAPLFKK_VT_BDT:0300/cntlCONT_VT_POS/shellcont/shell").firstVisibleColumn = "ZZ_ITEM_TYPE"
                item_type = self.session.findById(grid_val).GetCellValue(row_number, "ZZ_ITEM_TYPE")
                if item_type == 'FIXED':
                    subscriptionid = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA03P01:SAPLFKK_VT_BDT:0500"
                        "/txtDFKK_VT_I-RA_REFID").text
                    itemcode = self.session.findById(grid_val).GetCellValue(row_number, "ZZ_ITEMCODE")
                    resellertotalcontractvalue = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA04P01:SAPLFKK_VT_BDT:0510"
                        "/txtDFKK_VT_I-TRPRC_TOTAL").text
                    amount_currency = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA04P01:SAPLFKK_VT_BDT:0510"
                        "/ctxtFKK_VT_DYNP-TRPRC_CURR").text
                    vendortotalcontractvalue = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA05P01:SAPLZVKK_BDT_PRC:1000"
                        "/txtDFKK_VT_I-ZZ_COMMIT_COST").text
                    cost_currency = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA05P01:SAPLZVKK_BDT_PRC:1000"
                        "/txtDFKK_VT_I-ZZ_COST_CURRENCY").text
                    itemquantity = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA05P01:SAPLZVKK_BDT_PRC:1000"
                        "/txtDFKK_VT_I-ZZ_QTY").text
                    unit_of_measure = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA05P01:SAPLZVKK_BDT_PRC:1000"
                        "/txtDFKK_VT_I-ZZ_UOM").text
                    cancelable = self.session.findById(
                        "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR05/ssubGENSUB:SAPLBUSS:7073/subA05P01:SAPLZVKK_BDT_PRC"
                        ":1000/txtDFKK_VT_I-ZZ_CANCELLABLE_CB").text

                    if amount_currency == cost_currency:
                        if subs_id == subscriptionid:
                            mpn_dict.update(
                                {'test_id': self.test_id, 'subscriptionid': subscriptionid, 'itemcode': itemcode,
                                 'item_type': item_type, 'resellertotalcontractvalue': resellertotalcontractvalue,
                                 'vendortotalcontractvalue': vendortotalcontractvalue, 'currency': cost_currency,
                                 'itemquantity': itemquantity, 'unit_of_measure': unit_of_measure})
                        else:
                            self.logger.error(
                                "Subscription ID not matching with SAP Contract value. \n\t Subscription ID: " + subs_id + "\n\t"
                                                                                                                           " SAP Contract Value: " + subscriptionid)
                            mpn_dict.update(
                                {'test_id': self.test_id,
                                 'subscriptionid': "Commerce Subscription ID: " + subs_id + "SAP Subscription ID: " + subscriptionid,
                                 'itemcode': itemcode, 'item_type': item_type,
                                 'resellertotalcontractvalue': resellertotalcontractvalue,
                                 'vendortotalcontractvalue': vendortotalcontractvalue, 'currency': cost_currency,
                                 'itemquantity': itemquantity, 'unit_of_measure': unit_of_measure})

                    else:
                        self.logger.info('Currency mismatch between cost and total_amount.\n\t '
                                         'Total-Amount Currency: ' + amount_currency +
                                         "\n\t Cost Currency" + cost_currency)
                        if subs_id == subscriptionid:
                            mpn_dict.update(
                                {'test_id': self.test_id, 'subscriptionid': subscriptionid, 'itemcode': itemcode,
                                 'item_type': item_type, 'resellertotalcontractvalue': resellertotalcontractvalue,
                                 'vendortotalcontractvalue': vendortotalcontractvalue, 'currency':
                                     "resellertotalcontract_currency: " + amount_currency + " and cost_currency: " + cost_currency,
                                 'itemquantity': itemquantity, 'unit_of_measure': unit_of_measure})
                        else:
                            self.logger.error(
                                "Subscription ID not matching with SAP Contract value. \n\t Subscription ID: " +
                                subs_id + "\n\t SAP Contract Value: " + subscriptionid)

                    total_amount = resellertotalcontractvalue
                    cost = vendortotalcontractvalue
                    payment_card_id = ''
                    end_of_duration = eod
                    cb_cancellable_flag = cancelable
                    exchange_rate = ''
                    migration_date = ''
                    original_start_date = ''
                    currency = cost_currency

                    sap_mpn_obj = SAPMPNData(subscriptionid, total_amount, cost, itemquantity,
                                             payment_card_id, self.cc_number, end_of_duration, cb_cancellable_flag,
                                             exchange_rate, migration_date, original_start_date,
                                             item_type, itemcode, currency)

                    mpn_lst.append(sap_mpn_obj)

                else:
                    pass

            # inserting the subscription data into the database
            self.write_mpn_details_to_db(self.db_file_path, mpn_lst)

        except Exception as e:
            SAPOperations().take_screenshot(f'provider_contract_display_items_{self.subs_id}', status='error')
            exception_type, exception_object, exception_traceback = sys.exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            self.logger.error("Exception Object: ", exception_object)
            self.logger.error("Exception Traceback: ", traceback.print_exc())
            self.logger.error("Exception type: ", exception_type)
            self.logger.error("File name: ", filename)
            self.logger.error("Line number: ", line_number)
            self.logger.error(
                "[Method: get_provider_contract_display_items_revenue_accounting] \n[Internal Message: " + str(
                    sys.exc_info()[1]) + " ]")
            return False

        return True

    def write_sub_details_to_db(self, db_path, sub_list):
        SAPSubscriptionDataDbManagementService().insert_sap_subs_data(db_path, sub_list)

    def write_mpn_details_to_db(self, db_path, mpn_list):
        SAPMPNDbManagementService().insert_mpn_data(db_path, mpn_list)

    def fetch_cc_number(self, pc_id):
        cc_number = None

        # Select 'Get provider contract display: general data' tab
        self.session.findById("wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01").select()

        # Double Cick on Business Partner
        self.session.findById(
            "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200/"
            "ctxtDFKK_VT_H-GPART").setFocus()
        self.session.findById(
            "wnd[0]/usr/tabsTABSTRIP1/tabpBUSCR01/ssubGENSUB:SAPLBUSS:7072/subA02P01:SAPLFKK_VT_BDT:0200/"
            "ctxtDFKK_VT_H-GPART").caretPosition = 1
        self.session.findById("wnd[0]").sendVKey(2)

        # Click on Payment Transactions Tab
        self.session.findById(
            "wnd[0]/usr/subSCREEN_3000_RESIZING_AREA:SAPLBUS_LOCATOR:2000/subSCREEN_1010_RIGHT_AREA:"
            "SAPLBUPA_DIALOG_JOEL:1000/ssubSCREEN_1000_WORKAREA_AREA:SAPLBUPA_DIALOG_JOEL:1100/ssubSCREEN_1100_MAIN_AREA:"
            "SAPLBUPA_DIALOG_JOEL:1101/tabsGS_SCREEN_1100_TABSTRIP/tabpSCREEN_1100_TAB_05").select()

        # Select CC Number against Payment Card ID
        row_count = self.session.findById(
            "wnd[0]/usr/subSCREEN_3000_RESIZING_AREA:SAPLBUS_LOCATOR:2000/subSCREEN_1010_RIGHT_AREA:"
            "SAPLBUPA_DIALOG_JOEL:1000/ssubSCREEN_1000_WORKAREA_AREA:SAPLBUPA_DIALOG_JOEL:1100/"
            "ssubSCREEN_1100_MAIN_AREA:SAPLBUPA_DIALOG_JOEL:1101/tabsGS_SCREEN_1100_TABSTRIP/tabpSCREEN_1100_TAB_05/"
            "ssubSCREEN_1100_TABSTRIP_AREA:SAPLBUSS:0028/ssubGENSUB:SAPLBUSS:7013/subA03P01:SAPLBUD0:1600/"
            "tblSAPLBUD0TCTRL_BUT0CC").RowCount

        for row_number in range(row_count):
            # fetch the id to match with the pc_id
            cc_id = self.session.findById(f"wnd[0]/usr/subSCREEN_3000_RESIZING_AREA:SAPLBUS_LOCATOR:2000/"
                                          f"subSCREEN_1010_RIGHT_AREA:SAPLBUPA_DIALOG_JOEL:1000/ssubSCREEN_1000_WORKAREA_AREA:"
                                          f"SAPLBUPA_DIALOG_JOEL:1100/ssubSCREEN_1100_MAIN_AREA:SAPLBUPA_DIALOG_JOEL:1101/"
                                          f"tabsGS_SCREEN_1100_TABSTRIP/tabpSCREEN_1100_TAB_05/ssubSCREEN_1100_TABSTRIP_AREA:"
                                          f"SAPLBUSS:0028/ssubGENSUB:SAPLBUSS:7013/subA03P01:SAPLBUD0:1600/tblSAPLBUD0TCTRL_BUT0CC/"
                                          f"txtGT_BUT0CC-CCARD_ID[0,{row_number}]").text

            if cc_id == pc_id:
                # capture the credit card number
                cc_number = self.session.findById(f"wnd[0]/usr/subSCREEN_3000_RESIZING_AREA:SAPLBUS_LOCATOR:2000/"
                                                  f"subSCREEN_1010_RIGHT_AREA:SAPLBUPA_DIALOG_JOEL:1000/"
                                                  f"ssubSCREEN_1000_WORKAREA_AREA:SAPLBUPA_DIALOG_JOEL:1100/"
                                                  f"ssubSCREEN_1100_MAIN_AREA:SAPLBUPA_DIALOG_JOEL:1101/"
                                                  f"tabsGS_SCREEN_1100_TABSTRIP/tabpSCREEN_1100_TAB_05/"
                                                  f"ssubSCREEN_1100_TABSTRIP_AREA:SAPLBUSS:0028/ssubGENSUB:SAPLBUSS:7013/"
                                                  f"subA03P01:SAPLBUD0:1600/tblSAPLBUD0TCTRL_BUT0CC/"
                                                  f"txtGT_BUT0CC-CCNUM_MASK[3,{row_number}]").text
                break

        self.session.findById("wnd[0]/tbar[0]/btn[3]").press()

        return cc_number
