import requests as requests

from CommonUtilities.parse_config import ParseConfigFile
from SAP.API_Settings import webappAPIBaseURLs
from Tests.test_base import BaseTest
from db.service.WebAppBitsDataService import WebAppBitsDataService
from db.service.WebAppMPNDataService import WebAppMPNDataService
from db.service.WebAppSubDataService import WebAppSubDataService


class WebAppOperations(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "webapp_db_file_path")
    sub_validation_req_list1 = ["subscription_id", "subscription_name", "subscription_start_date",
                                "subscription_end_date", "reseller_bcn", "vendor_subscription_id",
                                "subscription_period", "subscription_period_unit", "billing_period",
                                "billing_period_unit", "status"]

    sub_validation_req_list2 = ["bill_to", "payment_method"]

    sub_validation_req_list3 = ["marketplace_id"]
    webapp_Api_url = ParseConfigFile().get_data_from_config_json("webAPPAddress", "api_url")

    mpn_validation_req_list = ["subscription_id", "reseller_total_contract_value", "vendor_total_contract_value",
                               "item_quantity", "billing_type", "item_code", "currency", "cancelable"]

    bits_validation_req_list = ["im_subscription_id", "order_number", "bcn", "bit_date_from", "bit_date_to",
                                "rb_bit_type", "bit_amount", "bit_quantity", "bit_quantity_unit", "duration",
                                "duration_unit", "unit_price", "unit_cost", "sku", "vendor_subscription_id",
                                "bit_class"]

    def fetch_web_app_subs_data(self, sub_id, order_id, order_ref):
        """ This method fetches subscription data from webapp tables in the database. """

        sub_val_data = None
        try:
            self.logger.info(f"Fetching details for subscription {sub_id} with Order ID {order_id}")
            sub_data1 = WebAppSubDataService().get_subs_req_list1_by_sub_details(self.db_file_path, sub_id, order_id)
            sub_data2 = WebAppSubDataService().get_subs_req_list2_by_sub_details(self.db_file_path, sub_id, order_ref)
            sub_data3 = WebAppSubDataService().get_subs_req_list3_by_sub_details(self.db_file_path, sub_id, order_id)

            if sub_data1 is None:
                raise Exception(f"No Subscription details found for Subscription {sub_id} with Order ID {order_id}!")
            if sub_data2 is None:
                raise Exception(
                    f"Bill_to and Payment_method not found for Subscription {sub_id} with Order ID {order_id}!")
            if sub_data3 is None:
                raise Exception(f"Marketplace_id not found for Subscription {sub_id} with Order ID {order_id}!")

            sub_val_data = self.extract_sub_data_to_be_validated(sub_data1, sub_data2, sub_data3)
            self.logger.info(
                f"Following subscription data fetched for Subscription {sub_id} with Order Number {order_id} "
                f"from Web App \n {sub_val_data}")

        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch records from "
                              "imts_rr_automation_web_app_subscriptionfeedcbrequest table " + str(e))
            raise e

        return sub_val_data

    def extract_sub_data_to_be_validated(self, sub_data_list1, sub_data_list2, sub_data_list3):
        """ This method formats the subscription data and prepares it for validation. """

        try:
            val_data_dict = {}
            if len(sub_data_list1) != 0:
                for item in self.sub_validation_req_list1:
                    val_data_dict[item] = sub_data_list1[0][item]

            if len(sub_data_list2) != 0:
                for item in self.sub_validation_req_list2:
                    val_data_dict[item] = sub_data_list2[0][item]

            if len(sub_data_list3) != 0:
                for item in self.sub_validation_req_list3:
                    val_data_dict[item] = sub_data_list3[0][item]

            return val_data_dict
        except Exception as e:
            self.logger.error(f'Exception Occurred : {str(e)}')
            return e

    def fetch_web_app_mpn_data(self, sub_id, order_id):
        """ This method fetches mpn data from webapp tables in the database. """

        mpn_val_list = None
        try:
            self.logger.info(f"Fetching item details for subscription {sub_id} with Order ID {order_id}")
            mpn_data = WebAppMPNDataService().get_details_by_sub_id(self.db_file_path, sub_id, order_id)

            if mpn_data is None:
                raise Exception(f"No MPN details found for subscription {sub_id}!")
            mpn_val_list = self.extract_mpn_data_to_be_validated(mpn_data)
            self.logger.info(
                f"Following mpn data fetched for Subscription {sub_id} with Order Number {order_id} "
                f"from Web App \n {mpn_val_list}")
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch records from "
                              "imts_rr_automation_web_app_subscriptionfeeditem table " + str(e))
            raise e

        return mpn_val_list

    def extract_mpn_data_to_be_validated(self, mpn_data_list):
        """ This method formats the mpn data and prepares it for validation. """

        val_data_list = []
        for row in range(len(mpn_data_list)):
            val_data_dict = {}
            for item in self.mpn_validation_req_list:
                val_data_dict[item] = mpn_data_list[row][item]
            val_data_list.append(val_data_dict)

        return val_data_list

    def fetch_web_app_bits_data(self, sub_id, order_id):
        """ This method fetches bits data from webapp tables in the database. """

        bits_val_list = None
        try:
            self.logger.info(f"Fetching bits details for subscription {sub_id} with Order ID {order_id}")
            bits_data = WebAppBitsDataService().get_details_by_sub_id(self.db_file_path, sub_id, order_id)
            if bits_data is None:
                raise Exception(f"No Bit found for subscription {sub_id} with Order Number {order_id}!")
            bits_val_list = self.extract_bits_data_to_be_validated(bits_data)
            self.logger.info(
                f"Following bits data fetched for Subscription {sub_id} with Order Number {order_id} "
                f"from Web App \n {bits_val_list}")

        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch records from "
                              "imts_rr_automation_web_app_bitfeedbit table " + str(e))
            raise e

        return bits_val_list

    def extract_bits_data_to_be_validated(self, bits_data_list):
        """ This method formats the bits data and prepares it for validation. """

        val_data_list = []
        for row in range(len(bits_data_list)):
            val_data_dict = {}
            for item in self.bits_validation_req_list:
                val_data_dict[item] = bits_data_list[row][item]
            val_data_list.append(val_data_dict)

        return val_data_list

    def call_health_check_api(self):
        """ This method checks if the Web App is up and running. """
        try:
            web_app_health_check_api = self.webapp_Api_url + webappAPIBaseURLs.web_app_health_check
            response = requests.get(web_app_health_check_api)
            response_status_code = response.status_code
            response = response.json()
            assert (response_status_code == 200)
            if response_status_code == 200:
                api_message = response["message"]
                assert (api_message["status"] == "Success")
                assert (api_message["text"] == "Automation web app is alive!")
                self.logger.info("Web App response message is : " + api_message["text"])
                return True
            else:
                return False
        except Exception as e:
            self.logger.exception("Something wrong on Web App health Check as: " + str(e))
            raise e

    def fetch_sap_response(self, sub_id, order_id):
        return WebAppSubDataService().get_sap_response_by_sub_details(self.db_file_path, sub_id, order_id)

    def fetch_order_number(self, sub_id, order_date):
        """ This method fetches the Order Reference Number form WebApp tables in DB. """

        order_list = WebAppSubDataService().get_latest_order_number(self.db_file_path, sub_id, order_date)
        for order_num in order_list[-1]:
            self.logger.info(f'Latest Order Number is : {order_num}')
            return order_num

    def fetch_usage_order_number(self, sub_id, order_date, min_commit_order_ref):
        """ This method fetches the Order Reference Number for Usage from WebApp tables in DB. """

        usage_order_ref = None
        order_list = WebAppSubDataService().get_latest_order_number(self.db_file_path, sub_id, order_date)
        for item in order_list:
            for value in item:
                if value != min_commit_order_ref:
                    usage_order_ref = value
        self.logger.info(f'Usage Order Number: {usage_order_ref}')
        return usage_order_ref
