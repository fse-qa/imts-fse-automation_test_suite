import re
import time
from pathlib import Path

from PIL import ImageGrab

from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.FilterOperations import Filter
from Tests.test_base import BaseTest
from db.model.SAPStatusData import SAPStatusData
from db.service.SAPBitsDataService import SAPBitsDataDbManagementService
from db.service.SAPMPNDataService import SAPMPNDbManagementService
from db.service.SAPStatusDataService import SAPStatusDataDbManagementService
from db.service.SAPSubscriptionDataService import SAPSubscriptionDataDbManagementService


class SAPOperations(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")
    screenshots_folder_path = ParseConfigFile().get_data_from_config_json("logData", "screenshotsDirectoryPath")

    sub_validation_req_list = ["test_case_id", "provider_contract", "name", "contract_start", "contract_end",
                               "key_in_external_system", "managing_company_code", "im_vendor_sub_id", "bill_to",
                               "im_payment_terms", "subscription_period", "subscrip_period_unit", "billing_period",
                               "billing_period_unit", "status", "flooring_bp", "plant"]

    mpn_validation_req_list = ["subscription_id", "total_amount", "cost", "quantity",
                               "payment_card_id", "end_of_duration", "cb_cancellable_flag", "exchange_rate",
                               "migration_date", "original_start_date", "item_type", "product_id", "currency"]

    bits_validation_req_list = ["im_sub_id", "order_number", "bcn_number", "from_date", "to_date", "bill_item_type",
                                "amount", "billing_quantity", "duration", "unit_prc_of_prod", "unit_cost_of_prod",
                                "sku_part", "ven_sub_id", "bit_class"]

    def fetch_sap_subs_data(self, sub_id):
        """ This method fetches subscription data from sap tables in the database. """

        sub_val_data = None
        try:
            sub_data = SAPSubscriptionDataDbManagementService().get_subscription_details_by_sub_id(self.db_file_path,
                                                                                                   sub_id)
            if not sub_data:
                raise Exception(f"No data found for subscription {sub_id}!")
            sub_val_data = self.extract_sub_data_to_be_validated(sub_data[-1])
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch subscription data from sap_subscription_data"
                              " table " + str(e))
            raise e

        return sub_val_data

    def extract_sub_data_to_be_validated(self, sub_data_list):
        """ This method formats the subscription data and prepares it for validation. """

        try:
            val_data_dict = {}
            for item in self.sub_validation_req_list:
                val_data_dict[item] = sub_data_list[item]

            return val_data_dict
        except Exception as e:
            return e

    def fetch_sap_mpn_data(self, sub_id):
        """ This method fetches mpn data from sap tables in the database. """

        mpn_val_data = None
        try:
            mpn_data = SAPMPNDbManagementService().get_items_by_sub_id(self.db_file_path, sub_id)
            # The sub_id and order_id above need to be passed from outside
            if mpn_data is None:
                raise Exception(f"No data found for subscription {sub_id}!")
            mpn_val_data = self.extract_mpn_data_to_be_validated(mpn_data)
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch mpn data from sap_mpn_data"
                              " table " + str(e))
            raise e
        return mpn_val_data

    def extract_mpn_data_to_be_validated(self, mpn_data_list):
        """ This method formats the mpn data and prepares it for validation. """

        val_data_list = []
        for row in range(len(mpn_data_list)):
            val_data_dict = {}
            for item in self.mpn_validation_req_list:
                val_data_dict[item] = mpn_data_list[row][item]
            val_data_list.append(val_data_dict)

        return val_data_list

    def fetch_sap_bits_data(self, sub_id, order_id):
        """ This method fetches bits data from sap tables in the database. """

        bits_val_data = None
        try:
            bits_data = SAPBitsDataDbManagementService().get_records_by_sub_details(self.db_file_path, sub_id, order_id)
            # The sub_id and order_id above need to be passed from outside
            if bits_data is None:
                raise Exception(f"No data found for subscription {sub_id} with order number {order_id}!")
            bits_val_data = self.extract_bits_data_to_be_validated(bits_data)
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch bits data from sap_mpn_data"
                              " table " + str(e))
            raise e

        return bits_val_data

    def extract_bits_data_to_be_validated(self, bits_data_list) -> list:
        """ This method formats the bits data and prepares it for validation. """

        val_data_list = []
        for row in range(len(bits_data_list)):
            val_data_dict = {}
            for item in self.bits_validation_req_list:
                val_data_dict[item] = bits_data_list[row][item]
            val_data_list.append(val_data_dict)

        return val_data_list

    def update_sap_status_data_table(self, tc_id, sub_id, order_id, feed_val_status, mc_bit_val_status,
                                     usg_bit_val_status, bill_status, invoiced_status, rar_status, status_code,
                                     image_ref, error_details, date) -> None:
        """ This method helps to insert records into the sap status table. """

        sap_status_data = []
        sap_status_obj = SAPStatusData(tc_id, sub_id, order_id, feed_val_status, mc_bit_val_status, usg_bit_val_status,
                                       bill_status, invoiced_status, rar_status, status_code, image_ref, error_details,
                                       date)
        sap_status_data.append(sap_status_obj)
        SAPStatusDataDbManagementService().insert_sap_status_data(self.db_file_path, sap_status_data)

    def compare_payment_terms(self, sub_id) -> None:
        """ This method checks if the payment term was updated successfully by comparison. """

        try:
            last_payment_term = SAPMPNDbManagementService().get_payment_term_by_sub_id(self.db_file_path,
                                                                                       sub_id, 'last')
            self.logger.info(f"Last payment Term was : {last_payment_term}")

            latest_payment_term = SAPMPNDbManagementService().get_payment_term_by_sub_id(self.db_file_path,
                                                                                         sub_id, 'latest')
            self.logger.info(f"Latest payment Term is : {latest_payment_term}")

            if last_payment_term == latest_payment_term:
                raise Exception(f'Payment Terms did not match!! Aborting this Test Script! '
                                f'\n Previous Payment Term: {last_payment_term} \t '
                                f'Current Payment Term: {latest_payment_term}')
            else:
                self.logger.info(f"Payment Terms changed Successfully!")

        except Exception as e:
            self.logger.error("Exception occurred while Comparing the Payment Terms : " + str(e))
            raise e

    def validate_cancel_operation(self, sys_date, sub_id, tc_id) -> None:
        """ This method validates if the End of Duration is updated in SAP. """
        flag = True
        try:
            # validate if eod is updated with the cancellation date.
            sys_date = Filter().format_date(sys_date, 'SAP')
            self.logger.info(f"Latest System date is : {sys_date}")

            eod_date = SAPMPNDbManagementService().get_eod_date(self.db_file_path, sub_id)
            self.logger.info(f"End of Duration Date is : {eod_date}")

            if eod_date != sys_date:
                self.logger.error(f"End of Duration date is not updated correctly. EOD: {eod_date} \n"
                                  f"Aborting further operations for Test Case {tc_id}")
                flag = False

            else:
                self.logger.info(f"End of Duration date correctly updated for Test Case {tc_id}")
                flag = True

            # validate if the status is updated to InActive
            status = SAPSubscriptionDataDbManagementService().get_subscription_status(self.db_file_path, sub_id)
            # if status != 'InActive':
            #     flag = False

            # TODO: Validation for TCC and TCV needs to be put in here...

        except Exception as e:
            self.logger.exception("Exception occurred while trying to validate Cancel Order Operation : " + str(e))
            raise e

        finally:
            if not flag:
                raise Exception(f'Cancellation did not happen successfully!!')

    def fetch_usage_amount(self, sub_id, bill_item_type):
        try:
            usage_amount = SAPBitsDataDbManagementService().fetch_usage_amount(self.db_file_path, sub_id,
                                                                               bill_item_type)
            return usage_amount
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch usage amount from sap_bits_data"
                              " table " + str(e))
            raise e

    def validate_prepaid_term(self, sub_id, sap_bits, sap_mpn_data):
        status = None
        for mpn_bit in sap_mpn_data:
            self.logger.info(mpn_bit)
            self.logger.info("First loop is started")
            # find subscription id in sap_mpn_data table
            if mpn_bit['subscription_id'] == sub_id:
                prod_id = mpn_bit['product_id']
                total_amount = mpn_bit['total_amount']
                if '.' in total_amount:
                    total_amount = total_amount.replace('.', '')
                f_total_amount = float(re.sub(",", ".", total_amount)) / 12
                cost = mpn_bit['cost']
                if '.' in cost:
                    cost = cost.replace('.', '')
                f_cost = float(re.sub("[^A-Za-z0-9]", ".", cost)) / 12
                for sap_bit in sap_bits:
                    # TSV1 bits validation
                    if sap_bit['sku_part'] == prod_id and sap_bit['bill_item_type'] == "TSV1":
                        self.logger.info(sap_bit)
                        self.logger.info("second loop is started")
                        cost_bit = sap_bit['amount']
                        s_cost_bit = str(cost_bit)
                        cost = s_cost_bit[1:]
                        if '.' in cost:
                            cost = cost.replace('.', '')
                        cost = float(re.sub(",", ".", cost))
                        f_cost_bit = float(cost)
                        if f_cost_bit == f_cost:
                            status = True
                        else:
                            status = False
                    # TSR1 bits validation
                    elif sap_bit['sku_part'] == prod_id and sap_bit['bill_item_type'] == "TSR1":
                        price_bit = sap_bit['amount']
                        if '.' in str(price_bit):
                            price_bit = str(price_bit).replace('.', '')
                        price_bit = float(re.sub(",", ".", price_bit))
                        if price_bit == f_total_amount:
                            status = True
                        else:
                            status = False
        return status

    def get_quantity_for_given_sku(self, sub_id, sku_part, bill_item_type, number_of_records=1):
        try:
            billing_quantities = SAPBitsDataDbManagementService().get_quantity_for_given_sku(self.db_file_path, sub_id,
                                                                                             sku_part, bill_item_type,
                                                                                             number_of_records)
            return billing_quantities
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch billing quantities from sap_bits_data"
                              " table " + str(e))
            raise e

    def get_amount_and_cost_for_given_sku(self, sub_id, sku_part, quantity):
        try:
            item = SAPMPNDbManagementService().get_amount_and_cost_for_given_sku(self.db_file_path, sub_id, sku_part,
                                                                                 quantity)
            return item
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch billing quantities from sap_bits_data"
                              " table " + str(e))
            raise e

    def get_contract_information(self, sub_id):
        try:
            contract_information = SAPSubscriptionDataDbManagementService().get_contract_term_for_given_sub_id(
                self.db_file_path, sub_id)
            return contract_information
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch contract information from sap_subscription_data"
                              " table " + str(e))
            raise e

    def get_end_date_and_billing_period_from_subscription_table(self, sub_id):
        try:
            sub_data = SAPSubscriptionDataDbManagementService().get_start_and_end_date_for_given_sku(self.db_file_path,
                                                                                                     sub_id)
            return sub_data
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch subscription details from sap_subscription_data"
                              " table " + e)
            raise e

    def get_cost_from_bits_table(self, item_mpn, sub_id):
        try:
            status_cost = SAPBitsDataDbManagementService().get_records_by_sub_details_for_cost_amd_amount(
                self.db_file_path,
                sub_id, item_mpn, 'TSV5', 3)
            return status_cost
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch billing cost from sap_bits_data"
                              " table " + str(e))
            raise e

    def get_amount_from_bits_table(self, item_mpn, sub_id):
        try:
            status_amount = SAPBitsDataDbManagementService().get_records_by_sub_details_for_cost_amd_amount(
                self.db_file_path, sub_id, item_mpn, 'TSR5', 3)

            return status_amount
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch billing amount from sap_bits_data"
                              " table " + str(e))
            raise e

    def take_screenshot(self, screen_name, status='success'):
        try:
            screenshot = ImageGrab.grab()
            time_stamp = time.strftime("%d_%m_%Y_%H_%M_%S", time.localtime())
            directory = f"{self.screenshots_folder_path}\\sap\\{status}"
            Path(directory).mkdir(parents=True, exist_ok=True)
            save_path = f"{directory}\\{screen_name}_{time_stamp}.png"
            screenshot.save(save_path)
        except Exception as ex:
            self.logger.error(
                f"Exception occurred while taking screenshot for {screen_name} with status {status}. " + str(ex))
