"""
Fetches cbc status data from cbc table
"""

from db.service.CBCStatusDbManagementService import CBCStatusDbManagementService
from CommonUtilities.parse_config import ParseConfigFile
from Tests.test_base import BaseTest


class CBCOperations(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def fetch_cbc_status_data(self, sub_id, order_id) -> bool:
        """ This method fetches the subscription status data from CB Commerce. """
        try:
            order_status = CBCStatusDbManagementService().get_status_by_sub_details(self.db_file_path, sub_id,
                                                                                    order_id)
            if order_status is None:
                raise Exception(f"Order status for subscription {sub_id} and Order ID {order_id} Not found!")
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch status from cbc_status table " + str(e))
            raise e

        return order_status

    def get_latest_date(self) -> str:
        for date in CBCStatusDbManagementService().fetch_latest_system_date(self.db_file_path):
            return date

