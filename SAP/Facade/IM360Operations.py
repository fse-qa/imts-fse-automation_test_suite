from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService
from db.service.IM360ItemDbManagementService import IM360ItemDbManagementService
from CommonUtilities.parse_config import ParseConfigFile
from Tests.test_base import BaseTest


class IM360Operations(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")
    sub_validation_req_list = ["test_case_id", "subscription_id", "reseller_bcn", "bill_to", "payment_term",
                               "subscription_period", "subscription_period_unit", "billing_period",
                               "billing_period_unit", "status"]

    mpn_validation_req_list = ["subscription_id", "reseller_total_contract_value", "vendor_total_contract_value",
                               "item_quantity", "sku"]

    def fetch_im360_subs_data(self, sub_id, order_id):
        """ This method fetches subscription data form im360 tables in the database. """

        im360_sub_details = None
        try:
            sub_data = IM360SubscriptionDbManagementService().get_subscription_details_by_sub_id(self.db_file_path,
                                                                                                 sub_id, order_id)
            ven_sub_id = IM360SubscriptionDbManagementService().get_vendor_sub_id(self.db_file_path, sub_id)

            if sub_data is None:
                raise Exception(f"Subscription Details with subscription {sub_id} Not found!")
            if ven_sub_id is None:
                raise Exception(f"Vendor Subscription ID for subscription {sub_id} Not found in IM360!")
            im360_sub_details = self.extract_sub_data_to_be_validated(sub_data, ven_sub_id)
        except Exception as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_subscription table " +
                              str(e))
            raise e

        return im360_sub_details

    def extract_sub_data_to_be_validated(self, sub_data_list, ven_sub_id):
        """ This method formats the subscription data and prepares it for validation. """

        val_data_dict = {}
        for item in self.sub_validation_req_list:
            val_data_dict[item] = sub_data_list[0][item]
        val_data_dict["vendor_sub_id"] = ven_sub_id
        return val_data_dict

    def fetch_im360_mpn_data(self, sub_id, order_id):
        """ This method fetches mpn data form im360 tables in the database. """

        mpn_val_list = None
        try:
            mpn_data = IM360ItemDbManagementService().get_mpn_data_by_sub_details(self.db_file_path, sub_id, order_id)
            item_type = IM360ItemDbManagementService().get_item_type_by_sub_details(self.db_file_path, sub_id, order_id)
            currency = IM360ItemDbManagementService().get_currency_by_sub_details(self.db_file_path, sub_id, order_id)
            if mpn_data is None:
                raise Exception(f"No MPN details found for subscription {sub_id}!")
            mpn_val_list = self.extract_mpn_data_to_be_validated(mpn_data, item_type, currency)
        except Exception as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_subscription table " +
                              str(e))
            raise e

        return mpn_val_list

    def extract_mpn_data_to_be_validated(self, mpn_data_list, item_type, currency):
        """ This method formats the mpn data and prepares it for validation. """

        val_data_list = []
        for row in range(len(mpn_data_list)):
            val_data_dict = {}
            for item in self.mpn_validation_req_list:
                val_data_dict[item] = mpn_data_list[row][item]
            for sku, mpn in item_type:
                if val_data_dict['sku'] == sku:
                    val_data_dict['item_type'] = mpn
            for item in currency[0]:
                val_data_dict['currency'] = item
            val_data_list.append(val_data_dict)

        return val_data_list

    def get_im360_item_data_for_added_sku(self, sub_id):
        """ This method returns the item data when the new sku is added"""
        try:
            return IM360ItemDbManagementService().get_im360_item_data_for_added_sku(self.db_file_path, sub_id)
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch item_data from im360_items"
                              " table " + str(e))
            raise e
