
from db.service.ConnectStatusDbManagementService import ConnectStatusDBManagementService
from CommonUtilities.parse_config import ParseConfigFile
from Tests.test_base import BaseTest


class ConnectOperations(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def fetch_connect_status_data(self, tc_id):
        try:
            conn_order_status, error_msg = ConnectStatusDBManagementService().get_connect_status(self.db_file_path,
                                                                                                 tc_id)
            if conn_order_status is None:
                self.logger.error(f'Connect Order Status value is {conn_order_status}')
                return None
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch status from cb_connect_status table " + str(e))
            raise e

        return conn_order_status, error_msg

    def fetch_usage_date(self, tc_id):
        """ This method fetches the date when the usage was created. """
        try:
            usage_date = ConnectStatusDBManagementService().get_usage_date_by_test_case(self.db_file_path, tc_id)
            if usage_date is None:
                return None
        except Exception as e:
            self.logger.error("Exception occurred while trying to usage date from connect_usage_upload_status table " +
                              str(e))
            raise e

        return usage_date
