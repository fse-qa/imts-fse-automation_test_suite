from datetime import date

from CommonUtilities.parse_config import ParseConfigFile
from SAP.Facade.CBCOperations import CBCOperations
from Tests.test_base import BaseTest
from db.model.sap_billing_and_invoicing import SAPBillingAndInvoicing
from db.model.sap_rar import SAPRAR
from db.service.IM360InputOrderDbManagementService import IM360InputOrderDbManagementService
from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService
from db.service.SAPTestCasesDataService import SAPTestCasesDataService
from db.service.sap_billing_and_invoicing_service import SAPBillingAndInvoicingService
from db.service.sap_rar_service import SAPRARService


class Filter(BaseTest):
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def fetch_subscription_id(self, tc_id) -> str:
        # This method fetches the subscription id against test case id from the database.
        im360_db_obj = IM360SubscriptionDbManagementService()
        return str(im360_db_obj.get_sub_id(self.db_file_path, tc_id))

    def fetch_subscription_id_for_each_test_case(self, tc_list):
        sub_id_dict = {}
        for item in tc_list:
            if item == 'NA':
                pass
            else:
                sub_id_dict[item] = str(IM360SubscriptionDbManagementService().get_sub_id(self.db_file_path, item))

        return sub_id_dict

    @staticmethod
    def fetch_subscription_status(sub_id, order_id):
        return CBCOperations().fetch_cbc_status_data(sub_id, order_id)

    def fetch_subscription_status_for_each_test_case(self, sub_dict):
        status_dict = {}
        for item in sub_dict.keys():
            status_dict[item] = CBCOperations().fetch_cbc_status_data(sub_dict[item],
                                                                      self.fetch_latest_order_id(sub_dict[item]))
        return status_dict

    def fetch_latest_order_id(self, sub_id):
        # wrapper method to fetch latest order id.
        return IM360SubscriptionDbManagementService().get_order_id(self.db_file_path, sub_id)

    def format_date(self, raw_date, system) -> str:
        """ This method modifies date format according to Module """
        date = None

        try:
            date_parts = raw_date.split('-')
            if system == 'WebApp':
                date = date_parts[0] + date_parts[1] + date_parts[2]
            if system == 'SAP':
                date = date_parts[1] + '/' + date_parts[2] + '/' + date_parts[0]

        except Exception as e:
            self.logger.exception(f'Exception occurred while trying to convert date: {str(e)}.')

        return date

    def is_all_bits_fetched_from_webapp(self, bits_list, im_360_sku_list, added_sku=False):
        """ This method checks if all bits are sent out from WebApp. """
        try:
            self.logger.info('\n\tChecking in WebApp if all bits are available! ')
            sku_list = []
            res_missing = []
            ven_missing = []
            im360_sku_set = set(im_360_sku_list)
            self.logger.info(f'SKU Set available from IM360 : {im360_sku_set}')

            for bit in bits_list:
                sku_list.append(bit['sku'])
            sku_set = set(sku_list)
            self.logger.info(f'SKU Set available in Web App : {sku_set}')
            if added_sku:
                if len(list(sku_set)) != 1:
                    return False
                bit_class_list = []
                sku = list(sku_set)[0]
                self.logger.info(f"\t Checking for SKU : '{sku}'")
                for bit in bits_list:
                    if sku == bit['sku']:
                        bit_class_list.append(bit['bit_class'])
                self.logger.info(f'\t Bits Fetched : {bit_class_list}')
                if bit_class_list.count('REV1') == 1 == bit_class_list.count('REV1'):
                    return True
                self.logger.info(f"Bits missing {bit_class_list} for added sku {sku}")
                return False

            diff_set = im360_sku_set - sku_set
            self.logger.info(f'Difference in the two SKU sets if any : {diff_set}')

            if len(diff_set) != 0:
                for sku in list(diff_set):
                    res_missing.append(sku)
                    ven_missing.append(sku)

            if len(bits_list) == 0:
                return None
            else:
                for sku in sku_set:
                    bit_class_list = []
                    self.logger.info(f"\t Checking for SKU : '{sku}'")
                    for bit in bits_list:
                        if sku == bit['sku']:
                            bit_class_list.append(bit['bit_class'])
                    self.logger.info(f'\t Bits Fetched : {bit_class_list}')
                    if len(bit_class_list) % 2 != 0 and len(bit_class_list) != 0:
                        for item in bit_class_list:
                            if item == 'REV1':
                                ven_missing.append(sku)
                                self.logger.error(f'Vendor Bit not found for {sku}')

                            elif item == 'COS1':
                                res_missing.append(sku)
                                self.logger.error(f'Reseller Bit not found for {sku}')

                            else:
                                self.logger.error(f'Unknown Bit Class {item}.')

                if len(set(res_missing)) == len(sku_set) and len(set(ven_missing)) == 0:
                    return 'Res Missing'
                elif len(set(ven_missing)) == len(sku_set) and len(set(res_missing)) == 0:
                    return 'Ven Missing'
                elif len(set(res_missing)) < len(sku_set) and len(set(res_missing)) != 0:
                    if len(set(ven_missing)) == 0:
                        return 'Res Missing Some'
                elif len(set(ven_missing)) < len(sku_set) and len(set(ven_missing)) != 0:
                    if len(set(res_missing)) == 0:
                        return 'Ven Missing Some'
                elif len(set(ven_missing)) < len(sku_set) and len(set(res_missing)) < len(sku_set):
                    if len(set(res_missing)) != 0 and len(set(res_missing)) != 0:
                        return 'Ven and Res Missing Some'
                    else:
                        if len(set(ven_missing)) == 0 and len(set(res_missing)) == 0:
                            return True
                elif set(res_missing) == set(sku_set) == set(ven_missing):
                    # TODO: Logic to be fixed. Check in case change order operation is performed
                    return True

        except Exception as e:
            self.logger.exception(f'All bits availability could not be determined due to the {str(e)}.')

    def all_bits_received_in_sap(self, bits_list, im360_sku_list, change_order_bits=False, added_sku=False):
        """ This method checks if all bits are received in SAP. """
        try:
            self.logger.info('\n\tChecking in SAP if all bits are available! ')
            sku_list = []
            res_missing = []
            ven_missing = []
            im360_sku_set = set(im360_sku_list)
            self.logger.info(f'SKU Set available from IM360 : {im360_sku_set}')

            for bit in bits_list:
                sku_list.append(bit['sku_part'])
            sku_set = set(sku_list)
            self.logger.info(f'SKU Set available in Web App : {sku_set}')

            if added_sku:
                if len(list(sku_set)) != 1:
                    return False
                bit_class_list = []
                sku = list(sku_set)[0]
                self.logger.info(f"\t Checking for SKU : '{sku}'")
                for bit in bits_list:
                    if sku == bit['sku_part']:
                        bit_class_list.append(bit['bit_class'])
                self.logger.info(f'\t Bits Fetched : {bit_class_list}')
                if bit_class_list.count('TCO1') == 1 == bit_class_list.count('TIN1'):
                    return True
                self.logger.info(f"Bits missing {bit_class_list} for added sku {sku}")
                return False

            diff_set = im360_sku_set - sku_set
            self.logger.info(f'Difference in the two SKU sets if any : {diff_set}')

            if diff_set != 0:
                for sku in list(diff_set):
                    res_missing.append(sku)
                    ven_missing.append(sku)

            if len(bits_list) == 0:
                return None
            else:
                for sku in sku_set:
                    bit_class_list = []
                    self.logger.info(f"\t Checking for SKU : '{sku}'")
                    for bit in bits_list:
                        if sku == bit['sku_part']:
                            bit_class_list.append(bit['bit_class'])
                    self.logger.info(f'\t Bits Fetched : {bit_class_list}')
                    if change_order_bits:
                        if bit_class_list.count('TCO1') == 0:
                            self.logger.error(f"Reseller Bits not found for change order bits for sku {sku}")
                            return False
                        elif bit_class_list.count('TIN1') == 0:
                            self.logger.error(f"Vendor Bits not found for change order bits for sku {sku}")
                            return False
                        elif not bit_class_list.count('TC01') == 1 + bit_class_list.count('TIN1'):
                            self.logger.error(f"Change order reseller bits not match with vendor bits for sku {sku}")
                            return False

                    else:
                        if len(bit_class_list) % 2 != 0 and len(bit_class_list) != 0:
                            for item in bit_class_list:
                                if item == 'TIN1':
                                    ven_missing.append(sku)
                                    self.logger.error(f'Vendor Bit not found for {sku}')
                                elif item == 'TCO1':
                                    res_missing.append(sku)
                                    self.logger.error(f'Reseller Bit not found for {sku}')
                                else:
                                    self.logger.error(f'Unknown Bit Class {item}.')

                if change_order_bits:
                    return True
                if len(set(res_missing)) == len(sku_set) and len(set(ven_missing)) == 0:
                    return 'Res Missing'
                elif len(set(ven_missing)) == len(sku_set) and len(set(res_missing)) == 0:
                    return 'Ven Missing'
                elif len(set(res_missing)) < len(sku_set) and len(set(res_missing)) != 0:
                    if len(set(ven_missing)) == 0:
                        return 'Res Missing Some'
                elif len(set(ven_missing)) < len(sku_set) and len(set(ven_missing)) != 0:
                    if len(set(res_missing)) == 0:
                        return 'Ven Missing Some'
                elif len(set(ven_missing)) < len(sku_set) and len(set(res_missing)) < len(sku_set):
                    if len(set(res_missing)) != 0 and len(set(res_missing)) != 0:
                        return 'Ven and Res Missing Some'
                    else:
                        if len(set(ven_missing)) == 0 and len(set(res_missing)) == 0:
                            return True

        except Exception as e:
            self.logger.exception(f'All bits availability could not be determined due to the {str(e)}.')

    def get_test_case_latest_status(self, tc_id, pytest_name) -> str:
        return SAPTestCasesDataService().get_tc_status(self.db_file_path, tc_id, pytest_name)

    def update_test_case_latest_status(self, tc_id, pytest_name, status) -> None:
        """ This method inserts the latest execution status of each test case for each script. """
        SAPTestCasesDataService().insert_tc_status(self.db_file_path, tc_id, pytest_name, status)

    def get_tc_list(self):
        return IM360InputOrderDbManagementService().get_tc_id_list(self.db_file_path)

    def update_billing_and_invoicing(self, provider_contract, billable_bit_class, bill_from, tc_id, bill_doc_num='NA',
                                     invoice_doc_num='NA', recon_key='NA', operation='Billing', status='Fail',
                                     error_reason=''):
        billing_and_invoicing_service = SAPBillingAndInvoicingService(self.db_file_path)
        record = SAPBillingAndInvoicing(
            tc_id=tc_id,
            date=bill_from,
            provider_contract=provider_contract,
            billable_bit_class=billable_bit_class,
            bill_doc_num=bill_doc_num,
            invoice_doc_num=invoice_doc_num,
            recon_key=recon_key,
            operation=operation,
            status=status,
            error_reason=error_reason
        )
        billing_and_invoicing_service.insert_record(record)

    def update_rar(self, tc_id, provider_contract, pob_id, g2n_rev, g2n_cost, pob_price, pob_cost, pc_tcv,
                   pc_commit_cost, operation, status, error_reason):
        rar_service = SAPRARService(self.db_file_path)
        record = SAPRAR(
            tc_id=tc_id,
            date=date.today().strftime('%m/%d/%Y'),
            provider_contract=provider_contract,
            pob_id=pob_id,
            g2n_rev=g2n_rev,
            g2n_cost=g2n_cost,
            pob_price=pob_price,
            pob_cost=pob_cost,
            pc_tcv=pc_tcv,
            pc_commit_cost=pc_commit_cost,
            operation=operation,
            status=status,
            error_reason=error_reason
        )
        rar_service.insert_record(record)
