from CommonUtilities.parse_config import ParseConfigFile

parse_config_file = ParseConfigFile()


class ReadConfig:

    @staticmethod
    def get_chrome_executable_path():
        # chrome_executable_path = config.get("browser driver paths", "CHROME_EXECUTABLE_PATH")
        chrome_executable_path = parse_config_file.get_data_from_config_json("browserDriverPaths",
                                                                             "chromeExecutablePath")
        return chrome_executable_path

    @staticmethod
    def get_firefox_executable_path():
        firefox_executable_path = parse_config_file.get_data_from_config_json("browserDriverPaths",
                                                                              "firefoxExecutablePath")
        return firefox_executable_path

    # @staticmethod
    # def get_ie_executable_path():
    #     ie_executable_path = config.get("browser driver paths", "IE_EXECUTABLE_PATH")
    #     return ie_executable_path

    @staticmethod
    def get_im360_base_url():
        im360_URL = parse_config_file.get_data_from_config_json("im360CommonData", "baseUrl")
        return im360_URL

    @staticmethod
    def get_im360_username():
        im360_username = parse_config_file.get_data_from_config_json("im360CommonData", "im360UserName")
        return im360_username

    @staticmethod
    def get_im360_password():
        im360_password = parse_config_file.get_data_from_config_json("im360CommonData", "enc_im360Password")
        return im360_password

    @staticmethod
    def getLogFileName():
        log_file_name = parse_config_file.get_data_from_config_json("logData", "logFileName")
        return log_file_name

    @staticmethod
    def get_test_data_file():
        test_data_file = parse_config_file.get_data_from_config_json("inputFile", "inputFileName")
        return test_data_file

    @staticmethod
    def get_sap_input_test_data_file():
        test_data_file = parse_config_file.get_data_from_config_json("inputFile", "sapInputFileName")
        return test_data_file

    # @staticmethod
    # def getOutputFile():
    #     output_file = parse_config_file.get_data_from_config_json("Output file", "OUTPUT_FILE_NAME")
    #     return output_file

    @staticmethod
    def getCBAuthTokenUrl():
        token = parse_config_file.get_data_from_config_json("cloudblueSimpleApiData", "cbBasicAuthTokenUrl")
        return token

    @staticmethod
    def getCBAuthTokenUsername():
        user_name = parse_config_file.get_data_from_config_json("cloudblueSimpleApiData", "cbBasicAuthUsername")
        return user_name

    @staticmethod
    def getCBAuthTokenPassword():
        password = parse_config_file.get_data_from_config_json("cloudblueSimpleApiData", "enc_cbBasicAuthPassword")
        # need to update encryption code
        return password

    @staticmethod
    def getCBAPIUrl():
        cb_api_url = parse_config_file.get_data_from_config_json("cloudblueSimpleApiData", "cbBasicAuthTokenUrl")
        return cb_api_url

    @staticmethod
    def getCBAPIUsername():
        user_name = parse_config_file.get_data_from_config_json("cloudblueSimpleApiData", "cbBasicAuthUsername")
        return user_name

    @staticmethod
    def getCBAPIPassword():
        password = parse_config_file.get_data_from_config_json("cloudblueSimpleApiData", "enc_cbBasicAuthPassword")
        return password

    # @staticmethod
    # def getIM360GeneralDataOutputFile():
    #     im360_general_data_csv = parse_config_file.get_data_from_config_json("Output file", "IM360_GENERAL_DATA_OUTPUT_FILE_NAME")
    #     return im360_general_data_csv

    # @staticmethod
    # def getIM360MPNDataOutputFile():
    #     im360_mpn_data_csv = config.get("Output file", "IM360_MPN_DATA_OUTPUT_FILE_NAME")
    #     return im360_mpn_data_csv

    @staticmethod
    def get_db_file_path():
        im360_db_file_path = parse_config_file.get_data_from_config_json("dbLocation", "db_file_path")
        return im360_db_file_path

    @staticmethod
    def getCBConnectAPIURL(marketplace):
        cb_connect_api_url = parse_config_file.get_data_from_config_json(marketplace, "cbConnectApi_url",
                                                                         "CredConfig.json")
        return cb_connect_api_url

    @staticmethod
    def getCBConnectAPIKEY(marketplace):
        cb_connect_api_key = parse_config_file.get_data_from_config_json(marketplace, "cbConnectApi_key",
                                                                         "CredConfig.json")
        return cb_connect_api_key

    @staticmethod
    def getCBConnectUsageSampleFile():
        cb_connect_usage_sample_file = parse_config_file.get_data_from_config_json('cbConnectUsage',
                                                                                   "usage_sample_file")
        return cb_connect_usage_sample_file

    @staticmethod
    def getCBConnectUsageOutputFile():
        cb_connect_usage_output_file = parse_config_file.get_data_from_config_json("cbConnectUsage",
                                                                                   "usage_output_file_location")
        return cb_connect_usage_output_file

    @staticmethod
    def getScreenshotPath():
        ScreenShot_path = parse_config_file.get_data_from_config_json("logData", "screenshotsDirectoryPath")
        return ScreenShot_path

    @staticmethod
    def getTestType():
        test_type = parse_config_file.get_data_from_config_json("testType", "test_type")
        return test_type
