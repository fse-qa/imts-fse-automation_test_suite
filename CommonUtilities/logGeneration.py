import logging

from CommonUtilities.readProperties import ReadConfig
from CommonUtilities.parse_config import ParseConfigFile


class LogGenerator:

    @staticmethod
    def logGen():
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        parse_config_json = ParseConfigFile()
        logging.basicConfig(filename=parse_config_json.get_data_from_config_json("logData", "logFileName"),
                            format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
        logger = logging.getLogger()
        return logger
#ReadConfig.getLogFileName()