from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from Usage.API_Settings.apiHeaderParam import cbConnectAuthorization


class BaseAPIUtility:
    logger = LogGenerator.logGen()

    """
        This is an internal function is to prepare authorization header to call CB Connect API
            param1 = apiKey
    """

    def get_header_for_connect_api(self, marketplace="default"):
        parse_config_json = ParseConfigFile()
        connect_api_url = parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_url", "CredConfig.json")
        connect_api_key = parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_key", "CredConfig.json")
        connect_api_key = {cbConnectAuthorization: connect_api_key}
        self.logger.info("Setting up the API header for CB Connect with API KEY as : %s", connect_api_key)
        return connect_api_key

    """
        This is an internal function is to replace API QUERY URL "?" with the desired parameter 
            param1 = api_method_name
            param2 = valueToReplace
    """

    def do_update_api_query_url(self, api_method_name, value_to_replace):
        api_method_name = api_method_name.replace("?", value_to_replace)
        self.logger.info("CB Connect API method is getting as : %s", api_method_name)
        return api_method_name
