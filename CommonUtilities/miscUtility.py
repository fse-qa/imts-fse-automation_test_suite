from CommonUtilities.parse_config import ParseConfigFile
import os
import pathlib


class MiscellaneousUtility:
    parse_config_json = ParseConfigFile()
    file_name = ""

    repo_path = os.path.dirname(os.path.abspath(__file__))
    root_path = pathlib.Path(repo_path)
    root_path = root_path.parent
    root_path = str(root_path).replace('WindowsPath(', '').replace(')', '')

    def do_search_file_name(self, subscription_id):
        keyword = subscription_id
        for self.file_name in os.listdir(
                self.root_path + self.parse_config_json.get_data_from_config_json("cbConnectUsage",
                                                                                  "usage_output_file_location")):
            if keyword in self.file_name:
                return self.file_name
        return self.file_name

    def do_open_directory(self, pathName):
        os.chdir(self.root_path + pathName)
