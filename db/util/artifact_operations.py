import logging
import os
import shutil
import time
from pathlib import Path

from CommonUtilities.file_operations import delete_files_older_than, delete_directory_older_than
from CommonUtilities.parse_config import ParseConfigFile

logger = logging.getLogger(__name__)


class ArtifactOperations:
    parse_config_file = ParseConfigFile()
    log_file_path = parse_config_file.get_data_from_config_json("logData", "logFileName")
    backup_log_file_path = parse_config_file.get_data_from_config_json("logData", "backupLogFileName")
    reports_dir_path = parse_config_file.get_data_from_config_json("logData", "reportsDirectoryPath")
    backup_reports_dir = parse_config_file.get_data_from_config_json("logData", "backupReportsDirectoryPath")
    screenshots_dir = parse_config_file.get_data_from_config_json("logData", "screenshotsDirectoryPath")
    backup_screenshots_dir = parse_config_file.get_data_from_config_json("logData", "backupScreenshotsDirectoryPath")

    def archive_logs(self):
        os.makedirs(Path(self.backup_log_file_path).parent.absolute(), exist_ok=True)
        delete_files_older_than(directory=Path(self.backup_log_file_path).parent.absolute(), days=5)
        if os.path.exists(self.log_file_path):
            backup_log_file_path = self.backup_log_file_path.replace('.txt', f'_{time.strftime("%Y%m%d_%H%M%S")}.txt')
            logger.info(f"Creating a backup file for [{self.log_file_path}]: [{backup_log_file_path}]")
            os.makedirs(os.path.dirname(backup_log_file_path), exist_ok=True)
            shutil.copy2(self.log_file_path, backup_log_file_path)
        open(self.log_file_path, 'w').close()

    def archive_reports(self):
        os.makedirs(self.backup_reports_dir, exist_ok=True)
        delete_files_older_than(directory=Path(self.backup_reports_dir).absolute(), days=5)
        for file in os.listdir(self.reports_dir_path):
            if file.endswith(".html"):
                backup_reports_file_path = os.path.join(
                    self.backup_reports_dir, file).replace('.html', f'_{time.strftime("%Y%m%d_%H%M%S")}.html')
                logger.info(f"Creating a backup file for [{file}]: [{backup_reports_file_path}]")
                shutil.move(os.path.join(self.reports_dir_path, file), backup_reports_file_path)
                logger.info(f"Successfully created a backup file for [{file}]: [{backup_reports_file_path}]")

    def archive_screenshots(self):
        os.makedirs(self.backup_screenshots_dir, exist_ok=True)
        delete_directory_older_than(directory=Path(self.backup_screenshots_dir).absolute(), days=5)
        sub_folders = [f.path for f in os.scandir(self.screenshots_dir) if f.is_dir()]
        for folder in sub_folders:
            if folder == self.backup_screenshots_dir:
                continue
            backup_folder_path = os.path.join(self.backup_screenshots_dir, Path(folder).name,
                                              time.strftime("%Y%m%d_%H%M%S"), )
            logger.info(f"Creating a backup folder for [{folder}]: [{backup_folder_path}]")
            shutil.move(folder, backup_folder_path)
            logger.info(f"Successfully created a backup folder for [{folder}]: [{backup_folder_path}]")

        for component in ["im360", "sap"]:
            logger.info("Creating screenshot folder for [%s]", component)
            os.makedirs(os.path.join(self.screenshots_dir, component, "success"), exist_ok=True)
            os.makedirs(os.path.join(self.screenshots_dir, component, "error"), exist_ok=True)
