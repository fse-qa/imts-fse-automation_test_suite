class SqlConstant:
    IM360_INPUT_ORDER_INSERT_SQL_QUERY = "INSERT INTO im360_input_order(" \
                                         "test_case_id, test_case_name, quote_name, reseller_bcn, contact, " \
                                         "payment_term, bill_to, deal_id, billing_period, subscription_period_unit, " \
                                         "subscription_period, service_plan_id, reseller_po, primary_vendor, " \
                                         "company_id, company_name, contact_phone, contact_name, contact_email, " \
                                         "address, city, postal_code, state, web_order_id, reqested_ship_start_date, " \
                                         "agreement_id, provisioning_contact_name, provisioning_contact_email, " \
                                         "provisioning_contact_phone, vendor_portal_submission, " \
                                         "saved_credit_card_name, cc_first_name, cc_last_name, cc_email, " \
                                         "cc_card_type, cc_card_number, cc_expiration_month, cc_expitation_year," \
                                         "cc_cvn, reseller_currency, vendor_currency, service_name, marketplace_name," \
                                         "change_service_plan_id, change_billing_period, change_subscription_period, change_payment_term, change_deal_id, test_type)" \
                                         "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " \
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    IM360_INPUT_ORDER_MAX_ID_SELECT_QUERY = "select id from im360_input_order where ROWID =?"

    IM360_INPUT_ORDER_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM im360_input_order"

    IM360_INPUT_ORDER_GET_QUOTE_NAME_BY_TEST_CASE_ID_SQL_QUERY = "SELECT quote_name FROM im360_input_order where " \
                                                                 "test_case_id =?"

    IM360_INPUT_ORDER_GET_TEST_CASE_NAME_BY_TEST_CASE_ID_SQL_QUERY = "SELECT test_case_name FROM im360_input_order " \
                                                                     "where test_case_id =?"

    IM360_INPUT_ORDER_DELETE_BY_ID_SQL_QUERY = "DELETE FROM im360_input_order WHERE rowid =?"

    IM360_INPUT_ORDER_GET_SERVICE_NAME_BY_TEST_CASE_ID_SQL_QUERY = "SELECT service_name FROM im360_input_order " \
                                                                   "where test_case_id =?"

    IM360_INPUT_ORDER_GET_RESELLER_CURRENCY_BY_TEST_CASE_ID_SQL_QUERY = "SELECT reseller_currency FROM " \
                                                                        "im360_input_order " \
                                                                        "where test_case_id =?"
    IM360_INPUT_ORDER_GET_VENDOR_CURRENCY_BY_TEST_CASE_ID_SQL_QUERY = "SELECT vendor_currency FROM im360_input_order " \
                                                                      "where test_case_id =?"
    IM360_INPUT_ORDER_GET_MARKETPLACE_NAME_BY_TEST_CASE_ID_SQL_QUERY = "SELECT marketplace_name FROM im360_input_order " \
                                                                       "where test_case_id =?"

    IM360_INPUT_ORDER_GET_TEST_TYPE_BY_TEST_CASE_ID_SQL_QUERY = "SELECT test_type FROM im360_input_order " \
                                                                       "where test_case_id =?"

    IM360_INPUT_ORDER_GET_DISTINCT_MARKETPLACES_SQL_QUERY = "SELECT DISTINCT lower(marketplace_name) FROM " \
                                                            "im360_input_order "

    IM360_INPUT_ORDER_COUNT_MARKETPLACE_TESTCASEWISE_ROW_SQL_QUERY = "SELECT count(id) " \
                                                                     "FROM im360_input_order where test_case_id=? and " \
                                                                     "lower(marketplace_name)=?"

    IM360_INPUT_ITEM_INSERT_SQL_QUERY = "INSERT INTO im360_input_item(" \
                                        "test_case_id, item_mpn, vendor_sku, item_quantity, item_type," \
                                        "unit_of_measure, usage_amount, order_action, im360_input_order_tbl_id)" \
                                        " VALUES(?, ?, ?, " \
                                        "?, ?, ?, ?, ?, ?)"

    IM360_INPUT_ITEM_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM im360_input_item"

    IM360_INPUT_ITEM_DELETE_BY_ID_SQL_QUERY = "DELETE FROM im360_input_item WHERE rowid =?"

    IM360_SUBSCRIPTION_INSERT_SQL_QUERY = "INSERT INTO im360_subscription(" \
                                          "test_case_id, quote_name, subscription_id, " \
                                          "status, bill_to, subscription_period, subscription_period_unit, " \
                                          "billing_period, billing_period_unit, reseller_bcn, reseller_po, " \
                                          "agreement_id, order_number, payment_term, payment_type," \
                                          " currency) VALUES(" \
                                          "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    IM360_SUBSCRIPTION_INSERT_WITH_UPDATED_TERM_SQL_QUERY = "INSERT INTO im360_subscription(" \
                                                            "test_case_id, quote_name, subscription_id, " \
                                                            "status, bill_to, subscription_period, " \
                                                            "subscription_period_unit, billing_period, " \
                                                            "billing_period_unit, reseller_bcn, reseller_po, " \
                                                            "agreement_id, order_number, payment_term, payment_type," \
                                                            " currency) SELECT test_case_id, quote_name, " \
                                                            "subscription_id, status, bill_to, subscription_period, " \
                                                            "subscription_period_unit, billing_period, " \
                                                            "billing_period_unit, reseller_bcn, reseller_po, " \
                                                            "agreement_id, order_number, ?, ?, currency FROM " \
                                                            "im360_subscription WHERE " \
                                                            "test_case_id=?"

    IM360_SUBSCRIPTION_INSERT_WITH_UPDATED_STATUS_SQL_QUERY = "INSERT INTO im360_subscription(" \
                                                              "test_case_id, quote_name, subscription_id, " \
                                                              "status, bill_to, subscription_period, " \
                                                              "subscription_period_unit, billing_period, " \
                                                              "billing_period_unit, reseller_bcn, reseller_po, " \
                                                              "agreement_id, order_number, payment_term, " \
                                                              "payment_type, currency) SELECT test_case_id, " \
                                                              "quote_name, subscription_id, ?, bill_to, " \
                                                              "subscription_period, subscription_period_unit, " \
                                                              "billing_period, billing_period_unit, reseller_bcn, " \
                                                              "reseller_po, agreement_id, order_number, payment_term, " \
                                                              "payment_type, currency FROM im360_subscription WHERE " \
                                                              "test_case_id=?"

    IM360_SUBSCRIPTION_INSERT_DUPLICATE_ROW_SQL_QUERY = "INSERT INTO im360_subscription(" \
                                                        "test_case_id, quote_name, subscription_id, " \
                                                        "status, bill_to, subscription_period, " \
                                                        "subscription_period_unit, billing_period, " \
                                                        "billing_period_unit, reseller_bcn, reseller_po, " \
                                                        "agreement_id, order_number, payment_term, " \
                                                        "payment_type, currency) SELECT test_case_id, " \
                                                        "quote_name, subscription_id, status, bill_to, " \
                                                        "subscription_period, subscription_period_unit, " \
                                                        "billing_period, billing_period_unit, reseller_bcn, " \
                                                        "reseller_po, agreement_id, order_number, payment_term, " \
                                                        "payment_type, currency FROM im360_subscription WHERE " \
                                                        "test_case_id=?"

    IM360_SUBSCRIPTION_MAX_ID_SELECT_QUERY = "select id from im360_subscription where rowid =?"

    IM360_SUBSCRIPTION_SECOND_MAX_ID_SELECT_QUERY = "select max(id) from im360_subscription where test_case_id=? and " \
                                                    "id<(select max(id) from im360_subscription where test_case_id=?)"

    IM360_SUBSCRIPTION_SELECT_ALL_SQL_QUERY = "SELECT * FROM im360_subscription"

    IM360_SUBSCRIPTION_UPDATE_STATUS_BY_TEST_CASE_ID_SQL_QUERY = "UPDATE im360_subscription set status=? " \
                                                                 " where test_case_id=?"

    IM360_SUBSCRIPTION_UPDATE_STATUS_BY_TEST_CASE_ID_AND_MAX_ID_SQL_QUERY = "update im360_subscription set status=? " \
                                                                            " where test_case_id=? and id=(select " \
                                                                            "max(id) from im360_subscription where " \
                                                                            "test_case_id=?)"

    IM360_SUBSCRIPTION_SELECT_ALL_SUB_BY_TEST_CASE_ID_SQL_QUERY = "SELECT id, subscription_id, order_number, quote_name FROM im360_subscription where test_case_id=?  AND id=(SELECT MAX(id) FROM im360_subscription where test_case_id=?)"

    IM360_SUBSCRIPTION_SELECT_STATUS_CHECK_SQL_QUERY = "SELECT DISTINCT test_case_id, subscription_id, order_number" \
                                                       " from im360_subscription where test_case_id=?"

    IM360_SUBSCRIPTION_SELECT_BY_SUB_ID_SQL_QUERY = "SELECT * FROM im360_subscription WHERE subscription_id =? " \
                                                    "and order_number =?"

    IM360_FETCH_VENDOR_SUB_ID_SQL_QUERY = "select p.parameter_value from im360_subscription_parameters p where " \
                                          "p.im360_parameter_name = 'Vendor Subscription ID' and " \
                                          "p.im360_subscription_tbl_id in (SELECT s.id from im360_subscription s " \
                                          "where s.subscription_id =?)"

    IM360_SUBSCRIPTION_DELETE_BY_ID_SQL_QUERY = "DELETE FROM im360_subscription WHERE rowid =?"

    IM360_SUBSCRIPTION_ID_SELECT_BY_TEST_CASE_ID_SQL_QUERY = "SELECT id FROM im360_subscription WHERE test_case_id=?"

    IM360_QUOTE_NAME_SELECT_BY_TEST_CASE_ID_SQL_QUERY = "SELECT quote_name FROM im360_subscription WHERE test_case_id=?"

    IM360_SUB_ID_SELECT_BY_TEST_CASE_ID_SQL_QUERY = "SELECT subscription_id FROM im360_subscription WHERE test_case_id=?"

    IM360_FETCH_SUB_ID_SQL_QUERY = "select subscription_id from im360_subscription where id =" \
                                   "(select max(id) from im360_subscription where test_case_id=?);"

    IM360_FETCH_ORDER_ID_SQL_QUERY = "select order_number from im360_subscription " \
                                     "where subscription_id=?"

    IM360_SUBSCRIPTION_PARAMETERS_INSERT_SQL_QUERY = "INSERT INTO im360_subscription_parameters(" \
                                                     "test_case_id, im360_parameter_name, parameter_value, " \
                                                     "im360_subscription_tbl_id) VALUES(?, ?, ?, ?)"

    IM360_SUBSCRIPTION_PARAMETERS_DUPLICATE_INSERT_SQL_QUERY = "INSERT INTO im360_subscription_parameters(" \
                                                               "test_case_id, im360_parameter_name, parameter_value, " \
                                                               "im360_subscription_tbl_id) SELECT test_case_id, " \
                                                               "im360_parameter_name, parameter_value, " \
                                                               "? FROM im360_subscription_parameters WHERE " \
                                                               "im360_subscription_tbl_id = ?"

    IM360_SUBSCRIPTION_PARAMETERS_MAX_ID_SELECT_QUERY = "select max(id) maxId from im360_subscription_parameters"

    IM360_SUBSCRIPTION_PARAMETERS_SELECT_ALL_SQL_QUERY = "SELECT * FROM im360_subscription_parameters"

    IM360_SUBS_PARAM_VAL_BY_ID_AND_PARAM_NAME_SELECT_QUERY = "SELECT parameter_value FROM im360_subscription_parameters where " \
                                                             "im360_subscription_tbl_id=? and im360_parameter_name=?"

    IM360_SUBSCRIPTION_PARAMETERS_DELETE_BY_ID_SQL_QUERY = "DELETE FROM im360_subscription_parameters WHERE rowid = (?)"

    IM360_SUBSCRIPTION_UPDATE_WITH_UPDATED_SUBSCRIPTION_PERIOD = "update im360_subscription set subscription_period=?" \
                                                                 " where test_case_id=? and id=(select " \
                                                                 "max(id) from im360_subscription where " \
                                                                 "test_case_id=?)"

    IM360_ITEM_INSERT_SQL_QUERY = "INSERT INTO im360_item(" \
                                  "test_case_id, subscription_id, item_quantity, reseller_total_contract_value, " \
                                  "vendor_total_contract_value, sku, vendor_sku, applicable_at_renewal, cost, " \
                                  "cost_after_credit, price, price_after_credit, " \
                                  "im360_subscription_tbl_id) " \
                                  "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    IM360_ITEM_INSERT_DUPLICATE_ROW_SQL_QUERY = "INSERT INTO im360_item(" \
                                                "test_case_id, subscription_id, item_quantity, " \
                                                "reseller_total_contract_value, vendor_total_contract_value, sku, " \
                                                "vendor_sku, applicable_at_renewal, cost, cost_after_credit, price, " \
                                                "price_after_credit, im360_subscription_tbl_id) " \
                                                "select test_case_id, subscription_id, item_quantity, " \
                                                "reseller_total_contract_value, vendor_total_contract_value, sku, " \
                                                "vendor_sku, applicable_at_renewal, cost, cost_after_credit, price, " \
                                                "price_after_credit, ? from im360_item where " \
                                                "im360_subscription_tbl_id = ?"

    IM360_ITEM_MAX_ID_SELECT_QUERY = "select max(id) maxId from im360_item"

    IM360_ITEM_SECOND_MAX_ID_SELECT_QUERY = "select max(id) from im360_item where test_case_id=? and " \
                                            "id<(select max(id) from im360_item where test_case_id=?)"

    IM360_ITEM_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM im360_item"

    IM360_ITEM_SELECT_BY_SUB_DETAILS_SQL_QUERY = "select * from im360_item as itm where itm.im360_subscription_tbl_id in " \
                                                 "(SELECT sub.id from im360_subscription as sub where " \
                                                 "sub.subscription_id=? and sub.order_number=?);"

    IM360_ITEM_UPDATE_BY_TEST_CASE_ID_SQL_QUERY = "UPDATE im360_item set subscription_id=? where test_case_id = ?"

    IM360_SUBSCRIPTION_UPDATE_BY_TEST_CASE_ID_SQL_QUERY = "UPDATE im360_subscription set subscription_id=?, " \
                                                          "order_number=? where test_case_id = ? "

    IM360_ITEM_CURRENCY_SELECT_BY_SUB_DETAILS_SQL_QUERY = "select case currency when 'US Dollar' then 'USD'" \
                                                          " else currency end as currency from im360_subscription" \
                                                          " where subscription_id=? and order_number=?;"

    IM360_ITEM_TYPE_SELECT_BY_SUB_DETAILS_SQL_QUERY = "select inp_itm.item_mpn, inp_itm.item_type " \
                                                      "from im360_input_item as inp_itm " \
                                                      "where inp_itm.item_mpn in (select itm.sku from im360_item as " \
                                                      "itm where itm.im360_subscription_tbl_id in " \
                                                      "(select sub.id from im360_subscription as sub where" \
                                                      " sub.subscription_id=? and sub.order_number=?));"

    IM360_ITEM_DELETE_BY_ID_SQL_QUERY = "DELETE FROM im360_item WHERE rowid = (?)"

    IM360_ITEMS_FILTER_BY_SUB_ID = "SELECT sku FROM im360_item WHERE im360_subscription_tbl_id in " \
                                   "(SELECT max(id) FROM im360_subscription where subscription_id =?);"

    IM360_ITEMS_FOR_ADDED_SKU = "SELECT * from im360_item where im360_subscription_tbl_id in" \
                                " (SELECT id from im360_subscription WHERE subscription_id=? ORDER by id desc limit 2)"

    IM360_STATUS_INSERT_SQL_QUERY = "INSERT INTO im360_status" \
                                    "(test_case_id, test_case_name, status, failure_reason," \
                                    "service_name, failure_image,reseller_currency,vendor_currency,marketplace_name) " \
                                    "VALUES(?, ?, ?, ?, ?, ? ,? ,? , ?); "

    IM360_STATUS_MAX_ID_SELECT_QUERY = "select id from im360_status where ROWID =?"

    IM360_STATUS_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM im360_status"

    IM360_STATUS_DELETE_BY_ID_SQL_QUERY = "DELETE FROM im360_status WHERE rowid =?"

    IM360_USAGE_MPN_BY_TC_ID = "select item_mpn from im360_input_item where test_case_id=? and item_type='Usage';"

    IM360_STATUS_FETCH_STATUS_BY_SUB_ID_SELECT_QUERY = "select status, failure_reason, failure_image from im360_status " \
                                                       "where id = (select max(id) from im360_status where " \
                                                       "test_case_id = ?);"

    IM360_INPUT_ORDER_GET_CC_NUM_BY_TEST_CASE_ID_SQL_QUERY = "select cc_card_number from im360_input_order where " \
                                                             "test_case_id = ?"

    PARAMETER_MAPPING_SELECT_ALL_SQL_QUERY = "SELECT * FROM parameter_mapping"

    PARAMETER_MAPPING_COUNT_ROW_BY_CONNECT_PARAM_SQL_QUERY = "SELECT count(id) FROM parameter_mapping where " \
                                                             "parameter_value = ?"

    TEST_CASE_ID_SQL_QUERY = "select test_case_id from im360_input_order;"

    CBC_STATUS_INSERT_SQL_QUERY = "INSERT INTO cbc_status(" \
                                  "test_case_id, sub_id, order_id, status) VALUES(?, ?, ?, ?)"

    CBC_STATUS_SELECT_ALL_SQL_QUERY = "SELECT * FROM cbc_status"

    CBC_STATUS_SELECT_BY_SUB_DETAILS_SQL_QUERY = "SELECT status FROM cbc_status where sub_id=? and order_id=?"

    CBC_STATUS_DELETE_BY_ID_SQL_QUERY = "DELETE FROM cbc_status WHERE rowid = (?)"

    CBC_LATEST_DATE_SQL_QUERY = "select cb_date from commerce_date_change_track where sl_no = " \
                                "(select max(sl_no) from commerce_date_change_track);"

    CONNECT_STATUS_SELECT_BY_SUB_DETAILS = "select subscription_status, error_message from cb_connect_status where " \
                                           "test_case_id=?"

    SAP_SUB_DATA_INSERT_SQL_QUERY = "INSERT INTO sap_subscription_data(" \
                                    "test_case_id, provider_contract, name, contract_start, contract_end, " \
                                    "key_in_external_system, managing_company_code, im_vendor_sub_id, bill_to, " \
                                    "im_payment_terms, subscription_period, subscrip_period_unit, billing_period," \
                                    " billing_period_unit, status, flooring_bp, plant) VALUES(?, ?, ?, ?, ?, ?, ?, ?, " \
                                    "?, ?, ?, ?, ?, ?, ?,?, ?)"

    SAP_SUB_DATA_SELECT_ALL_SQL_QUERY = "SELECT * FROM sap_subscription_data"

    SAP_SUB_DATA_BY_SUB_ID_SQL_QUERY = "SELECT * FROM sap_subscription_data where provider_contract=?"

    SAP_SUB_STATUS_BY_SUB_ID_SQL_QUERY = "select status from sap_subscription_data where id = " \
                                         "(select max(id) from sap_subscription_data where provider_contract=?);"

    SAP_SUB_DATA_DELETE_BY_ID_SQL_QUERY = "DELETE FROM sap_subscription_data WHERE rowid = (?)"

    SAP_PAYMENT_TERM_LAST_BY_SUB_ID_SQL_QUERY = "select sub.im_payment_terms from sap_subscription_data as sub " \
                                                "where sub.id = (SELECT max(sap_sub.id) from sap_subscription_data" \
                                                " as sap_sub where sap_sub.provider_contract = ?);"

    SAP_PAYMENT_TERM_LATEST_BY_SUB_ID_SQL_QUERY = "select sub.im_payment_terms from sap_subscription_data as sub where" \
                                                  " sub.id = (SELECT max(sap_sub.id) from sap_subscription_data as" \
                                                  " sap_sub where sap_sub.provider_contract = ? and sap_sub.id<" \
                                                  "(SELECT max(sap_sub.id) from sap_subscription_data as sap_sub where" \
                                                  " sap_sub.provider_contract = ?));"

    SAP_MPN_DATA_INSERT_SQL_QUERY = "INSERT INTO sap_mpn_data(" \
                                    "subscription_id, total_amount, cost, quantity, payment_card_id, cc_number, " \
                                    "end_of_duration, cb_cancellable_flag, exchange_rate, migration_date, " \
                                    "original_start_date, item_type, product_id, currency) VALUES(?, ?, ?, ?, ?, ?," \
                                    " ?, ?, ?, ?, ?, ?, ?, ?)"

    SAP_MPN_DATA_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM sap_mpn_data"

    SAP_MPN_DATA_BY_SUB_ID_SQL_QUERY = "SELECT * FROM sap_mpn_data where subscription_id=?"

    SAP_MPN_DATA_DELETE_BY_ID_SQL_QUERY = "DELETE FROM sap_mpn_data WHERE rowid = (?)"

    SAP_BITS_DATA_INSERT_SQL_QUERY = "INSERT INTO sap_bits_data(" \
                                     "im_sub_id, order_number, bcn_number, from_date, to_date, bill_item_type," \
                                     " amount, billing_quantity, duration, unit_prc_of_prod, unit_cost_of_prod," \
                                     "sku_part, ven_sub_id, bit_class) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " \
                                     "?) "

    SAP_BITS_DATA_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM sap_bits_data"

    SAP_BITS_DATA_SELECT_BY_SUB_DETAILS_SQL_QUERY = "SELECT * FROM sap_bits_data where im_sub_id=? " \
                                                    "and order_number=?"

    SAP_BITS_DATA_DELETE_BY_ID_SQL_QUERY = "DELETE FROM sap_bits_data WHERE rowid = (?)"

    SAP_BITS_DATA_FETCH_USAGE_AMOUNT_QUERY = "SELECT amount FROM sap_bits_data where im_sub_id=? " \
                                             "and bill_item_type=? order by im_sub_id DESC limit 1"

    SAP_BITS_DATA_SELECT_BY_SKU_PART_SQL_QUERY = "SELECT billing_quantity FROM sap_bits_data where " \
                                                 "im_sub_id=? and sku_part=? " \
                                                 "and bill_item_type=? order by im_sub_id DESC limit ?"

    SAP_STATUS_CODE_INSERT_SQL_QUERY = "INSERT INTO sap_status_code(" \
                                       "status, code, reason) VALUES(?, ?, ?)"

    SAP_STATUS_CODE_SELECT_ALL_SQL_QUERY = "SELECT * FROM sap_status_code"

    SAP_STATUS_CODE_DELETE_BY_ID_SQL_QUERY = "DELETE FROM sap_status_code WHERE rowid = (?)"

    SAP_TEST_CASES_DATA_INSERT_SQL_QUERY = "INSERT INTO sap_test_cases_data(tc_id, pytest_name, status) " \
                                           "VALUES(?, ?, ?);"

    SAP_TEST_CASES_DATA_SELECT_ALL_SQL_QUERY = "SELECT status from sap_test_cases_data WHERE tc_id =? and " \
                                               "pytest_name = ? order by id desc"

    SAP_REASON_SELECT_SQL_QUERY = "SELECT reason FROM sap_status_code where trim(status) = ? and trim(code) = ?"

    SAP_STATUS_DATA_INSERT_SQL_QUERY = "INSERT INTO sap_status_data(" \
                                       "test_case_id, subscription_id, order_id, feed_validation_status, " \
                                       "minCommitBit_validation_status, usage_bit_validation_status, billing_status, " \
                                       "invoiced_status, rar_status, status_code, image_ref, error_details, date)" \
                                       " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    SAP_STATUS_DATA_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM sap_status_data"

    SAP_STATUS_DATA_SELECT_ALL_BY_TEST_CASE_ID_SQL_QUERY = "SELECT * FROM sap_status_data where test_case_id = ?"

    SAP_STATUS_DATA_SELECT_ALL_TEST_CASE_ID_SQL_QUERY = "SELECT distinct test_case_id  FROM sap_status_data"

    SAP_STATUS_DATA_DELETE_BY_ID_SQL_QUERY = "DELETE FROM sap_status_data WHERE rowid = (?)"

    SAP_EOD_DATE_BY_SUB_ID_SQL_QUERY = "select sub.end_of_duration from sap_mpn_data as sub where sub.id = " \
                                       "(select max(sub_mpn.id) from sap_mpn_data as sub_mpn where " \
                                       "sub_mpn.subscription_id = ?);"

    SAP_CC_NUM_BY_TC_ID_SQL_QUERY = "select cc_number from sap_mpn_data where id = (select id " \
                                    "from sap_subscription_data where test_case_id = ?);"

    SAP_AMOUNT_AND_COST_SQL_QUERY = "SELECT total_amount, cost from sap_mpn_data WHERE subscription_id=? " \
                                    "and product_id=? and quantity=? ORDER by subscription_id desc"

    SAP_USAGE_DATE_BY_TC_ID = "select usage_start_date from connect_usage_upload_status where test_case_id = ?;"

    SAP_BILLING_AND_INVOICE_INSERT_SQL_QUERY = "INSERT INTO sap_bill_invoice(" \
                                               "tc_id, date, provider_contract, bill_doc_num, invoice_doc_num, " \
                                               "recon_key, billable_bit_class, operation, status, error_reason) " \
                                               "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    SAP_RAR_INSERT_SQL_QUERY = "INSERT INTO sap_rar(" \
                               "tc_id, date, provider_contract, pob_id, g2n_rev, g2n_cost, pob_price, pob_cost, pc_tcv," \
                               " pc_commit_cost, operation, status, error_reason) " \
                               "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?)"

    SAP_CONTRACT_TERM_BY_PROVIDER_CONTRACT_SQL_QUERY = "select contract_start, contract_end, subscription_period from " \
                                                       "sap_subscription_data where provider_contract = ? " \
                                                       "order by id DESC LIMIT 2"

    WEB_APP_SUB_DATA_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM subscriptionfeed"

    WEB_APP_SUB_DATA_LIST1_SELECT_BY_SUB_DETAILS_SQL_QUERY = "select * " \
                                                             "from imts_rr_automation_web_app_subscriptionfeedcbrequest " \
                                                             "where subscription_id =? and order_id=?;"

    WEB_APP_SUB_DATA_LIST2_SELECT_BY_SUB_DETAILS_SQL_QUERY = \
        "select bit_ca.bill_to, bit_ca.payment_method from imts_rr_automation_web_app_bitfeedcustomattribute as bit_ca" \
        " where bit_ca.bit_feed_id in (SELECT bit_fd.bit_feed_id from imts_rr_automation_web_app_bitfeedbit as bit_fd " \
        "where bit_fd.im_subscription_id =? and bit_fd.order_number =? and bit_fd.bit_feed_id in " \
        "(SELECT id from imts_rr_automation_web_app_bitfeed));"

    WEB_APP_SUB_DATA_LIST3_SELECT_BY_SUB_DETAILS_SQL_QUERY = \
        "select feed_pr.marketplace_id from imts_rr_automation_web_app_subscriptionfeedrequestpreamble as feed_pr" \
        " where feed_pr.subscription_feed_id in (select feed_req.subscription_feed_id " \
        "from imts_rr_automation_web_app_subscriptionfeedcbrequest as feed_req " \
        "where feed_req.subscription_id = ? and feed_req.order_id =? and " \
        "feed_req.subscription_feed_id in (select id from imts_rr_automation_web_app_subscriptionfeed));"

    WEB_APP_FETCH_SAP_RESPONSE_BY_SUB_DETAILS_SQL_QUERY = \
        "select res.status, res.message from imts_rr_automation_web_app_subscriptionfeedresponsesap as res " \
        "where subscription_feed_id in (select id from imts_rr_automation_web_app_subscriptionfeed where id in " \
        "(select fd.subscription_feed_id from imts_rr_automation_web_app_subscriptionfeedcbrequest as fd " \
        "where fd.subscription_id=? and fd.order_id=?));"

    WEB_APP_MPN_DATA_SELECT_ALL_SQL_QUERY = "SELECT rowid, * FROM subscriptionfeed_item"

    WEB_APP_MPN_DATA_SELECT_BY_SUB_DETAILS_SQL_QUERY = "select sub.subscription_id, * from " \
                                                       "imts_rr_automation_web_app_subscriptionfeeditem as itm," \
                                                       " imts_rr_automation_web_app_subscriptionfeedcbrequest as sub" \
                                                       " where itm.billing_type = 'FIXED' and " \
                                                       "itm.subscription_feed_cb_request_id = sub.id and " \
                                                       "sub.subscription_id= ? and sub.order_id= ?;"

    WEB_APP_BITS_DATA_SELECT_BY_SUB_DETAILS_SQL_QUERY = "select * from imts_rr_automation_web_app_bitfeedbit where" \
                                                        " im_subscription_id=? and order_number=?"

    WEB_APP_FETCH_ORDER_NUMBER_SQL_QUERY = "select distinct order_number from imts_rr_automation_web_app_bitfeedbit " \
                                           "where im_subscription_id = ? and bit_date_from = ?;"

    USAGE_STATUS_DETAILS_INSERT_SQL_QUERY = "INSERT INTO `connect_usage_upload_status`" \
                                            "(`test_case_id`, `subscription_id`, `usage_id`, " \
                                            "`usage_start_date`, `usage_end_date`, `usage_status_in_connect`)" \
                                            "VALUES (?, ?, ?, ?, ?, ?)"

    COMMERCE_DATE_CHANGE_TRACK_INSERT_SQL_QUERY = "INSERT INTO `commerce_date_change_track`" \
                                                  "(`sl_no`, `cb_date`, `condition`)" \
                                                  "VALUES (?, ?, ?)"

    USAGE_STATUS_DETAILS_SPECIFIC_SELECT_SQL_QUERY = "SELECT * FROM connect_usage_upload_status WHERE usage_id = (?)"

    USAGE_STATUS_DETAILS_ALL_SELECT_SQL_QUERY = "SELECT * FROM connect_usage_upload_status"

    COMMERCE_NOT_INTERMITTENT_MAX_DATE_SELECT_QUERY = "SELECT max(`cb_date`) as `current_cb_date` FROM `commerce_date_change_track` " \
                                                      "WHERE `condition` <> 'Intermittent'"

    COMMERCE_MAX_DATE_SELECT_QUERY = "SELECT DATE(`cb_date`, '-1 days') as current_cb_date FROM `commerce_date_change_track`" \
                                     "WHERE sl_no = (SELECT max(`sl_no`) FROM `commerce_date_change_track`)"

    COMMERCE_INTERMITTENT_MAX_DATE_SELECT_QUERY = "SELECT DATE(`cb_date`, '-1 days') as current_cb_date FROM `commerce_date_change_track` " \
                                                  "WHERE sl_no = (SELECT max(sl_no) FROM commerce_date_change_track " \
                                                  "WHERE condition = 'Intermittent')"

    COMMERCE_LAST_MAX_DATE_SELECT_SQL_QUERY_FOR_CANCEL_FLOW = "select `cb_date` from `commerce_date_change_track` " \
                                                              "where `sl_no`=(select max(`sl_no`)-1 from `commerce_date_change_track` " \
                                                              "where `condition`='Intermittent')"

    COMMERCE_LAST_MAX_DATE_SELECT_SQL_QUERY_FOR_BASIC_FLOW = "SELECT `cb_date` as `current_cb_date` FROM `commerce_date_change_track` " \
                                                             "WHERE `sl_no` = (SELECT MAX(`sl_no`) FROM `commerce_date_change_track`" \
                                                             "WHERE (`sl_no` NOT IN (SELECT MAX(`sl_no`) FROM `commerce_date_change_track`)) " \
                                                             "and `condition` like '1');"

    IM360_SUBSCRIPTION_SELECT_SQL_QUERY_AS_PER_TESTCASE = "SELECT `subscription_id` FROM `im360_subscription` " \
                                                          "WHERE `test_case_id` =?"

    INPUT_USAGE_MPN_SELECT_SQL_BY_TESTCASE = "SELECT `vendor_sku`, `item_quantity`, `item_type`, `unit_of_measure`, `usage_amount`" \
                                             " FROM `im360_input_item` " \
                                             "WHERE `item_type` = 'Usage' AND `test_case_id` =?"

    IM360_SUBSCRIPTION_SELECT_PENDING_SUBS_BY_TC_SQL_QUERY = "SELECT id, subscription_id, order_number FROM " \
                                                             "im360_subscription WHERE" \
                                                             " test_case_id =? and id not in (" \
                                                             "select im360_subscription_id from cb_connect_status)"

    IM360_SUBSCRIPTION_PARAMETERS_SELECT_BY_ID_SQL_QUERY = " select m.parameter_type,m.is_mandatory," \
                                                           "ifnull(p.parameter_value,'') as parameter_value, " \
                                                           "is_validation_required " \
                                                           "from im360_subscription_parameters p, parameter_mapping m" \
                                                           " where  p.im360_subscription_tbl_id=? and m.parameter_value=? " \
                                                           "and m.parameter_name = p.im360_parameter_name"

    CONNECT_STATUS_INSERT_SQL_QUERY = "INSERT INTO cb_connect_status(subscription_status, error_message," \
                                      " im360_subscription_id, test_case_id) VALUES(?, ?, ?, ?)"

    CBC_STATUS_UPDATE_SQL_QUERY = "Update cbc_status set status=? where test_case_id =? and sub_id = ?"

    AUTOMATION_REPORT_DATA_INSERT_SQL_QUERY = "INSERT INTO automation_report_data(test_case_id, test_case_name, " \
                                              "status, failure_reason, status_level_fail, status_level_warning, " \
                                              "subscription_id, reseller_bcn, reseller_currency, vendor_currency," \
                                              "service_name, marketplace_name, module, failure_image) " \
                                              "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    AUTOMATION_REPORT_DATA_SELECT_ALL_SQL_QUERY = "SELECT id, test_case_id, test_case_name, status, " \
                                                  "ifnull(failure_reason, '') as failure_reason, " \
                                                  "ifnull(status_level_fail, '') as status_level_fail, " \
                                                  "ifnull(status_level_warning, '') as status_level_warning," \
                                                  " subscription_id, reseller_bcn, reseller_currency, " \
                                                  "vendor_currency, service_name, marketplace_name, module," \
                                                  "failure_image FROM automation_report_data"

    AUTOMATION_REPORT_DATA_DELETE_BY_ID_SQL_QUERY = "DELETE FROM AUTOMATION_REPORT_DATA WHERE rowid = (?);"

    IM360_INPUT_ITEM_GET_TEST_CASE_RECORD_SQL_QUERY = "SELECT rowid, * FROM im360_input_item where test_case_id =? ORDER BY id DESC LIMIT 4"

    IM360_INPUT_GET_ORDER_TEST_CASE_RECORD_SQL_QUERY = "SELECT * FROM im360_input_order where test_case_id =? ORDER BY id DESC LIMIT 1"

    IM360_INPUT_GET_FILTERED_ITEM_TEST_CASE_RECORD_SQL_QUERY = "SELECT rowid, * FROM im360_input_item where test_case_id =? and item_type =? and order_action =? AND im360_input_order_tbl_id = (SELECT max(id) from im360_input_order WHERE test_case_id =?) ORDER BY id DESC LIMIT 3"

    IM360_ERP_ORDER_NUMBER_UPDATE_BY_TEST_CASE_ID_SQL_QUERY = "UPDATE im360_subscription set order_number=? where test_case_id=? and id=?"

    IM360_SUB_MAX_ID_SELECT_QUERY = "SELECT max(id) from im360_subscription where test_case_id=?"

    SAP_BILLING_PERIOD_START_AND_END_DATE_SQL_QUERY = "select * from sap_subscription_data where provider_contract=?" \
                                                      "and test_case_id='TS-134' order by id desc limit 1;"

    SAP_BITS_DATA_SELECT_BY_SKU_PART_AMOUNT_COST_SQL_QUERY = "SELECT * FROM sap_bits_data where " \
                                                             "im_sub_id=? and sku_part=? " \
                                                             "and bill_item_type=? order by im_sub_id DESC limit ?"

    IM360_SUBSCRIPTION_PARAMETERS_UPDATE_WITH_UPDATED_DEAL_ID = "UPDATE im360_subscription_parameters SET parameter_value =? WHERE test_case_id =? " \
                                                                "AND im360_parameter_name = 'Agreement/Deal ID' " \
                                                                "AND im360_subscription_tbl_id = (SELECT max(id) FROM im360_subscription WHERE test_case_id = ?)"

    IM360_SUBSCRIPTION_UPDATE_WITH_UPDATED_BILLING_FREQUENCY = "update im360_subscription set billing_period_unit=?" \
                                                               " where test_case_id=? and id=(select " \
                                                               "max(id) from im360_subscription where " \
                                                               "test_case_id=?)"