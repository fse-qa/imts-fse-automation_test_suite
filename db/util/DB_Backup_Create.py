import datetime
import os
import shutil
import sqlite3
from sqlite3 import Error


class DB_Bak_Create:

    def DB_Backup(self, Current_DB_Location, Backup_DB_Location, File_Name):
        # Copy the content of
        # source to destination

        try:
            datetimestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()).replace(" ","-").replace(":","-")
            if os.path.isfile(Current_DB_Location+File_Name) == True:
                if str(File_Name).__contains__('.sqlite3'):
                    File_Name_date_time = str(File_Name).replace(".sqlite3", datetimestamp + ".sqlite3")
                shutil.copyfile(Current_DB_Location + File_Name, Backup_DB_Location+File_Name_date_time)
                print("File copied successfully.")
            else:
                print('File does not exists. File Name: ' + File_Name)

        # If source and destination are same
        except shutil.SameFileError:
            print("Source and destination represents the same file.")

        # If destination is a directory.
        except IsADirectoryError:
            print("Destination is a directory.")

        # If there is any permission issue
        except PermissionError:
            print("Permission denied.")

        # For other errors
        except:
            print("Error occurred while copying file.")

    def Delete_Database(self, conn, Table_Name):
        # Connecting to sqlite
        # conn = sqlite3.connect(Current_DB_Location+DB_Name)

        # Creating a cursor object using the cursor() method
        cursor = conn.cursor()

        # Doping EMPLOYEE table if already exists
        cursor.execute("DELETE from " +Table_Name)
        print(Table_Name + " Table contents deleted... ")

        # Commit your changes in the database
        conn.commit()

        # # Closing the connection
        # conn.close()

    def create_connection(self, Current_DB_Location, DB_Name):
        """ create a database connection to the SQLite database
            specified by the db_file
        :param db_file: database file
        :return: Connection object or None
        """
        db_file = Current_DB_Location + DB_Name
        conn = None
        try:
            conn = sqlite3.connect(db_file)
        except Error as e:
            print(e)

        return conn

    def update_database(conn, table_name, task):
        """
        update priority, begin_date, and end date of a task
        :param conn:
        :param task:
        :return: project id
        """
        sql = ''' UPDATE '''+table_name+'''
                  SET seq = ? 
                  WHERE name like ?'''

        # sql = "UPDATE "+table_name+" SET seq = 0 WHERE name like "
        cur = conn.cursor()
        cur.execute(sql, task)
        conn.commit()




def main():
    Current_DB_Location = "D:\\RR_Automation_Web_App\\imts-rr-automation_web_suite\\"
    Backup_DB_Location = "D:\\RR_Automation_Web_App\\imts-rr-automation_web_suite\\DB_Bak\\"
    DB_Name = 'imts_rr_automation_db.sqlite3'

    table_lst = ['imts_rr_automation_web_app_subscriptionfeed',
                 'imts_rr_automation_web_app_subscriptionfeedrequestpreamble',
                 'imts_rr_automation_web_app_subscriptionfeedcbrequest',
                 'imts_rr_automation_web_app_subscriptionfeedserviceparameter',
                 'imts_rr_automation_web_app_subscriptionfeedcustomattribute',
                 'imts_rr_automation_web_app_subscriptionfeedcustomerdetails',
                 'imts_rr_automation_web_app_subscriptionfeeditem',
                 'imts_rr_automation_web_app_subscriptionfeedresponsesap',
                 'imts_rr_automation_web_app_bitfeed',
                 'imts_rr_automation_web_app_bitfeedbit',
                 'imts_rr_automation_web_app_bitfeedbitorderparameter',
                 'imts_rr_automation_web_app_bitfeedbitusageparameter',
                 'imts_rr_automation_web_app_bitfeedcustomattribute',
                 'imts_rr_automation_web_app_bitfeedpayments',
                 'imts_rr_automation_web_app_bitfeedresponsesap',
                 'im360_input_order',
                 'im360_input_item',
                 'im360_subscription',
                 'im360_subscription_parameters',
                 'im360_item',
                 'parameter_mapping',
                 'cbc_status',
                 'cb_connect_status',
                 'sap_subscription_data',
                 'sap_mpn_data',
                 'sap_bits_data',
                 'sap_status_code',
                 'sap_status_data',
                 'connect_usage_upload_status',
                 'commerce_date_change_track',
                 'automation_report_data']

    DB_Bak_Create.DB_Backup('', Current_DB_Location, Backup_DB_Location, DB_Name)
    # create a database connection
    conn = DB_Bak_Create.create_connection('', Current_DB_Location, DB_Name)
    with conn:
        for table_name in table_lst:
            print(table_name)
            DB_Bak_Create.Delete_Database('', conn, table_name)
        DB_Bak_Create.update_database(conn, 'sqlite_sequence', (0, 'imts_rr_automation_web_app_%'))


main()