import logging
import os
import shutil
import time
from pathlib import Path

from CommonUtilities.file_operations import delete_files_older_than
from CommonUtilities.parse_config import ParseConfigFile
from db.util.SqlUtil import SqlUtil

logger = logging.getLogger(__name__)


class DataOperations:
    parse_config_file = ParseConfigFile()
    db_file_path = parse_config_file.get_data_from_config_json("dbLocation", "db_file_path")
    backup_db_file_path = parse_config_file.get_data_from_config_json("dbLocation", "backup_db_file_path")
    db_script_file_path = parse_config_file.get_data_from_config_json("dbLocation", "db_script_file_path")
    sql_util = SqlUtil(db_file_path)

    def create_database_backup(self):
        backup_db_file_path = self.backup_db_file_path.replace('.db', f'_{time.strftime("%Y%m%d_%H%M%S")}.db')
        logger.info(f"Creating a backup file for [{self.db_file_path}]: [{backup_db_file_path}]")
        os.makedirs(os.path.dirname(backup_db_file_path), exist_ok=True)
        shutil.copy2(self.db_file_path, backup_db_file_path)
        logger.info(f"Successfully created a backup file for [{self.db_file_path}]: [{backup_db_file_path}]")
        delete_files_older_than(directory=Path(self.backup_db_file_path).parent.absolute(), days=5)

    def truncate_database_data(self):
        logger.info(f"Deleting database file [{self.db_file_path}]")
        os.remove(self.db_file_path)
        self.execute_db_script()

    def execute_db_script(self, script_file_path=None):
        if script_file_path is None:
            script_file_path = self.db_script_file_path
        logger.info(f"Writing DB Script file [{script_file_path}] to database file [{self.db_file_path}]")
        with open(script_file_path) as sql_file:
            sql = sql_file.read()
        conn = self.sql_util.get_connection()
        cur = conn.cursor()
        cur.executescript(sql)
        conn.commit()
        self.sql_util.close_connection(conn)
        logger.info(f"Successfully truncated the database [{self.db_file_path}]")
