class SAPBitsData:
    def __init__(self, im_sub_id, order_id, bcn_number, from_date, to_date, bill_item_type, amount, billing_quantity,
                 duration, unit_prc_of_prod, unit_cost_of_prod, sku_part, ven_sub_id, bit_class):
        self.im_sub_id = im_sub_id
        self.order_id = order_id
        self.bcn_number = bcn_number
        self.from_date = from_date
        self.to_date = to_date
        self.bill_item_type = bill_item_type
        self.amount = amount
        self.billing_quantity = billing_quantity
        self.duration = duration
        self.unit_prc_of_prod = unit_prc_of_prod
        self.unit_cost_of_prod = unit_cost_of_prod
        self.sku_part = sku_part
        self.ven_sub_id = ven_sub_id
        self.bit_class = bit_class
