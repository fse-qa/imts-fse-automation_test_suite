
class AutomationReportData:
    def __init__(self, test_case_id, test_case_name, status, failure_reason, status_level_fail, status_level_warning,
                 subscription_id, reseller_bcn, reseller_currency, vendor_currency, service_name, marketplace_name,
                 module, failure_image):
        self.test_case_id = test_case_id
        self.test_case_name = test_case_name
        self.status = status
        self.failure_reason = failure_reason
        self.status_level_fail = status_level_fail
        self.status_level_warning = status_level_warning
        self.subscription_id = subscription_id
        self.reseller_bcn = reseller_bcn
        self.reseller_currency = reseller_currency
        self.vendor_currency = vendor_currency
        self.service_name = service_name
        self.marketplace_name = marketplace_name
        self.module = module
        self.failure_image = failure_image

