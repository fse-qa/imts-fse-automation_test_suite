
class IM360Item:
    test_case_id = None
    subscription_id = None
    item_quantity = None
    reseller_total_contract_value = None
    vendor_total_contract_value = None
    sku = None
    vendor_sku = None
    applicable_at_renewal = None
    cost = None
    cost_after_credit = None
    price = None
    price_after_credit = None
    im360_subscription_tbl_id = None
    # def __init__(self, test_case_id, subscription_id, item_quantity, reseller_total_contract_value,
    #              vendor_total_contract_value, currency, mpn, cost, cost_after_credit, price, price_after_credit,
    #              im360_subscription_id):
    #     self.test_case_id = test_case_id
    #     self.subscription_id = subscription_id
    #     self.item_quantity = item_quantity
    #     self.reseller_total_contract_value = reseller_total_contract_value
    #     self.vendor_total_contract_value = vendor_total_contract_value
    #     self.currency = currency
    #     self.mpn = mpn
    #     self.cost = cost
    #     self.cost_after_credit = cost_after_credit
    #     self.price = price
    #     self.price_after_credit = price_after_credit
    #     self.im360_subscription_id = im360_subscription_id

