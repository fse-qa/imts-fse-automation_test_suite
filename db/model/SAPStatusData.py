class SAPStatusData:
    def __init__(self, test_case_id, subscription_id, order_id, feed_validation_status, minCommitBit_validation_status,
                 usage_bit_validation_status, billing_status, invoiced_status, rar_status, status_code, image_ref,
                 error_details, date):
        self.test_case_id = 'NA' if test_case_id is None else test_case_id
        self.subscription_id = 'NA' if subscription_id is None else subscription_id
        self.order_id = 'NA' if order_id is None else order_id
        self.feed_validation_status = 'NA' if feed_validation_status is None else feed_validation_status
        self.minCommitBit_validation_status = 'NA' if minCommitBit_validation_status is None \
            else minCommitBit_validation_status
        self.usage_bit_validation_status = 'NA' if usage_bit_validation_status is None else usage_bit_validation_status
        self.billing_status = 'NA' if billing_status is None else billing_status
        self.invoiced_status = 'NA' if invoiced_status is None else invoiced_status
        self.rar_status = 'NA' if rar_status is None else rar_status
        self.status_code = 'NA' if status_code is None else status_code
        self.image_ref = 'NA' if image_ref is None else image_ref
        self.error_details = 'NA' if error_details is None else error_details
        self.date = 'NA' if date is None else date


