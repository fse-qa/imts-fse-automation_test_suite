
class IM360Subscription:

    test_case_id = None
    subscription_id = None
    quote_name = None
    subscription_name = None
    status = None
    bill_to = None
    subscription_period = None
    subscription_period_unit = None
    billing_period = None
    billing_period_unit = None
    reseller_bcn = None
    reseller_po = None
    agreement_id = None
    order_number = None
    payment_term = None
    payment_type = None
    currency = None

    # def __init__(self, test_case_id, subscription_id, subscription_name, subscription_start_date, subscription_end_date,
    #              customer_ac_ope_uid, status, bill_to, payment_method, subscription_period, subscription_period_unit,
    #              billing_period, billing_period_unit, reseller_bcn, reseller_po, primary_vendor, agreement_id,
    #              order_number, payment_term):
    #     self.test_case_id = test_case_id
    #     self.subscription_id = subscription_id
    #     self.subscription_name = subscription_name
    #     self.subscription_start_date = subscription_start_date
    #     self.subscription_end_date = subscription_end_date
    #     self.customer_ac_ope_uid = customer_ac_ope_uid
    #     self.status = status
    #     self.bill_to = bill_to
    #     self.payment_method = payment_method
    #     self.subscription_period = subscription_period
    #     self.subscription_period_unit = subscription_period_unit
    #     self.billing_period = billing_period
    #     self.billing_period_unit = billing_period_unit
    #     self.reseller_bcn = reseller_bcn
    #     self.reseller_po = reseller_po
    #     self.primary_vendor = primary_vendor
    #     self.agreement_id = agreement_id
    #     self.order_number = order_number
    #     self.payment_term = payment_term
