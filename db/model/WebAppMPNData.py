class WebAppMPNData:
    def __init__(self, item_id, item_code, item_name, item_mpn, item_quantity, item_unit_price, item_unit_cost,
                 currency, status, reseller_total_contract_value, vendor_total_contract_value, cancelable,
                 resellercentric, billing_type, change_date, discount_applicable_at_renewal, item_unit_cost_discounted,
                 item_unit_price_discounted, subscription_feed_cb_request_id):
        self.item_id = item_id
        self.item_code = item_code
        self.item_name = item_name
        self.item_mpn = item_mpn
        self.item_quantity = item_quantity
        self.item_unit_price = item_unit_price
        self.item_unit_cost = item_unit_cost
        self.currency = currency
        self.status = status
        self.reseller_total_contract_value = reseller_total_contract_value
        self.vendor_total_contract_value = vendor_total_contract_value
        self.cancelable = cancelable
        self.resellercentric = resellercentric
        self.billing_type = billing_type
        self.change_date = change_date
        self.discount_applicable_at_renewal = discount_applicable_at_renewal
        self.item_unit_cost_discounted = item_unit_cost_discounted
        self.item_unit_price_discounted = item_unit_price_discounted
        self.subscription_feed_cb_request_id = subscription_feed_cb_request_id
