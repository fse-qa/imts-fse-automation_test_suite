class SAPRAR:
    def __init__(self, tc_id, date, provider_contract, pob_id='', g2n_rev='', g2n_cost='', pob_price='', pob_cost='',
                 pc_tcv='', pc_commit_cost='', operation='', status='PASS', error_reason=''):
        self.tc_id = tc_id
        self.date = date
        self.provider_contract = provider_contract
        self.pob_id = pob_id
        self.g2n_rev = g2n_rev
        self.g2n_cost = g2n_cost
        self.pob_price = pob_price
        self.pob_cost = pob_cost
        self.pc_tcv = pc_tcv
        self.pc_commit_cost = pc_commit_cost
        self.operation = operation
        self.status = status
        self.error_reason = error_reason
