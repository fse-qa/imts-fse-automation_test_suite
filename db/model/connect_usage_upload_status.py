
class UsageUploadStatus:
    def __init__(self, id, test_case_id, subscription_id, usage_id, usage_start_date,
                 usage_end_date, usage_status_in_connect):
        self.id = id
        self.test_case_id = test_case_id
        self.subscription_id = subscription_id
        self.usage_id = usage_id
        self.usage_start_date = usage_start_date
        self.usage_end_date = usage_end_date
        self.usage_status_in_connect = usage_status_in_connect
