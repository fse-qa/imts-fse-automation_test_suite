class IM360Status:
    def __init__(self, test_case_id, test_case_name, status, failure_reason,
                 service_name, failure_image, reseller_currency, vendor_currency, marketplace_name):

        self.test_case_id = test_case_id
        self.test_case_name = test_case_name
        self.status = status
        self.failure_reason = failure_reason
        self.service_name = service_name
        self.failure_image = failure_image
        self.reseller_currency = reseller_currency
        self.vendor_currency = vendor_currency
        self.marketplace_name = marketplace_name

