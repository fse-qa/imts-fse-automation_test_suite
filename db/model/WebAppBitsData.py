class WebAppBitsData:
    def __init__(self, im_subscription_id, order_number, bcn, bit_date_from, bit_date_to, rb_bit_type, bit_amount,
                 bit_quantity, bit_quantity_unit, duration, duration_unit, unit_price, unit_cost,
                 sku, vendor_subscription_id):
        self.im_subscription_id = im_subscription_id
        self.order_number = order_number
        self.bcn = bcn
        self.bit_date_from = bit_date_from
        self.bit_date_to = bit_date_to
        self.rb_bit_type = rb_bit_type
        self.bit_amount = bit_amount
        self.bit_quantity = bit_quantity
        self.bit_quantity_unit = bit_quantity_unit
        self.duration = duration
        self.duration_unit = duration_unit
        self.unit_price = unit_price
        self.unit_cost = unit_cost
        self.sku = sku
        self.vendor_subscription_id = vendor_subscription_id

