class SAPBillingAndInvoicing:
    def __init__(self, tc_id='', date='', provider_contract='', bill_doc_num='NA', invoice_doc_num='NA', recon_key='NA', billable_bit_class='',
                 operation='', status='PASS', error_reason=''):
        self.tc_id = tc_id
        self.date = date
        self.provider_contract = provider_contract
        self.bill_doc_num = bill_doc_num
        self.invoice_doc_num = invoice_doc_num
        self.recon_key = recon_key
        self.billable_bit_class = billable_bit_class
        self.operation = operation
        self.status = status
        self.error_reason = error_reason
