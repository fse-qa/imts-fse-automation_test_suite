
class IM360InputItem:
    def __init__(self, test_case_id, item_mpn, item_quantity, vendor_sku, item_type,
                 unit_of_measure, usage_amount, order_action, im360_input_order_tbl_id):
        self.test_case_id = test_case_id
        self.item_mpn = item_mpn
        self.item_quantity = item_quantity
        self.vendor_sku = vendor_sku
        self.item_type = item_type
        self.unit_of_measure = unit_of_measure
        self.usage_amount = usage_amount
        self.order_action = order_action
        self.im360_input_order_tbl_id = im360_input_order_tbl_id
