class SAPSubscriptionData:
    def __init__(self, test_case_id, provider_contract, name, contract_start, contract_end, key_in_external_system,
                 company_code, im_vendor_sub_id, bill_to, payment_terms, subscription_period, subscrip_period_unit,
                 billing_period, billing_period_unit, status, flooring_bp, plant):
        self.test_case_id = test_case_id
        self.provider_contract = provider_contract
        self.name = name
        self.contract_start = contract_start
        self.contract_end = contract_end
        self.key_in_external_system = key_in_external_system
        self.company_code = company_code
        self.im_vendor_sub_id = im_vendor_sub_id
        self.bill_to = bill_to
        self.payment_terms = payment_terms
        self.subscription_period = subscription_period
        self.subscrip_period_unit = subscrip_period_unit
        self.billing_period = billing_period
        self.billing_period_unit = billing_period_unit
        self.status = status
        self.flooring_bp = flooring_bp
        self.plant = plant





