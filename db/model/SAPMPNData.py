class SAPMPNData:
    def __init__(self, subscription_id, total_amount, cost, quantity, payment_card_id, cc_num, end_of_duration,
                 cb_cancellable_flag, exchange_rate, migration_date, original_start_date, item_type, product_id,
                 currency):
        self.subscription_id = subscription_id
        self.total_amount = total_amount
        self.cost = cost
        self.quantity = quantity
        self.payment_card_id = payment_card_id
        self.cc_number = cc_num
        self.end_of_duration = end_of_duration
        self.cb_cancellable_flag = cb_cancellable_flag
        self.exchange_rate = exchange_rate
        self.migration_date = migration_date
        self.original_start_date = original_start_date
        self.item_type = item_type
        self.product_id = product_id
        self.currency = currency



