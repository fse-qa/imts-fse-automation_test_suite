class IM360InputOrder:
    def __init__(self, test_case_id, test_case_name, quote_name, reseller_bcn, contact, payment_term, bill_to, deal_id,
                 billing_period,
                 subscription_period_unit, subscription_period, service_plan_id, reseller_po, primary_vendor,
                 company_id, company_name, contact_phone, contact_name, contact_email, address, city, postal_code,
                 state, web_order_id,
                 requested_ship_start_date, agreement_id, provisioning_contact_name, provisioning_contact_email,
                 provisioning_contact_phone, vendor_portal_submission, saved_credit_card_name, cc_first_name,
                 cc_last_name, cc_email, cc_card_type, cc_card_number, cc_expiration_month, cc_expiration_year, cc_cvn,
                 reseller_currency, vendor_currency, service_name, marketplace_name, change_service_plan_id,
                 change_billing_period, change_subscription_period, change_payment_term, change_deal_id, test_type):
        self.test_case_id = test_case_id
        self.test_case_name = test_case_name
        self.quote_name = quote_name
        self.reseller_bcn = reseller_bcn
        self.contact = contact
        self.payment_term = payment_term
        self.bill_to = bill_to
        self.deal_id = deal_id
        self.billing_period = billing_period
        self.subscription_period_unit = subscription_period_unit
        self.subscription_period = subscription_period
        self.service_plan_id = service_plan_id
        self.reseller_po = reseller_po
        self.primary_vendor = primary_vendor
        self.company_id = company_id
        self.company_name = company_name
        self.contact_phone = contact_phone
        self.contact_name = contact_name
        self.contact_email = contact_email
        self.address = address
        self.city = city
        self.postal_code = postal_code
        self.state = state
        self.web_order_id = web_order_id
        self.requested_ship_start_date = requested_ship_start_date
        self.agreement_id = agreement_id
        self.provisioning_contact_name = provisioning_contact_name
        self.provisioning_contact_email = provisioning_contact_email
        self.provisioning_contact_phone = provisioning_contact_phone
        self.vendor_portal_submission = vendor_portal_submission
        self.saved_credit_card_name = saved_credit_card_name
        self.cc_first_name = cc_first_name
        self.cc_last_name = cc_last_name
        self.cc_email = cc_email
        self.cc_card_type = cc_card_type
        self.cc_card_number = cc_card_number
        self.cc_expiration_month = cc_expiration_month
        self.cc_expiration_year = cc_expiration_year
        self.cc_cvn = cc_cvn
        self.reseller_currency = reseller_currency
        self.vendor_currency = vendor_currency
        self.service_name = service_name
        self.marketplace_name = marketplace_name
        self.change_service_plan_id = change_service_plan_id
        self.change_billing_period = change_billing_period
        self.change_subscription_period = change_subscription_period
        self.change_payment_term = change_payment_term
        self.change_deal_id = change_deal_id
        self.test_type = test_type
