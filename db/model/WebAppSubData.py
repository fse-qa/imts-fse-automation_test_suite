class WebAppSubData:
    def __init__(self, subscription_id, vendor_subscription_id, subscription_name, reseller_id, reseller_bcn,
                 subscription_start_date, subscription_end_date, billing_period, billing_period_unit,
                 subscription_period, subscription_period_unit, currency, reseller_total_contract_value,
                 vendor_total_contract_value, company_name, reseller_name, vendor_id, vendor_name, status,
                 reseller_po, auto_renew, is_migrated, migration_date, order_id, order_type, plan_id,
                 subscription_feed_id):
        self.subscription_id = subscription_id
        self.vendor_subscription_id = vendor_subscription_id
        self.subscription_name = subscription_name
        self.reseller_id = reseller_id
        self.reseller_bcn = reseller_bcn
        self.subscription_start_date = subscription_start_date
        self.subscription_end_date = subscription_end_date
        self.billing_period = billing_period
        self.billing_period_unit = billing_period_unit
        self.subscription_period = subscription_period
        self.subscription_period_unit = subscription_period_unit
        self.currency = currency
        self.reseller_total_contract_value = reseller_total_contract_value
        self.vendor_total_contract_value = vendor_total_contract_value
        self.company_name = company_name
        self.reseller_name = reseller_name
        self.vendor_id = vendor_id
        self.vendor_name = vendor_name
        self.status = status
        self.reseller_po = reseller_po
        self.auto_renew = auto_renew
        self.is_migrated = is_migrated
        self.migration_date = migration_date
        self.order_id = order_id
        self.order_type = order_type
        self.plan_id = plan_id
        self.subscription_feed_id = subscription_feed_id




