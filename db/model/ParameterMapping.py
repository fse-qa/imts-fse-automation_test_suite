

class ParameterMapping:
    def __init__(self, parameter_name, parameter_value, parameter_type, service_name, is_mandatory, module, description):
        self.parameter_name = parameter_name
        self.parameter_value = parameter_value
        self.parameter_type = parameter_type
        self.service_name = service_name
        self.is_mandatory = is_mandatory
        self.module = module
        self.description = description
    