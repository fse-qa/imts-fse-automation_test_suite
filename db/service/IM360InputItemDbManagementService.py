from db.util.SqlUtil import SqlUtil
from db.dao.IM360InputItemDAO import IM360InputItemDAO


class IM360InputItemDbManagementService:

    def save_im360_input_item(self, db_path, im360_input_item_list):
        sql_util = SqlUtil(db_path)
        im360_input_item_dao = IM360InputItemDAO()
        im360_input_item_dao.insert_records(sql_util, im360_input_item_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_input_item_dao = IM360InputItemDAO()
        return im360_input_item_dao.show_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_input_item_dao = IM360InputItemDAO()
        im360_input_item_dao.delete_record(sql_util, row_id)

    def get_item_test_case_details(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_item_dao = IM360InputItemDAO()
        return im360_input_item_dao.show_item_test_case_records(sql_util, test_case_id)    

    def get_filtered_items_test_case_details(self, db_path, test_case_id, item_type, order_action):
        sql_util = SqlUtil(db_path)
        im360_input_item_dao = IM360InputItemDAO()
        return im360_input_item_dao.show_filtered_items_test_case_records(sql_util, test_case_id, item_type, order_action)
