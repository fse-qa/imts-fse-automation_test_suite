from db.util.SqlUtil import SqlUtil
from db.dao.CBCStatusDAO import CBCStatusDAO


class CBCStatusDbManagementService:

    def insert_cbc_status_data(self, db_path, cbc_status_list):
        sql_util = SqlUtil(db_path)
        cbc_status_check_dao = CBCStatusDAO()
        cbc_status_check_dao.insert_records(sql_util, cbc_status_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        cbc_status_check_dao = CBCStatusDAO()
        return cbc_status_check_dao.show_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        cbc_status_check_dao = CBCStatusDAO()
        cbc_status_check_dao.delete_record(sql_util, row_id)

    def get_status_by_sub_details(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        cbc_status_check_dao = CBCStatusDAO()
        return cbc_status_check_dao.show_status_by_sub_details(sql_util, sub_id, order_id)

    def fetch_latest_system_date(self, db_path):
        sql_util = SqlUtil(db_path)
        cbc_status_check_dao = CBCStatusDAO()
        return cbc_status_check_dao.show_latest_system_date(sql_util)

    def update_status(self, db_path, cbc_status_data_obj):
        sql_util = SqlUtil(db_path)
        cbc_status_check_dao = CBCStatusDAO()
        return cbc_status_check_dao.update_status(sql_util, cbc_status_data_obj)
