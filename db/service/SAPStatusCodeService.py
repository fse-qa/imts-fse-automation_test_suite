from db.util.SqlUtil import SqlUtil
from db.dao.SAPStatusCodeDAO import SAPStatusCodeDAO


class SAPStatusCodeDbManagementService:

    def insert_sap_status_code_data(self, db_path, cbc_status_list):
        sql_util = SqlUtil(db_path)
        sap_status_code_dao = SAPStatusCodeDAO()
        sap_status_code_dao.insert_records(sql_util, cbc_status_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        sap_status_code_dao = SAPStatusCodeDAO()
        return sap_status_code_dao.show_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        sap_status_code_dao = SAPStatusCodeDAO()
        sap_status_code_dao.delete_record(sql_util, row_id)

    def get_reason_by_status_and_code(self, db_path, status, reason_code):
        sql_util = SqlUtil(db_path)
        sap_status_code_dao = SAPStatusCodeDAO()
        return sap_status_code_dao.get_reason_by_status_and_code(sql_util, status, reason_code)