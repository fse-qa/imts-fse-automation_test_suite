from db.dao.SAPSubscriptionDataDAO import SAPSubDataDAO
from db.util.SqlUtil import SqlUtil


class SAPSubscriptionDataDbManagementService:

    def insert_sap_subs_data(self, db_path, sap_subs_list):
        sql_util = SqlUtil(db_path)
        sap_sub_data_dao = SAPSubDataDAO()
        sap_sub_data_dao.insert_records(sql_util, sap_subs_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        sap_sub_data_dao = SAPSubDataDAO()
        return sap_sub_data_dao.show_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        sap_sub_data_dao = SAPSubDataDAO()
        sap_sub_data_dao.delete_record(sql_util, row_id)

    def get_subscription_details_by_sub_id(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        sap_sub_data_dao = SAPSubDataDAO()
        return sap_sub_data_dao.show_records_by_sub_id(sql_util, sub_id)

    def get_subscription_status(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        sap_sub_data_dao = SAPSubDataDAO()
        return sap_sub_data_dao.show_sub_status_by_sub_id(sql_util, sub_id)

    def get_contract_term_for_given_sub_id(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPSubDataDAO()
        return sap_bits_data_dao.show_contract_term_by_sub_id(sql_util, sub_id)

    def get_start_and_end_date_for_given_sku(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPSubDataDAO()
        return sap_mpn_data_dao.get_start_and_end_date_for_given_sku(sql_util, sub_id)