from db.util.SqlUtil import SqlUtil
from db.dao.WebAppBitsDataDAO import WebAppBitsDataDao


class WebAppBitsDataService:

    def get_details_by_sub_id(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        web_app_bits_data_dao = WebAppBitsDataDao()
        return web_app_bits_data_dao.show_records_by_sub_details(sql_util, sub_id, order_id)