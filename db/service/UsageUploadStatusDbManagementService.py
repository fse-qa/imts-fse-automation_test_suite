from db.dao.usageUploadStatusDAO import UsageUploadStatusDAO
from db.util.SqlUtil import SqlUtil


class UsageDataDbManagementService():

    def get_subscription_from_im360_by_testcase(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        cb_connect_usage_dao = UsageUploadStatusDAO()
        subscription_id = cb_connect_usage_dao.select_im360_subscription_by_testcase(sql_util, test_case_id)
        return subscription_id

    def get_usage_dates_from_commerce_date_track_for_cancel(self, db_path):
        sql_util = SqlUtil(db_path)
        cb_connect_usage_dao = UsageUploadStatusDAO()
        return cb_connect_usage_dao.select_usage_date_for_sub_cancel_flow(sql_util)

    def get_usage_dates_from_commerce_date_track_for_e2e(self, db_path):
        sql_util = SqlUtil(db_path)
        cb_connect_usage_dao = UsageUploadStatusDAO()
        return cb_connect_usage_dao.select_usage_date_for_basic_e2e(sql_util)

    def get_usage_mpn_from_input_by_testcase(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        cb_connect_usage_dao = UsageUploadStatusDAO()
        usage_mpn_details = cb_connect_usage_dao.select_input_usage_mpn_data(sql_util, test_case_id)
        return usage_mpn_details

    def insert_usage_details_by_testcase(self, db_path, usage_detail_list):
        sql_util = SqlUtil(db_path)
        cb_connect_usage_dao = UsageUploadStatusDAO()
        return cb_connect_usage_dao.insert_usage_details_as_per_testcase(sql_util, usage_detail_list)
