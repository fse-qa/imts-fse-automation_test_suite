from db.dao.sap_billing_and_invoicing_dao import SAPBillingAndInvoicingDAO


class SAPBillingAndInvoicingService:

    def __init__(self, db_path):
        self.sap_billing_and_invoicing_dao = SAPBillingAndInvoicingDAO(db_path)

    def insert_record(self, record):
        self.sap_billing_and_invoicing_dao.insert_record(record)
