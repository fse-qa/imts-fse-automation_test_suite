from db.util.SqlUtil import SqlUtil
from db.dao.IM360SubscriptionParametersDAO import IM360SubscriptionParametersDAO


class IM360SubscriptionParameterDbManagementService:

    def save_im360_subscription_parameter(self, db_path, im360_item_list):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        im360_subs_param_dao.insert_records(sql_util, im360_item_list)

    def get_all_im360_subscription_parameter_details(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        return im360_subs_param_dao.show_all_records(sql_util)

    def get_max_id(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        return im360_subs_param_dao.get_max_id(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        im360_subs_param_dao.delete_record(sql_util, row_id)

    def get_param_val_by_id_and_param_name(self, db_path, id, param_name):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        return im360_subs_param_dao.get_param_val_by_id_and_param_name(sql_util, id, param_name)

    def insert_duplicate_row(self, db_path, im360_subs_tbl_old_id, im360_subs_tbl_new_id):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        return im360_subs_param_dao.insert_duplicate_row(sql_util, im360_subs_tbl_old_id, im360_subs_tbl_new_id)

    def get_record_by_param_id(self, db_path, sub_id, param_id):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        return im360_subs_param_dao.get_record_by_param_id(sql_util, sub_id, param_id)

    def update_records_with_updated_deal_id(self, db_path, test_case_id, change_deal_id ):
        sql_util = SqlUtil(db_path)
        im360_subs_param_dao = IM360SubscriptionParametersDAO()
        return im360_subs_param_dao.update_records_with_updated_deal_id(sql_util, test_case_id, change_deal_id)
