from db.dao.SAPMPNDataDAO import SAPMPNDataDAO
from db.util.SqlUtil import SqlUtil


class SAPMPNDbManagementService:

    def insert_mpn_data(self, db_path, sap_mpn_list):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        sap_mpn_data_dao.insert_records(sql_util, sap_mpn_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        return sap_mpn_data_dao.show_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        sap_mpn_data_dao.delete_record(sql_util, row_id)

    def get_items_by_sub_id(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        return sap_mpn_data_dao.show_records_by_sub_id(sql_util, sub_id)

    def get_payment_term_by_sub_id(self, db_path, sub_id, flag):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        return sap_mpn_data_dao.show_payment_term_by_sub_id(sql_util, sub_id, flag)

    def get_eod_date(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        return sap_mpn_data_dao.show_eod_date_by_sub_id(sql_util, sub_id)

    def get_cc_number(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        return sap_mpn_data_dao.get_cc_number_by_tc_id(sql_util, tc_id)

    def get_amount_and_cost_for_given_sku(self, db_path, sub_id, sku_part, quantity):
        sql_util = SqlUtil(db_path)
        sap_mpn_data_dao = SAPMPNDataDAO()
        return sap_mpn_data_dao.get_amount_and_cost_for_given_sku(sql_util, sub_id, sku_part, quantity)
