from db.dao.SAPBitsDataDAO import SAPBitsDataDAO
from db.util.SqlUtil import SqlUtil


class SAPBitsDataDbManagementService:

    def insert_sap_bits_data(self, db_path, sap_bits_list):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        sap_bits_data_dao.insert_records(sql_util, sap_bits_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        return sap_bits_data_dao.show_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        sap_bits_data_dao.delete_record(sql_util, row_id)

    def get_records_by_sub_details(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        return sap_bits_data_dao.show_records_by_sub_details(sql_util, sub_id, order_id)

    def fetch_usage_amount(self, db_path, sub_id, bill_item_type):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        return sap_bits_data_dao.fetch_usage_amount(sql_util, sub_id, bill_item_type)

    def get_quantity_for_given_sku(self, db_path, sub_id, sku_part, bill_item_type, number_of_records=1):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        return sap_bits_data_dao.get_billing_quantity_by_sku_part(sql_util, sub_id, sku_part, bill_item_type,
                                                                  number_of_records)

    def get_records_by_sub_details_for_cost_amd_amount(self, db_path, sub_id, sku_part, bill_item_type,
                                                       number_of_records=1):
        sql_util = SqlUtil(db_path)
        sap_bits_data_dao = SAPBitsDataDAO()
        return sap_bits_data_dao.get_billing_period_cost_and_amount(sql_util, sub_id, sku_part, bill_item_type,
                                                                    number_of_records)


