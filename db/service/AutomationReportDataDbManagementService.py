from db.dao.AutomationReportDataDAO import AutomationReportDataDAO
from db.util.SqlUtil import SqlUtil


class AutomationReportDataDbManagementService:
    def insert_records(self, db_path, automation_report_data_list):
        sql_util = SqlUtil(db_path)
        automation_report_data_dao = AutomationReportDataDAO()
        automation_report_data_dao.insert_records(sql_util, automation_report_data_list)

    def get_all_record(self, db_path):
        sql_util = SqlUtil(db_path)
        automation_report_data_dao = AutomationReportDataDAO()
        return automation_report_data_dao.get_all_records(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        automation_report_data_dao = AutomationReportDataDAO()
        automation_report_data_dao.delete_record(sql_util, row_id)