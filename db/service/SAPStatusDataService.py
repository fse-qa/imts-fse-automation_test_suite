from db.util.SqlUtil import SqlUtil
from db.dao.SAPStatusDataDAO import SAPStatusDataDAO


class SAPStatusDataDbManagementService:

    def insert_sap_status_data(self, db_path, sap_status_list):
        sql_util = SqlUtil(db_path)
        sap_status_data_dao = SAPStatusDataDAO()
        sap_status_data_dao.insert_records(sql_util, sap_status_list)

    def get_all_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        sap_status_data_dao = SAPStatusDataDAO()
        return sap_status_data_dao.show_all_records(sql_util)

    def get_all_test_case_ids(self, db_path):
        sql_util = SqlUtil(db_path)
        sap_status_data_dao = SAPStatusDataDAO()
        return sap_status_data_dao.get_all_test_case_ids(sql_util)

    def get_all_records_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        sap_status_data_dao = SAPStatusDataDAO()
        return sap_status_data_dao.get_all_records_by_test_case_id(sql_util, test_case_id)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        sap_status_data_dao = SAPStatusDataDAO()
        sap_status_data_dao.delete_record(sql_util, row_id)