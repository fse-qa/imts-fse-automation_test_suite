from db.util.SqlUtil import SqlUtil
from db.dao.WebAppSubDataDAO import WebAppSubDataDao


class WebAppSubDataService:

    def get_all_subscription_details(self, db_path):
        sql_util = SqlUtil(db_path)
        web_app_sub_data_dao = WebAppSubDataDao()
        return web_app_sub_data_dao.show_all_records(sql_util)

    def get_subs_req_list1_by_sub_details(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        web_app_sub_data_dao = WebAppSubDataDao()
        return web_app_sub_data_dao.show_records_by_sub_details(sql_util, sub_id, order_id, 'req_list1')

    def get_subs_req_list2_by_sub_details(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        web_app_sub_data_dao = WebAppSubDataDao()
        return web_app_sub_data_dao.show_records_by_sub_details(sql_util, sub_id, order_id, 'req_list2')

    def get_subs_req_list3_by_sub_details(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        web_app_sub_data_dao = WebAppSubDataDao()
        return web_app_sub_data_dao.show_records_by_sub_details(sql_util, sub_id, order_id, 'req_list3')

    def get_sap_response_by_sub_details(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        web_app_sub_data_dao = WebAppSubDataDao()
        return web_app_sub_data_dao.show_sap_response_by_sub_details(sql_util, sub_id, order_id)

    def get_latest_order_number(self, db_path, sub_id, date):
        sql_util = SqlUtil(db_path)
        web_app_sub_data_dao = WebAppSubDataDao()
        return web_app_sub_data_dao.show_order_reference_number(sql_util, sub_id, date)






