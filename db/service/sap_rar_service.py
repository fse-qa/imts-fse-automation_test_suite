from db.dao.sap_rar_dao import SAPRARDAO


class SAPRARService:

    def __init__(self, db_path):
        self.sap_rar_dao = SAPRARDAO(db_path)

    def insert_record(self, record):
        self.sap_rar_dao.insert_record(record)
