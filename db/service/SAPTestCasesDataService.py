from db.util.SqlUtil import SqlUtil
from db.dao.SAPTestCasesDataDAO import SAPTestCasesDataDAO


class SAPTestCasesDataService:

    def insert_tc_status(self, db_path, tc_id, pytest_name, status):
        sql_util = SqlUtil(db_path)
        sap_status_code_dao = SAPTestCasesDataDAO()
        return sap_status_code_dao.insert_records(sql_util, tc_id, pytest_name, status)

    def get_tc_status(self, db_path, tc_id, pytest_name):
        sql_util = SqlUtil(db_path)
        sap_status_code_dao = SAPTestCasesDataDAO()
        return sap_status_code_dao.show_status(sql_util, tc_id, pytest_name)
