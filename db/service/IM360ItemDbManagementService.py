from db.dao.IM360ItemDAO import IM360ItemDAO
from db.util.SqlUtil import SqlUtil


class IM360ItemDbManagementService:

    def save_im360_item(self, db_path, im360_item_list):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        im360_item_dao.insert_records(sql_util, im360_item_list)

    def get_all_im360_item_details(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.show_all_records(sql_util)

    def get_max_id(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.get_max_id(sql_util)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        im360_item_dao.delete_record(sql_util, row_id)

    def get_mpn_data_by_sub_details(self, db_path, sub_id, order_num):
        # breakpoint()
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.show_records_by_sub_details(sql_util, sub_id, order_num)

    def update_row(self, db_path, test_case_id, subs_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        im360_item_dao.update_row(sql_util, test_case_id, subs_id)

    def get_currency_by_sub_details(self, db_path, sub_id, order_num):
        # breakpoint()
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.show_currency_by_sub_details(sql_util, sub_id, order_num)

    def get_item_type_by_sub_details(self, db_path, sub_id, order_num):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.show_item_type_by_sub_details(sql_util, sub_id, order_num)

    def insert_duplicate_row(self, db_path, im360_subs_tbl_old_id, im360_subs_tbl_new_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.insert_duplicate_row(sql_util, im360_subs_tbl_old_id, im360_subs_tbl_new_id)

    def get_all_sku_by_sub_details(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.show_sku_by_sub_details(sql_util, sub_id)

    def get_usage_sku_by_sub_details(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.show_usage_sku_by_tc_details(sql_util, tc_id)

    def get_im360_item_data_for_added_sku(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.get_im360_item_data_for_added_sku(sql_util, sub_id)

    def get_second_max_id_item_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_item_dao = IM360ItemDAO()
        return im360_item_dao.get_second_max_id_item_by_test_case_id(sql_util, test_case_id)
