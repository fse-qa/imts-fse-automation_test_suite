from db.dao.ConnectStatusDAO import ConnectStatusDAO
from db.util.SqlUtil import SqlUtil


class ConnectStatusDBManagementService:

    def save_connect_status(self, db_path, connect_status_list):
        sql_util = SqlUtil(db_path)
        connect_status_dao = ConnectStatusDAO()
        return connect_status_dao.insert_records(sql_util, connect_status_list)

    def get_connect_status(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        connect_status_dao = ConnectStatusDAO()
        return connect_status_dao.show_connect_status_by_tc_id(sql_util, tc_id)

    def get_usage_date_by_test_case(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        connect_status_dao = ConnectStatusDAO()
        return connect_status_dao.show_usage_date(sql_util, tc_id)
