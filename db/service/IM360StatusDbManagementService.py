from db.util.SqlUtil import SqlUtil
from db.dao.IM360StatusDAO import IM360StatusDAO


class IM360StatusDbManagementService:

    def save_im360_status(self, db_path, im360_status_list):
        sql_util = SqlUtil(db_path)
        im360_status_dao = IM360StatusDAO()
        return im360_status_dao.insert_records(sql_util, im360_status_list)

    def get_all_im360_status_details(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_status_dao = IM360StatusDAO()
        im360_status_dao.show_all_records(sql_util)

    def get_id_by_row_id(self, db_path, im360_status_row_id):
        sql_util = SqlUtil(db_path)
        im360_status_dao = IM360StatusDAO()
        return im360_status_dao.get_id_by_row_id(sql_util, im360_status_row_id)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_status_dao = IM360StatusDAO()
        im360_status_dao.delete_record(sql_util, row_id)

    def get_quote_name_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_status_dao = IM360StatusDAO()
        return im360_status_dao.get_quote_name_by_test_case_id(sql_util, test_case_id)

    def get_status_and_image_by_sub_id(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        im360_status_dao = IM360StatusDAO()
        return im360_status_dao.get_status_and_image_by_tc_id(sql_util, tc_id)
