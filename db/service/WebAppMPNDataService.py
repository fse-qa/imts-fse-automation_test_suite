from db.util.SqlUtil import SqlUtil
from db.dao.WebAppMPNDataDAO import WebAppMPNDataDao


class WebAppMPNDataService:

    def get_all_subscription_details(self, db_path):
        sql_util = SqlUtil(db_path)
        web_app_mpn_data_dao = WebAppMPNDataDao()
        return web_app_mpn_data_dao.show_all_records(sql_util)

    def get_details_by_sub_id(self, db_path, sub_id, order_id):
        sql_util = SqlUtil(db_path)
        web_app_mpn_data_dao = WebAppMPNDataDao()
        return web_app_mpn_data_dao.show_records_by_sub_details(sql_util, sub_id, order_id)