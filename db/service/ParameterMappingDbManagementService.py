from db.util.SqlUtil import SqlUtil
from db.dao.ParameterMappingDAO import ParameterMappingDAO


class ParameterMappingDbManagementService:

    def get_all_parameter_details(self, db_path):
        sql_util = SqlUtil(db_path)
        parameter_mapping_dao = ParameterMappingDAO()
        all_records = parameter_mapping_dao.show_all_records(sql_util)
        return all_records

    def count_record_by_connect_param(self, db_path, parameter):
        sql_util = SqlUtil(db_path)
        parameter_mapping_dao = ParameterMappingDAO()
        count_records = parameter_mapping_dao.count_record_by_connect_param(sql_util, parameter)
        return count_records
