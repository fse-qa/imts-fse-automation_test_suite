from db.dao.IM360SubscriptionDAO import IM360SubscriptionDAO
from db.util.SqlUtil import SqlUtil


class IM360SubscriptionDbManagementService:

    def save_im360_subscription(self, db_path, im360_subscription_list):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.insert_records(sql_util, im360_subscription_list)

    def get_all_subscription_details(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.show_all_records(sql_util)

    def get_id_by_row_id(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_id_by_row_id(sql_util, row_id)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        im360_subscription_dao.delete_record(sql_util, row_id)

    def get_subscription_details_by_sub_id(self, db_path, sub_id, order_num):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.show_records_by_sub_id(sql_util, sub_id, order_num)

    def get_vendor_sub_id(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_vendor_sub_id(sql_util, sub_id)

    def get_sub_details_for_status_check(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.show_sub_details_for_status_check(sql_util, tc_id)

    def get_all_sub_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_all_sub_by_test_case_id(sql_util, test_case_id)

    def update_subs_and_order_id(self, db_path, test_case_id, subs_id, erp_order):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        im360_subscription_dao.update_subs_and_order_id(sql_util, test_case_id, subs_id, erp_order)

    def get_id_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_id_by_test_case_id(sql_util, test_case_id)

    def update_status_by_test_case_id(self, db_path, test_case_id, status):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        im360_subscription_dao.update_status_by_test_case_id(sql_util, test_case_id, status)

    def get_quote_name_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_quote_name_by_test_case_id(sql_util, test_case_id)

    def get_sub_id_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_sub_id_by_test_case_id(sql_util, test_case_id)

    def get_sub_id(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_sub_id(sql_util, tc_id)

    def get_order_id(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_order_id(sql_util, sub_id)

    def insert_records_with_updated_term(self, db_path, test_case_id, payment_term, payment_type):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.insert_records_with_updated_term(sql_util, test_case_id, payment_term,
                                                                       payment_type)

    def insert_records_with_updated_status(self, db_path, test_case_id, status):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.insert_records_with_updated_status(sql_util, test_case_id, status)

    def get_second_max_id_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_second_max_id_by_test_case_id(sql_util, test_case_id)

    def get_pending_subs_by_tc(self, test_case_id, db_path):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_pending_subs_by_tc(sql_util, test_case_id)

    def get_mpn_list(self, db_path, sub_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_latest_mpn_list_by_sub_id(sql_util, sub_id)

    def insert_duplicate_row(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.insert_duplicate_row(sql_util, test_case_id)

    def update_status_by_test_case_id_and_tbl_id(self, db_path, test_case_id, status):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        im360_subscription_dao.update_status_by_test_case_id_and_tbl_id(sql_util, test_case_id, status)

    def update_order_id(self, db_path, test_case_id, im360_subs_max_id, erp_order):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        im360_subscription_dao.update_order_id(sql_util, test_case_id, im360_subs_max_id, erp_order)

    def insert_records_with_updated_order_number(self, db_path, test_case_id, order_number):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.insert_records_with_updated_order_number(sql_util, test_case_id, order_number)

    def get_max_id_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.get_max_id_by_test_case_id(sql_util, test_case_id)

    def update_records_with_updated_subscription_period(self, db_path, test_case_id, subscription_period):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.update_records_with_updated_subscription_period(sql_util, test_case_id,
                                                                                      subscription_period)

    def update_the_billing_frequency(self, db_path, test_case_id, change_billing_frequency):
        sql_util = SqlUtil(db_path)
        im360_subscription_dao = IM360SubscriptionDAO()
        return im360_subscription_dao.update_the_billing_frequency(sql_util, test_case_id, change_billing_frequency)