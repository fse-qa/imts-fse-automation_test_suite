from db.dao.IM360InputOrderDAO import IM360InputOrderDAO
from db.util.SqlUtil import SqlUtil


class IM360InputOrderDbManagementService:

    def save_im360_input_order(self, db_path, im360_input_order_list):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.insert_records(sql_util, im360_input_order_list)

    def get_all_im360_input_order_details(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        im360_input_order_dao.show_all_records(sql_util)

    def get_id_by_row_id(self, db_path, im360_input_order_row_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_id_by_row_id(sql_util, im360_input_order_row_id)

    def delete_record(self, db_path, row_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        im360_input_order_dao.delete_record(sql_util, row_id)

    def get_quote_name_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_quote_name_by_test_case_id(sql_util, test_case_id)

    def get_test_case_name_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_test_case_name_by_test_case_id(sql_util, test_case_id)

    def get_tc_id_list(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_test_case_id(sql_util)

    def get_im360_input_test_case_order_detail(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.show_order_test_case_record(sql_util, test_case_id)

    def get_service_name_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_service_name_by_test_case_id(sql_util, test_case_id)

    def get_cc_number(self, db_path, tc_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_cc_number_by_tc_id(sql_util, tc_id)

    def get_reseller_currency_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_reseller_currency_by_test_case_id(sql_util, test_case_id)

    def get_vendor_currency_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_vendor_currency_by_test_case_id(sql_util, test_case_id)

    def get_marketplace_name_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_marketplace_name_by_test_case_id(sql_util, test_case_id)

    def get_distinct_marketplaces(self, db_path):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_distinct_marketplaces(sql_util)

    def count_marketplaces_testcase(self, db_path, marketplace, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.count_marketplaces_testcase(sql_util, marketplace, test_case_id)

    def get_test_type_by_test_case_id(self, db_path, test_case_id):
        sql_util = SqlUtil(db_path)
        im360_input_order_dao = IM360InputOrderDAO()
        return im360_input_order_dao.get_test_type_by_test_case_id(sql_util, test_case_id)

