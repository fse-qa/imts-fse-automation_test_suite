import sqlite3

from CommonUtilities.logGeneration import LogGenerator
from db.util.SqlConstant import SqlConstant
from sqlite3 import Error


class ConnectStatusDAO:
    logger = LogGenerator.logGen()

    def insert_records(self, sql_util, connectStatus):
        # This method is responsible to insert a record into cb_connect_status table
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.CONNECT_STATUS_INSERT_SQL_QUERY,
                           (connectStatus.subscription_status, connectStatus.error_message,
                            connectStatus.im360_subscription_id, connectStatus.test_case_id))

            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the input data into IM360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("data inserted successfully into IM360_subscription table")
        return row_id

    def show_connect_status_by_tc_id(self, sql_util, tc_id):
        status_list = None
        try:
            self.logger.info("Fetching the records from cb_connect_status table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.CONNECT_STATUS_SELECT_BY_SUB_DETAILS, [tc_id])
            status_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from cb_connect_status table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from cb_connect_status table")
        for item in status_list:
            return item['subscription_status'], item['error_message']

    def show_usage_date(self, sql_util, tc_id):
        try:
            self.logger.info("Fetching the records from connect_usage_upload_status table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.SAP_USAGE_DATE_BY_TC_ID, [tc_id])
            status_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from connect_usage_upload_status "
                              "table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from connect_usage_upload_status table")
        for item in status_list[-1]:
            return item
