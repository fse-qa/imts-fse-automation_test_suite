import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class AutomationReportDataDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, automation_report_data_list):
        try:
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting data into automation_report_data table")
            for automation_data in automation_report_data_list:
                cursor.execute(SqlConstant.AUTOMATION_REPORT_DATA_INSERT_SQL_QUERY,
                               (automation_data.test_case_id, automation_data.test_case_name,
                                automation_data.status, automation_data.failure_reason,
                                automation_data.status_level_fail, automation_data.status_level_warning,
                                automation_data.subscription_id, automation_data.reseller_bcn,
                                automation_data.reseller_currency, automation_data.vendor_currency,
                                automation_data.service_name, automation_data.marketplace_name,
                                automation_data.module, automation_data.failure_image))
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into automation_report_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("data inserted successfully into automation_report_data table")

    def get_all_records(self, sql_util):
        automation_report_data_list = None
        try:
            self.logger.info("Fetching the records from automation_report_data table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.AUTOMATION_REPORT_DATA_SELECT_ALL_SQL_QUERY)
            automation_report_data_list = cursor.fetchall()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch records from automation_report_data table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from automation_report_data table")
        return automation_report_data_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from automation_report_data table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.AUTOMATION_REPORT_DATA_DELETE_BY_ID_SQL_QUERY, row_id)
            connection.commit()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to delete records from automation_report_data table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from automation_report_data table with row ID : " + row_id)
