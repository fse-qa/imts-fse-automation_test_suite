import sqlite3
from sqlite3 import Error

from Tests.im360.test_base_im360 import BaseTest
from db.util.SqlConstant import SqlConstant


class IM360StatusDAO(BaseTest):

    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, im360_status_list):
        # This method is responsible to insert multiple records into IM360_status table
        row_id = None
        try:
            self.logger.info("Inserting the input data into IM360_status table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            for im360_status in im360_status_list:
                cursor.execute(SqlConstant.IM360_STATUS_INSERT_SQL_QUERY,
                               (im360_status.test_case_id, im360_status.test_case_name, im360_status.status,
                                im360_status.failure_reason, im360_status.service_name,
                                im360_status.failure_image, im360_status.reseller_currency,
                                im360_status.vendor_currency, im360_status.marketplace_name))
                connection.commit()
                row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_status table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("data inserted successfully into IM360_status table")
        return row_id

    def get_id_by_row_id(self, sql_util, row_id):
        last_id = None
        try:
            self.logger.info("Fetching ID from IM360_status table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_STATUS_MAX_ID_SELECT_QUERY, [str(row_id)])
            im360_status = cursor.fetchall()
            for record in im360_status:
                last_id = record[0]
            self.logger.info("ID %s fetched successfully from IM360_status table" % str(last_id))
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch ID from IM360_status "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        return last_id

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from IM360_status table
        subs_list = None
        try:
            self.logger.info("Fetching the records from IM360_status table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_STATUS_SELECT_ALL_SQL_QUERY)
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_status table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from IM360_status table")
            return subs_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from IM360_status table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_STATUS_DELETE_BY_ID_SQL_QUERY, [row_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_status table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from IM360_status table with row ID : " + row_id)

    def get_status_and_image_by_tc_id(self, sql_util, tc_id):
        try:
            self.logger.info("Fetching Subscription Status and image_reference, if any, from IM360_status table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_STATUS_FETCH_STATUS_BY_SUB_ID_SELECT_QUERY, [tc_id])
            status_data = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch status and image reference from IM360_status "
                              "table : " + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        for item in status_data:
            return item['status'], item['failure_reason'], item['failure_image']
