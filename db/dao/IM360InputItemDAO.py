from db.util.SqlConstant import SqlConstant
from Tests.im360.test_base_im360 import BaseTest
from sqlite3 import Error
import sqlite3

class IM360InputItemDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, im360_input_item_list):
        # This method is responsible to insert multiple records into im360_input_item table
        try:
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting the input data into im360_input_item table")
            for im360_input_item in im360_input_item_list:
                cursor.execute(SqlConstant.IM360_INPUT_ITEM_INSERT_SQL_QUERY,
                               (im360_input_item.test_case_id, im360_input_item.item_mpn,
                                im360_input_item.vendor_sku, im360_input_item.item_quantity,
                                im360_input_item.item_type, im360_input_item.unit_of_measure,
                                im360_input_item.usage_amount, im360_input_item.order_action,
                                im360_input_item.im360_input_order_tbl_id
                                ))
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_input_item table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("data inserted successfully into im360_input_item table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from im360_input_order table
        item_list = None
        try:
            self.logger.info("Fetching the records from IM360_input_item table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ITEM_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_input_item table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from IM360_input_item table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from IM360_input_item table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ITEM_DELETE_BY_ID_SQL_QUERY, row_id)
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_input_item table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from IM360_input_item table with row ID : " + row_id)
    
    def show_item_test_case_records(self, sql_util, test_case_id):
        # This method is responsible to get item test case records from im360_input_item table
        item_list_json = None
        try:
            self.logger.info("Fetching the item test case records from IM360_input_item table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ITEM_GET_TEST_CASE_RECORD_SQL_QUERY, [str(test_case_id)])
            row_headers = [x[0] for x in cursor.description]  # this will extract row headers
            item_list = cursor.fetchall()
            item_list.reverse()
            json_data = []
            for result in item_list:
                json_data.append(dict(zip(row_headers, result)))
            item_list_json = json_data
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch test case records from IM360_input_item table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Test case records fetched successfully from IM360_input_item table")
        return item_list_json

    def show_filtered_items_test_case_records(self, sql_util, test_case_id, item_type, order_action):
        # This method is responsible to get test case records from im360_input_order table
        item_list_json = None
        try:
            self.logger.info("Fetching the filtered items test case records from IM360_input_item table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_GET_FILTERED_ITEM_TEST_CASE_RECORD_SQL_QUERY, [test_case_id, item_type, order_action, test_case_id])
            row_headers = [x[0] for x in cursor.description]  # this will extract row headers
            item_list = cursor.fetchall()
            item_list.reverse()
            json_data = []
            for result in item_list:
                json_data.append(dict(zip(row_headers, result)))
            item_list_json = json_data
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch filtered items test case records from IM360_input_item table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All filtered item Test case records fetched successfully from IM360_input_item table")
        return item_list_json