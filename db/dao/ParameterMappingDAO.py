import sqlite3

from db.util.SqlConstant import SqlConstant
from Tests.test_base import BaseTest
from sqlite3 import Error


class ParameterMappingDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from im360_input_order table
        parameter_list = None
        try:
            self.logger.info("Fetching the records from parameter_mapping table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.PARAMETER_MAPPING_SELECT_ALL_SQL_QUERY)
            parameter_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from parameter_mapping table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from parameter_mapping table")
        return parameter_list

    def count_record_by_connect_param(self, sql_util, parameter):
        count_record = 1
        try:
            self.logger.info("Fetching the records from parameter_mapping table by connect parameter name")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.PARAMETER_MAPPING_COUNT_ROW_BY_CONNECT_PARAM_SQL_QUERY, [parameter])

            param_list = cursor.fetchall()
            for record in param_list:
                count_record = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records by connect parameter name from "
                              "parameter_mapping table " + str(e))
        finally:
            sql_util.close_connection(connection)
        self.logger.info("Records fetched successfully from parameter_mapping table by connect parameter name")
        return count_record
