import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class SAPStatusCodeDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, sap_status_code_list):
        # This method is responsible to insert multiple records into sap_status_code table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting subscription data with cb status data into sap_status_code table")
            for item in sap_status_code_list:
                cursor.execute(SqlConstant.SAP_STATUS_CODE_INSERT_SQL_QUERY,
                               (item.status, item.code, item.reason))
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into sap_status_code table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into sap_status_code table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from sap_status_code table
        item_list = None
        try:
            self.logger.info("Fetching the records from sap_status_code table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_STATUS_CODE_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_status_code table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_status_code table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from sap_status_code table with row ID : " + row_id)
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_STATUS_CODE_DELETE_BY_ID_SQL_QUERY, row_id)
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from sap_status_code table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Records deleted successfully from sap_status_code table with row ID : " + row_id)

    def get_reason_by_status_and_code(self, sql_util, status, code):
        reason = None
        try:
            self.logger.info("Fetching the reason from sap_status_code table by status %s and reason_code %s ",
                             status, reason)
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_REASON_SELECT_SQL_QUERY, [status, code])
            sap_status_code = cursor.fetchall()
            for record in sap_status_code:
                reason = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch reason from sap_status_code table by status and reason_code "
                "%s", str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

            self.logger.info("Reason fetched successfully from sap_status_code table by status and reason_code")
            return reason
