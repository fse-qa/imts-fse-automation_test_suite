import sqlite3

from db.util.SqlConstant import SqlConstant
from Tests.test_base import BaseTest
from sqlite3 import Error


class WebAppBitsDataDao(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def show_records_by_sub_details(self, sql_util, sub_id, order_id):
        # This method is responsible to get all the records from bitfeed_bits table
        item_list = None
        try:
            self.logger.info(f"Fetching the records from bitfeed_bits table for Subscription: {sub_id} with"
                             f"Order_Number: {order_id}")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.WEB_APP_BITS_DATA_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from bitfeed_bits table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from Tibco Web App bitfeed_bits table")
        return item_list
