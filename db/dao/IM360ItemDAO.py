import sqlite3
from sqlite3 import Error

from Tests.im360.test_base_im360 import BaseTest
from db.util.SqlConstant import SqlConstant


class IM360ItemDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, im360_item_list):
        # This method is responsible to insert multiple records into IM360_item table
        try:
            self.logger.info("Fetching the Max row ID from im360_subscription table for inserting the data into "
                             "IM360_item table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting the data into im360_item table")
            for im360_item in im360_item_list:
                cursor.execute(SqlConstant.IM360_ITEM_INSERT_SQL_QUERY,
                               [im360_item.test_case_id, im360_item.subscription_id,
                                im360_item.item_quantity, im360_item.reseller_total_contract_value,
                                im360_item.vendor_total_contract_value,
                                im360_item.sku, im360_item.vendor_sku, im360_item.applicable_at_renewal,
                                im360_item.cost, im360_item.cost_after_credit, im360_item.price,
                                im360_item.price_after_credit, im360_item.im360_subscription_tbl_id])
            connection.commit()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the data into im360_item table ")
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Data inserted successfully into im360_item table")

    def insert_new_records(self, sql_util, im360_item):
        # This method is responsible to insert multiple records into IM360_item table
        try:
            self.logger.info("Fetching the Max row ID from im360_subscription table for inserting the data into "
                             "IM360_item table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting the data into im360_item table")
            cursor.execute(SqlConstant.IM360_ITEM_INSERT_SQL_QUERY,
                           (im360_item.test_case_id, im360_item.subscription_id,
                            im360_item.item_quantity, im360_item.reseller_total_contract_value,
                            im360_item.vendor_total_contract_value,
                            im360_item.sku, im360_item.vendor_sku, im360_item.applicable_at_renewal,
                            im360_item.cost, im360_item.cost_after_credit, im360_item.price,
                            im360_item.price_after_credit, im360_item.im360_subscription_tbl_id))
            connection.commit()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the data into im360_item table ")
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Data inserted successfully into im360_item table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from im360_input_order table
        item_list = None
        try:
            self.logger.info("Fetching the records from IM360_item table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_item table ")
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from IM360_item table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from IM360_item table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_DELETE_BY_ID_SQL_QUERY, row_id)
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_item table ")
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from IM360_item table with row ID : " + row_id)

    def show_records_by_sub_details(self, sql_util, sub_id, order_num):
        item_list = None
        try:
            self.logger.info("Fetching the subscription data from IM360_item table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_num))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription data from IM360_item table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from IM360_item table")
        return item_list

    def update_row(self, sql_util, test_case_id, subs_id):
        try:
            self.logger.info("Updating the subscription ID in im360_item table by test case ID " + test_case_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_UPDATE_BY_TEST_CASE_ID_SQL_QUERY,
                           [subs_id, test_case_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to update im360_item table "
                              )
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("IM360_subscription table updated successfully ")

    def show_currency_by_sub_details(self, sql_util, sub_id, order_num):
        item_list = None
        try:
            self.logger.info("Fetching the subscription data from im360_subscription table")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_CURRENCY_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_num))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch currency from im360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from im360_subscription table")
        return item_list

    def show_item_type_by_sub_details(self, sql_util, sub_id, order_num):
        item_list = None
        try:
            self.logger.info("Fetching the subscription data from im360_subscription table")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_TYPE_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_num))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch currency from im360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from im360_subscription table")
        return item_list

    def insert_duplicate_row(self, sql_util, im360_subs_tbl_old_id, im360_subs_tbl_new_id):
        # This method is copy the rows into IM360_item table while new row added for IM360_subscription table but no
        # impact on this table
        try:
            self.logger.info("Inserting the duplicate row into IM360_item table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting the data into im360_item table")
            cursor.execute(SqlConstant.IM360_ITEM_INSERT_DUPLICATE_ROW_SQL_QUERY,
                           [im360_subs_tbl_new_id, im360_subs_tbl_old_id])
            connection.commit()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the duplicate row into im360_item table ")
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Duplicate row inserted successfully into im360_item table")

    def show_sku_by_sub_details(self, sql_util, sub_id):
        item_list = None
        return_list = []
        # breakpoint()
        try:
            self.logger.info("Fetching the subscription data from IM360_item table")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEMS_FILTER_BY_SUB_ID, [sub_id])
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription data from IM360_item table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from im360_item table")
        for item in item_list:
            return_list.append(item[0])
        return return_list

    def show_usage_sku_by_tc_details(self, sql_util, tc_id):
        item_list = None
        return_list = []
        try:
            self.logger.info("Fetching the usage sku data from im360_input_item table")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_USAGE_MPN_BY_TC_ID, [tc_id])
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch subscription data from im360_input_item table ")
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from im360_input_item table")
        for item in item_list:
            return_list.append(item[0])
            return return_list

    def get_im360_item_data_for_added_sku(self, sql_util, sub_id):
        try:
            self.logger.info("Fetching the data for added sku data from IM360_item table")
            self.connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEMS_FOR_ADDED_SKU, [sub_id])
            item_list = cursor.fetchall()
            self.logger.info("Added sku data fetched successfully from im360_item table")
            return item_list
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch added sku data from IM360_item table ")
        finally:
            sql_util.close_connection(self.connection)

    def get_second_max_id_item_by_test_case_id(self, sql_util, test_case_id):
        # This method is responsible to get second max ID from im360_item table
        last_id = None
        try:
            self.logger.info("Fetching second max ID from IM360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ITEM_SECOND_MAX_ID_SELECT_QUERY, [test_case_id, test_case_id])
            im360_subscription_order = cursor.fetchall()
            for record in im360_subscription_order:
                last_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch second max ID from IM360_item table "
                              )
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Second max ID fetched successfully from IM360_item table")
        return last_id
