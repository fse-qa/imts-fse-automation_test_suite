import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class WebAppSubDataDao(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from subscriptionfeed table
        item_list = None
        try:
            self.logger.info("Fetching all records from Tibco Web App subscriptionfeed table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.WEB_APP_SUB_DATA_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from subscriptionfeed table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from Tibco Web App subscriptionfeed table")
        return item_list

    def show_records_by_sub_details(self, sql_util, sub_id, order_id, req):
        # This method is responsible to get all the records from subscriptionfeed table
        item_list = None
        try:
            self.logger.info(f"Fetching the records from sap_subscription_data table for Subscription: {sub_id} with"
                             f"Order_Number: {order_id}")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            if req == 'req_list1':
                cursor.execute(SqlConstant.WEB_APP_SUB_DATA_LIST1_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            elif req == 'req_list2':
                cursor.execute(SqlConstant.WEB_APP_SUB_DATA_LIST2_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            elif req == 'req_list3':
                cursor.execute(SqlConstant.WEB_APP_SUB_DATA_LIST3_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from "
                              "imts_rr_automation_web_app_subscriptionfeedrequestpreamble table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from Tibco Web App "
                         "imts_rr_automation_web_app_subscriptionfeedrequestpreamble table")
        return item_list

    def show_sap_response_by_sub_details(self, sql_util, sub_id, order_id):
        response_list = None
        try:
            self.logger.info(f"Fetching sap response from imts_rr_automation_web_app_subscriptionfeedresponsesap table"
                             f" for Subscription: {sub_id} with Order_Number: {order_id}")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.WEB_APP_FETCH_SAP_RESPONSE_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            response_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from "
                              "imts_rr_automation_web_app_subscriptionfeedresponsesap table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from Tibco Web App "
                         "imts_rr_automation_web_app_subscriptionfeedresponsesap table")
        for status, message in response_list:
            return status, message

    def show_order_reference_number(self, sql_util, sub_id, date):
        order_list = None
        try:
            self.logger.info(f"Fetching Order Number for subscription {sub_id} created on {date}")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.WEB_APP_FETCH_ORDER_NUMBER_SQL_QUERY, (sub_id, date))
            order_list = cursor.fetchall()
            self.logger.info("Latest Order Number fetched successfully from Tibco Web App "
                             "imts_rr_automation_web_app_bitfeedbit table")
            return order_list
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from "
                              "imts_rr_automation_web_app_bitfeedbit table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)
