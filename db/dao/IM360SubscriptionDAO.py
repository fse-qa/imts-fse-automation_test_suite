import sqlite3
from sqlite3 import Error

from Tests.im360.test_base_im360 import BaseTest
from db.util.SqlConstant import SqlConstant


class IM360SubscriptionDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, im360_subscription_list):
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            for im360_subscription_order in im360_subscription_list:
                cursor.execute(SqlConstant.IM360_SUBSCRIPTION_INSERT_SQL_QUERY,
                               (im360_subscription_order.test_case_id, im360_subscription_order.quote_name,
                                im360_subscription_order.subscription_id,
                                im360_subscription_order.status, im360_subscription_order.bill_to,
                                im360_subscription_order.subscription_period,
                                im360_subscription_order.subscription_period_unit,
                                im360_subscription_order.billing_period, im360_subscription_order.billing_period_unit,
                                im360_subscription_order.reseller_bcn, im360_subscription_order.reseller_po,
                                im360_subscription_order.agreement_id, im360_subscription_order.order_number,
                                im360_subscription_order.payment_term, im360_subscription_order.payment_type,
                                im360_subscription_order.currency
                                ))
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_subscription table "
                              + e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Data inserted successfully into IM360_subscription table")
        return row_id

    def get_id_by_row_id(self, sql_util, row_id):
        # This method is responsible to get all the records from im360_subscription table
        last_id = None
        try:
            self.logger.info("Fetching max row ID from IM360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_MAX_ID_SELECT_QUERY, [str(row_id)])
            im360_subscription_order = cursor.fetchall()
            for record in im360_subscription_order:
                last_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch max row ID from IM360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Max row ID fetched successfully from IM360_subscription table")
        return last_id

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from im360_subscription table
        subs_list = None
        try:
            self.logger.info("Fetching the records from IM360_subscription table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SELECT_ALL_SQL_QUERY)
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from IM360_subscription table")
            return subs_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from IM360_subscription table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_DELETE_BY_ID_SQL_QUERY, [row_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from IM360_subscription table with row ID : " + row_id)

    def show_records_by_sub_id(self, sql_util, sub_id, order_number):
        # This method is responsible to get all the records from im360_subscription table
        subs_list = None
        try:
            self.logger.info("Fetching the records from IM360_subscription table by sub_id")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SELECT_BY_SUB_ID_SQL_QUERY, [sub_id, order_number])
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription details from"
                              " IM360_subscription table " + e + " by sub_id")
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from IM360_subscription table by sub_id")
            return subs_list

    def get_vendor_sub_id(self, sql_util, sub_id):
        # This method is responsible to get all the records from im360_subscription_parameters table
        ven_sub_id = None
        try:
            self.logger.info("Fetching the records from im360_subscription_parameters table by sub_id")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_FETCH_VENDOR_SUB_ID_SQL_QUERY, [sub_id])
            subs_list = cursor.fetchall()
            for record in subs_list:
                ven_sub_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription details from"
                              " im360_subscription_parameters table " + e + " by sub_id")
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from im360_subscription_parameters table by sub_id")
            return ven_sub_id

    def show_sub_details_for_status_check(self, sql_util, tc_id):
        # This method is responsible to get all the records from im360_subscription table
        subs_list = []
        try:
            # breakpoint()
            self.logger.info("Fetching distinct values of  IM360_subscription table by sub_id")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SELECT_STATUS_CHECK_SQL_QUERY, [tc_id])
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription details from"
                              " IM360_subscription table " + e + " by sub_id")
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from IM360_subscription table by sub_id")
            return subs_list

    def get_all_sub_by_test_case_id(self, sql_util, test_case_id):
        subs_list_json = None
        try:
            self.logger.info("Fetching all subID from  IM360_subscription table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SELECT_ALL_SUB_BY_TEST_CASE_ID_SQL_QUERY,
                           [test_case_id, test_case_id])
            subs_list = cursor.fetchall()
            subs_list_json = [dict(ix) for ix in subs_list]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch all subID from IM360_subscription table "
                              + e)
        finally:
            sql_util.close_connection(connection)
            self.logger.info("All subID fetched successfully from IM360_subscription table")
            return subs_list_json

    def update_subs_and_order_id(self, sql_util, test_case_id, subs_id, erp_order):
        try:
            self.logger.info("Updating the subscription ID, ERP Order_No in IM360_subscription table by test case ID "
                             + test_case_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_UPDATE_BY_TEST_CASE_ID_SQL_QUERY,
                           [subs_id, erp_order, test_case_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to update IM360_subscription table "
                              + e)
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("IM360_subscription table updated successfully ")

    def get_id_by_test_case_id(self, sql_util, test_case_id):

        tbl_id = None

        try:
            self.logger.info("Fetching the ID from im360_subscription table by test_case_id")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_ID_SELECT_BY_TEST_CASE_ID_SQL_QUERY, [test_case_id])
            subs_list = cursor.fetchall()
            for record in subs_list:
                tbl_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch id from im360_subscription "
                              " im360_subscription table " + e + " by test_case_id")
        finally:
            sql_util.close_connection(connection)

        self.logger.info("ID fetched successfully from im360_subscription table by test_case_id")
        return tbl_id

    def update_status_by_test_case_id(self, sql_util, test_case_id, status):
        try:
            self.logger.info("Updating the IM360 subscription status in IM360_subscription table by test case ID "
                             + test_case_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_UPDATE_STATUS_BY_TEST_CASE_ID_SQL_QUERY,
                           [status, test_case_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to update status on IM360_subscription table "
                              + e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Status updated in IM360_subscription table successfully ")

    def get_quote_name_by_test_case_id(self, sql_util, test_case_id):
        # This method is responsible to get quote_name by test_case_id
        quote_name = None
        try:
            self.logger.info("Fetching the Quote name from im360_subscription table by test_case_id")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_QUOTE_NAME_SELECT_BY_TEST_CASE_ID_SQL_QUERY, [test_case_id])
            subs_list = cursor.fetchall()
            for record in subs_list:
                quote_name = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch Quote Name from im360_subscription "
                              " im360_subscription table " + e + " by test_case_id")
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Quote Name fetched successfully from im360_subscription table by test_case_id")
        return quote_name

    def get_sub_id_by_test_case_id(self, sql_util, test_case_id):
        # This method is responsible to get sub_id by test_case_id
        sub_id = None
        try:
            self.logger.info("Fetching the subscription ID from im360_subscription table by test_case_id")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUB_ID_SELECT_BY_TEST_CASE_ID_SQL_QUERY, [test_case_id])
            subs_list = cursor.fetchall()
            for record in subs_list:
                sub_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription ID from im360_subscription "
                              " im360_subscription table " + e + " by test_case_id")
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("subscription ID fetched successfully from im360_subscription table by test_case_id")
        return sub_id

    def get_sub_id(self, sql_util, tc_id):
        # This method is responsible to get all the records from im360_subscription_parameters table
        subs_list = None
        try:
            self.logger.info("Fetching subscription id from im360_subscription table against test_case_id")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_FETCH_SUB_ID_SQL_QUERY, [tc_id])
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch subscription id from"
                              " im360_subscription table " + e + " by test_case_id")
        finally:
            sql_util.close_connection(connection)

            self.logger.info("Subscription ID fetched successfully!")
            if subs_list:
                for sub_id in subs_list[-1]:
                    return sub_id

    def get_order_id(self, sql_util, sub_id):
        # This method is responsible to get all the records from im360_subscription_parameters table
        order_id = None
        try:
            self.logger.info(f"Fetching order id for subscription {sub_id} from im360_subscription table.")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_FETCH_ORDER_ID_SQL_QUERY, [sub_id])
            orders_list = cursor.fetchall()
            for record in orders_list:
                order_id = record[-1]
        except Error as e:
            self.logger.error(f"Exception occurred while trying to fetch latest order id against subscription {sub_id}"
                              f" from im360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

            self.logger.info(
                f"Order IDs for subscription id {sub_id} fetched successfully from im360_subscription table")
            return order_id

    def insert_records_with_updated_term(self, sql_util, test_case_id, payment_term, payment_type):
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription table with updated payment term and type")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_INSERT_WITH_UPDATED_TERM_SQL_QUERY, [payment_term,
                                                                                               payment_type,
                                                                                               test_case_id])
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_subscription table "
                              "with updated payment term and type " + e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Data inserted successfully into IM360_subscription table with updated payment term and type")
        return row_id

    def insert_records_with_updated_status(self, sql_util, test_case_id, status):
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription table with updated status")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_INSERT_WITH_UPDATED_STATUS_SQL_QUERY, [status, test_case_id])
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_subscription table "
                              "with updated status " + e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Data inserted successfully into IM360_subscription table with updated status")
        return row_id

    def get_second_max_id_by_test_case_id(self, sql_util, test_case_id):
        # This method is responsible to get second max ID from im360_subscription table
        last_id = None
        try:
            self.logger.info("Fetching second max ID from IM360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SECOND_MAX_ID_SELECT_QUERY, [test_case_id, test_case_id])
            im360_subscription_order = cursor.fetchall()
            for record in im360_subscription_order:
                last_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch second max ID from IM360_subscription table "
                              + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Second max ID fetched successfully from IM360_subscription table")
        return last_id

    def get_pending_subs_by_tc(self, sql_util, test_case_id):

        # This method is responsible to get un approved the records from im360_subscription table
        subs_list = []
        try:
            self.logger.info("Fetching the records from IM360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SELECT_PENDING_SUBS_BY_TC_SQL_QUERY, [test_case_id])
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from IM360_subscription table")
            return subs_list

    def get_latest_mpn_list_by_sub_id(self, sql_util, sub_id):
        # This method fetches latest mpn list in use for a subscription
        mpn_list = None
        try:
            self.logger.info("Fetching the mpn set from IM360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_SELECT_MPN_SET_BY_SUB_ID_SQL_QUERY, [sub_id])
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_subscription table " + e)
        finally:
            sql_util.close_connection(connection)

    def insert_duplicate_row(self, sql_util, test_case_id):
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription table with duplicate row")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_INSERT_DUPLICATE_ROW_SQL_QUERY, [test_case_id])
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert duplicate row into IM360_subscription table "
                              + e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Duplicate row inserted successfully into IM360_subscription table")
        return row_id

    def update_status_by_test_case_id_and_tbl_id(self, sql_util, test_case_id, status):
        try:
            self.logger.info("Updating the IM360 subscription status in IM360_subscription table by test case ID "
                             "%s and max ID ", test_case_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_UPDATE_STATUS_BY_TEST_CASE_ID_AND_MAX_ID_SQL_QUERY,
                           [status, test_case_id, test_case_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to update status on IM360_subscription table by "
                              "test case ID and max ID %s", e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Status updated in IM360_subscription table successfully ")

    def update_order_id(self, sql_util, test_case_id, im360_subs_max_id, erp_order):
        try:
            self.logger.info("Updating the ERP Order_No in IM360_subscription table by test case ID "
                             + test_case_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_ERP_ORDER_NUMBER_UPDATE_BY_TEST_CASE_ID_SQL_QUERY,
                           [im360_subs_max_id, test_case_id, erp_order])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to update IM360_subscription table "
                              + e)
            raise e
        finally:
            sql_util.close_connection(connection)
            self.logger.info("IM360_subscription table updated successfully ")

    def get_max_id_by_test_case_id(self, sql_util, test_case_id):
        # This method is responsible to get max ID from im360_subscription table
        last_id = None
        try:
            self.logger.info("Fetching max ID from IM360_subscription table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUB_MAX_ID_SELECT_QUERY, [test_case_id])
            im360_subscription_order = cursor.fetchall()
            for record in im360_subscription_order:
                last_id = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch second max ID from IM360_subscription table "
                              + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Second max ID fetched successfully from IM360_subscription table")
        return last_id

    def update_records_with_updated_subscription_period(self, sql_util, test_case_id, subscription_period):
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription table with updated subscription period ")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_UPDATE_WITH_UPDATED_SUBSCRIPTION_PERIOD, [subscription_period,
                                                                                                    test_case_id,
                                                                                                    test_case_id])
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_subscription table "
                              "with updated subscription period " + e)
            raise e
        finally:
            sql_util.close_connection(connection)
        self.logger.info("Data inserted successfully into IM360_subscription table with updated contract term")
        return row_id

    def update_the_billing_frequency(self, sql_util, test_case_id, change_billing_frequency):
        row_id = None
        try:
            self.logger.info("Updating the data into im360_subscription table with updated billing frequency ")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_UPDATE_WITH_UPDATED_BILLING_FREQUENCY, [change_billing_frequency,
                                                                                                    test_case_id,
                                                                                                    test_case_id])
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to update the input data into IM360_subscription table "
                              "with updated billing frequency " + e)
            raise e
        finally:
            sql_util.close_connection(connection)
        self.logger.info("Data updated successfully into IM360_subscription table with updated billing frequency")
        return row_id
