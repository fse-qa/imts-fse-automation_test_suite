import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class SAPSubDataDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, sap_sub_data_list):
        # This method is responsible to insert multiple records into cbc_status table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting subscription data with sap subscription data into sap_subscription_data table")
            for item in sap_sub_data_list:
                cursor.execute(SqlConstant.SAP_SUB_DATA_INSERT_SQL_QUERY,
                               [
                                   item.test_case_id, item.provider_contract, item.name,
                                   item.contract_start,
                                   item.contract_end, item.key_in_external_system, item.company_code,
                                   item.im_vendor_sub_id,
                                   item.bill_to, item.payment_terms, item.subscription_period,
                                   item.subscrip_period_unit,
                                   item.billing_period, item.billing_period_unit, item.status, item.flooring_bp,
                                   item.plant])
            self.connection.commit()
        except Exception as e:
            self.logger.error("Exception occurred while trying to insert data into sap_subscription_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into sap_subscription_data table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from sap_subscription_data table
        try:
            self.logger.info("Fetching the records from sap_subscription_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_SUB_DATA_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to fetch records from sap_subscription_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_subscription_data table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from sap_subscription_data table with row ID : " + row_id)
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_SUB_DATA_DELETE_BY_ID_SQL_QUERY, row_id)
            self.connection.commit()
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to delete records from sap_subscription_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Records deleted successfully from sap_subscription_data table with row ID : " + row_id)

    def show_records_by_sub_id(self, sql_util, sub_id):
        # This method is responsible to get all the records from sap_subscription_data table
        try:
            self.logger.info("Fetching the records from sap_subscription_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_SUB_DATA_BY_SUB_ID_SQL_QUERY, [sub_id])
            item_list = cursor.fetchall()
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to fetch records from sap_subscription_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_subscription_data table")
        return item_list

    def show_sub_status_by_sub_id(self, sql_util, sub_id):
        # This method is responsible to get all the records from sap_subscription_data table
        try:
            self.logger.info("Fetching subscription status from sap_subscription_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_SUB_STATUS_BY_SUB_ID_SQL_QUERY, [sub_id])
            status_list = cursor.fetchall()
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to fetch records from sap_subscription_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_subscription_data table")
        for item in status_list:
            return item['status']

    def show_contract_term_by_sub_id(self, sql_util, sub_id):
        """ This method is responsible to get contract start date, contract end date and subscription period from
        sap_subscription_data table """
        item_list = []
        try:
            self.logger.info("Fetching contract information from sap_subscription_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_CONTRACT_TERM_BY_PROVIDER_CONTRACT_SQL_QUERY, [sub_id])
            item_list = cursor.fetchall()
            self.logger.info("Contract term fetched successfully from sap_subscription_data table")
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch records from sap_subscription_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)
        return item_list

    def get_start_and_end_date_for_given_sku(self, sql_util, sub_id):
        # This method is responsible for fetching amount and cost from sap_mpn_data table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BILLING_PERIOD_START_AND_END_DATE_SQL_QUERY, [sub_id])
            status_list = cursor.fetchall()
            return status_list
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to fetch amount and cost from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)
