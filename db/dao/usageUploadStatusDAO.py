import sqlite3

from db.util.SqlConstant import SqlConstant
from sqlite3 import Error
from Tests.test_base import BaseTest


class UsageUploadStatusDAO(BaseTest):
    sql_query = SqlConstant()

    def __init__(self):
        self.connection = None
        self.cursor = None

    def select_im360_subscription_by_testcase(self, sql_util, test_case_id):
        subscription_id_by_testcase = None
        try:
            self.logger.info("Fetching the subscription data from 'im360_subscription' table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(self.sql_query.IM360_SUBSCRIPTION_SELECT_SQL_QUERY_AS_PER_TESTCASE, [test_case_id])
            subscription_ids_in_list = cursor.fetchall()
            for item in subscription_ids_in_list[0]:
                subscription_id_by_testcase = item
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch subscription data from 'im360_subscription' table " + e)
        finally:
            sql_util.close_connection(self.connection)
        self.logger.info("Subscription id fetched successfully from IM360_item table")
        return subscription_id_by_testcase

    def select_input_usage_mpn_data(self, sql_util, test_case_id):
        usage_mpn_list_by_testcase = []

        try:
            self.logger.info("Fetching the input usage mpn data from 'IM360_input_item' table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(self.sql_query.INPUT_USAGE_MPN_SELECT_SQL_BY_TESTCASE, [test_case_id])
            usage_mpn_tuple = cursor.fetchall()
            for usage_mpn_list in usage_mpn_tuple:
                usage_mpn_by_testcase = dict(usage_mpn_list)
                usage_mpn_by_testcase = usage_mpn_by_testcase.copy()
                usage_mpn_list_by_testcase.append(usage_mpn_by_testcase)
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch subscription data from 'im360_subscription' table " + e)
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("usage mpns are fetched successfully from IM360_item table")
        return usage_mpn_list_by_testcase

    def select_usage_date_for_basic_e2e(self, sql_util):

        usage_dates = {}
        try:
            self.logger.info("Fetching the usage dates from 'commerce_date_change_track' table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(self.sql_query.COMMERCE_LAST_MAX_DATE_SELECT_SQL_QUERY_FOR_BASIC_FLOW)
            usage_start_date = cursor.fetchall()
            for item in usage_start_date[0]:
                usage_dates["usage_start_date"] = item
            cursor.execute(self.sql_query.COMMERCE_MAX_DATE_SELECT_QUERY)
            usage_end_date = cursor.fetchall()
            for item in usage_end_date[0]:
                usage_dates["usage_end_date"] = item

        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch cb dates from 'commerce_date_change_track' table " + e)
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Usage start and end date are fetched successfully from IM360_item table")
        return usage_dates

    def select_usage_date_for_sub_cancel_flow(self, sql_util):

        usage_dates = {}

        try:
            self.logger.info("Fetching the usage dates from 'commerce_date_change_track' table for cancel")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(self.sql_query.COMMERCE_LAST_MAX_DATE_SELECT_SQL_QUERY_FOR_BASIC_FLOW)
            usage_start_date = cursor.fetchall()
            for item in usage_start_date[0]:
                usage_dates["usage_start_date"] = item
            cursor.execute(self.sql_query.COMMERCE_INTERMITTENT_MAX_DATE_SELECT_QUERY)
            usage_end_date = cursor.fetchall()
            for item in usage_end_date[0]:
                usage_dates["usage_end_date"] = item

        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch cb dates from 'commerce_date_change_track' table " + e)
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Usage start and end date are fetched successfully from 'commerce_date_change_track' table")
        return usage_dates

    def insert_usage_details_as_per_testcase(self, sql_util, usage_list):
        row_id = None
        try:
            self.logger.info("Inserting the usage details in 'connect_usage_upload_status' table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(self.sql_query.USAGE_STATUS_DETAILS_INSERT_SQL_QUERY, (tuple(usage_list)))
            self.connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert usage data in 'connect_usage_upload_status' table " + e)
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Usage details data inserted successfully into 'connect_usage_upload_status' table")
        return row_id



