import sqlite3
from sqlite3 import Error

from Tests.im360.test_base_im360 import BaseTest
from db.util.SqlConstant import SqlConstant


class IM360InputOrderDAO(BaseTest):

    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, im360_input_order_list):
        # This method is responsible to insert multiple records into IM360_input_order table
        row_id = None
        try:
            self.logger.info("Inserting the input data into IM360_input_order table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            for im360_input_order in im360_input_order_list:
                cursor.execute(SqlConstant.IM360_INPUT_ORDER_INSERT_SQL_QUERY,
                               (im360_input_order.test_case_id, im360_input_order.test_case_name,
                                im360_input_order.quote_name,
                                im360_input_order.reseller_bcn, im360_input_order.contact,
                                im360_input_order.payment_term, im360_input_order.bill_to,
                                im360_input_order.deal_id, im360_input_order.billing_period,
                                im360_input_order.subscription_period_unit, im360_input_order.subscription_period,
                                im360_input_order.service_plan_id, im360_input_order.reseller_po,
                                im360_input_order.primary_vendor, im360_input_order.company_id,
                                im360_input_order.company_name, im360_input_order.contact_phone,
                                im360_input_order.contact_name, im360_input_order.contact_email,
                                im360_input_order.address, im360_input_order.city,
                                im360_input_order.postal_code, im360_input_order.state,
                                im360_input_order.web_order_id, im360_input_order.requested_ship_start_date,
                                im360_input_order.agreement_id, im360_input_order.provisioning_contact_name,
                                im360_input_order.provisioning_contact_email,
                                im360_input_order.provisioning_contact_phone,
                                im360_input_order.vendor_portal_submission,
                                im360_input_order.saved_credit_card_name, im360_input_order.cc_first_name,
                                im360_input_order.cc_last_name, im360_input_order.cc_email,
                                im360_input_order.cc_card_type, im360_input_order.cc_card_number,
                                im360_input_order.cc_expiration_month, im360_input_order.cc_expiration_year,
                                im360_input_order.cc_cvn, im360_input_order.reseller_currency,
                                im360_input_order.vendor_currency, im360_input_order.service_name,
                                im360_input_order.marketplace_name, im360_input_order.change_service_plan_id,
                                im360_input_order.change_billing_period, im360_input_order.change_subscription_period,
                                im360_input_order.change_payment_term, im360_input_order.change_deal_id,
                                im360_input_order.test_type))
                connection.commit()
                row_id = cursor.lastrowid
        except Error as e:
            self.logger.error("Exception occurred while trying to insert the input data into IM360_input_order table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("data inserted successfully into IM360_input_order table")
        return row_id

    def get_id_by_row_id(self, sql_util, row_id):
        last_id = None
        try:
            self.logger.info("Fetching ID from IM360_input_order table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_MAX_ID_SELECT_QUERY, [str(row_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                last_id = record[0]
            self.logger.info("ID %s fetched successfully from IM360_input_order table" % str(last_id))
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch ID from IM360_input_order table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        return last_id

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from IM360_input_order table
        subs_list = None
        try:
            self.logger.info("Fetching the records from IM360_input_order table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_SELECT_ALL_SQL_QUERY)
            subs_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from IM360_input_order table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("All records fetched successfully from IM360_input_order table")
            return subs_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from IM360_input_order table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_DELETE_BY_ID_SQL_QUERY, [row_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from IM360_input_order table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from IM360_input_order table with row ID : " + row_id)

    def get_quote_name_by_test_case_id(self, sql_util, test_case_id):
        quote_name = None
        try:
            self.logger.info("Fetching the quote name from IM360_input_order table by test case ID")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_QUOTE_NAME_BY_TEST_CASE_ID_SQL_QUERY, [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                quote_name = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch quote name from IM360_input_order table by test "
                "case ID"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("Quote name %s fetched successfully from IM360_input_order table by test case ID ",
                             quote_name)
            return quote_name

    def get_test_case_name_by_test_case_id(self, sql_util, test_case_id):
        test_case_name = None
        try:
            self.logger.info("Fetching the test case name from IM360_input_order table by test case ID %s",
                             test_case_id)
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_TEST_CASE_NAME_BY_TEST_CASE_ID_SQL_QUERY,
                           [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                test_case_name = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch test case name from IM360_input_order table by test case ID"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("test case name %s fetched successfully from IM360_input_order table by test case ID ",
                             test_case_name)
        return test_case_name

    def get_test_case_id(self, sql_util):
        tc_id_list = []
        test_case_ids = None
        try:
            self.logger.info("Fetching test case id from IM360_input_order table")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.TEST_CASE_ID_SQL_QUERY)
            test_case_ids = cursor.fetchall()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch test case id from IM360_input_order table"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info(f"test case id fetched successfully from IM360_input_order table : {test_case_ids}")

        for item in test_case_ids:
            for value in item:
                tc_id_list.append(value)
        return tc_id_list

    def show_order_test_case_record(self, sql_util, test_case_id):
        # This method is responsible to get given test case records from IM360_input_order table
        order_test_case_details_json = None
        try:
            self.logger.info("Fetching the test record records from IM360_input_order table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_GET_ORDER_TEST_CASE_RECORD_SQL_QUERY, [str(test_case_id)])
            order_test_case_details = cursor.fetchall()
            order_test_case_details_json = [dict(ix) for ix in order_test_case_details][0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch test case record from IM360_input_order table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)
            self.logger.info("Test case records fetched successfully from IM360_input_order table")
            return order_test_case_details_json

    def get_service_name_by_test_case_id(self, sql_util, test_case_id):
        service_name = None
        try:
            self.logger.info("Fetching the service_name  from IM360_input_order table by test case ID %s",
                             test_case_id)
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_SERVICE_NAME_BY_TEST_CASE_ID_SQL_QUERY,
                           [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                service_name = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch service_name  from IM360_input_order table by test case ID"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("service_name  %s fetched successfully from IM360_input_order table by test case ID ",
                             service_name)
        return service_name

    def get_cc_number_by_tc_id(self, sql_util, tc_id):
        cc_num = None
        try:
            self.logger.info(f"Fetching Credit Card Number for {tc_id}.")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_CC_NUM_BY_TEST_CASE_ID_SQL_QUERY, [tc_id])
            cc_num_list = cursor.fetchall()
            for record in cc_num_list:
                cc_num = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch cc_number from IM360_input_order table."
                + str(e))
            raise e
        else:
            self.logger.info("CC Number fetched successfully!")
        finally:
            sql_util.close_connection(self.connection)

        return cc_num

    def get_reseller_currency_by_test_case_id(self, sql_util, test_case_id):
        reseller_currency = None
        try:
            self.logger.info("Fetching the reseller_currency  from IM360_input_order table by test case ID %s",
                             test_case_id)
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_RESELLER_CURRENCY_BY_TEST_CASE_ID_SQL_QUERY,
                           [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                reseller_currency = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch reseller_currency  from IM360_input_order "
                              "table by test case ID "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("reseller_currency  %s fetched successfully from IM360_input_order table by test case ID ",
                             reseller_currency)
        return reseller_currency

    def get_vendor_currency_by_test_case_id(self, sql_util, test_case_id):
        vendor_currency = None
        try:
            self.logger.info("Fetching the vendor_currency  from IM360_input_order table by test case ID %s",
                             test_case_id)
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_VENDOR_CURRENCY_BY_TEST_CASE_ID_SQL_QUERY,
                           [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                vendor_currency = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch vendor_currency from IM360_input_order table by test case ID"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("service_name  %s fetched successfully from IM360_input_order table by test case ID ",
                             vendor_currency)
        return vendor_currency

    def get_marketplace_name_by_test_case_id(self, sql_util, test_case_id):
        marketplace_name = None
        try:
            self.logger.info("Fetching the marketplace_name  from IM360_input_order table by test case ID %s",
                             test_case_id)
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_MARKETPLACE_NAME_BY_TEST_CASE_ID_SQL_QUERY,
                           [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                marketplace_name = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch marketplace_name from IM360_input_order table by test case ID"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("service_name  %s fetched successfully from IM360_input_order table by test case ID ",
                             marketplace_name)
        return marketplace_name

    def get_distinct_marketplaces(self, sql_util):
        marketplaces = []
        try:
            self.logger.info("Fetching distinct marketplaces from IM360_input_order table ")

            connection = sql_util.get_connection()
            connection.row_factory = lambda cursor, row: row[0]
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_DISTINCT_MARKETPLACES_SQL_QUERY)
            marketplaces = cursor.fetchall()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch distinct marketplace_name from IM360_input_order table "
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("Distinct marketplace_name fetched successfully from IM360_input_order table  ")

        return marketplaces

    def count_marketplaces_testcase(self, sql_util, marketplace, test_case_id):
        count_record = 0
        try:
            self.logger.info("Count testcase and marketplacewise row from IM360_input_order table ")

            connection = sql_util.get_connection()
            connection.row_factory = lambda cursor, row: row[0]
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_COUNT_MARKETPLACE_TESTCASEWISE_ROW_SQL_QUERY,
                           [str(test_case_id), marketplace.lower()])
            record_list = cursor.fetchall()
            for record in record_list:
                count_record = record
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to count testcase and marketplacewise row from IM360_input_order "
                "table " + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("testcase and marketplacewise row counted successfully from IM360_input_order table  ")

        return count_record

    def get_test_type_by_test_case_id(self, sql_util, test_case_id):
        test_type = None
        try:
            self.logger.info("Fetching the test_type  from IM360_input_order table by test case ID %s",
                             test_case_id)
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_INPUT_ORDER_GET_TEST_TYPE_BY_TEST_CASE_ID_SQL_QUERY,
                           [str(test_case_id)])
            im360_input_order = cursor.fetchall()
            for record in im360_input_order:
                test_type = record[0]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch test_type from IM360_input_order table by test case ID"
                + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

            self.logger.info("test_type  %s fetched successfully from IM360_input_order table by test case ID ",
                             test_type)
        return test_type
