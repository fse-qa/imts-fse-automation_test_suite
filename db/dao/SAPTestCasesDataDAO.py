from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class SAPTestCasesDataDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, tc_id, pytest_name, status):
        # This method is responsible to insert records into sap_test_cases_data table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting records into sap_test_cases_data table")
            cursor.execute(SqlConstant.SAP_TEST_CASES_DATA_INSERT_SQL_QUERY, (tc_id, pytest_name, status))
            self.connection.commit()
        except Exception as e:
            self.logger.error("Exception occurred while trying to insert record into sap_test_cases_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Test Case status inserted successfully into sap_test_cases_data table.")

    def show_status(self, sql_util, tc_id, pytest_name):
        # This method is responsible to get all the records from sap_status_code table
        try:
            self.logger.info("Fetching the records from sap_test_cases_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_TEST_CASES_DATA_SELECT_ALL_SQL_QUERY, (tc_id, pytest_name))
            pytest_status = cursor.fetchall()
        except Exception as e:
            self.logger.error(
                "Exception occurred while trying to fetch records from sap_test_cases_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Test Case status fetched successfully from sap_test_cases_data table")
        for status in pytest_status[0]:
            return status
