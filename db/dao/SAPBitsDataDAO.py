import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class SAPBitsDataDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, sap_bits_data_list):
        # This method is responsible to insert multiple records into sap_bits_data table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting subscription data into sap_bits_data table")
            for bit in sap_bits_data_list:
                cursor.execute(SqlConstant.SAP_BITS_DATA_INSERT_SQL_QUERY,
                               (bit.im_sub_id, bit.order_id, bit.bcn_number, bit.from_date, bit.to_date,
                                bit.bill_item_type, bit.amount, bit.billing_quantity, bit.duration,
                                bit.unit_prc_of_prod, bit.unit_cost_of_prod, bit.sku_part, bit.ven_sub_id,
                                bit.bit_class))
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into sap_bits_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into sap_bits_data table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from sap_bits_data table
        item_list = None
        try:
            self.logger.info("Fetching the records from sap_bits_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BITS_DATA_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_bits_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_bits_data table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from sap_bits_data table with row ID : " + row_id)
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BITS_DATA_DELETE_BY_ID_SQL_QUERY, row_id)
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from sap_bits_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Records deleted successfully from sap_bits_data table with row ID : " + row_id)

    def show_records_by_sub_details(self, sql_util, sub_id, order_id):
        # This method is responsible to get all the records from sap_bits_data table
        item_list = None
        try:
            self.logger.info("Fetching the records from sap_bits_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BITS_DATA_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_bits_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_bits_data table")
        return item_list

    def fetch_usage_amount(self, sql_util, sub_id, bill_item_type):
        # This method is responsible to get the usage amount for a given subscription id from sap_bits_data table
        try:
            self.logger.info("Fetching the usage_amount from sap_bits_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BITS_DATA_FETCH_USAGE_AMOUNT_QUERY, (sub_id, bill_item_type))
            usage_amount = cursor.fetchone()
            self.logger.info("Usage amount fetched successfully from sap_bits_data table")
            return usage_amount[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_bits_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

    def get_billing_quantity_by_sku_part(self, sql_util, sub_id, sku_part, bill_item_type, number_of_records=1):
        # This method is responsible to get billing quantity for given sku part from sap_bits_data table
        item_list = None
        try:
            self.logger.info("Fetching the records from sap_bits_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BITS_DATA_SELECT_BY_SKU_PART_SQL_QUERY,
                           (sub_id, sku_part, bill_item_type, number_of_records))
            item_list = cursor.fetchall()
            self.logger.info("All records fetched successfully from sap_bits_data table")

        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch billing quantity from sap_bits_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)
        return item_list

    def get_billing_period_cost_and_amount(self, sql_util, sub_id, sku_part, bill_item_type, number_of_records=1):
        # This method is responsible to get billing quantity for given sku part from sap_bits_data table
        item_list = None
        try:
            self.logger.info("Fetching the records from sap_bits_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_BITS_DATA_SELECT_BY_SKU_PART_AMOUNT_COST_SQL_QUERY,
                           (sub_id, sku_part, bill_item_type, number_of_records))
            item_list = cursor.fetchall()
            self.logger.info("All records fetched successfully from sap_bits_data table")
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch billing quantity from sap_bits_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)
        return item_list
