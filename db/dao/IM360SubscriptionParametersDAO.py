import sqlite3

from db.util.SqlConstant import SqlConstant
from db.dao import IM360SubscriptionDAO
from Tests.im360.test_base_im360 import BaseTest
from sqlite3 import Error


class IM360SubscriptionParametersDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, im360_subs_param_list):
        try:
            self.logger.info("Fetching the Max row ID from im360_subscription_parameters table for "
                             "inserting data into im360_subscription_parameters table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()

            # cursor.executemany(SqlConstant.im360_subscription_parameters, subs_list)
            self.logger.info("Inserting data into im360_subscription_parameters table")
            for im360_sub_param in im360_subs_param_list:
                cursor.execute(SqlConstant.IM360_SUBSCRIPTION_PARAMETERS_INSERT_SQL_QUERY,
                               (im360_sub_param.test_case_id, im360_sub_param.im360_parameter_name,
                                im360_sub_param.parameter_value, im360_sub_param.im360_subscription_tbl_id))
            connection.commit()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the data into im360_item table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Data inserted successfully into im360_subscription_parameters table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from im360_subscription_parameters table
        item_list = None
        try:
            self.logger.info("Fetching the records from im360_subscription_parameters table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_PARAMETERS_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from im360_subscription_parameters "
                              "table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from im360_subscription_parameters table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from im360_subscription_parameters table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_PARAMETERS_DELETE_BY_ID_SQL_QUERY, row_id)
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from "
                              "im360_subscription_parameters table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info(
            "Records deleted successfully from im360_subscription_parameters table with row ID : " + row_id)

    def get_param_val_by_id_and_param_name(self, sql_util, id, param_name):
        im360_parameter_list = None
        param_val = None
        try:
            self.logger.info("Fetching the records from im360_subscription_parameters table by subID and param name ")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBS_PARAM_VAL_BY_ID_AND_PARAM_NAME_SELECT_QUERY, [id, param_name])
            im360_parameter_list = cursor.fetchall()
            for record in im360_parameter_list:
                param_val = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from im360_subscription_parameters "
                              "table by subID and param name " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Value fetched successfully from im360_subscription_parameters table by subID and param name")
        return param_val

    def insert_duplicate_row(self, sql_util, im360_subs_tbl_old_id, im360_subs_tbl_new_id):
        # This method is copy the rows into IM360_parameter table while new row added for IM360_subscription table but no
        # impact on this table
        try:
            self.logger.info("Inserting the duplicate row into IM360_parameter table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting the data into IM360_parameter table")
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_PARAMETERS_DUPLICATE_INSERT_SQL_QUERY,
                           [im360_subs_tbl_new_id, im360_subs_tbl_old_id])
            connection.commit()
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the duplicate row into IM360_parameter table " + e)
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Duplicate row inserted successfully into IM360_parameter table")

    def get_record_by_param_id(self, sql_util, subs_id, param_id):
        # This method is responsible to get records by param ID from im360_subscription_parameters table
        record = None
        try:
            self.logger.info("Fetching the records from im360_subscription_parameters table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_PARAMETERS_SELECT_BY_ID_SQL_QUERY, [subs_id, param_id])
            record = cursor.fetchone()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from im360_subscription_parameters "
                              "table " + e)
        finally:
            sql_util.close_connection(connection)

        self.logger.info(" a record by param id is fetched successfully from im360_subscription_parameters table")
        return record

    def update_records_with_updated_deal_id(self, sql_util, test_case_id, change_deal_id):
        row_id = None
        try:
            self.logger.info("Inserting the data into im360_subscription parameter table with updated deal id ")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.IM360_SUBSCRIPTION_PARAMETERS_UPDATE_WITH_UPDATED_DEAL_ID, [change_deal_id,
                                                                                                   test_case_id,
                                                                                                   test_case_id])
            connection.commit()
            row_id = cursor.lastrowid
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to insert the input data into IM360_subscription parameter table "
                "with updated deal id " + e)
            raise e
        finally:
            sql_util.close_connection(connection)
        self.logger.info("Data inserted successfully into IM360_subscription parameter table with updated deal id")
        return row_id
