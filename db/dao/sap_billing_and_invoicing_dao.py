from db.model.sap_billing_and_invoicing import SAPBillingAndInvoicing
from db.util.SqlConstant import SqlConstant
from db.util.SqlUtil import SqlUtil
from Tests.test_base import BaseTest
from sqlite3 import Error


class SAPBillingAndInvoicingDAO(BaseTest):
    def __init__(self, db_path=None):
        self.db_path = db_path
        self.connection = None
        self.sql_util = SqlUtil(self.db_path)

    def insert_record(self, record: SAPBillingAndInvoicing):
        try:
            self.connection = self.sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting record into sap_bill_invoice table")
            cursor.execute(SqlConstant.SAP_BILLING_AND_INVOICE_INSERT_SQL_QUERY,
                           (record.tc_id, record.date, record.provider_contract, record.bill_doc_num,
                            record.invoice_doc_num,  record.recon_key, record.billable_bit_class, record.operation,
                            record.status, record.error_reason))
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into sap_bill_invoice table "
                              + str(e))
            raise e
        finally:
            if self.connection:
                self.sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into sap_bill_invoice table")
