import sqlite3

from db.util.SqlConstant import SqlConstant
from Tests.test_base import BaseTest


class SAPStatusDataDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, sap_status_list):
        # This method is responsible to insert multiple records into cbc_status table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting subscription data with cb status data into SAP_status table")
            for row in sap_status_list:
                cursor.execute(SqlConstant.SAP_STATUS_DATA_INSERT_SQL_QUERY,
                               [row.test_case_id, row.subscription_id, row.order_id, row.feed_validation_status,
                                row.minCommitBit_validation_status, row.usage_bit_validation_status,
                                row.billing_status, row.invoiced_status, row.rar_status, row.status_code,
                                row.image_ref, row.error_details, row.date])
            self.connection.commit()
        except Exception as e:
            self.logger.error("Exception occurred while trying to insert data into sap_status_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into SAP_status table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from CBC_status table
        try:
            self.logger.info("Fetching the records from sap_status_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_STATUS_DATA_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_status_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_status_data table")
        return item_list

    def get_all_test_case_ids(self, sql_util):
        try:
            self.logger.info("Fetching the test case IDs from sap_status_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_STATUS_DATA_SELECT_ALL_TEST_CASE_ID_SQL_QUERY)
            test_case_list = cursor.fetchall()
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch test case IDs from sap_status_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("test case IDs fetched successfully from sap_status_data table")
        return test_case_list

    def get_all_records_by_test_case_id(self, sql_util, test_case_id):
        try:
            self.logger.info("Fetching the records from sap_status_data table by test case ID")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_STATUS_DATA_SELECT_ALL_BY_TEST_CASE_ID_SQL_QUERY, [test_case_id])
            data_list = cursor.fetchall()
        except Exception as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_status_data table  by test "
                              "case ID " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_status_data table by test case ID")
        return data_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from sap_status_data table with row ID : " + row_id)
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_STATUS_DATA_DELETE_BY_ID_SQL_QUERY, row_id)
            self.connection.commit()
        except Exception as e:
            self.logger.error("Exception occurred while trying to delete records from sap_status_data table " + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Records deleted successfully from sap_status_data table with row ID : " + row_id)