from sqlite3 import Error

from Tests.test_base import BaseTest
from db.model.sap_rar import SAPRAR
from db.util.SqlConstant import SqlConstant
from db.util.SqlUtil import SqlUtil


class SAPRARDAO(BaseTest):
    def __init__(self, db_path=None):
        self.db_path = db_path
        self.connection = None
        self.sql_util = SqlUtil(self.db_path)

    def insert_record(self, record: SAPRAR):
        try:
            self.connection = self.sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting record into sap_rar table")
            cursor.execute(SqlConstant.SAP_RAR_INSERT_SQL_QUERY,
                           (record.tc_id, record.date, record.provider_contract, record.pob_id,
                            record.g2n_rev, record.g2n_cost, record.pob_price, record.pob_cost, record.pc_tcv,
                            record.pc_commit_cost, record.operation, record.status, record.error_reason))
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into sap_rar table "
                              + str(e))
            raise e
        finally:
            if self.connection:
                self.sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into sap_rar table")
