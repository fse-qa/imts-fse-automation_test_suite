import sqlite3

from db.util.SqlConstant import SqlConstant
from Tests.test_base import BaseTest
from sqlite3 import Error


class WebAppMPNDataDao(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from subscriptionfeed_item table
        item_list = None
        try:
            self.logger.info("Fetching the records from subscriptionfeed_item table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.WEB_APP_MPN_DATA_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from subscriptionfeed_item table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from Tibco Web App subscriptionfeed_item table")
        return item_list

    def show_records_by_sub_details(self, sql_util, sub_id, order_id):
        # This method is responsible to get all the records from subscriptionfeed_item table
        item_list = None
        try:
            self.logger.info("Fetching the records from subscriptionfeed_item table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.WEB_APP_MPN_DATA_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from subscriptionfeed_item table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from Tibco Web App subscriptionfeed_item table")
        return item_list
