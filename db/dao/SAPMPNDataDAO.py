import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class SAPMPNDataDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, sap_mpn_data_list):
        # This method is responsible to insert multiple records into sap_mpn_data table
        try:
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            self.logger.info("Inserting subscription data with cb status data into sap_mpn_data table")
            for item in sap_mpn_data_list:
                cursor.execute(SqlConstant.SAP_MPN_DATA_INSERT_SQL_QUERY,
                               [item.subscription_id, item.total_amount, item.cost, item.quantity,
                                item.payment_card_id, item.cc_number, item.end_of_duration, item.cb_cancellable_flag,
                                item.exchange_rate, item.migration_date, item.original_start_date, item.item_type,
                                item.product_id, item.currency])
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into sap_mpn_data table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("data inserted successfully into sap_mpn_data table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from sap_mpn_data table
        item_list = None
        try:
            self.logger.info("Fetching the records from sap_mpn_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_MPN_DATA_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_mpn_data table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from sap_mpn_data table with row ID : " + row_id)
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_MPN_DATA_DELETE_BY_ID_SQL_QUERY, row_id)
            self.connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Records deleted successfully from sap_mpn_data table with row ID : " + row_id)

    def show_records_by_sub_id(self, sql_util, sub_id):
        # This method is responsible to get all the records from sap_mpn_data table
        item_list = None
        try:
            self.logger.info(f"Fetching records for subscription {sub_id} from sap_mpn_data table")
            self.connection = sql_util.get_connection()
            self.connection.row_factory = sqlite3.Row
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_MPN_DATA_BY_SUB_ID_SQL_QUERY, [sub_id])
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_mpn_data table")
        return item_list

    def show_payment_term_by_sub_id(self, sql_util, sub_id, flag):
        # This method is responsible for fetching payment_terms from sap_subscription_data table
        item_list = None
        try:
            self.logger.info(f"Fetching payment term for subscription {sub_id} from sap_mpn_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            if flag == 'last':
                cursor.execute(SqlConstant.SAP_PAYMENT_TERM_LATEST_BY_SUB_ID_SQL_QUERY, [sub_id, sub_id])
            elif flag == 'latest':
                cursor.execute(SqlConstant.SAP_PAYMENT_TERM_LAST_BY_SUB_ID_SQL_QUERY, [sub_id])
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_subscription_data table")
        for pay_term in item_list[0]:
            return pay_term

    def show_eod_date_by_sub_id(self, sql_util, sub_id):
        # This method is responsible for fetching end of duration date from sap_mpn_data table
        item_list = None
        try:
            self.logger.info(f"Fetching payment term for subscription {sub_id} from sap_mpn_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_EOD_DATE_BY_SUB_ID_SQL_QUERY, [sub_id])
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("All records fetched successfully from sap_mpn_data table")
        for date in item_list[0]:
            return date

    def get_cc_number_by_tc_id(self, sql_util, tc_id):
        # This method is responsible for fetching end of duration date from sap_mpn_data table
        cc_num = None
        try:
            self.logger.info(f"Fetching Credit Card details for {tc_id} from SAP Table.")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_CC_NUM_BY_TC_ID_SQL_QUERY, [tc_id])
            cc_list = cursor.fetchall()
            for record in cc_list[0]:
                cc_num = record
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch cc_number from sap_mpn_data table " + str(e))
        else:
            self.logger.info('Credit Card Number fetched successfully!')
        finally:
            sql_util.close_connection(self.connection)

        return cc_num

    def get_amount_and_cost_for_given_sku(self, sql_util, sub_id, sku_part, quantity):
        # This method is responsible for fetching amount and cost from sap_mpn_data table
        try:
            self.logger.info(
                f"Fetching amount and cost of given sku {sku_part} with given quantity {quantity} from sap_mpn_data table")
            self.connection = sql_util.get_connection()
            cursor = self.connection.cursor()
            cursor.execute(SqlConstant.SAP_AMOUNT_AND_COST_SQL_QUERY, (sub_id, sku_part, f'{quantity:.2f}'))
            item = cursor.fetchone()
            return item[0], item[1]
        except Error as e:
            self.logger.error(
                "Exception occurred while trying to fetch amount and cost from sap_mpn_data table " + str(e))
        finally:
            sql_util.close_connection(self.connection)

        self.logger.info("Amount and cost fetched successfully from sap_mpn_data table")
