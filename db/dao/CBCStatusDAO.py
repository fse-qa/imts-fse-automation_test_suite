import sqlite3
from sqlite3 import Error

from Tests.test_base import BaseTest
from db.util.SqlConstant import SqlConstant


class CBCStatusDAO(BaseTest):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def insert_records(self, sql_util, cbc_status_data_obj):
        # This method is responsible to insert multiple records into cbc_status table
        try:
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Inserting subscription data with cb status data into CBC_status table")
            # for item in cbc_status_list:
            cursor.execute(SqlConstant.CBC_STATUS_INSERT_SQL_QUERY,
                           (cbc_status_data_obj.test_case_id, cbc_status_data_obj.sub_id,
                            cbc_status_data_obj.order_id, cbc_status_data_obj.status))
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into CBC_status table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("data inserted successfully into CBC_status table")

    def show_all_records(self, sql_util):
        # This method is responsible to get all the records from CBC_status table
        item_list = None
        try:
            self.logger.info("Fetching the records from CBC_status table")
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.CBC_STATUS_SELECT_ALL_SQL_QUERY)
            item_list = cursor.fetchall()
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from CBC_status table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("All records fetched successfully from CBC_status table")
        return item_list

    def delete_record(self, sql_util, row_id):
        try:
            self.logger.info("Deleting the records from CBC_status table with row ID : " + row_id)
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            cursor.execute(SqlConstant.CBC_STATUS_DELETE_BY_ID_SQL_QUERY, row_id)
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to delete records from CBC_status table " + str(e))
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Records deleted successfully from CBC_status table with row ID : " + row_id)

    def show_status_by_sub_details(self, sql_util, sub_id, order_id):
        # This method is responsible to get all the records from CBC_status table
        sub_status = None
        try:
            self.logger.info("Fetching the records from CBC_status table")
            connection = sql_util.get_connection()
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.CBC_STATUS_SELECT_BY_SUB_DETAILS_SQL_QUERY, (sub_id, order_id))
            status_list = cursor.fetchall()
            for record in status_list:
                sub_status = record[0]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch records from CBC_status table " + str(e))
        finally:
            sql_util.close_connection(connection)
        self.logger.info("All records fetched successfully from CBC_status table")
        return sub_status

    def show_latest_system_date(self, sql_util):
        try:
            self.logger.info("Fetching latest system date from commerce_date_change_track table")
            connection = sql_util.get_connection()
            # connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute(SqlConstant.CBC_LATEST_DATE_SQL_QUERY)
            dates_list = cursor.fetchall()
            self.logger.info("Latest date successfully fetched from commerce_date_change_track table")
            return dates_list[-1]
        except Error as e:
            self.logger.error("Exception occurred while trying to fetch latest date from commerce_date_change_track "
                              "table " + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

    def update_status(self, sql_util, cbc_status_data_obj):
        # This method is responsible to update status in cbc_status table
        try:
            connection = sql_util.get_connection()
            cursor = connection.cursor()
            self.logger.info("Updating status into CBC_status table with value " + cbc_status_data_obj.status)
            cursor.execute(SqlConstant.CBC_STATUS_UPDATE_SQL_QUERY, [cbc_status_data_obj.status,
                                                                     cbc_status_data_obj.test_case_id,
                                                                     cbc_status_data_obj.sub_id])
            connection.commit()
        except Error as e:
            self.logger.error("Exception occurred while trying to insert data into CBC_status table "
                              + str(e))
            raise e
        finally:
            sql_util.close_connection(connection)

        self.logger.info("Status updated successfully in CBC_status table")
