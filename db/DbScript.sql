--------------------------------------------------------------- Version 1.1 [25th March 2022] ---------------------------------------------------------------

--------------------------------------------------------------- DDL -----------------------------------------------------------------------

--------------------------------------------------------------- IM360 ---------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS im360_input_order(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id String NOT NULL,
test_case_name String NOT NULL,
quote_name String NOT NULL,
reseller_bcn String NOT NULL,
contact DOUBLE NOT NULL,
payment_term String NOT NULL,
bill_to String NOT NULL,
deal_id String,
billing_period String,
subscription_period_unit String,
subscription_period INTEGER,
service_plan_id String,
reseller_po String,
primary_vendor String,
company_id String,
company_name String,
contact_phone String,
contact_name String,
contact_email String,
address String,
city String,
postal_code String,
state String,
web_order_id String,
reqested_ship_start_date String,
agreement_id String,
provisioning_contact_name String,
provisioning_contact_email String,
provisioning_contact_phone String,
vendor_portal_submission String,
saved_credit_card_name String,
cc_first_name String,
cc_last_name String,
cc_email String,
cc_card_type String,
cc_card_number String,
cc_expiration_month String,
cc_expitation_year String,
cc_cvn String
);


CREATE TABLE IF NOT EXISTS im360_input_item(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id String NOT NULL,
item_mpn String NOT NULL,
vendor_sku String NOT NULL,
item_quantity DOUBLE NOT NULL,
item_type String,
unit_of_measure String,
usage_amount DOUBLE,
im360_input_order_tbl_id INTEGER NOT NULL,
FOREIGN KEY(im360_input_order_tbl_id) REFERENCES im360_input_order(id)
);



CREATE TABLE IF NOT EXISTS im360_subscription(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id String,
quote_name String NOT NULL,
subscription_id String,
status String,
bill_to String,
subscription_period INTEGER,
subscription_period_unit String,
billing_period INTEGER,
billing_period_unit String,
reseller_bcn String,
reseller_po String,
agreement_id String,
order_number String,
payment_term String,
payment_type String,
currency String
);



CREATE TABLE IF NOT EXISTS im360_subscription_parameters(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id String NOT NULL,
im360_parameter_name String,
parameter_value String,
im360_subscription_tbl_id INTEGER Not Null,
FOREIGN KEY(im360_subscription_tbl_id) REFERENCES im360_subscription(id)
);



CREATE TABLE IF NOT EXISTS im360_item(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id String NOT NULL,
subscription_id String,
item_quantity DOUBLE,
reseller_total_contract_value DOUBLE,
vendor_total_contract_value DOUBLE,
sku String,
vendor_sku String,
applicable_at_renewal String,
cost DOUBLE,
cost_after_credit DOUBLE,
price DOUBLE,
price_after_credit DOUBLE,
im360_subscription_tbl_id INTEGER Not Null,
FOREIGN KEY(im360_subscription_tbl_id) REFERENCES im360_subscription(id)
);


------------------------------------------------------------------ Misc ----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS parameter_mapping(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
parameter_name String NOT NULL,
parameter_value String NOT NULL,
parameter_type String NOT NULL,
is_mandatory Boolean,
module String NOT NULL,
description Text
);

------------------------------------------------------------------ CBC ------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS cbc_status(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id TEXT NOT NULL,
sub_id INTEGER NOT NULL,
order_id TEXT NOT NULL,
status TEXT
);


CREATE TABLE IF NOT EXISTS cb_connect_status(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
subscription_status TEXT NOT NULL,
error_message TEXT NOT NULL,
test_case_id TEXT NOT NULL,
im360_subscription_id INTEGER NOT NULL,
FOREIGN KEY(im360_subscription_id) REFERENCES im360_subscription(id)
);


-------------------------------------------------------------------- SAP -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS sap_subscription_data(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id TEXT NOT NULL,
provider_contract TEXT NOT NULL,
name TEXT,
contract_start TEXT NOT NULL,
contract_end TEXT NOT NULL,
key_in_external_system TEXT NOT NULL,
managing_company_code TEXT,
im_vendor_sub_id TEXT,
bill_to TEXT,
im_payment_terms TEXT,
subscription_period INTEGER, 
subscrip_period_unit TEXT,
billing_period INTEGER,
billing_period_unit TEXT,
status TEXT,
flooring_bp TEXT,
plant TEXT
);


CREATE TABLE IF NOT EXISTS sap_bits_data(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
im_sub_id TEXT NOT NULL,
order_number TEXT NOT NULL,
bcn_number TEXT,
from_date TEXT,
to_date TEXT,
bill_item_type TEXT, 
amount DOUBLE,
billing_quantity DOUBLE,
duration DOUBLE,
unit_prc_of_prod TEXT,
unit_cost_of_prod TEXT,
sku_part TEXT,
ven_sub_id TEXT,
bit_class TEXT,
sap_subscription_data_id TEXT,  
FOREIGN KEY(sap_subscription_data_id) REFERENCES sap_subscription_data(id)
);


CREATE TABLE IF NOT EXISTS sap_status_code(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
status TEXT NOT NULL,
code TEXT NOT NULL,
reason TEXT NOT NULL
);





------------------------------------------------------------------- Automation Report ------------------------------------------

CREATE TABLE IF NOT EXISTS automation_report_data (
id INTEGER NOT NULL,
test_case_id TEXT NOT NULL,
test_case_name TEXT NOT NULL,
status TEXT NOT NULL,
failure_reason TEXT,
status_level_fail TEXT,
status_level_warning TEXT,
PRIMARY KEY("id" AUTOINCREMENT)
);


--------------------------------------------------------------------- Usage ----------------------------------------------------------

CREATE TABLE IF NOT EXISTS connect_usage_upload_status (
	id	INTEGER NOT NULL,
	test_case_id	TEXT NOT NULL,
	subscription_id	INTEGER NOT NULL,
	usage_id	TEXT NOT NULL,
	usage_start_date	TEXT,
	usage_end_date	TEXT,
	usage_status_in_connect	TEXT,
	PRIMARY KEY(id AUTOINCREMENT)
);


CREATE TABLE IF NOT EXISTS commerce_date_change_track (
	sl_no	INTEGER NOT NULL,
	cb_date	TEXT NOT NULL,
	condition	TEXT,
	PRIMARY KEY(sl_no AUTOINCREMENT)
);



-------------------------------------------------------------------- DML ------------------------------------------------------------

INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Agreement/Deal ID", "deal_id","Order", "True", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Provisioning Contact Name", "provisioning_contact_name","Order", "True", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Provisioning Contact Email", "provisioning_contact_email","Order", "True", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Provisioning Contact Phone", "provisioning_contact_phone","Order", "True", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Requested Ship / Start Date", "delayshipdate","Order", "True", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Web Order ID", "cisco_web_order_id","Order", "False", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Vendor Portal Submission", "vendor_portal_submission","Fulfillment", "False", "Connect" , "");
INSERT INTO parameter_mapping (parameter_name, parameter_value, parameter_type, is_mandatory, module, description) VALUES("Vendor Subscription ID", "vendor_subscription_id","Fulfillment", "True", "Connect" , "");

INSERT INTO sap_status_code (status, code, reason) VALUES ("Pass","SUC","All steps were completed successfully for this Test Case.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail","NP-S-1","Subscription did not move from IM360 to CB Commerce.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail","IN-S-1","Subscription is not Completed in CB Connect.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail","IN-S-2","Subscription is not Completed in CB Connect.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-S-1", "Subscription is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-S-2", "Subscription is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Warning", "BP-S-1", "SAP subscription data did not match.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-B-1", "No Min Commit Bit is available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-1", "Min Commit Reseller Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-2", "Min Commit Vendor Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-B-2", "No Min Commit bit is present in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-3", "No Min Commit Reseller Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-4", "No Min Commit Vendor Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-5", "Min Commit Reseller Bit for specific item/items is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-6", "Min Commit Vendor Bit for specific item/items is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Warning", "BP-B-1", "Min Commit Bit Data did not match");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-U-1", "No Usage Bit is available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-1", "Usage Reseller Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-2", "Usage Vendor Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-U-2", "No Usage bit is present in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-3", "No Usage Reseller Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-4", "No Usage Vendor Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Warning", "BP-B-2", "Usage Bit Data did not match");

------------------------------------------------- ------------------- Version 1.2 [27th April 2022] ----------------------------------------------------

-------------------------------------------------------------------- DDL -------------------------------------------------------------

-------------------------------------------------------------------- IM360 -------------------------------------------------------------

CREATE TABLE IF NOT EXISTS Im360_Status(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id STRING NOT NULL,
test_case_name STRING NOT NULL,
status STRING NOT NULL,
failure_reason TEXT,
reseller_currency STRING,
service_name STRING,
marketplace_name STRING,
vendor_currency STRING,
failure_image TEXT
);


-------------------------------------------------------------------- SAP -------------------------------------------------------------

CREATE TABLE IF NOT EXISTS "sap_bill_invoice" (
    "id" INTEGER NOT NULL,
    "tc_id" VARCHAR(16) NOT NULL,
    "date" VARCHAR(16) NOT NULL,
    "provider_contract" VARCHAR(16) NOT NULL,
    "bill_doc_num" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "invoice_doc_num" INT(16) NOT NULL DEFAULT 'NA',
    "recon_key" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "billable_bit_class" VARCHAR(8),
    "operation" VARCHAR(16),
    "status" VARCHAR(8),
    "error_reason" VARCHAR(2048),
    PRIMARY KEY("id" AUTOINCREMENT)
);


-------------------------------------------------------------------- DML ------------------------------------------------------------

-------------------------------------------------------------------- IM360 ----------------------------------------------------------

ALTER TABLE im360_input_order ADD COLUMN reseller_currency Text;
ALTER TABLE im360_input_order ADD COLUMN vendor_currency Text;
ALTER TABLE im360_input_order ADD COLUMN service_name Text;
ALTER TABLE im360_input_order ADD COLUMN marketplace_name Text;


-------------------------------------------------------------------- Automation Report ---------------------------------------------------

alter table automation_report_data add subscription_id String;
alter table automation_report_data add reseller_bcn String;
alter table automation_report_data add reseller_currency String;
alter table automation_report_data add vendor_currency String;
alter table automation_report_data add service_name String;
alter table automation_report_data add marketplace_name String;
alter table automation_report_data add module String;
alter table automation_report_data add failure_image TEXT;


-------------------------------------------------------------------- DML ------------------------------------------------------------------

INSERT INTO sap_status_code (status, code, reason) VALUES ("Pass","SUC","All steps were completed successfully for this Test Case.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail","NP-S-1","Subscription did not move from IM360 to CB Commerce.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail","IN-S-1","Subscription is not Completed in CB Connect.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail","IN-S-2","Subscription is not Completed in CB Connect.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-S-1", "Subscription is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-S-2", "Subscription is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Warning", "BP-S-1", "SAP subscription data did not match.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-B-1", "No Min Commit Bit is available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-1", "Min Commit Reseller Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-2", "Min Commit Vendor Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-B-2", "No Min Commit bit is present in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-3", "No Min Commit Reseller Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-4", "No Min Commit Vendor Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-5", "Min Commit Reseller Bit for specific item/items is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-6", "Min Commit Vendor Bit for specific item/items is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-7", "Min Commit Reseller and Vendor bits for specific item/items is not available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-8", "Min Commit Reseller Bit for specific item/items is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-9", "Min Commit Vendor Bit for specific item/items is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-B-10", "Min Commit Reseller and Vendor bits for specific item/items is not available in web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Warning", "BP-B-1", "Min Commit Bit Data did not match");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-U-1", "No Usage Bit is available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-1", "Usage Reseller Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-2", "Usage Vendor Bit is not available in Web App.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "NA-U-2", "No Usage bit is present in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-3", "No Usage Reseller Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "MI-U-4", "No Usage Vendor Bit is available in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Warning", "BP-B-2", "Usage Bit Data did not match");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "BIL-F", "Billing has Failed in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "INV-F", "Invoicing has Failed in SAP.");
INSERT INTO sap_status_code (status, code, reason) VALUES ("Fail", "RAR-F", "RAR has Failed in SAP.");

------------------------------------------------- ------------------- Version 1.3 [13th July 2022] --------------------------------------------------

-------------------------------------------------------------------- DDL ---------------------------------------------------------------------------

-------------------------------------------------------------------- IM360--------------------------------------------------------------------------

ALTER TABLE im360_input_item ADD COLUMN order_action String;
ALTER TABLE im360_input_order ADD COLUMN change_service_plan_id INTEGER;
ALTER TABLE im360_input_order ADD COLUMN change_billing_period Text;
ALTER TABLE im360_input_order ADD COLUMN change_subscription_period INTEGER;
ALTER TABLE im360_input_order ADD COLUMN change_payment_term String;
ALTER TABLE im360_input_order ADD change_deal_id INTEGER;
ALTER TABLE im360_input_order ADD COLUMN test_type String;

-------------------------------------------------------------------- SAP ----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS "sap_subscription_data"(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id TEXT NOT NULL,
provider_contract TEXT NOT NULL,
name TEXT,
contract_start TEXT NOT NULL,
contract_end TEXT NOT NULL,
key_in_external_system TEXT NOT NULL,
managing_company_code TEXT,
im_vendor_sub_id TEXT,
bill_to TEXT,
im_payment_terms TEXT,
subscription_period INTEGER,
subscrip_period_unit TEXT,
billing_period INTEGER,
billing_period_unit TEXT,
status TEXT,
flooring_bp TEXT,
plant TEXT
);


CREATE TABLE IF NOT EXISTS "sap_mpn_data"(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
subscription_id TEXT NOT NULL,
total_amount TEXT,
cost TEXT,
quantity TEXT,
payment_card_id TEXT,
cc_number TEXT,
payment_terms TEXT,
end_of_duration TEXT,
cb_cancellable_flag TEXT,
exchange_rate TEXT,
migration_date TEXT,
original_start_date TEXT,
item_type TEXT,
product_id TEXT,
currency TEXT,
sap_subscription_data_id TEXT,
FOREIGN KEY(sap_subscription_data_id) REFERENCES sap_subscription_data(id)
);


create table IF NOT EXISTS sap_status_data(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
test_case_id TEXT NOT NULL,
subscription_id TEXT NOT NULL,
order_id TEXT NOT NULL,
feed_validation_status TEXT,
minCommitBit_validation_status TEXT,
usage_bit_validation_status TEXT,
billing_status TEXT,
invoiced_status TEXT,
rar_status TEXT,
status_code TEXT,
image_ref TEXT,
error_details TEXT,
date TEXT
);

create table IF NOT EXISTS sap_test_cases_data(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
tc_id TEXT NOT NULL,
pytest_name TEXT NOT NULL,
status VARCHAR(8) NOT NULL
);


-------------------------------------------------------------------- RAR -----------------------------------------------

CREATE TABLE IF NOT EXISTS "sap_rar" (
    "id" INTEGER NOT NULL,
    "tc_id" VARCHAR(16) NOT NULL,
    "date" VARCHAR(16) NOT NULL,
    "provider_contract" VARCHAR(16) NOT NULL,
    "pob_id" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "g2n_rev" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "g2n_cost" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "pob_price" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "pob_cost" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "pc_tcv" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "pc_commit_cost" VARCHAR(16) NOT NULL DEFAULT 'NA',
    "operation" VARCHAR(16),
    "status" VARCHAR(8),
    "error_reason" VARCHAR(2048),
    PRIMARY KEY("id" AUTOINCREMENT)
);


-------------------------------------------------------------------- Misc --------------------------------------------------------------------------

alter table parameter_mapping add is_validation_required Boolean;

-------------------------------------------------------------------- DML --------------------------------------------------------------------------

update parameter_mapping set is_validation_required='True' where parameter_name<>'Web Order ID';
update parameter_mapping set is_validation_required='False' where parameter_name='Web Order ID';
update parameter_mapping set parameter_type='ordering' where parameter_type='Ordering';
update parameter_mapping set parameter_type='ordering' where parameter_type='Order';
update parameter_mapping set parameter_name='Agreement/Deal ID' where parameter_value='deal_id';
update parameter_mapping set parameter_type='Fulfillment' where parameter_type='Fulfilment';
update parameter_mapping set is_mandatory='True' where is_mandatory='False';
update parameter_mapping set is_mandatory='False' where parameter_value in('cisco_web_order_id', 'vendor_portal_submission');

