import json

from CBConnect.Operations.InsertConnectStatus import InsertConnectStatus
from CBConnect.RestAPI.UpdateFulfilmentParamAPI import UpdateFulfillmentParamAPI
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from db.service.IM360SubscriptionParameterDbManagementService import IM360SubscriptionParameterDbManagementService


class UpdateFulfillmentParams:
    parse_config_file = ParseConfigFile()
    db_file_path = parse_config_file.get_data_from_config_json("dbLocation", "db_file_path")
    logger = LogGenerator.logGen()

    def update_params(self, request, im360_sub_id, tc_id, marketplace=None):
        request_asset = request['asset']
        update_params_request_body = {'asset': {'params': []}}
        self.logger.warning(f" set fulfillment parameters value.")
        fulfillment_parameter_list = []
        error_message = ''
        parameter_db_management_service = IM360SubscriptionParameterDbManagementService()
        for param in request_asset['params']:
            if param['phase'] == 'fulfillment':
                self.logger.warning(
                    f" param id is {param['id']}, phase {param['phase']}, required {param['constraints']['required']},"
                    f" value {param['value']}")
                parameter_details = parameter_db_management_service.get_record_by_param_id(self.db_file_path,
                                                                                           im360_sub_id, param['id'])

                # validate fulfillment parameter
                error_message = error_message + self.validate_fulfillment_parameter(param, parameter_details)
                update_params_request_body['asset']['params'].append({"id": param['id'], "value": parameter_details[2]})
        insert_connect_status = InsertConnectStatus()
        if len(error_message) > 0:
            self.logger.warning(
                f" Task is failed. Required fulfillment parameters are none. error: {error_message}.")
            insert_connect_status.insert(tc_id, "FAILED", error_message, im360_sub_id)
            return False

        if len(update_params_request_body['asset']['params']) > 0:
            update_param_api = UpdateFulfillmentParamAPI()
            response = update_param_api.update(request['id'], json.dumps(update_params_request_body), marketplace)
            if response.status_code != 200:
                error_message = f"api response while updating fulfillment params is - {response.text}"
                insert_connect_status.insert(tc_id, "FAILED", error_message, im360_sub_id)
                return False

        return True

    def validate_fulfillment_parameter(self, response_param, db_param):
        error_message = ''
        if db_param is None:
            error_message = f"{response_param['id']} is not a valid order parameter.;"
            return error_message
        if eval(db_param[1]) != bool(response_param['constraints']['required']) or \
                str(db_param[0]).lower() != str(response_param['phase']).lower():
            error_message = f"{response_param['id']} has mismatched value.(required, type) " \
                            f"has Actual value" \
                            f" ({response_param['constraints']['required']}," \
                            f" {response_param['phase']}) while expected value is " \
                            f"({eval(db_param[1])}," \
                            f"{db_param[0]});"

        if len(error_message) == 0 and (eval(db_param[1]) and len(str(db_param[2])) == 0):
            error_message = error_message + f"{response_param['id']} is required parameter, but it has none value;"

        return error_message
