import json

from CBConnect.Operations.InsertConnectStatus import InsertConnectStatus
from CBConnect.RestAPI.ApproveSubscriptionAPI import ApproveSubscriptionAPI
from CBConnect.RestAPI.ProductTemplateAPI import ProductTemplateAPI
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile


class ApproveOrder:
    logger = LogGenerator.logGen()
    parse_config_file = ParseConfigFile()
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def approve(self, order, im360_sub_id, tc_id, marketplace=None):
        approve_subscription_api = ApproveSubscriptionAPI()
        insert_connect_status = InsertConnectStatus()
        get_templateId_api = ProductTemplateAPI()
        template_id = get_templateId_api.get_template_id(order, marketplace)
        approval_api_body = {'template_id': template_id}
        response = approve_subscription_api.approve(json.dumps(approval_api_body), order['id'], marketplace)
        if response.status_code != 200:
            error_message = f"api response while approving order is - {response.text}"
            insert_connect_status.insert(tc_id, "FAILED", error_message, im360_sub_id)
        else:
            insert_connect_status.insert(tc_id, "APPROVED", "", im360_sub_id)
