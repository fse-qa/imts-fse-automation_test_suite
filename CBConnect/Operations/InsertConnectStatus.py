from CommonUtilities.parse_config import ParseConfigFile
from db.dao.ConnectStatusDAO import ConnectStatusDAO
from db.model.CBConnectStatus import CBConnectStatus
from db.util.SqlUtil import SqlUtil

class InsertConnectStatus:
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def insert(self, tc_id, subscription_status, error_message, subscription_fk_id):
        cb_connect_status = CBConnectStatus()
        cb_connect_status.subscription_status = subscription_status
        cb_connect_status.error_message = error_message
        cb_connect_status.test_case_id = tc_id
        cb_connect_status.im360_subscription_id = subscription_fk_id
        connectStatusDAO = ConnectStatusDAO()
        sql_util = SqlUtil(self.db_file_path)
        connectStatusDAO.insert_records(sql_util, cb_connect_status)
