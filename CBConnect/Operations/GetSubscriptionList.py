from db.service.IM360SubscriptionDbManagementService import IM360SubscriptionDbManagementService
from CommonUtilities.parse_config import ParseConfigFile


class GetSubscriptionList:
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def list(self, test_case_id):
        im360Subscription_db_management_service = IM360SubscriptionDbManagementService()
        im360_subscription_list = im360Subscription_db_management_service.get_pending_subs_by_tc \
            (test_case_id, self.db_file_path)
        return im360_subscription_list
