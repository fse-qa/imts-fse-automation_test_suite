from CBConnect.RestAPI.AssetDetailsAPI import AssetDetailsAPI
from CommonUtilities.logGeneration import LogGenerator


class SubscriptionDetails:
    logger = LogGenerator.logGen()

    def get_details(self, subscription, marketplace):
        getAssetDetailsAPI = AssetDetailsAPI()
        response = getAssetDetailsAPI.get_asset_details(subscription, marketplace)
        self.logger.info("subscription details get from connect %s", response.content)
        return response
