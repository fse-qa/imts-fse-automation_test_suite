from datetime import datetime

from CBConnect.Operations.InsertConnectStatus import InsertConnectStatus
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from db.service.IM360SubscriptionParameterDbManagementService import IM360SubscriptionParameterDbManagementService
from db.service.ParameterMappingDbManagementService import ParameterMappingDbManagementService


class ValidateOrderParams:
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")
    logger = LogGenerator.logGen()

    def validate_params(self, request, im360_sub_id, tc_id):
        request_asset = request['asset']
        self.logger.info("validate order parameter for %s", request_asset['id'])
        error_message = ''
        parameter_db_management_service = IM360SubscriptionParameterDbManagementService()
        parameter_mapping_db_management_service = ParameterMappingDbManagementService()
        for param in request_asset['params']:
            is_param_available_in_db = parameter_mapping_db_management_service.count_record_by_connect_param(
                self.db_file_path, param['id'])
            if is_param_available_in_db == 0:
                continue

            if param['phase'] == 'ordering':
                self.logger.warning(
                    f" param id is {param['id']}, phase {param['phase']}, required {param['constraints']['required']},"
                    f" value {param['value']}")

                parameter_details = parameter_db_management_service. \
                    get_record_by_param_id(self.db_file_path, im360_sub_id, param['id'])
                if parameter_details is not None:
                    if not eval(parameter_details[3]):
                        self.logger.info(f" param id is {param['id']} is skipped for validation as it value of "
                                         f"is_validation_required is set as false in parameter_mapping")
                        continue

                    if not self.compareValue(parameter_details, param['value'], param['id'], error_message) or \
                            eval(parameter_details[1]) != bool(param['constraints']['required']) \
                            or str(parameter_details[0]).lower() != str(param['phase']).lower():
                        error_message = error_message + f"{param['id']} has mismatched value.(value, required, type) " \
                                                        f"has Actual value" \
                                                        f" ({param['value']}, {param['constraints']['required']}," \
                                                        f" {param['phase']}) while expected value is " \
                                                        f"({str(parameter_details[2])},{eval(parameter_details[1])}," \
                                                        f"{parameter_details[0]});"
                else:
                    error_message = error_message + f"{param['id']} is not a valid ordering parameter.;"

        if len(error_message) > 0:
            self.logger.warning(
                f" Task is failed. Ordering parameters are not same as in IM360. error: {error_message}.")
            insert_connect_status = InsertConnectStatus()
            insert_connect_status.insert(tc_id, "FAILED", error_message, im360_sub_id)
            return False

        return True

    def compareValue(self, db_param_details, param_value, param_id, error_message=None):
        try:
            is_date_param = False
            db_param_value = db_param_details[2]
            connectParamsId = ParseConfigFile().get_data_from_config_json("dateFormat", "connectParamsId")
            IM360DateFormat = ''
            connectDateFormat = ''
            if connectParamsId == param_id:
                is_date_param = True
                IM360DateFormat = ParseConfigFile().get_data_from_config_json("dateFormat", "IM360DateFormat")
                connectDateFormat = ParseConfigFile().get_data_from_config_json("dateFormat", "connectDateFormat")

            if is_date_param:
                im360_date_obj = datetime.strptime(db_param_value, IM360DateFormat)
                connect_date_obj = datetime.strptime(param_value, connectDateFormat)
                return im360_date_obj == connect_date_obj
            else:
                return str(db_param_value) == str(param_value)
        except Exception as e:
            self.logger.error("Exception occurred while comparing the parameter %s %s", param_id, e)
            error_message = e
            return False
