import requests

from CBConnect.RestAPI import ApiBaseURLs
from CommonUtilities.baseSet.baseAPIUtil import BaseAPIUtility
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.miscUtility import MiscellaneousUtility
from CommonUtilities.parse_config import ParseConfigFile


class ApproveSubscriptionAPI:
    logger = LogGenerator.logGen()
    parse_config_json = ParseConfigFile()
    misc_utility = MiscellaneousUtility()
    base_api_utility = BaseAPIUtility()

    def approve(self, approval_api_body, request_id, marketplace=None):
        connect_api_url = self.parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_url",
                                                                           "CredConfig.json")
        connect_api_key = self.parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_key",
                                                                           "CredConfig.json")
        connect_subscription_approve = self.base_api_utility.do_update_api_query_url(
            ApiBaseURLs.connect_subscription_approve, request_id)

        update_api_url = connect_api_url + connect_subscription_approve
        headers = {'Content-type': 'application/json', 'Accept': 'application/json',
                   'Authorization': connect_api_key}
        response = requests.post(url=update_api_url, data=approval_api_body, headers=headers)

        return response
