import requests

from CBConnect.RestAPI import ApiBaseURLs
from CommonUtilities.baseSet.baseAPIUtil import BaseAPIUtility
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.miscUtility import MiscellaneousUtility
from CommonUtilities.parse_config import ParseConfigFile


class AssetDetailsAPI:
    logger = LogGenerator.logGen()

    misc_utility = MiscellaneousUtility()
    base_api_utility = BaseAPIUtility()

    def get_asset_details(self, im360_subscription, marketplace):
        parse_config_json = ParseConfigFile()
        connect_api_url = parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_url",
                                                                      "CredConfig.json")
        connect_api_key = parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_key",
                                                                      "CredConfig.json")
        get_subs_details = ApiBaseURLs.connect_get_subscription_details.replace("<id>", str(im360_subscription[1]))
        detail_api_url = connect_api_url + get_subs_details
        headers = self.base_api_utility.get_header_for_connect_api(marketplace)
        response = requests.get(url=detail_api_url, headers=headers)
        return response
