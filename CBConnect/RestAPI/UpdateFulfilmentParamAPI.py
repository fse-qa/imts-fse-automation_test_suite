import requests
import urllib3 as urllib3

from CBConnect.RestAPI import ApiBaseURLs
from CommonUtilities.baseSet.baseAPIUtil import BaseAPIUtility
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.miscUtility import MiscellaneousUtility
from CommonUtilities.parse_config import ParseConfigFile

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class UpdateFulfillmentParamAPI:
    logger = LogGenerator.logGen()
    parse_config_json = ParseConfigFile()
    misc_utility = MiscellaneousUtility()
    base_api_utility = BaseAPIUtility()

    def update(self, connect_obj_id, update_params_request_body, marketplace):
        connect_api_url = self.parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_url",
                                                                           "CredConfig.json")
        connect_api_key = self.parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_key",
                                                                           "CredConfig.json")
        connect_fulfillment_param_update = self.base_api_utility.do_update_api_query_url(
            ApiBaseURLs.connect_fulfillment_param_upload, connect_obj_id)

        update_api_url = connect_api_url + connect_fulfillment_param_update
        headers = {'Content-type': 'application/json', 'Accept': 'application/json',
                   'Authorization': connect_api_key}
        response = requests.put(url=update_api_url, data=update_params_request_body, headers=headers)
        return response
