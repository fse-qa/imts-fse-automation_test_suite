import json

import requests

from CBConnect.RestAPI import ApiBaseURLs
from CommonUtilities.baseSet.baseAPIUtil import BaseAPIUtility
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.miscUtility import MiscellaneousUtility
from CommonUtilities.parse_config import ParseConfigFile


class ProductTemplateAPI:
    logger = LogGenerator.logGen()
    parse_config_json = ParseConfigFile()
    misc_utility = MiscellaneousUtility()
    base_api_utility = BaseAPIUtility()

    def get_template_id(self, order, marketplace=None):
        connect_api_url = self.parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_url",
                                                                           "CredConfig.json")
        connect_api_key = self.parse_config_json.get_data_from_config_json(marketplace, "cbConnectApi_key",
                                                                           "CredConfig.json")

        product_id = order['asset']['product']['id']
        connect_template_id = self.base_api_utility.do_update_api_query_url(
            ApiBaseURLs.connect_template_id_subscription, product_id)

        template_api_url = connect_api_url + connect_template_id + \
                           ApiBaseURLs.connect_template_id_subscription_subscription_static
        
        headers = self.base_api_utility.get_header_for_connect_api(marketplace)
        template = requests.get(url=template_api_url, headers=headers)
        if json.loads(template.text) is None:
            return ""
        return json.loads(template.text)[0]['id']
