import json

import pytest

from CBConnect.Operations.ApproveOrder import ApproveOrder
from CBConnect.Operations.GetSubscriptionList import GetSubscriptionList
from CBConnect.Operations.InsertConnectStatus import InsertConnectStatus
from CBConnect.Operations.SubscriptionDetails import SubscriptionDetails
from CBConnect.Operations.UpdateFullfimentParams import UpdateFulfillmentParams
from CBConnect.Operations.ValidateOrderParams import ValidateOrderParams
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile
from db.service.CBCStatusDbManagementService import CBCStatusDbManagementService


class ApproveSalesOrder:
    logger = LogGenerator.logGen()
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path", "config.json")

    def execute(self, test_case_id, marketplace):
        self.logger.info("Executing connect component for test case- %s for approve sales order", test_case_id)
        insert_connect_status = InsertConnectStatus()
        get_subscription_list = GetSubscriptionList()
        try:
            im360_subscription_list = get_subscription_list.list(test_case_id)
            if len(im360_subscription_list) == 0:
                self.logger.info("number of subscription is zero for test case %s of approve sales order", test_case_id)
                raise ("Number of subscription is zero for test case %s of approve sales order", test_case_id)
                return

            status_db_management_service = CBCStatusDbManagementService()

            for im360_subscription in im360_subscription_list:
                check_lo_status = status_db_management_service.get_status_by_sub_details(self.db_file_path,
                                                                                         im360_subscription[1],
                                                                                         im360_subscription[2])
                if check_lo_status != "LO":
                    self.logger.error("The subscription %s with status code LO not found into cbc_status table for "
                                      "approve sales order", im360_subscription[1])
                    raise ("The subscription %s with status code LO not found into cbc_status table for "
                           "approve sales order", im360_subscription[1])
                    continue
                # step 1 get asset details using api
                api_subs_detail_response = SubscriptionDetails().get_details(im360_subscription, marketplace)
                response_content = api_subs_detail_response.text
                if response_content == '[]':
                    self.logger.error(f"subscription {im360_subscription[1]} is not available in connect for approve "
                                      f"sales order")
                    raise (f"subscription {im360_subscription[1]} is not available in connect for approve "
                           f"sales order")
                    continue

                order = json.loads(response_content)[0]

                # exit if subscription is not in pending state.
                if order['status'] != 'pending':
                    self.logger.error(f"{im360_subscription[0]} subscription is not processed "
                                      f"as it status in connect is {order['status']}")
                    error_message = f" error: subscription status in connect is {order['status']}"
                    insert_connect_status.insert(test_case_id, "FAILED", error_message, im360_subscription[0])
                    raise "{im360_subscription[0]} subscription is not processed as it status in connect is " \
                          "{order['status']}"

                # step 2 validate order params and insert row in connect_status table if params are invalid.
                validate_order_params = ValidateOrderParams()
                
                is_valid_order_params = validate_order_params.validate_params(order,
                                                                              im360_subscription[0], test_case_id)

                if is_valid_order_params:
                    # step 3 update asset via api call
                    update_fulfillment_params = UpdateFulfillmentParams()
                    is_updated = update_fulfillment_params.update_params(order, im360_subscription[0],
                                                                         test_case_id, marketplace)

                    # step 4 approve asset via api and insert row in connect_status table

                    if is_updated:
                        approve_order = ApproveOrder()
                        approve_order.approve(order, im360_subscription[0], test_case_id, marketplace)
                else:
                    raise 'Parameter is not matched'

        except Exception as e:
            self.logger.error("Exception occurred while executing connect component for test case %s %s  ",
                              test_case_id, str(e))
            raise e

        finally:
            self.logger.info("Execution of connect component for test case %s is completed.", test_case_id)
