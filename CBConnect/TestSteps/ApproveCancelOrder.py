import json
from sqlite3 import Error

import pytest

from CBConnect.Operations.ApproveOrder import ApproveOrder
from CBConnect.Operations.GetSubscriptionList import GetSubscriptionList
from CBConnect.Operations.InsertConnectStatus import InsertConnectStatus
from CBConnect.Operations.SubscriptionDetails import SubscriptionDetails
from CommonUtilities.logGeneration import LogGenerator
from CommonUtilities.parse_config import ParseConfigFile


class ApproveCancelOrder:
    logger = LogGenerator.logGen()
    db_file_path = ParseConfigFile().get_data_from_config_json("dbLocation", "db_file_path")

    def execute(self, test_case_id, marketplace):
        self.logger.info("Executing connect component ApproveCancelOrder for test case- %s", test_case_id)
        try:
            im360_subscription_list = GetSubscriptionList().list(test_case_id)
            if len(im360_subscription_list) == 0:
                self.logger.info("number of subscription is zero for test case %s cancellation of order", test_case_id)
                pytest.skip("number of subscription is zero for test case %s cancellation of order", test_case_id)

            get_subs_details = SubscriptionDetails()

            for im360_subscription in im360_subscription_list:

                # step 1 get asset details using api
                api_subs_detail_response = get_subs_details.get_details(im360_subscription, marketplace)

                response_content = api_subs_detail_response.text
                if response_content == '[]':
                    self.logger.error(f"subscription {im360_subscription[1]} is not available in connect for cancel "
                                      f"order")
                    continue
                order = json.loads(response_content)[0]
                # exit if subscription is not in pending state.
                insert_connect_status = InsertConnectStatus()
                if order['status'] != 'pending':
                    self.logger.error(f"{im360_subscription[0]} subscription is not processed "
                                      f"as it status in connect is {order['status']}")
                    error_message = f" error: subscription status in connect is {order['status']}"
                    insert_connect_status.insert(test_case_id, "FAILED", error_message, im360_subscription[0])
                    continue

                # step 4 approve asset via api and insert row in connect_status table
                approve_order = ApproveOrder()
                approve_order.approve(order, im360_subscription[0], test_case_id, marketplace)

        except Error as e:
            self.logger.error("Exception occurred while executing connect component for test case %s. %s of cancel "
                              "order", test_case_id, e)
        finally:
            self.logger.info("Execution of connect component for test case %s of cancel order steps is completed.",
                             test_case_id)
